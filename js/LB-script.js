!(function (a, b) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports
        ? (module.exports = a.document
            ? b(a, !0)
            : function (a) {
                if (!a.document) throw new Error("jQuery requires a window with a document");
                return b(a);
            })
        : b(a);
})("undefined" != typeof window ? window : this, function (a, b) {
    "use strict";
    var c = [],
        d = a.document,
        e = Object.getPrototypeOf,
        f = c.slice,
        g = c.concat,
        h = c.push,
        i = c.indexOf,
        j = {},
        k = j.toString,
        l = j.hasOwnProperty,
        m = l.toString,
        n = m.call(Object),
        o = {};
    function p(a, b) {
        b = b || d;
        var c = b.createElement("script");
        (c.text = a), b.head.appendChild(c).parentNode.removeChild(c);
    }
    var q = "3.2.0",
        r = function (a, b) {
            return new r.fn.init(a, b);
        },
        s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        t = /^-ms-/,
        u = /-([a-z])/g,
        v = function (a, b) {
            return b.toUpperCase();
        };
    (r.fn = r.prototype = {
        jquery: q,
        constructor: r,
        length: 0,
        toArray: function () {
            return f.call(this);
        },
        get: function (a) {
            return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a];
        },
        pushStack: function (a) {
            var b = r.merge(this.constructor(), a);
            return (b.prevObject = this), b;
        },
        each: function (a) {
            return r.each(this, a);
        },
        map: function (a) {
            return this.pushStack(
                r.map(this, function (b, c) {
                    return a.call(b, c, b);
                })
            );
        },
        slice: function () {
            return this.pushStack(f.apply(this, arguments));
        },
        first: function () {
            return this.eq(0);
        },
        last: function () {
            return this.eq(-1);
        },
        eq: function (a) {
            var b = this.length,
                c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [this[c]] : []);
        },
        end: function () {
            return this.prevObject || this.constructor();
        },
        push: h,
        sort: c.sort,
        splice: c.splice,
    }),
        (r.extend = r.fn.extend = function () {
            var a,
                b,
                c,
                d,
                e,
                f,
                g = arguments[0] || {},
                h = 1,
                i = arguments.length,
                j = !1;
            for ("boolean" == typeof g && ((j = g), (g = arguments[h] || {}), h++), "object" == typeof g || r.isFunction(g) || (g = {}), h === i && ((g = this), h--); h < i; h++)
                if (null != (a = arguments[h]))
                    for (b in a)
                        (c = g[b]),
                            (d = a[b]),
                            g !== d &&
                            (j && d && (r.isPlainObject(d) || (e = Array.isArray(d)))
                                ? (e ? ((e = !1), (f = c && Array.isArray(c) ? c : [])) : (f = c && r.isPlainObject(c) ? c : {}), (g[b] = r.extend(j, f, d)))
                                : void 0 !== d && (g[b] = d));
            return g;
        }),
        r.extend({
            expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function (a) {
                throw new Error(a);
            },
            noop: function () { },
            isFunction: function (a) {
                return "function" === r.type(a);
            },
            isWindow: function (a) {
                return null != a && a === a.window;
            },
            isNumeric: function (a) {
                var b = r.type(a);
                return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
            },
            isPlainObject: function (a) {
                var b, c;
                return !(!a || "[object Object]" !== k.call(a)) && (!(b = e(a)) || ((c = l.call(b, "constructor") && b.constructor), "function" == typeof c && m.call(c) === n));
            },
            isEmptyObject: function (a) {
                var b;
                for (b in a) return !1;
                return !0;
            },
            type: function (a) {
                return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? j[k.call(a)] || "object" : typeof a;
            },
            globalEval: function (a) {
                p(a);
            },
            camelCase: function (a) {
                return a.replace(t, "ms-").replace(u, v);
            },
            each: function (a, b) {
                var c,
                    d = 0;
                if (w(a)) {
                    for (c = a.length; d < c; d++) if (b.call(a[d], d, a[d]) === !1) break;
                } else for (d in a) if (b.call(a[d], d, a[d]) === !1) break;
                return a;
            },
            trim: function (a) {
                return null == a ? "" : (a + "").replace(s, "");
            },
            makeArray: function (a, b) {
                var c = b || [];
                return null != a && (w(Object(a)) ? r.merge(c, "string" == typeof a ? [a] : a) : h.call(c, a)), c;
            },
            inArray: function (a, b, c) {
                return null == b ? -1 : i.call(b, a, c);
            },
            merge: function (a, b) {
                for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
                return (a.length = e), a;
            },
            grep: function (a, b, c) {
                for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++) (d = !b(a[f], f)), d !== h && e.push(a[f]);
                return e;
            },
            map: function (a, b, c) {
                var d,
                    e,
                    f = 0,
                    h = [];
                if (w(a)) for (d = a.length; f < d; f++) (e = b(a[f], f, c)), null != e && h.push(e);
                else for (f in a) (e = b(a[f], f, c)), null != e && h.push(e);
                return g.apply([], h);
            },
            guid: 1,
            proxy: function (a, b) {
                var c, d, e;
                if (("string" == typeof b && ((c = a[b]), (b = a), (a = c)), r.isFunction(a)))
                    return (
                        (d = f.call(arguments, 2)),
                        (e = function () {
                            return a.apply(b || this, d.concat(f.call(arguments)));
                        }),
                        (e.guid = a.guid = a.guid || r.guid++),
                        e
                    );
            },
            now: Date.now,
            support: o,
        }),
        "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]),
        r.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
            j["[object " + b + "]"] = b.toLowerCase();
        });
    function w(a) {
        var b = !!a && "length" in a && a.length,
            c = r.type(a);
        return "function" !== c && !r.isWindow(a) && ("array" === c || 0 === b || ("number" == typeof b && b > 0 && b - 1 in a));
    }
    var x = (function (a) {
        var b,
            c,
            d,
            e,
            f,
            g,
            h,
            i,
            j,
            k,
            l,
            m,
            n,
            o,
            p,
            q,
            r,
            s,
            t,
            u = "sizzle" + 1 * new Date(),
            v = a.document,
            w = 0,
            x = 0,
            y = ha(),
            z = ha(),
            A = ha(),
            B = function (a, b) {
                return a === b && (l = !0), 0;
            },
            C = {}.hasOwnProperty,
            D = [],
            E = D.pop,
            F = D.push,
            G = D.push,
            H = D.slice,
            I = function (a, b) {
                for (var c = 0, d = a.length; c < d; c++) if (a[c] === b) return c;
                return -1;
            },
            J = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            K = "[\\x20\\t\\r\\n\\f]",
            L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
            M = "\\[" + K + "*(" + L + ")(?:" + K + "*([*^$|!~]?=)" + K + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + L + "))|)" + K + "*\\]",
            N = ":(" + L + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + M + ")*)|.*)\\)|)",
            O = new RegExp(K + "+", "g"),
            P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
            Q = new RegExp("^" + K + "*," + K + "*"),
            R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
            S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
            T = new RegExp(N),
            U = new RegExp("^" + L + "$"),
            V = {
                ID: new RegExp("^#(" + L + ")"),
                CLASS: new RegExp("^\\.(" + L + ")"),
                TAG: new RegExp("^(" + L + "|[*])"),
                ATTR: new RegExp("^" + M),
                PSEUDO: new RegExp("^" + N),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + K + "*(even|odd|(([+-]|)(\\d*)n|)" + K + "*(?:([+-]|)" + K + "*(\\d+)|))" + K + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + J + ")$", "i"),
                needsContext: new RegExp("^" + K + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + K + "*((?:-\\d)?\\d*)" + K + "*\\)|)(?=[^-]|$)", "i"),
            },
            W = /^(?:input|select|textarea|button)$/i,
            X = /^h\d$/i,
            Y = /^[^{]+\{\s*\[native \w/,
            Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            $ = /[+~]/,
            _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
            aa = function (a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode((d >> 10) | 55296, (1023 & d) | 56320);
            },
            ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
            ca = function (a, b) {
                return b ? ("\0" === a ? "�" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " ") : "\\" + a;
            },
            da = function () {
                m();
            },
            ea = ta(
                function (a) {
                    return a.disabled === !0 && ("form" in a || "label" in a);
                },
                { dir: "parentNode", next: "legend" }
            );
        try {
            G.apply((D = H.call(v.childNodes)), v.childNodes), D[v.childNodes.length].nodeType;
        } catch (fa) {
            G = {
                apply: D.length
                    ? function (a, b) {
                        F.apply(a, H.call(b));
                    }
                    : function (a, b) {
                        var c = a.length,
                            d = 0;
                        while ((a[c++] = b[d++]));
                        a.length = c - 1;
                    },
            };
        }
        function ga(a, b, d, e) {
            var f,
                h,
                j,
                k,
                l,
                o,
                r,
                s = b && b.ownerDocument,
                w = b ? b.nodeType : 9;
            if (((d = d || []), "string" != typeof a || !a || (1 !== w && 9 !== w && 11 !== w))) return d;
            if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), (b = b || n), p)) {
                if (11 !== w && (l = Z.exec(a)))
                    if ((f = l[1])) {
                        if (9 === w) {
                            if (!(j = b.getElementById(f))) return d;
                            if (j.id === f) return d.push(j), d;
                        } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d;
                    } else {
                        if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
                        if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d;
                    }
                if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
                    if (1 !== w) (s = b), (r = a);
                    else if ("object" !== b.nodeName.toLowerCase()) {
                        (k = b.getAttribute("id")) ? (k = k.replace(ba, ca)) : b.setAttribute("id", (k = u)), (o = g(a)), (h = o.length);
                        while (h--) o[h] = "#" + k + " " + sa(o[h]);
                        (r = o.join(",")), (s = ($.test(a) && qa(b.parentNode)) || b);
                    }
                    if (r)
                        try {
                            return G.apply(d, s.querySelectorAll(r)), d;
                        } catch (x) {
                        } finally {
                            k === u && b.removeAttribute("id");
                        }
                }
            }
            return i(a.replace(P, "$1"), b, d, e);
        }
        function ha() {
            var a = [];
            function b(c, e) {
                return a.push(c + " ") > d.cacheLength && delete b[a.shift()], (b[c + " "] = e);
            }
            return b;
        }
        function ia(a) {
            return (a[u] = !0), a;
        }
        function ja(a) {
            var b = n.createElement("fieldset");
            try {
                return !!a(b);
            } catch (c) {
                return !1;
            } finally {
                b.parentNode && b.parentNode.removeChild(b), (b = null);
            }
        }
        function ka(a, b) {
            var c = a.split("|"),
                e = c.length;
            while (e--) d.attrHandle[c[e]] = b;
        }
        function la(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
            if (d) return d;
            if (c) while ((c = c.nextSibling)) if (c === b) return -1;
            return a ? 1 : -1;
        }
        function ma(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return "input" === c && b.type === a;
            };
        }
        function na(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a;
            };
        }
        function oa(a) {
            return function (b) {
                return "form" in b
                    ? b.parentNode && b.disabled === !1
                        ? "label" in b
                            ? "label" in b.parentNode
                                ? b.parentNode.disabled === a
                                : b.disabled === a
                            : b.isDisabled === a || (b.isDisabled !== !a && ea(b) === a)
                        : b.disabled === a
                    : "label" in b && b.disabled === a;
            };
        }
        function pa(a) {
            return ia(function (b) {
                return (
                    (b = +b),
                    ia(function (c, d) {
                        var e,
                            f = a([], c.length, b),
                            g = f.length;
                        while (g--) c[(e = f[g])] && (c[e] = !(d[e] = c[e]));
                    })
                );
            });
        }
        function qa(a) {
            return a && "undefined" != typeof a.getElementsByTagName && a;
        }
        (c = ga.support = {}),
            (f = ga.isXML = function (a) {
                var b = a && (a.ownerDocument || a).documentElement;
                return !!b && "HTML" !== b.nodeName;
            }),
            (m = ga.setDocument = function (a) {
                var b,
                    e,
                    g = a ? a.ownerDocument || a : v;
                return g !== n && 9 === g.nodeType && g.documentElement
                    ? ((n = g),
                        (o = n.documentElement),
                        (p = !f(n)),
                        v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener("unload", da, !1) : e.attachEvent && e.attachEvent("onunload", da)),
                        (c.attributes = ja(function (a) {
                            return (a.className = "i"), !a.getAttribute("className");
                        })),
                        (c.getElementsByTagName = ja(function (a) {
                            return a.appendChild(n.createComment("")), !a.getElementsByTagName("*").length;
                        })),
                        (c.getElementsByClassName = Y.test(n.getElementsByClassName)),
                        (c.getById = ja(function (a) {
                            return (o.appendChild(a).id = u), !n.getElementsByName || !n.getElementsByName(u).length;
                        })),
                        c.getById
                            ? ((d.filter.ID = function (a) {
                                var b = a.replace(_, aa);
                                return function (a) {
                                    return a.getAttribute("id") === b;
                                };
                            }),
                                (d.find.ID = function (a, b) {
                                    if ("undefined" != typeof b.getElementById && p) {
                                        var c = b.getElementById(a);
                                        return c ? [c] : [];
                                    }
                                }))
                            : ((d.filter.ID = function (a) {
                                var b = a.replace(_, aa);
                                return function (a) {
                                    var c = "undefined" != typeof a.getAttributeNode && a.getAttributeNode("id");
                                    return c && c.value === b;
                                };
                            }),
                                (d.find.ID = function (a, b) {
                                    if ("undefined" != typeof b.getElementById && p) {
                                        var c,
                                            d,
                                            e,
                                            f = b.getElementById(a);
                                        if (f) {
                                            if (((c = f.getAttributeNode("id")), c && c.value === a)) return [f];
                                            (e = b.getElementsByName(a)), (d = 0);
                                            while ((f = e[d++])) if (((c = f.getAttributeNode("id")), c && c.value === a)) return [f];
                                        }
                                        return [];
                                    }
                                })),
                        (d.find.TAG = c.getElementsByTagName
                            ? function (a, b) {
                                return "undefined" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0;
                            }
                            : function (a, b) {
                                var c,
                                    d = [],
                                    e = 0,
                                    f = b.getElementsByTagName(a);
                                if ("*" === a) {
                                    while ((c = f[e++])) 1 === c.nodeType && d.push(c);
                                    return d;
                                }
                                return f;
                            }),
                        (d.find.CLASS =
                            c.getElementsByClassName &&
                            function (a, b) {
                                if ("undefined" != typeof b.getElementsByClassName && p) return b.getElementsByClassName(a);
                            }),
                        (r = []),
                        (q = []),
                        (c.qsa = Y.test(n.querySelectorAll)) &&
                        (ja(function (a) {
                            (o.appendChild(a).innerHTML = "<a id='" + u + "'></a><select id='" + u + "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                                a.querySelectorAll("[msallowcapture^='']").length && q.push("[*^$]=" + K + "*(?:''|\"\")"),
                                a.querySelectorAll("[selected]").length || q.push("\\[" + K + "*(?:value|" + J + ")"),
                                a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="),
                                a.querySelectorAll(":checked").length || q.push(":checked"),
                                a.querySelectorAll("a#" + u + "+*").length || q.push(".#.+[+~]");
                        }),
                            ja(function (a) {
                                a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                                var b = n.createElement("input");
                                b.setAttribute("type", "hidden"),
                                    a.appendChild(b).setAttribute("name", "D"),
                                    a.querySelectorAll("[name=d]").length && q.push("name" + K + "*[*^$|!~]?="),
                                    2 !== a.querySelectorAll(":enabled").length && q.push(":enabled", ":disabled"),
                                    (o.appendChild(a).disabled = !0),
                                    2 !== a.querySelectorAll(":disabled").length && q.push(":enabled", ":disabled"),
                                    a.querySelectorAll("*,:x"),
                                    q.push(",.*:");
                            })),
                        (c.matchesSelector = Y.test((s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector))) &&
                        ja(function (a) {
                            (c.disconnectedMatch = s.call(a, "*")), s.call(a, "[s!='']:x"), r.push("!=", N);
                        }),
                        (q = q.length && new RegExp(q.join("|"))),
                        (r = r.length && new RegExp(r.join("|"))),
                        (b = Y.test(o.compareDocumentPosition)),
                        (t =
                            b || Y.test(o.contains)
                                ? function (a, b) {
                                    var c = 9 === a.nodeType ? a.documentElement : a,
                                        d = b && b.parentNode;
                                    return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
                                }
                                : function (a, b) {
                                    if (b) while ((b = b.parentNode)) if (b === a) return !0;
                                    return !1;
                                }),
                        (B = b
                            ? function (a, b) {
                                if (a === b) return (l = !0), 0;
                                var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
                                return d
                                    ? d
                                    : ((d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1),
                                        1 & d || (!c.sortDetached && b.compareDocumentPosition(a) === d)
                                            ? a === n || (a.ownerDocument === v && t(v, a))
                                                ? -1
                                                : b === n || (b.ownerDocument === v && t(v, b))
                                                    ? 1
                                                    : k
                                                        ? I(k, a) - I(k, b)
                                                        : 0
                                            : 4 & d
                                                ? -1
                                                : 1);
                            }
                            : function (a, b) {
                                if (a === b) return (l = !0), 0;
                                var c,
                                    d = 0,
                                    e = a.parentNode,
                                    f = b.parentNode,
                                    g = [a],
                                    h = [b];
                                if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;
                                if (e === f) return la(a, b);
                                c = a;
                                while ((c = c.parentNode)) g.unshift(c);
                                c = b;
                                while ((c = c.parentNode)) h.unshift(c);
                                while (g[d] === h[d]) d++;
                                return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0;
                            }),
                        n)
                    : n;
            }),
            (ga.matches = function (a, b) {
                return ga(a, null, null, b);
            }),
            (ga.matchesSelector = function (a, b) {
                if (((a.ownerDocument || a) !== n && m(a), (b = b.replace(S, "='$1']")), c.matchesSelector && p && !A[b + " "] && (!r || !r.test(b)) && (!q || !q.test(b))))
                    try {
                        var d = s.call(a, b);
                        if (d || c.disconnectedMatch || (a.document && 11 !== a.document.nodeType)) return d;
                    } catch (e) { }
                return ga(b, n, null, [a]).length > 0;
            }),
            (ga.contains = function (a, b) {
                return (a.ownerDocument || a) !== n && m(a), t(a, b);
            }),
            (ga.attr = function (a, b) {
                (a.ownerDocument || a) !== n && m(a);
                var e = d.attrHandle[b.toLowerCase()],
                    f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
                return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null;
            }),
            (ga.escape = function (a) {
                return (a + "").replace(ba, ca);
            }),
            (ga.error = function (a) {
                throw new Error("Syntax error, unrecognized expression: " + a);
            }),
            (ga.uniqueSort = function (a) {
                var b,
                    d = [],
                    e = 0,
                    f = 0;
                if (((l = !c.detectDuplicates), (k = !c.sortStable && a.slice(0)), a.sort(B), l)) {
                    while ((b = a[f++])) b === a[f] && (e = d.push(f));
                    while (e--) a.splice(d[e], 1);
                }
                return (k = null), a;
            }),
            (e = ga.getText = function (a) {
                var b,
                    c = "",
                    d = 0,
                    f = a.nodeType;
                if (f) {
                    if (1 === f || 9 === f || 11 === f) {
                        if ("string" == typeof a.textContent) return a.textContent;
                        for (a = a.firstChild; a; a = a.nextSibling) c += e(a);
                    } else if (3 === f || 4 === f) return a.nodeValue;
                } else while ((b = a[d++])) c += e(b);
                return c;
            }),
            (d = ga.selectors = {
                cacheLength: 50,
                createPseudo: ia,
                match: V,
                attrHandle: {},
                find: {},
                relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
                preFilter: {
                    ATTR: function (a) {
                        return (a[1] = a[1].replace(_, aa)), (a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa)), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
                    },
                    CHILD: function (a) {
                        return (
                            (a[1] = a[1].toLowerCase()),
                            "nth" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), (a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3]))), (a[5] = +(a[7] + a[8] || "odd" === a[3]))) : a[3] && ga.error(a[0]),
                            a
                        );
                    },
                    PSEUDO: function (a) {
                        var b,
                            c = !a[6] && a[2];
                        return V.CHILD.test(a[0])
                            ? null
                            : (a[3] ? (a[2] = a[4] || a[5] || "") : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && ((a[0] = a[0].slice(0, b)), (a[2] = c.slice(0, b))), a.slice(0, 3));
                    },
                },
                filter: {
                    TAG: function (a) {
                        var b = a.replace(_, aa).toLowerCase();
                        return "*" === a
                            ? function () {
                                return !0;
                            }
                            : function (a) {
                                return a.nodeName && a.nodeName.toLowerCase() === b;
                            };
                    },
                    CLASS: function (a) {
                        var b = y[a + " "];
                        return (
                            b ||
                            ((b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) &&
                                y(a, function (a) {
                                    return b.test(("string" == typeof a.className && a.className) || ("undefined" != typeof a.getAttribute && a.getAttribute("class")) || "");
                                }))
                        );
                    },
                    ATTR: function (a, b, c) {
                        return function (d) {
                            var e = ga.attr(d, a);
                            return null == e
                                ? "!=" === b
                                : !b ||
                                ((e += ""),
                                    "=" === b
                                        ? e === c
                                        : "!=" === b
                                            ? e !== c
                                            : "^=" === b
                                                ? c && 0 === e.indexOf(c)
                                                : "*=" === b
                                                    ? c && e.indexOf(c) > -1
                                                    : "$=" === b
                                                        ? c && e.slice(-c.length) === c
                                                        : "~=" === b
                                                            ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1
                                                            : "|=" === b && (e === c || e.slice(0, c.length + 1) === c + "-"));
                        };
                    },
                    CHILD: function (a, b, c, d, e) {
                        var f = "nth" !== a.slice(0, 3),
                            g = "last" !== a.slice(-4),
                            h = "of-type" === b;
                        return 1 === d && 0 === e
                            ? function (a) {
                                return !!a.parentNode;
                            }
                            : function (b, c, i) {
                                var j,
                                    k,
                                    l,
                                    m,
                                    n,
                                    o,
                                    p = f !== g ? "nextSibling" : "previousSibling",
                                    q = b.parentNode,
                                    r = h && b.nodeName.toLowerCase(),
                                    s = !i && !h,
                                    t = !1;
                                if (q) {
                                    if (f) {
                                        while (p) {
                                            m = b;
                                            while ((m = m[p])) if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
                                            o = p = "only" === a && !o && "nextSibling";
                                        }
                                        return !0;
                                    }
                                    if (((o = [g ? q.firstChild : q.lastChild]), g && s)) {
                                        (m = q), (l = m[u] || (m[u] = {})), (k = l[m.uniqueID] || (l[m.uniqueID] = {})), (j = k[a] || []), (n = j[0] === w && j[1]), (t = n && j[2]), (m = n && q.childNodes[n]);
                                        while ((m = (++n && m && m[p]) || (t = n = 0) || o.pop()))
                                            if (1 === m.nodeType && ++t && m === b) {
                                                k[a] = [w, n, t];
                                                break;
                                            }
                                    } else if ((s && ((m = b), (l = m[u] || (m[u] = {})), (k = l[m.uniqueID] || (l[m.uniqueID] = {})), (j = k[a] || []), (n = j[0] === w && j[1]), (t = n)), t === !1))
                                        while ((m = (++n && m && m[p]) || (t = n = 0) || o.pop()))
                                            if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && ((l = m[u] || (m[u] = {})), (k = l[m.uniqueID] || (l[m.uniqueID] = {})), (k[a] = [w, t])), m === b)) break;
                                    return (t -= e), t === d || (t % d === 0 && t / d >= 0);
                                }
                            };
                    },
                    PSEUDO: function (a, b) {
                        var c,
                            e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error("unsupported pseudo: " + a);
                        return e[u]
                            ? e(b)
                            : e.length > 1
                                ? ((c = [a, a, "", b]),
                                    d.setFilters.hasOwnProperty(a.toLowerCase())
                                        ? ia(function (a, c) {
                                            var d,
                                                f = e(a, b),
                                                g = f.length;
                                            while (g--) (d = I(a, f[g])), (a[d] = !(c[d] = f[g]));
                                        })
                                        : function (a) {
                                            return e(a, 0, c);
                                        })
                                : e;
                    },
                },
                pseudos: {
                    not: ia(function (a) {
                        var b = [],
                            c = [],
                            d = h(a.replace(P, "$1"));
                        return d[u]
                            ? ia(function (a, b, c, e) {
                                var f,
                                    g = d(a, null, e, []),
                                    h = a.length;
                                while (h--) (f = g[h]) && (a[h] = !(b[h] = f));
                            })
                            : function (a, e, f) {
                                return (b[0] = a), d(b, null, f, c), (b[0] = null), !c.pop();
                            };
                    }),
                    has: ia(function (a) {
                        return function (b) {
                            return ga(a, b).length > 0;
                        };
                    }),
                    contains: ia(function (a) {
                        return (
                            (a = a.replace(_, aa)),
                            function (b) {
                                return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
                            }
                        );
                    }),
                    lang: ia(function (a) {
                        return (
                            U.test(a || "") || ga.error("unsupported lang: " + a),
                            (a = a.replace(_, aa).toLowerCase()),
                            function (b) {
                                var c;
                                do if ((c = p ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang"))) return (c = c.toLowerCase()), c === a || 0 === c.indexOf(a + "-");
                                while ((b = b.parentNode) && 1 === b.nodeType);
                                return !1;
                            }
                        );
                    }),
                    target: function (b) {
                        var c = a.location && a.location.hash;
                        return c && c.slice(1) === b.id;
                    },
                    root: function (a) {
                        return a === o;
                    },
                    focus: function (a) {
                        return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
                    },
                    enabled: oa(!1),
                    disabled: oa(!0),
                    checked: function (a) {
                        var b = a.nodeName.toLowerCase();
                        return ("input" === b && !!a.checked) || ("option" === b && !!a.selected);
                    },
                    selected: function (a) {
                        return a.parentNode && a.parentNode.selectedIndex, a.selected === !0;
                    },
                    empty: function (a) {
                        for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1;
                        return !0;
                    },
                    parent: function (a) {
                        return !d.pseudos.empty(a);
                    },
                    header: function (a) {
                        return X.test(a.nodeName);
                    },
                    input: function (a) {
                        return W.test(a.nodeName);
                    },
                    button: function (a) {
                        var b = a.nodeName.toLowerCase();
                        return ("input" === b && "button" === a.type) || "button" === b;
                    },
                    text: function (a) {
                        var b;
                        return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
                    },
                    first: pa(function () {
                        return [0];
                    }),
                    last: pa(function (a, b) {
                        return [b - 1];
                    }),
                    eq: pa(function (a, b, c) {
                        return [c < 0 ? c + b : c];
                    }),
                    even: pa(function (a, b) {
                        for (var c = 0; c < b; c += 2) a.push(c);
                        return a;
                    }),
                    odd: pa(function (a, b) {
                        for (var c = 1; c < b; c += 2) a.push(c);
                        return a;
                    }),
                    lt: pa(function (a, b, c) {
                        for (var d = c < 0 ? c + b : c; --d >= 0;) a.push(d);
                        return a;
                    }),
                    gt: pa(function (a, b, c) {
                        for (var d = c < 0 ? c + b : c; ++d < b;) a.push(d);
                        return a;
                    }),
                },
            }),
            (d.pseudos.nth = d.pseudos.eq);
        for (b in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) d.pseudos[b] = ma(b);
        for (b in { submit: !0, reset: !0 }) d.pseudos[b] = na(b);
        function ra() { }
        (ra.prototype = d.filters = d.pseudos),
            (d.setFilters = new ra()),
            (g = ga.tokenize = function (a, b) {
                var c,
                    e,
                    f,
                    g,
                    h,
                    i,
                    j,
                    k = z[a + " "];
                if (k) return b ? 0 : k.slice(0);
                (h = a), (i = []), (j = d.preFilter);
                while (h) {
                    (c && !(e = Q.exec(h))) || (e && (h = h.slice(e[0].length) || h), i.push((f = []))), (c = !1), (e = R.exec(h)) && ((c = e.shift()), f.push({ value: c, type: e[0].replace(P, " ") }), (h = h.slice(c.length)));
                    for (g in d.filter) !(e = V[g].exec(h)) || (j[g] && !(e = j[g](e))) || ((c = e.shift()), f.push({ value: c, type: g, matches: e }), (h = h.slice(c.length)));
                    if (!c) break;
                }
                return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
            });
        function sa(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d;
        }
        function ta(a, b, c) {
            var d = b.dir,
                e = b.next,
                f = e || d,
                g = c && "parentNode" === f,
                h = x++;
            return b.first
                ? function (b, c, e) {
                    while ((b = b[d])) if (1 === b.nodeType || g) return a(b, c, e);
                    return !1;
                }
                : function (b, c, i) {
                    var j,
                        k,
                        l,
                        m = [w, h];
                    if (i) {
                        while ((b = b[d])) if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
                    } else
                        while ((b = b[d]))
                            if (1 === b.nodeType || g)
                                if (((l = b[u] || (b[u] = {})), (k = l[b.uniqueID] || (l[b.uniqueID] = {})), e && e === b.nodeName.toLowerCase())) b = b[d] || b;
                                else {
                                    if ((j = k[f]) && j[0] === w && j[1] === h) return (m[2] = j[2]);
                                    if (((k[f] = m), (m[2] = a(b, c, i)))) return !0;
                                }
                    return !1;
                };
        }
        function ua(a) {
            return a.length > 1
                ? function (b, c, d) {
                    var e = a.length;
                    while (e--) if (!a[e](b, c, d)) return !1;
                    return !0;
                }
                : a[0];
        }
        function va(a, b, c) {
            for (var d = 0, e = b.length; d < e; d++) ga(a, b[d], c);
            return c;
        }
        function wa(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) (f = a[h]) && ((c && !c(f, d, e)) || (g.push(f), j && b.push(h)));
            return g;
        }
        function xa(a, b, c, d, e, f) {
            return (
                d && !d[u] && (d = xa(d)),
                e && !e[u] && (e = xa(e, f)),
                ia(function (f, g, h, i) {
                    var j,
                        k,
                        l,
                        m = [],
                        n = [],
                        o = g.length,
                        p = f || va(b || "*", h.nodeType ? [h] : h, []),
                        q = !a || (!f && b) ? p : wa(p, m, a, h, i),
                        r = c ? (e || (f ? a : o || d) ? [] : g) : q;
                    if ((c && c(q, r, h, i), d)) {
                        (j = wa(r, n)), d(j, [], h, i), (k = j.length);
                        while (k--) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
                    }
                    if (f) {
                        if (e || a) {
                            if (e) {
                                (j = []), (k = r.length);
                                while (k--) (l = r[k]) && j.push((q[k] = l));
                                e(null, (r = []), j, i);
                            }
                            k = r.length;
                            while (k--) (l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l));
                        }
                    } else (r = wa(r === g ? r.splice(o, r.length) : r)), e ? e(null, g, r, i) : G.apply(g, r);
                })
            );
        }
        function ya(a) {
            for (
                var b,
                c,
                e,
                f = a.length,
                g = d.relative[a[0].type],
                h = g || d.relative[" "],
                i = g ? 1 : 0,
                k = ta(
                    function (a) {
                        return a === b;
                    },
                    h,
                    !0
                ),
                l = ta(
                    function (a) {
                        return I(b, a) > -1;
                    },
                    h,
                    !0
                ),
                m = [
                    function (a, c, d) {
                        var e = (!g && (d || c !== j)) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
                        return (b = null), e;
                    },
                ];
                i < f;
                i++
            )
                if ((c = d.relative[a[i].type])) m = [ta(ua(m), c)];
                else {
                    if (((c = d.filter[a[i].type].apply(null, a[i].matches)), c[u])) {
                        for (e = ++i; e < f; e++) if (d.relative[a[e].type]) break;
                        return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({ value: " " === a[i - 2].type ? "*" : "" })).replace(P, "$1"), c, i < e && ya(a.slice(i, e)), e < f && ya((a = a.slice(e))), e < f && sa(a));
                    }
                    m.push(c);
                }
            return ua(m);
        }
        function za(a, b) {
            var c = b.length > 0,
                e = a.length > 0,
                f = function (f, g, h, i, k) {
                    var l,
                        o,
                        q,
                        r = 0,
                        s = "0",
                        t = f && [],
                        u = [],
                        v = j,
                        x = f || (e && d.find.TAG("*", k)),
                        y = (w += null == v ? 1 : Math.random() || 0.1),
                        z = x.length;
                    for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {
                        if (e && l) {
                            (o = 0), g || l.ownerDocument === n || (m(l), (h = !p));
                            while ((q = a[o++]))
                                if (q(l, g || n, h)) {
                                    i.push(l);
                                    break;
                                }
                            k && (w = y);
                        }
                        c && ((l = !q && l) && r--, f && t.push(l));
                    }
                    if (((r += s), c && s !== r)) {
                        o = 0;
                        while ((q = b[o++])) q(t, u, g, h);
                        if (f) {
                            if (r > 0) while (s--) t[s] || u[s] || (u[s] = E.call(i));
                            u = wa(u);
                        }
                        G.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i);
                    }
                    return k && ((w = y), (j = v)), t;
                };
            return c ? ia(f) : f;
        }
        return (
            (h = ga.compile = function (a, b) {
                var c,
                    d = [],
                    e = [],
                    f = A[a + " "];
                if (!f) {
                    b || (b = g(a)), (c = b.length);
                    while (c--) (f = ya(b[c])), f[u] ? d.push(f) : e.push(f);
                    (f = A(a, za(e, d))), (f.selector = a);
                }
                return f;
            }),
            (i = ga.select = function (a, b, c, e) {
                var f,
                    i,
                    j,
                    k,
                    l,
                    m = "function" == typeof a && a,
                    n = !e && g((a = m.selector || a));
                if (((c = c || []), 1 === n.length)) {
                    if (((i = n[0] = n[0].slice(0)), i.length > 2 && "ID" === (j = i[0]).type && 9 === b.nodeType && p && d.relative[i[1].type])) {
                        if (((b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0]), !b)) return c;
                        m && (b = b.parentNode), (a = a.slice(i.shift().value.length));
                    }
                    f = V.needsContext.test(a) ? 0 : i.length;
                    while (f--) {
                        if (((j = i[f]), d.relative[(k = j.type)])) break;
                        if ((l = d.find[k]) && (e = l(j.matches[0].replace(_, aa), ($.test(i[0].type) && qa(b.parentNode)) || b))) {
                            if ((i.splice(f, 1), (a = e.length && sa(i)), !a)) return G.apply(c, e), c;
                            break;
                        }
                    }
                }
                return (m || h(a, n))(e, b, !p, c, !b || ($.test(a) && qa(b.parentNode)) || b), c;
            }),
            (c.sortStable = u.split("").sort(B).join("") === u),
            (c.detectDuplicates = !!l),
            m(),
            (c.sortDetached = ja(function (a) {
                return 1 & a.compareDocumentPosition(n.createElement("fieldset"));
            })),
            ja(function (a) {
                return (a.innerHTML = "<a href='#'></a>"), "#" === a.firstChild.getAttribute("href");
            }) ||
            ka("type|href|height|width", function (a, b, c) {
                if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
            }),
            (c.attributes &&
                ja(function (a) {
                    return (a.innerHTML = "<input/>"), a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
                })) ||
            ka("value", function (a, b, c) {
                if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
            }),
            ja(function (a) {
                return null == a.getAttribute("disabled");
            }) ||
            ka(J, function (a, b, c) {
                var d;
                if (!c) return a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
            }),
            ga
        );
    })(a);
    (r.find = x), (r.expr = x.selectors), (r.expr[":"] = r.expr.pseudos), (r.uniqueSort = r.unique = x.uniqueSort), (r.text = x.getText), (r.isXMLDoc = x.isXML), (r.contains = x.contains), (r.escapeSelector = x.escape);
    var y = function (a, b, c) {
        var d = [],
            e = void 0 !== c;
        while ((a = a[b]) && 9 !== a.nodeType)
            if (1 === a.nodeType) {
                if (e && r(a).is(c)) break;
                d.push(a);
            }
        return d;
    },
        z = function (a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c;
        },
        A = r.expr.match.needsContext;
    function B(a, b) {
        return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
    }
    var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
        D = /^.[^:#\[\.,]*$/;
    function E(a, b, c) {
        return r.isFunction(b)
            ? r.grep(a, function (a, d) {
                return !!b.call(a, d, a) !== c;
            })
            : b.nodeType
                ? r.grep(a, function (a) {
                    return (a === b) !== c;
                })
                : "string" != typeof b
                    ? r.grep(a, function (a) {
                        return i.call(b, a) > -1 !== c;
                    })
                    : D.test(b)
                        ? r.filter(b, a, c)
                        : ((b = r.filter(b, a)),
                            r.grep(a, function (a) {
                                return i.call(b, a) > -1 !== c && 1 === a.nodeType;
                            }));
    }
    (r.filter = function (a, b, c) {
        var d = b[0];
        return (
            c && (a = ":not(" + a + ")"),
            1 === b.length && 1 === d.nodeType
                ? r.find.matchesSelector(d, a)
                    ? [d]
                    : []
                : r.find.matches(
                    a,
                    r.grep(b, function (a) {
                        return 1 === a.nodeType;
                    })
                )
        );
    }),
        r.fn.extend({
            find: function (a) {
                var b,
                    c,
                    d = this.length,
                    e = this;
                if ("string" != typeof a)
                    return this.pushStack(
                        r(a).filter(function () {
                            for (b = 0; b < d; b++) if (r.contains(e[b], this)) return !0;
                        })
                    );
                for (c = this.pushStack([]), b = 0; b < d; b++) r.find(a, e[b], c);
                return d > 1 ? r.uniqueSort(c) : c;
            },
            filter: function (a) {
                return this.pushStack(E(this, a || [], !1));
            },
            not: function (a) {
                return this.pushStack(E(this, a || [], !0));
            },
            is: function (a) {
                return !!E(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1).length;
            },
        });
    var F,
        G = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
        H = (r.fn.init = function (a, b, c) {
            var e, f;
            if (!a) return this;
            if (((c = c || F), "string" == typeof a)) {
                if (((e = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : G.exec(a)), !e || (!e[1] && b))) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
                if (e[1]) {
                    if (((b = b instanceof r ? b[0] : b), r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), C.test(e[1]) && r.isPlainObject(b)))
                        for (e in b) r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
                    return this;
                }
                return (f = d.getElementById(e[2])), f && ((this[0] = f), (this.length = 1)), this;
            }
            return a.nodeType ? ((this[0] = a), (this.length = 1), this) : r.isFunction(a) ? (void 0 !== c.ready ? c.ready(a) : a(r)) : r.makeArray(a, this);
        });
    (H.prototype = r.fn), (F = r(d));
    var I = /^(?:parents|prev(?:Until|All))/,
        J = { children: !0, contents: !0, next: !0, prev: !0 };
    r.fn.extend({
        has: function (a) {
            var b = r(a, this),
                c = b.length;
            return this.filter(function () {
                for (var a = 0; a < c; a++) if (r.contains(this, b[a])) return !0;
            });
        },
        closest: function (a, b) {
            var c,
                d = 0,
                e = this.length,
                f = [],
                g = "string" != typeof a && r(a);
            if (!A.test(a))
                for (; d < e; d++)
                    for (c = this[d]; c && c !== b; c = c.parentNode)
                        if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {
                            f.push(c);
                            break;
                        }
            return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f);
        },
        index: function (a) {
            return a ? ("string" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a)) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
        },
        add: function (a, b) {
            return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))));
        },
        addBack: function (a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
        },
    });
    function K(a, b) {
        while ((a = a[b]) && 1 !== a.nodeType);
        return a;
    }
    r.each(
        {
            parent: function (a) {
                var b = a.parentNode;
                return b && 11 !== b.nodeType ? b : null;
            },
            parents: function (a) {
                return y(a, "parentNode");
            },
            parentsUntil: function (a, b, c) {
                return y(a, "parentNode", c);
            },
            next: function (a) {
                return K(a, "nextSibling");
            },
            prev: function (a) {
                return K(a, "previousSibling");
            },
            nextAll: function (a) {
                return y(a, "nextSibling");
            },
            prevAll: function (a) {
                return y(a, "previousSibling");
            },
            nextUntil: function (a, b, c) {
                return y(a, "nextSibling", c);
            },
            prevUntil: function (a, b, c) {
                return y(a, "previousSibling", c);
            },
            siblings: function (a) {
                return z((a.parentNode || {}).firstChild, a);
            },
            children: function (a) {
                return z(a.firstChild);
            },
            contents: function (a) {
                return B(a, "iframe") ? a.contentDocument : (B(a, "template") && (a = a.content || a), r.merge([], a.childNodes));
            },
        },
        function (a, b) {
            r.fn[a] = function (c, d) {
                var e = r.map(this, b, c);
                return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = r.filter(d, e)), this.length > 1 && (J[a] || r.uniqueSort(e), I.test(a) && e.reverse()), this.pushStack(e);
            };
        }
    );
    var L = /[^\x20\t\r\n\f]+/g;
    function M(a) {
        var b = {};
        return (
            r.each(a.match(L) || [], function (a, c) {
                b[c] = !0;
            }),
            b
        );
    }
    r.Callbacks = function (a) {
        a = "string" == typeof a ? M(a) : r.extend({}, a);
        var b,
            c,
            d,
            e,
            f = [],
            g = [],
            h = -1,
            i = function () {
                for (e = e || a.once, d = b = !0; g.length; h = -1) {
                    c = g.shift();
                    while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && ((h = f.length), (c = !1));
                }
                a.memory || (c = !1), (b = !1), e && (f = c ? [] : "");
            },
            j = {
                add: function () {
                    return (
                        f &&
                        (c && !b && ((h = f.length - 1), g.push(c)),
                            (function d(b) {
                                r.each(b, function (b, c) {
                                    r.isFunction(c) ? (a.unique && j.has(c)) || f.push(c) : c && c.length && "string" !== r.type(c) && d(c);
                                });
                            })(arguments),
                            c && !b && i()),
                        this
                    );
                },
                remove: function () {
                    return (
                        r.each(arguments, function (a, b) {
                            var c;
                            while ((c = r.inArray(b, f, c)) > -1) f.splice(c, 1), c <= h && h--;
                        }),
                        this
                    );
                },
                has: function (a) {
                    return a ? r.inArray(a, f) > -1 : f.length > 0;
                },
                empty: function () {
                    return f && (f = []), this;
                },
                disable: function () {
                    return (e = g = []), (f = c = ""), this;
                },
                disabled: function () {
                    return !f;
                },
                lock: function () {
                    return (e = g = []), c || b || (f = c = ""), this;
                },
                locked: function () {
                    return !!e;
                },
                fireWith: function (a, c) {
                    return e || ((c = c || []), (c = [a, c.slice ? c.slice() : c]), g.push(c), b || i()), this;
                },
                fire: function () {
                    return j.fireWith(this, arguments), this;
                },
                fired: function () {
                    return !!d;
                },
            };
        return j;
    };
    function N(a) {
        return a;
    }
    function O(a) {
        throw a;
    }
    function P(a, b, c, d) {
        var e;
        try {
            a && r.isFunction((e = a.promise)) ? e.call(a).done(b).fail(c) : a && r.isFunction((e = a.then)) ? e.call(a, b, c) : b.apply(void 0, [a].slice(d));
        } catch (a) {
            c.apply(void 0, [a]);
        }
    }
    r.extend({
        Deferred: function (b) {
            var c = [
                ["notify", "progress", r.Callbacks("memory"), r.Callbacks("memory"), 2],
                ["resolve", "done", r.Callbacks("once memory"), r.Callbacks("once memory"), 0, "resolved"],
                ["reject", "fail", r.Callbacks("once memory"), r.Callbacks("once memory"), 1, "rejected"],
            ],
                d = "pending",
                e = {
                    state: function () {
                        return d;
                    },
                    always: function () {
                        return f.done(arguments).fail(arguments), this;
                    },
                    catch: function (a) {
                        return e.then(null, a);
                    },
                    pipe: function () {
                        var a = arguments;
                        return r
                            .Deferred(function (b) {
                                r.each(c, function (c, d) {
                                    var e = r.isFunction(a[d[4]]) && a[d[4]];
                                    f[d[1]](function () {
                                        var a = e && e.apply(this, arguments);
                                        a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments);
                                    });
                                }),
                                    (a = null);
                            })
                            .promise();
                    },
                    then: function (b, d, e) {
                        var f = 0;
                        function g(b, c, d, e) {
                            return function () {
                                var h = this,
                                    i = arguments,
                                    j = function () {
                                        var a, j;
                                        if (!(b < f)) {
                                            if (((a = d.apply(h, i)), a === c.promise())) throw new TypeError("Thenable self-resolution");
                                            (j = a && ("object" == typeof a || "function" == typeof a) && a.then),
                                                r.isFunction(j)
                                                    ? e
                                                        ? j.call(a, g(f, c, N, e), g(f, c, O, e))
                                                        : (f++, j.call(a, g(f, c, N, e), g(f, c, O, e), g(f, c, N, c.notifyWith)))
                                                    : (d !== N && ((h = void 0), (i = [a])), (e || c.resolveWith)(h, i));
                                        }
                                    },
                                    k = e
                                        ? j
                                        : function () {
                                            try {
                                                j();
                                            } catch (a) {
                                                r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== O && ((h = void 0), (i = [a])), c.rejectWith(h, i));
                                            }
                                        };
                                b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k));
                            };
                        }
                        return r
                            .Deferred(function (a) {
                                c[0][3].add(g(0, a, r.isFunction(e) ? e : N, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : N)), c[2][3].add(g(0, a, r.isFunction(d) ? d : O));
                            })
                            .promise();
                    },
                    promise: function (a) {
                        return null != a ? r.extend(a, e) : e;
                    },
                },
                f = {};
            return (
                r.each(c, function (a, b) {
                    var g = b[2],
                        h = b[5];
                    (e[b[1]] = g.add),
                        h &&
                        g.add(
                            function () {
                                d = h;
                            },
                            c[3 - a][2].disable,
                            c[0][2].lock
                        ),
                        g.add(b[3].fire),
                        (f[b[0]] = function () {
                            return f[b[0] + "With"](this === f ? void 0 : this, arguments), this;
                        }),
                        (f[b[0] + "With"] = g.fireWith);
                }),
                e.promise(f),
                b && b.call(f, f),
                f
            );
        },
        when: function (a) {
            var b = arguments.length,
                c = b,
                d = Array(c),
                e = f.call(arguments),
                g = r.Deferred(),
                h = function (a) {
                    return function (c) {
                        (d[a] = this), (e[a] = arguments.length > 1 ? f.call(arguments) : c), --b || g.resolveWith(d, e);
                    };
                };
            if (b <= 1 && (P(a, g.done(h(c)).resolve, g.reject, !b), "pending" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();
            while (c--) P(e[c], h(c), g.reject);
            return g.promise();
        },
    });
    var Q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    (r.Deferred.exceptionHook = function (b, c) {
        a.console && a.console.warn && b && Q.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
    }),
        (r.readyException = function (b) {
            a.setTimeout(function () {
                throw b;
            });
        });
    var R = r.Deferred();
    (r.fn.ready = function (a) {
        return (
            R.then(a)["catch"](function (a) {
                r.readyException(a);
            }),
            this
        );
    }),
        r.extend({
            isReady: !1,
            readyWait: 1,
            ready: function (a) {
                (a === !0 ? --r.readyWait : r.isReady) || ((r.isReady = !0), (a !== !0 && --r.readyWait > 0) || R.resolveWith(d, [r]));
            },
        }),
        (r.ready.then = R.then);
    function S() {
        d.removeEventListener("DOMContentLoaded", S), a.removeEventListener("load", S), r.ready();
    }
    "complete" === d.readyState || ("loading" !== d.readyState && !d.documentElement.doScroll) ? a.setTimeout(r.ready) : (d.addEventListener("DOMContentLoaded", S), a.addEventListener("load", S));
    var T = function (a, b, c, d, e, f, g) {
        var h = 0,
            i = a.length,
            j = null == c;
        if ("object" === r.type(c)) {
            e = !0;
            for (h in c) T(a, b, h, c[h], !0, f, g);
        } else if (
            void 0 !== d &&
            ((e = !0),
                r.isFunction(d) || (g = !0),
                j &&
                (g
                    ? (b.call(a, d), (b = null))
                    : ((j = b),
                        (b = function (a, b, c) {
                            return j.call(r(a), c);
                        }))),
                b)
        )
            for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
        return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    },
        U = function (a) {
            return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
        };
    function V() {
        this.expando = r.expando + V.uid++;
    }
    (V.uid = 1),
        (V.prototype = {
            cache: function (a) {
                var b = a[this.expando];
                return b || ((b = {}), U(a) && (a.nodeType ? (a[this.expando] = b) : Object.defineProperty(a, this.expando, { value: b, configurable: !0 }))), b;
            },
            set: function (a, b, c) {
                var d,
                    e = this.cache(a);
                if ("string" == typeof b) e[r.camelCase(b)] = c;
                else for (d in b) e[r.camelCase(d)] = b[d];
                return e;
            },
            get: function (a, b) {
                return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)];
            },
            access: function (a, b, c) {
                return void 0 === b || (b && "string" == typeof b && void 0 === c) ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b);
            },
            remove: function (a, b) {
                var c,
                    d = a[this.expando];
                if (void 0 !== d) {
                    if (void 0 !== b) {
                        Array.isArray(b) ? (b = b.map(r.camelCase)) : ((b = r.camelCase(b)), (b = b in d ? [b] : b.match(L) || [])), (c = b.length);
                        while (c--) delete d[b[c]];
                    }
                    (void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? (a[this.expando] = void 0) : delete a[this.expando]);
                }
            },
            hasData: function (a) {
                var b = a[this.expando];
                return void 0 !== b && !r.isEmptyObject(b);
            },
        });
    var W = new V(),
        X = new V(),
        Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        Z = /[A-Z]/g;
    function $(a) {
        return "true" === a || ("false" !== a && ("null" === a ? null : a === +a + "" ? +a : Y.test(a) ? JSON.parse(a) : a));
    }
    function _(a, b, c) {
        var d;
        if (void 0 === c && 1 === a.nodeType)
            if (((d = "data-" + b.replace(Z, "-$&").toLowerCase()), (c = a.getAttribute(d)), "string" == typeof c)) {
                try {
                    c = $(c);
                } catch (e) { }
                X.set(a, b, c);
            } else c = void 0;
        return c;
    }
    r.extend({
        hasData: function (a) {
            return X.hasData(a) || W.hasData(a);
        },
        data: function (a, b, c) {
            return X.access(a, b, c);
        },
        removeData: function (a, b) {
            X.remove(a, b);
        },
        _data: function (a, b, c) {
            return W.access(a, b, c);
        },
        _removeData: function (a, b) {
            W.remove(a, b);
        },
    }),
        r.fn.extend({
            data: function (a, b) {
                var c,
                    d,
                    e,
                    f = this[0],
                    g = f && f.attributes;
                if (void 0 === a) {
                    if (this.length && ((e = X.get(f)), 1 === f.nodeType && !W.get(f, "hasDataAttrs"))) {
                        c = g.length;
                        while (c--) g[c] && ((d = g[c].name), 0 === d.indexOf("data-") && ((d = r.camelCase(d.slice(5))), _(f, d, e[d])));
                        W.set(f, "hasDataAttrs", !0);
                    }
                    return e;
                }
                return "object" == typeof a
                    ? this.each(function () {
                        X.set(this, a);
                    })
                    : T(
                        this,
                        function (b) {
                            var c;
                            if (f && void 0 === b) {
                                if (((c = X.get(f, a)), void 0 !== c)) return c;
                                if (((c = _(f, a)), void 0 !== c)) return c;
                            } else
                                this.each(function () {
                                    X.set(this, a, b);
                                });
                        },
                        null,
                        b,
                        arguments.length > 1,
                        null,
                        !0
                    );
            },
            removeData: function (a) {
                return this.each(function () {
                    X.remove(this, a);
                });
            },
        }),
        r.extend({
            queue: function (a, b, c) {
                var d;
                if (a) return (b = (b || "fx") + "queue"), (d = W.get(a, b)), c && (!d || Array.isArray(c) ? (d = W.access(a, b, r.makeArray(c))) : d.push(c)), d || [];
            },
            dequeue: function (a, b) {
                b = b || "fx";
                var c = r.queue(a, b),
                    d = c.length,
                    e = c.shift(),
                    f = r._queueHooks(a, b),
                    g = function () {
                        r.dequeue(a, b);
                    };
                "inprogress" === e && ((e = c.shift()), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire();
            },
            _queueHooks: function (a, b) {
                var c = b + "queueHooks";
                return (
                    W.get(a, c) ||
                    W.access(a, c, {
                        empty: r.Callbacks("once memory").add(function () {
                            W.remove(a, [b + "queue", c]);
                        }),
                    })
                );
            },
        }),
        r.fn.extend({
            queue: function (a, b) {
                var c = 2;
                return (
                    "string" != typeof a && ((b = a), (a = "fx"), c--),
                    arguments.length < c
                        ? r.queue(this[0], a)
                        : void 0 === b
                            ? this
                            : this.each(function () {
                                var c = r.queue(this, a, b);
                                r._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a);
                            })
                );
            },
            dequeue: function (a) {
                return this.each(function () {
                    r.dequeue(this, a);
                });
            },
            clearQueue: function (a) {
                return this.queue(a || "fx", []);
            },
            promise: function (a, b) {
                var c,
                    d = 1,
                    e = r.Deferred(),
                    f = this,
                    g = this.length,
                    h = function () {
                        --d || e.resolveWith(f, [f]);
                    };
                "string" != typeof a && ((b = a), (a = void 0)), (a = a || "fx");
                while (g--) (c = W.get(f[g], a + "queueHooks")), c && c.empty && (d++, c.empty.add(h));
                return h(), e.promise(b);
            },
        });
    var aa = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        ba = new RegExp("^(?:([+-])=|)(" + aa + ")([a-z%]*)$", "i"),
        ca = ["Top", "Right", "Bottom", "Left"],
        da = function (a, b) {
            return (a = b || a), "none" === a.style.display || ("" === a.style.display && r.contains(a.ownerDocument, a) && "none" === r.css(a, "display"));
        },
        ea = function (a, b, c, d) {
            var e,
                f,
                g = {};
            for (f in b) (g[f] = a.style[f]), (a.style[f] = b[f]);
            e = c.apply(a, d || []);
            for (f in b) a.style[f] = g[f];
            return e;
        };
    function fa(a, b, c, d) {
        var e,
            f = 1,
            g = 20,
            h = d
                ? function () {
                    return d.cur();
                }
                : function () {
                    return r.css(a, b, "");
                },
            i = h(),
            j = (c && c[3]) || (r.cssNumber[b] ? "" : "px"),
            k = (r.cssNumber[b] || ("px" !== j && +i)) && ba.exec(r.css(a, b));
        if (k && k[3] !== j) {
            (j = j || k[3]), (c = c || []), (k = +i || 1);
            do (f = f || ".5"), (k /= f), r.style(a, b, k + j);
            while (f !== (f = h() / i) && 1 !== f && --g);
        }
        return c && ((k = +k || +i || 0), (e = c[1] ? k + (c[1] + 1) * c[2] : +c[2]), d && ((d.unit = j), (d.start = k), (d.end = e))), e;
    }
    var ga = {};
    function ha(a) {
        var b,
            c = a.ownerDocument,
            d = a.nodeName,
            e = ga[d];
        return e ? e : ((b = c.body.appendChild(c.createElement(d))), (e = r.css(b, "display")), b.parentNode.removeChild(b), "none" === e && (e = "block"), (ga[d] = e), e);
    }
    function ia(a, b) {
        for (var c, d, e = [], f = 0, g = a.length; f < g; f++)
            (d = a[f]),
                d.style &&
                ((c = d.style.display),
                    b ? ("none" === c && ((e[f] = W.get(d, "display") || null), e[f] || (d.style.display = "")), "" === d.style.display && da(d) && (e[f] = ha(d))) : "none" !== c && ((e[f] = "none"), W.set(d, "display", c)));
        for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
        return a;
    }
    r.fn.extend({
        show: function () {
            return ia(this, !0);
        },
        hide: function () {
            return ia(this);
        },
        toggle: function (a) {
            return "boolean" == typeof a
                ? a
                    ? this.show()
                    : this.hide()
                : this.each(function () {
                    da(this) ? r(this).show() : r(this).hide();
                });
        },
    });
    var ja = /^(?:checkbox|radio)$/i,
        ka = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
        la = /^$|\/(?:java|ecma)script/i,
        ma = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""],
        };
    (ma.optgroup = ma.option), (ma.tbody = ma.tfoot = ma.colgroup = ma.caption = ma.thead), (ma.th = ma.td);
    function na(a, b) {
        var c;
        return (c = "undefined" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || "*") : "undefined" != typeof a.querySelectorAll ? a.querySelectorAll(b || "*") : []), void 0 === b || (b && B(a, b)) ? r.merge([a], c) : c;
    }
    function oa(a, b) {
        for (var c = 0, d = a.length; c < d; c++) W.set(a[c], "globalEval", !b || W.get(b[c], "globalEval"));
    }
    var pa = /<|&#?\w+;/;
    function qa(a, b, c, d, e) {
        for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++)
            if (((f = a[n]), f || 0 === f))
                if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);
                else if (pa.test(f)) {
                    (g = g || l.appendChild(b.createElement("div"))), (h = (ka.exec(f) || ["", ""])[1].toLowerCase()), (i = ma[h] || ma._default), (g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2]), (k = i[0]);
                    while (k--) g = g.lastChild;
                    r.merge(m, g.childNodes), (g = l.firstChild), (g.textContent = "");
                } else m.push(b.createTextNode(f));
        (l.textContent = ""), (n = 0);
        while ((f = m[n++]))
            if (d && r.inArray(f, d) > -1) e && e.push(f);
            else if (((j = r.contains(f.ownerDocument, f)), (g = na(l.appendChild(f), "script")), j && oa(g), c)) {
                k = 0;
                while ((f = g[k++])) la.test(f.type || "") && c.push(f);
            }
        return l;
    }
    !(function () {
        var a = d.createDocumentFragment(),
            b = a.appendChild(d.createElement("div")),
            c = d.createElement("input");
        c.setAttribute("type", "radio"),
            c.setAttribute("checked", "checked"),
            c.setAttribute("name", "t"),
            b.appendChild(c),
            (o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked),
            (b.innerHTML = "<textarea>x</textarea>"),
            (o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue);
    })();
    var ra = d.documentElement,
        sa = /^key/,
        ta = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
        ua = /^([^.]*)(?:\.(.+)|)/;
    function va() {
        return !0;
    }
    function wa() {
        return !1;
    }
    function xa() {
        try {
            return d.activeElement;
        } catch (a) { }
    }
    function ya(a, b, c, d, e, f) {
        var g, h;
        if ("object" == typeof b) {
            "string" != typeof c && ((d = d || c), (c = void 0));
            for (h in b) ya(a, h, c, d, b[h], f);
            return a;
        }
        if ((null == d && null == e ? ((e = c), (d = c = void 0)) : null == e && ("string" == typeof c ? ((e = d), (d = void 0)) : ((e = d), (d = c), (c = void 0))), e === !1)) e = wa;
        else if (!e) return a;
        return (
            1 === f &&
            ((g = e),
                (e = function (a) {
                    return r().off(a), g.apply(this, arguments);
                }),
                (e.guid = g.guid || (g.guid = r.guid++))),
            a.each(function () {
                r.event.add(this, b, e, d, c);
            })
        );
    }
    (r.event = {
        global: {},
        add: function (a, b, c, d, e) {
            var f,
                g,
                h,
                i,
                j,
                k,
                l,
                m,
                n,
                o,
                p,
                q = W.get(a);
            if (q) {
                c.handler && ((f = c), (c = f.handler), (e = f.selector)),
                    e && r.find.matchesSelector(ra, e),
                    c.guid || (c.guid = r.guid++),
                    (i = q.events) || (i = q.events = {}),
                    (g = q.handle) ||
                    (g = q.handle = function (b) {
                        return "undefined" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0;
                    }),
                    (b = (b || "").match(L) || [""]),
                    (j = b.length);
                while (j--)
                    (h = ua.exec(b[j]) || []),
                        (n = p = h[1]),
                        (o = (h[2] || "").split(".").sort()),
                        n &&
                        ((l = r.event.special[n] || {}),
                            (n = (e ? l.delegateType : l.bindType) || n),
                            (l = r.event.special[n] || {}),
                            (k = r.extend({ type: n, origType: p, data: d, handler: c, guid: c.guid, selector: e, needsContext: e && r.expr.match.needsContext.test(e), namespace: o.join(".") }, f)),
                            (m = i[n]) || ((m = i[n] = []), (m.delegateCount = 0), (l.setup && l.setup.call(a, d, o, g) !== !1) || (a.addEventListener && a.addEventListener(n, g))),
                            l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)),
                            e ? m.splice(m.delegateCount++, 0, k) : m.push(k),
                            (r.event.global[n] = !0));
            }
        },
        remove: function (a, b, c, d, e) {
            var f,
                g,
                h,
                i,
                j,
                k,
                l,
                m,
                n,
                o,
                p,
                q = W.hasData(a) && W.get(a);
            if (q && (i = q.events)) {
                (b = (b || "").match(L) || [""]), (j = b.length);
                while (j--)
                    if (((h = ua.exec(b[j]) || []), (n = p = h[1]), (o = (h[2] || "").split(".").sort()), n)) {
                        (l = r.event.special[n] || {}), (n = (d ? l.delegateType : l.bindType) || n), (m = i[n] || []), (h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)")), (g = f = m.length);
                        while (f--)
                            (k = m[f]),
                                (!e && p !== k.origType) ||
                                (c && c.guid !== k.guid) ||
                                (h && !h.test(k.namespace)) ||
                                (d && d !== k.selector && ("**" !== d || !k.selector)) ||
                                (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
                        g && !m.length && ((l.teardown && l.teardown.call(a, o, q.handle) !== !1) || r.removeEvent(a, n, q.handle), delete i[n]);
                    } else for (n in i) r.event.remove(a, n + b[j], c, d, !0);
                r.isEmptyObject(i) && W.remove(a, "handle events");
            }
        },
        dispatch: function (a) {
            var b = r.event.fix(a),
                c,
                d,
                e,
                f,
                g,
                h,
                i = new Array(arguments.length),
                j = (W.get(this, "events") || {})[b.type] || [],
                k = r.event.special[b.type] || {};
            for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];
            if (((b.delegateTarget = this), !k.preDispatch || k.preDispatch.call(this, b) !== !1)) {
                (h = r.event.handlers.call(this, b, j)), (c = 0);
                while ((f = h[c++]) && !b.isPropagationStopped()) {
                    (b.currentTarget = f.elem), (d = 0);
                    while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped())
                        (b.rnamespace && !b.rnamespace.test(g.namespace)) ||
                            ((b.handleObj = g), (b.data = g.data), (e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i)), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()));
                }
                return k.postDispatch && k.postDispatch.call(this, b), b.result;
            }
        },
        handlers: function (a, b) {
            var c,
                d,
                e,
                f,
                g,
                h = [],
                i = b.delegateCount,
                j = a.target;
            if (i && j.nodeType && !("click" === a.type && a.button >= 1))
                for (; j !== this; j = j.parentNode || this)
                    if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
                        for (f = [], g = {}, c = 0; c < i; c++) (d = b[c]), (e = d.selector + " "), void 0 === g[e] && (g[e] = d.needsContext ? r(e, this).index(j) > -1 : r.find(e, this, null, [j]).length), g[e] && f.push(d);
                        f.length && h.push({ elem: j, handlers: f });
                    }
            return (j = this), i < b.length && h.push({ elem: j, handlers: b.slice(i) }), h;
        },
        addProp: function (a, b) {
            Object.defineProperty(r.Event.prototype, a, {
                enumerable: !0,
                configurable: !0,
                get: r.isFunction(b)
                    ? function () {
                        if (this.originalEvent) return b(this.originalEvent);
                    }
                    : function () {
                        if (this.originalEvent) return this.originalEvent[a];
                    },
                set: function (b) {
                    Object.defineProperty(this, a, { enumerable: !0, configurable: !0, writable: !0, value: b });
                },
            });
        },
        fix: function (a) {
            return a[r.expando] ? a : new r.Event(a);
        },
        special: {
            load: { noBubble: !0 },
            focus: {
                trigger: function () {
                    if (this !== xa() && this.focus) return this.focus(), !1;
                },
                delegateType: "focusin",
            },
            blur: {
                trigger: function () {
                    if (this === xa() && this.blur) return this.blur(), !1;
                },
                delegateType: "focusout",
            },
            click: {
                trigger: function () {
                    if (ja.test(this.type) && this.click && B(this, "input")) return this.click(), !1;
                },
                _default: function (a) {
                    return B(a.target, "a");
                },
            },
            beforeunload: {
                postDispatch: function (a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
                },
            },
        },
    }),
        (r.removeEvent = function (a, b, c) {
            a.removeEventListener && a.removeEventListener(b, c);
        }),
        (r.Event = function (a, b) {
            return this instanceof r.Event
                ? (a && a.type
                    ? ((this.originalEvent = a),
                        (this.type = a.type),
                        (this.isDefaultPrevented = a.defaultPrevented || (void 0 === a.defaultPrevented && a.returnValue === !1) ? va : wa),
                        (this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target),
                        (this.currentTarget = a.currentTarget),
                        (this.relatedTarget = a.relatedTarget))
                    : (this.type = a),
                    b && r.extend(this, b),
                    (this.timeStamp = (a && a.timeStamp) || r.now()),
                    void (this[r.expando] = !0))
                : new r.Event(a, b);
        }),
        (r.Event.prototype = {
            constructor: r.Event,
            isDefaultPrevented: wa,
            isPropagationStopped: wa,
            isImmediatePropagationStopped: wa,
            isSimulated: !1,
            preventDefault: function () {
                var a = this.originalEvent;
                (this.isDefaultPrevented = va), a && !this.isSimulated && a.preventDefault();
            },
            stopPropagation: function () {
                var a = this.originalEvent;
                (this.isPropagationStopped = va), a && !this.isSimulated && a.stopPropagation();
            },
            stopImmediatePropagation: function () {
                var a = this.originalEvent;
                (this.isImmediatePropagationStopped = va), a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation();
            },
        }),
        r.each(
            {
                altKey: !0,
                bubbles: !0,
                cancelable: !0,
                changedTouches: !0,
                ctrlKey: !0,
                detail: !0,
                eventPhase: !0,
                metaKey: !0,
                pageX: !0,
                pageY: !0,
                shiftKey: !0,
                view: !0,
                char: !0,
                charCode: !0,
                key: !0,
                keyCode: !0,
                button: !0,
                buttons: !0,
                clientX: !0,
                clientY: !0,
                offsetX: !0,
                offsetY: !0,
                pointerId: !0,
                pointerType: !0,
                screenX: !0,
                screenY: !0,
                targetTouches: !0,
                toElement: !0,
                touches: !0,
                which: function (a) {
                    var b = a.button;
                    return null == a.which && sa.test(a.type) ? (null != a.charCode ? a.charCode : a.keyCode) : !a.which && void 0 !== b && ta.test(a.type) ? (1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0) : a.which;
                },
            },
            r.event.addProp
        ),
        r.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (a, b) {
            r.event.special[a] = {
                delegateType: b,
                bindType: b,
                handle: function (a) {
                    var c,
                        d = this,
                        e = a.relatedTarget,
                        f = a.handleObj;
                    return (e && (e === d || r.contains(d, e))) || ((a.type = f.origType), (c = f.handler.apply(this, arguments)), (a.type = b)), c;
                },
            };
        }),
        r.fn.extend({
            on: function (a, b, c, d) {
                return ya(this, a, b, c, d);
            },
            one: function (a, b, c, d) {
                return ya(this, a, b, c, d, 1);
            },
            off: function (a, b, c) {
                var d, e;
                if (a && a.preventDefault && a.handleObj) return (d = a.handleObj), r(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
                if ("object" == typeof a) {
                    for (e in a) this.off(e, b, a[e]);
                    return this;
                }
                return (
                    (b !== !1 && "function" != typeof b) || ((c = b), (b = void 0)),
                    c === !1 && (c = wa),
                    this.each(function () {
                        r.event.remove(this, a, c, b);
                    })
                );
            },
        });
    var za = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
        Aa = /<script|<style|<link/i,
        Ba = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ca = /^true\/(.*)/,
        Da = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    function Ea(a, b) {
        return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr") ? r(">tbody", a)[0] || a : a;
    }
    function Fa(a) {
        return (a.type = (null !== a.getAttribute("type")) + "/" + a.type), a;
    }
    function Ga(a) {
        var b = Ca.exec(a.type);
        return b ? (a.type = b[1]) : a.removeAttribute("type"), a;
    }
    function Ha(a, b) {
        var c, d, e, f, g, h, i, j;
        if (1 === b.nodeType) {
            if (W.hasData(a) && ((f = W.access(a)), (g = W.set(b, f)), (j = f.events))) {
                delete g.handle, (g.events = {});
                for (e in j) for (c = 0, d = j[e].length; c < d; c++) r.event.add(b, e, j[e][c]);
            }
            X.hasData(a) && ((h = X.access(a)), (i = r.extend({}, h)), X.set(b, i));
        }
    }
    function Ia(a, b) {
        var c = b.nodeName.toLowerCase();
        "input" === c && ja.test(a.type) ? (b.checked = a.checked) : ("input" !== c && "textarea" !== c) || (b.defaultValue = a.defaultValue);
    }
    function Ja(a, b, c, d) {
        b = g.apply([], b);
        var e,
            f,
            h,
            i,
            j,
            k,
            l = 0,
            m = a.length,
            n = m - 1,
            q = b[0],
            s = r.isFunction(q);
        if (s || (m > 1 && "string" == typeof q && !o.checkClone && Ba.test(q)))
            return a.each(function (e) {
                var f = a.eq(e);
                s && (b[0] = q.call(this, e, f.html())), Ja(f, b, c, d);
            });
        if (m && ((e = qa(b, a[0].ownerDocument, !1, a, d)), (f = e.firstChild), 1 === e.childNodes.length && (e = f), f || d)) {
            for (h = r.map(na(e, "script"), Fa), i = h.length; l < m; l++) (j = e), l !== n && ((j = r.clone(j, !0, !0)), i && r.merge(h, na(j, "script"))), c.call(a[l], j, l);
            if (i)
                for (k = h[h.length - 1].ownerDocument, r.map(h, Ga), l = 0; l < i; l++)
                    (j = h[l]), la.test(j.type || "") && !W.access(j, "globalEval") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Da, ""), k));
        }
        return a;
    }
    function Ka(a, b, c) {
        for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || r.cleanData(na(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && oa(na(d, "script")), d.parentNode.removeChild(d));
        return a;
    }
    r.extend({
        htmlPrefilter: function (a) {
            return a.replace(za, "<$1></$2>");
        },
        clone: function (a, b, c) {
            var d,
                e,
                f,
                g,
                h = a.cloneNode(!0),
                i = r.contains(a.ownerDocument, a);
            if (!(o.noCloneChecked || (1 !== a.nodeType && 11 !== a.nodeType) || r.isXMLDoc(a))) for (g = na(h), f = na(a), d = 0, e = f.length; d < e; d++) Ia(f[d], g[d]);
            if (b)
                if (c) for (f = f || na(a), g = g || na(h), d = 0, e = f.length; d < e; d++) Ha(f[d], g[d]);
                else Ha(a, h);
            return (g = na(h, "script")), g.length > 0 && oa(g, !i && na(a, "script")), h;
        },
        cleanData: function (a) {
            for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++)
                if (U(c)) {
                    if ((b = c[W.expando])) {
                        if (b.events) for (d in b.events) e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
                        c[W.expando] = void 0;
                    }
                    c[X.expando] && (c[X.expando] = void 0);
                }
        },
    }),
        r.fn.extend({
            detach: function (a) {
                return Ka(this, a, !0);
            },
            remove: function (a) {
                return Ka(this, a);
            },
            text: function (a) {
                return T(
                    this,
                    function (a) {
                        return void 0 === a
                            ? r.text(this)
                            : this.empty().each(function () {
                                (1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = a);
                            });
                    },
                    null,
                    a,
                    arguments.length
                );
            },
            append: function () {
                return Ja(this, arguments, function (a) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var b = Ea(this, a);
                        b.appendChild(a);
                    }
                });
            },
            prepend: function () {
                return Ja(this, arguments, function (a) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var b = Ea(this, a);
                        b.insertBefore(a, b.firstChild);
                    }
                });
            },
            before: function () {
                return Ja(this, arguments, function (a) {
                    this.parentNode && this.parentNode.insertBefore(a, this);
                });
            },
            after: function () {
                return Ja(this, arguments, function (a) {
                    this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
                });
            },
            empty: function () {
                for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (r.cleanData(na(a, !1)), (a.textContent = ""));
                return this;
            },
            clone: function (a, b) {
                return (
                    (a = null != a && a),
                    (b = null == b ? a : b),
                    this.map(function () {
                        return r.clone(this, a, b);
                    })
                );
            },
            html: function (a) {
                return T(
                    this,
                    function (a) {
                        var b = this[0] || {},
                            c = 0,
                            d = this.length;
                        if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
                        if ("string" == typeof a && !Aa.test(a) && !ma[(ka.exec(a) || ["", ""])[1].toLowerCase()]) {
                            a = r.htmlPrefilter(a);
                            try {
                                for (; c < d; c++) (b = this[c] || {}), 1 === b.nodeType && (r.cleanData(na(b, !1)), (b.innerHTML = a));
                                b = 0;
                            } catch (e) { }
                        }
                        b && this.empty().append(a);
                    },
                    null,
                    a,
                    arguments.length
                );
            },
            replaceWith: function () {
                var a = [];
                return Ja(
                    this,
                    arguments,
                    function (b) {
                        var c = this.parentNode;
                        r.inArray(this, a) < 0 && (r.cleanData(na(this)), c && c.replaceChild(b, this));
                    },
                    a
                );
            },
        }),
        r.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (a, b) {
            r.fn[a] = function (a) {
                for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++) (c = g === f ? this : this.clone(!0)), r(e[g])[b](c), h.apply(d, c.get());
                return this.pushStack(d);
            };
        });
    var La = /^margin/,
        Ma = new RegExp("^(" + aa + ")(?!px)[a-z%]+$", "i"),
        Na = function (b) {
            var c = b.ownerDocument.defaultView;
            return (c && c.opener) || (c = a), c.getComputedStyle(b);
        };
    !(function () {
        function b() {
            if (i) {
                (i.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"), (i.innerHTML = ""), ra.appendChild(h);
                var b = a.getComputedStyle(i);
                (c = "1%" !== b.top), (g = "2px" === b.marginLeft), (e = "4px" === b.width), (i.style.marginRight = "50%"), (f = "4px" === b.marginRight), ra.removeChild(h), (i = null);
            }
        }
        var c,
            e,
            f,
            g,
            h = d.createElement("div"),
            i = d.createElement("div");
        i.style &&
            ((i.style.backgroundClip = "content-box"),
                (i.cloneNode(!0).style.backgroundClip = ""),
                (o.clearCloneStyle = "content-box" === i.style.backgroundClip),
                (h.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
                h.appendChild(i),
                r.extend(o, {
                    pixelPosition: function () {
                        return b(), c;
                    },
                    boxSizingReliable: function () {
                        return b(), e;
                    },
                    pixelMarginRight: function () {
                        return b(), f;
                    },
                    reliableMarginLeft: function () {
                        return b(), g;
                    },
                }));
    })();
    function Oa(a, b, c) {
        var d,
            e,
            f,
            g,
            h = a.style;
        return (
            (c = c || Na(a)),
            c &&
            ((g = c.getPropertyValue(b) || c[b]),
                "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)),
                !o.pixelMarginRight() && Ma.test(g) && La.test(b) && ((d = h.width), (e = h.minWidth), (f = h.maxWidth), (h.minWidth = h.maxWidth = h.width = g), (g = c.width), (h.width = d), (h.minWidth = e), (h.maxWidth = f))),
            void 0 !== g ? g + "" : g
        );
    }
    function Pa(a, b) {
        return {
            get: function () {
                return a() ? void delete this.get : (this.get = b).apply(this, arguments);
            },
        };
    }
    var Qa = /^(none|table(?!-c[ea]).+)/,
        Ra = /^--/,
        Sa = { position: "absolute", visibility: "hidden", display: "block" },
        Ta = { letterSpacing: "0", fontWeight: "400" },
        Ua = ["Webkit", "Moz", "ms"],
        Va = d.createElement("div").style;
    function Wa(a) {
        if (a in Va) return a;
        var b = a[0].toUpperCase() + a.slice(1),
            c = Ua.length;
        while (c--) if (((a = Ua[c] + b), a in Va)) return a;
    }
    function Xa(a) {
        var b = r.cssProps[a];
        return b || (b = r.cssProps[a] = Wa(a) || a), b;
    }
    function Ya(a, b, c) {
        var d = ba.exec(b);
        return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
    }
    function Za(a, b, c, d, e) {
        var f,
            g = 0;
        for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2)
            "margin" === c && (g += r.css(a, c + ca[f], !0, e)),
                d
                    ? ("content" === c && (g -= r.css(a, "padding" + ca[f], !0, e)), "margin" !== c && (g -= r.css(a, "border" + ca[f] + "Width", !0, e)))
                    : ((g += r.css(a, "padding" + ca[f], !0, e)), "padding" !== c && (g += r.css(a, "border" + ca[f] + "Width", !0, e)));
        return g;
    }
    function $a(a, b, c) {
        var d,
            e = Na(a),
            f = Oa(a, b, e),
            g = "border-box" === r.css(a, "boxSizing", !1, e);
        return Ma.test(f) ? f : ((d = g && (o.boxSizingReliable() || f === a.style[b])), (f = parseFloat(f) || 0), f + Za(a, b, c || (g ? "border" : "content"), d, e) + "px");
    }
    r.extend({
        cssHooks: {
            opacity: {
                get: function (a, b) {
                    if (b) {
                        var c = Oa(a, "opacity");
                        return "" === c ? "1" : c;
                    }
                },
            },
        },
        cssNumber: { animationIterationCount: !0, columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 },
        cssProps: { float: "cssFloat" },
        style: function (a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e,
                    f,
                    g,
                    h = r.camelCase(b),
                    i = Ra.test(b),
                    j = a.style;
                return (
                    i || (b = Xa(h)),
                    (g = r.cssHooks[b] || r.cssHooks[h]),
                    void 0 === c
                        ? g && "get" in g && void 0 !== (e = g.get(a, !1, d))
                            ? e
                            : j[b]
                        : ((f = typeof c),
                            "string" === f && (e = ba.exec(c)) && e[1] && ((c = fa(a, b, e)), (f = "number")),
                            null != c &&
                            c === c &&
                            ("number" === f && (c += (e && e[3]) || (r.cssNumber[h] ? "" : "px")),
                                o.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"),
                                (g && "set" in g && void 0 === (c = g.set(a, c, d))) || (i ? j.setProperty(b, c) : (j[b] = c))),
                            void 0)
                );
            }
        },
        css: function (a, b, c, d) {
            var e,
                f,
                g,
                h = r.camelCase(b),
                i = Ra.test(b);
            return (
                i || (b = Xa(h)),
                (g = r.cssHooks[b] || r.cssHooks[h]),
                g && "get" in g && (e = g.get(a, !0, c)),
                void 0 === e && (e = Oa(a, b, d)),
                "normal" === e && b in Ta && (e = Ta[b]),
                "" === c || c ? ((f = parseFloat(e)), c === !0 || isFinite(f) ? f || 0 : e) : e
            );
        },
    }),
        r.each(["height", "width"], function (a, b) {
            r.cssHooks[b] = {
                get: function (a, c, d) {
                    if (c)
                        return !Qa.test(r.css(a, "display")) || (a.getClientRects().length && a.getBoundingClientRect().width)
                            ? $a(a, b, d)
                            : ea(a, Sa, function () {
                                return $a(a, b, d);
                            });
                },
                set: function (a, c, d) {
                    var e,
                        f = d && Na(a),
                        g = d && Za(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
                    return g && (e = ba.exec(c)) && "px" !== (e[3] || "px") && ((a.style[b] = c), (c = r.css(a, b))), Ya(a, c, g);
                },
            };
        }),
        (r.cssHooks.marginLeft = Pa(o.reliableMarginLeft, function (a, b) {
            if (b)
                return (
                    (parseFloat(Oa(a, "marginLeft")) ||
                        a.getBoundingClientRect().left -
                        ea(a, { marginLeft: 0 }, function () {
                            return a.getBoundingClientRect().left;
                        })) + "px"
                );
        })),
        r.each({ margin: "", padding: "", border: "Width" }, function (a, b) {
            (r.cssHooks[a + b] = {
                expand: function (c) {
                    for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + ca[d] + b] = f[d] || f[d - 2] || f[0];
                    return e;
                },
            }),
                La.test(a) || (r.cssHooks[a + b].set = Ya);
        }),
        r.fn.extend({
            css: function (a, b) {
                return T(
                    this,
                    function (a, b, c) {
                        var d,
                            e,
                            f = {},
                            g = 0;
                        if (Array.isArray(b)) {
                            for (d = Na(a), e = b.length; g < e; g++) f[b[g]] = r.css(a, b[g], !1, d);
                            return f;
                        }
                        return void 0 !== c ? r.style(a, b, c) : r.css(a, b);
                    },
                    a,
                    b,
                    arguments.length > 1
                );
            },
        });
    function _a(a, b, c, d, e) {
        return new _a.prototype.init(a, b, c, d, e);
    }
    (r.Tween = _a),
        (_a.prototype = {
            constructor: _a,
            init: function (a, b, c, d, e, f) {
                (this.elem = a), (this.prop = c), (this.easing = e || r.easing._default), (this.options = b), (this.start = this.now = this.cur()), (this.end = d), (this.unit = f || (r.cssNumber[c] ? "" : "px"));
            },
            cur: function () {
                var a = _a.propHooks[this.prop];
                return a && a.get ? a.get(this) : _a.propHooks._default.get(this);
            },
            run: function (a) {
                var b,
                    c = _a.propHooks[this.prop];
                return (
                    this.options.duration ? (this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration)) : (this.pos = b = a),
                    (this.now = (this.end - this.start) * b + this.start),
                    this.options.step && this.options.step.call(this.elem, this.now, this),
                    c && c.set ? c.set(this) : _a.propHooks._default.set(this),
                    this
                );
            },
        }),
        (_a.prototype.init.prototype = _a.prototype),
        (_a.propHooks = {
            _default: {
                get: function (a) {
                    var b;
                    return 1 !== a.elem.nodeType || (null != a.elem[a.prop] && null == a.elem.style[a.prop]) ? a.elem[a.prop] : ((b = r.css(a.elem, a.prop, "")), b && "auto" !== b ? b : 0);
                },
                set: function (a) {
                    r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || (null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop]) ? (a.elem[a.prop] = a.now) : r.style(a.elem, a.prop, a.now + a.unit);
                },
            },
        }),
        (_a.propHooks.scrollTop = _a.propHooks.scrollLeft = {
            set: function (a) {
                a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
            },
        }),
        (r.easing = {
            linear: function (a) {
                return a;
            },
            swing: function (a) {
                return 0.5 - Math.cos(a * Math.PI) / 2;
            },
            _default: "swing",
        }),
        (r.fx = _a.prototype.init),
        (r.fx.step = {});
    var ab,
        bb,
        cb = /^(?:toggle|show|hide)$/,
        db = /queueHooks$/;
    function eb() {
        bb && (d.hidden === !1 && a.requestAnimationFrame ? a.requestAnimationFrame(eb) : a.setTimeout(eb, r.fx.interval), r.fx.tick());
    }
    function fb() {
        return (
            a.setTimeout(function () {
                ab = void 0;
            }),
            (ab = r.now())
        );
    }
    function gb(a, b) {
        var c,
            d = 0,
            e = { height: a };
        for (b = b ? 1 : 0; d < 4; d += 2 - b) (c = ca[d]), (e["margin" + c] = e["padding" + c] = a);
        return b && (e.opacity = e.width = a), e;
    }
    function hb(a, b, c) {
        for (var d, e = (kb.tweeners[b] || []).concat(kb.tweeners["*"]), f = 0, g = e.length; f < g; f++) if ((d = e[f].call(c, b, a))) return d;
    }
    function ib(a, b, c) {
        var d,
            e,
            f,
            g,
            h,
            i,
            j,
            k,
            l = "width" in b || "height" in b,
            m = this,
            n = {},
            o = a.style,
            p = a.nodeType && da(a),
            q = W.get(a, "fxshow");
        c.queue ||
            ((g = r._queueHooks(a, "fx")),
                null == g.unqueued &&
                ((g.unqueued = 0),
                    (h = g.empty.fire),
                    (g.empty.fire = function () {
                        g.unqueued || h();
                    })),
                g.unqueued++,
                m.always(function () {
                    m.always(function () {
                        g.unqueued--, r.queue(a, "fx").length || g.empty.fire();
                    });
                }));
        for (d in b)
            if (((e = b[d]), cb.test(e))) {
                if ((delete b[d], (f = f || "toggle" === e), e === (p ? "hide" : "show"))) {
                    if ("show" !== e || !q || void 0 === q[d]) continue;
                    p = !0;
                }
                n[d] = (q && q[d]) || r.style(a, d);
            }
        if (((i = !r.isEmptyObject(b)), i || !r.isEmptyObject(n))) {
            l &&
                1 === a.nodeType &&
                ((c.overflow = [o.overflow, o.overflowX, o.overflowY]),
                    (j = q && q.display),
                    null == j && (j = W.get(a, "display")),
                    (k = r.css(a, "display")),
                    "none" === k && (j ? (k = j) : (ia([a], !0), (j = a.style.display || j), (k = r.css(a, "display")), ia([a]))),
                    ("inline" === k || ("inline-block" === k && null != j)) &&
                    "none" === r.css(a, "float") &&
                    (i ||
                        (m.done(function () {
                            o.display = j;
                        }),
                            null == j && ((k = o.display), (j = "none" === k ? "" : k))),
                        (o.display = "inline-block"))),
                c.overflow &&
                ((o.overflow = "hidden"),
                    m.always(function () {
                        (o.overflow = c.overflow[0]), (o.overflowX = c.overflow[1]), (o.overflowY = c.overflow[2]);
                    })),
                (i = !1);
            for (d in n)
                i ||
                    (q ? "hidden" in q && (p = q.hidden) : (q = W.access(a, "fxshow", { display: j })),
                        f && (q.hidden = !p),
                        p && ia([a], !0),
                        m.done(function () {
                            p || ia([a]), W.remove(a, "fxshow");
                            for (d in n) r.style(a, d, n[d]);
                        })),
                    (i = hb(p ? q[d] : 0, d, m)),
                    d in q || ((q[d] = i.start), p && ((i.end = i.start), (i.start = 0)));
        }
    }
    function jb(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (((d = r.camelCase(c)), (e = b[d]), (f = a[c]), Array.isArray(f) && ((e = f[1]), (f = a[c] = f[0])), c !== d && ((a[d] = f), delete a[c]), (g = r.cssHooks[d]), g && "expand" in g)) {
                (f = g.expand(f)), delete a[d];
                for (c in f) c in a || ((a[c] = f[c]), (b[c] = e));
            } else b[d] = e;
    }
    function kb(a, b, c) {
        var d,
            e,
            f = 0,
            g = kb.prefilters.length,
            h = r.Deferred().always(function () {
                delete i.elem;
            }),
            i = function () {
                if (e) return !1;
                for (var b = ab || fb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (i || h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j]), !1);
            },
            j = h.promise({
                elem: a,
                props: r.extend({}, b),
                opts: r.extend(!0, { specialEasing: {}, easing: r.easing._default }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: ab || fb(),
                duration: c.duration,
                tweens: [],
                createTween: function (b, c) {
                    var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d;
                },
                stop: function (b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; c < d; c++) j.tweens[c].run(1);
                    return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this;
                },
            }),
            k = j.props;
        for (jb(k, j.opts.specialEasing); f < g; f++) if ((d = kb.prefilters[f].call(j, a, k, j.opts))) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;
        return (
            r.map(k, hb, j),
            r.isFunction(j.opts.start) && j.opts.start.call(a, j),
            j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always),
            r.fx.timer(r.extend(i, { elem: a, anim: j, queue: j.opts.queue })),
            j
        );
    }
    (r.Animation = r.extend(kb, {
        tweeners: {
            "*": [
                function (a, b) {
                    var c = this.createTween(a, b);
                    return fa(c.elem, a, ba.exec(b), c), c;
                },
            ],
        },
        tweener: function (a, b) {
            r.isFunction(a) ? ((b = a), (a = ["*"])) : (a = a.match(L));
            for (var c, d = 0, e = a.length; d < e; d++) (c = a[d]), (kb.tweeners[c] = kb.tweeners[c] || []), kb.tweeners[c].unshift(b);
        },
        prefilters: [ib],
        prefilter: function (a, b) {
            b ? kb.prefilters.unshift(a) : kb.prefilters.push(a);
        },
    })),
        (r.speed = function (a, b, c) {
            var d = a && "object" == typeof a ? r.extend({}, a) : { complete: c || (!c && b) || (r.isFunction(a) && a), duration: a, easing: (c && b) || (b && !r.isFunction(b) && b) };
            return (
                r.fx.off ? (d.duration = 0) : "number" != typeof d.duration && (d.duration in r.fx.speeds ? (d.duration = r.fx.speeds[d.duration]) : (d.duration = r.fx.speeds._default)),
                (null != d.queue && d.queue !== !0) || (d.queue = "fx"),
                (d.old = d.complete),
                (d.complete = function () {
                    r.isFunction(d.old) && d.old.call(this), d.queue && r.dequeue(this, d.queue);
                }),
                d
            );
        }),
        r.fn.extend({
            fadeTo: function (a, b, c, d) {
                return this.filter(da).css("opacity", 0).show().end().animate({ opacity: b }, a, c, d);
            },
            animate: function (a, b, c, d) {
                var e = r.isEmptyObject(a),
                    f = r.speed(b, c, d),
                    g = function () {
                        var b = kb(this, r.extend({}, a), f);
                        (e || W.get(this, "finish")) && b.stop(!0);
                    };
                return (g.finish = g), e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g);
            },
            stop: function (a, b, c) {
                var d = function (a) {
                    var b = a.stop;
                    delete a.stop, b(c);
                };
                return (
                    "string" != typeof a && ((c = b), (b = a), (a = void 0)),
                    b && a !== !1 && this.queue(a || "fx", []),
                    this.each(function () {
                        var b = !0,
                            e = null != a && a + "queueHooks",
                            f = r.timers,
                            g = W.get(this);
                        if (e) g[e] && g[e].stop && d(g[e]);
                        else for (e in g) g[e] && g[e].stop && db.test(e) && d(g[e]);
                        for (e = f.length; e--;) f[e].elem !== this || (null != a && f[e].queue !== a) || (f[e].anim.stop(c), (b = !1), f.splice(e, 1));
                        (!b && c) || r.dequeue(this, a);
                    })
                );
            },
            finish: function (a) {
                return (
                    a !== !1 && (a = a || "fx"),
                    this.each(function () {
                        var b,
                            c = W.get(this),
                            d = c[a + "queue"],
                            e = c[a + "queueHooks"],
                            f = r.timers,
                            g = d ? d.length : 0;
                        for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                        for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                        delete c.finish;
                    })
                );
            },
        }),
        r.each(["toggle", "show", "hide"], function (a, b) {
            var c = r.fn[b];
            r.fn[b] = function (a, d, e) {
                return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(gb(b, !0), a, d, e);
            };
        }),
        r.each({ slideDown: gb("show"), slideUp: gb("hide"), slideToggle: gb("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (a, b) {
            r.fn[a] = function (a, c, d) {
                return this.animate(b, a, c, d);
            };
        }),
        (r.timers = []),
        (r.fx.tick = function () {
            var a,
                b = 0,
                c = r.timers;
            for (ab = r.now(); b < c.length; b++) (a = c[b]), a() || c[b] !== a || c.splice(b--, 1);
            c.length || r.fx.stop(), (ab = void 0);
        }),
        (r.fx.timer = function (a) {
            r.timers.push(a), r.fx.start();
        }),
        (r.fx.interval = 13),
        (r.fx.start = function () {
            bb || ((bb = !0), eb());
        }),
        (r.fx.stop = function () {
            bb = null;
        }),
        (r.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
        (r.fn.delay = function (b, c) {
            return (
                (b = r.fx ? r.fx.speeds[b] || b : b),
                (c = c || "fx"),
                this.queue(c, function (c, d) {
                    var e = a.setTimeout(c, b);
                    d.stop = function () {
                        a.clearTimeout(e);
                    };
                })
            );
        }),
        (function () {
            var a = d.createElement("input"),
                b = d.createElement("select"),
                c = b.appendChild(d.createElement("option"));
            (a.type = "checkbox"), (o.checkOn = "" !== a.value), (o.optSelected = c.selected), (a = d.createElement("input")), (a.value = "t"), (a.type = "radio"), (o.radioValue = "t" === a.value);
        })();
    var lb,
        mb = r.expr.attrHandle;
    r.fn.extend({
        attr: function (a, b) {
            return T(this, r.attr, a, b, arguments.length > 1);
        },
        removeAttr: function (a) {
            return this.each(function () {
                r.removeAttr(this, a);
            });
        },
    }),
        r.extend({
            attr: function (a, b, c) {
                var d,
                    e,
                    f = a.nodeType;
                if (3 !== f && 8 !== f && 2 !== f)
                    return "undefined" == typeof a.getAttribute
                        ? r.prop(a, b, c)
                        : ((1 === f && r.isXMLDoc(a)) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? lb : void 0)),
                            void 0 !== c
                                ? null === c
                                    ? void r.removeAttr(a, b)
                                    : e && "set" in e && void 0 !== (d = e.set(a, c, b))
                                        ? d
                                        : (a.setAttribute(b, c + ""), c)
                                : e && "get" in e && null !== (d = e.get(a, b))
                                    ? d
                                    : ((d = r.find.attr(a, b)), null == d ? void 0 : d));
            },
            attrHooks: {
                type: {
                    set: function (a, b) {
                        if (!o.radioValue && "radio" === b && B(a, "input")) {
                            var c = a.value;
                            return a.setAttribute("type", b), c && (a.value = c), b;
                        }
                    },
                },
            },
            removeAttr: function (a, b) {
                var c,
                    d = 0,
                    e = b && b.match(L);
                if (e && 1 === a.nodeType) while ((c = e[d++])) a.removeAttribute(c);
            },
        }),
        (lb = {
            set: function (a, b, c) {
                return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c;
            },
        }),
        r.each(r.expr.match.bool.source.match(/\w+/g), function (a, b) {
            var c = mb[b] || r.find.attr;
            mb[b] = function (a, b, d) {
                var e,
                    f,
                    g = b.toLowerCase();
                return d || ((f = mb[g]), (mb[g] = e), (e = null != c(a, b, d) ? g : null), (mb[g] = f)), e;
            };
        });
    var nb = /^(?:input|select|textarea|button)$/i,
        ob = /^(?:a|area)$/i;
    r.fn.extend({
        prop: function (a, b) {
            return T(this, r.prop, a, b, arguments.length > 1);
        },
        removeProp: function (a) {
            return this.each(function () {
                delete this[r.propFix[a] || a];
            });
        },
    }),
        r.extend({
            prop: function (a, b, c) {
                var d,
                    e,
                    f = a.nodeType;
                if (3 !== f && 8 !== f && 2 !== f)
                    return (
                        (1 === f && r.isXMLDoc(a)) || ((b = r.propFix[b] || b), (e = r.propHooks[b])),
                        void 0 !== c ? (e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a[b] = c)) : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
                    );
            },
            propHooks: {
                tabIndex: {
                    get: function (a) {
                        var b = r.find.attr(a, "tabindex");
                        return b ? parseInt(b, 10) : nb.test(a.nodeName) || (ob.test(a.nodeName) && a.href) ? 0 : -1;
                    },
                },
            },
            propFix: { for: "htmlFor", class: "className" },
        }),
        o.optSelected ||
        (r.propHooks.selected = {
            get: function (a) {
                var b = a.parentNode;
                return b && b.parentNode && b.parentNode.selectedIndex, null;
            },
            set: function (a) {
                var b = a.parentNode;
                b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
            },
        }),
        r.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
            r.propFix[this.toLowerCase()] = this;
        });
    function pb(a) {
        var b = a.match(L) || [];
        return b.join(" ");
    }
    function qb(a) {
        return (a.getAttribute && a.getAttribute("class")) || "";
    }
    r.fn.extend({
        addClass: function (a) {
            var b,
                c,
                d,
                e,
                f,
                g,
                h,
                i = 0;
            if (r.isFunction(a))
                return this.each(function (b) {
                    r(this).addClass(a.call(this, b, qb(this)));
                });
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while ((c = this[i++]))
                    if (((e = qb(c)), (d = 1 === c.nodeType && " " + pb(e) + " "))) {
                        g = 0;
                        while ((f = b[g++])) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
                        (h = pb(d)), e !== h && c.setAttribute("class", h);
                    }
            }
            return this;
        },
        removeClass: function (a) {
            var b,
                c,
                d,
                e,
                f,
                g,
                h,
                i = 0;
            if (r.isFunction(a))
                return this.each(function (b) {
                    r(this).removeClass(a.call(this, b, qb(this)));
                });
            if (!arguments.length) return this.attr("class", "");
            if ("string" == typeof a && a) {
                b = a.match(L) || [];
                while ((c = this[i++]))
                    if (((e = qb(c)), (d = 1 === c.nodeType && " " + pb(e) + " "))) {
                        g = 0;
                        while ((f = b[g++])) while (d.indexOf(" " + f + " ") > -1) d = d.replace(" " + f + " ", " ");
                        (h = pb(d)), e !== h && c.setAttribute("class", h);
                    }
            }
            return this;
        },
        toggleClass: function (a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c
                ? b
                    ? this.addClass(a)
                    : this.removeClass(a)
                : r.isFunction(a)
                    ? this.each(function (c) {
                        r(this).toggleClass(a.call(this, c, qb(this), b), b);
                    })
                    : this.each(function () {
                        var b, d, e, f;
                        if ("string" === c) {
                            (d = 0), (e = r(this)), (f = a.match(L) || []);
                            while ((b = f[d++])) e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
                        } else (void 0 !== a && "boolean" !== c) || ((b = qb(this)), b && W.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : W.get(this, "__className__") || ""));
                    });
        },
        hasClass: function (a) {
            var b,
                c,
                d = 0;
            b = " " + a + " ";
            while ((c = this[d++])) if (1 === c.nodeType && (" " + pb(qb(c)) + " ").indexOf(b) > -1) return !0;
            return !1;
        },
    });
    var rb = /\r/g;
    r.fn.extend({
        val: function (a) {
            var b,
                c,
                d,
                e = this[0];
            {
                if (arguments.length)
                    return (
                        (d = r.isFunction(a)),
                        this.each(function (c) {
                            var e;
                            1 === this.nodeType &&
                                ((e = d ? a.call(this, c, r(this).val()) : a),
                                    null == e
                                        ? (e = "")
                                        : "number" == typeof e
                                            ? (e += "")
                                            : Array.isArray(e) &&
                                            (e = r.map(e, function (a) {
                                                return null == a ? "" : a + "";
                                            })),
                                    (b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()]),
                                    (b && "set" in b && void 0 !== b.set(this, e, "value")) || (this.value = e));
                        })
                    );
                if (e) return (b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()]), b && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : ((c = e.value), "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c);
            }
        },
    }),
        r.extend({
            valHooks: {
                option: {
                    get: function (a) {
                        var b = r.find.attr(a, "value");
                        return null != b ? b : pb(r.text(a));
                    },
                },
                select: {
                    get: function (a) {
                        var b,
                            c,
                            d,
                            e = a.options,
                            f = a.selectedIndex,
                            g = "select-one" === a.type,
                            h = g ? null : [],
                            i = g ? f + 1 : e.length;
                        for (d = f < 0 ? i : g ? f : 0; d < i; d++)
                            if (((c = e[d]), (c.selected || d === f) && !c.disabled && (!c.parentNode.disabled || !B(c.parentNode, "optgroup")))) {
                                if (((b = r(c).val()), g)) return b;
                                h.push(b);
                            }
                        return h;
                    },
                    set: function (a, b) {
                        var c,
                            d,
                            e = a.options,
                            f = r.makeArray(b),
                            g = e.length;
                        while (g--) (d = e[g]), (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);
                        return c || (a.selectedIndex = -1), f;
                    },
                },
            },
        }),
        r.each(["radio", "checkbox"], function () {
            (r.valHooks[this] = {
                set: function (a, b) {
                    if (Array.isArray(b)) return (a.checked = r.inArray(r(a).val(), b) > -1);
                },
            }),
                o.checkOn ||
                (r.valHooks[this].get = function (a) {
                    return null === a.getAttribute("value") ? "on" : a.value;
                });
        });
    var sb = /^(?:focusinfocus|focusoutblur)$/;
    r.extend(r.event, {
        trigger: function (b, c, e, f) {
            var g,
                h,
                i,
                j,
                k,
                m,
                n,
                o = [e || d],
                p = l.call(b, "type") ? b.type : b,
                q = l.call(b, "namespace") ? b.namespace.split(".") : [];
            if (
                ((h = i = e = e || d),
                    3 !== e.nodeType &&
                    8 !== e.nodeType &&
                    !sb.test(p + r.event.triggered) &&
                    (p.indexOf(".") > -1 && ((q = p.split(".")), (p = q.shift()), q.sort()),
                        (k = p.indexOf(":") < 0 && "on" + p),
                        (b = b[r.expando] ? b : new r.Event(p, "object" == typeof b && b)),
                        (b.isTrigger = f ? 2 : 3),
                        (b.namespace = q.join(".")),
                        (b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)") : null),
                        (b.result = void 0),
                        b.target || (b.target = e),
                        (c = null == c ? [b] : r.makeArray(c, [b])),
                        (n = r.event.special[p] || {}),
                        f || !n.trigger || n.trigger.apply(e, c) !== !1))
            ) {
                if (!f && !n.noBubble && !r.isWindow(e)) {
                    for (j = n.delegateType || p, sb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), (i = h);
                    i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a);
                }
                g = 0;
                while ((h = o[g++]) && !b.isPropagationStopped())
                    (b.type = g > 1 ? j : n.bindType || p),
                        (m = (W.get(h, "events") || {})[b.type] && W.get(h, "handle")),
                        m && m.apply(h, c),
                        (m = k && h[k]),
                        m && m.apply && U(h) && ((b.result = m.apply(h, c)), b.result === !1 && b.preventDefault());
                return (
                    (b.type = p),
                    f ||
                    b.isDefaultPrevented() ||
                    (n._default && n._default.apply(o.pop(), c) !== !1) ||
                    !U(e) ||
                    (k && r.isFunction(e[p]) && !r.isWindow(e) && ((i = e[k]), i && (e[k] = null), (r.event.triggered = p), e[p](), (r.event.triggered = void 0), i && (e[k] = i))),
                    b.result
                );
            }
        },
        simulate: function (a, b, c) {
            var d = r.extend(new r.Event(), c, { type: a, isSimulated: !0 });
            r.event.trigger(d, null, b);
        },
    }),
        r.fn.extend({
            trigger: function (a, b) {
                return this.each(function () {
                    r.event.trigger(a, b, this);
                });
            },
            triggerHandler: function (a, b) {
                var c = this[0];
                if (c) return r.event.trigger(a, b, c, !0);
            },
        }),
        r.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (a, b) {
            r.fn[b] = function (a, c) {
                return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
            };
        }),
        r.fn.extend({
            hover: function (a, b) {
                return this.mouseenter(a).mouseleave(b || a);
            },
        }),
        (o.focusin = "onfocusin" in a),
        o.focusin ||
        r.each({ focus: "focusin", blur: "focusout" }, function (a, b) {
            var c = function (a) {
                r.event.simulate(b, a.target, r.event.fix(a));
            };
            r.event.special[b] = {
                setup: function () {
                    var d = this.ownerDocument || this,
                        e = W.access(d, b);
                    e || d.addEventListener(a, c, !0), W.access(d, b, (e || 0) + 1);
                },
                teardown: function () {
                    var d = this.ownerDocument || this,
                        e = W.access(d, b) - 1;
                    e ? W.access(d, b, e) : (d.removeEventListener(a, c, !0), W.remove(d, b));
                },
            };
        });
    var tb = a.location,
        ub = r.now(),
        vb = /\?/;
    r.parseXML = function (b) {
        var c;
        if (!b || "string" != typeof b) return null;
        try {
            c = new a.DOMParser().parseFromString(b, "text/xml");
        } catch (d) {
            c = void 0;
        }
        return (c && !c.getElementsByTagName("parsererror").length) || r.error("Invalid XML: " + b), c;
    };
    var wb = /\[\]$/,
        xb = /\r?\n/g,
        yb = /^(?:submit|button|image|reset|file)$/i,
        zb = /^(?:input|select|textarea|keygen)/i;
    function Ab(a, b, c, d) {
        var e;
        if (Array.isArray(b))
            r.each(b, function (b, e) {
                c || wb.test(a) ? d(a, e) : Ab(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d);
            });
        else if (c || "object" !== r.type(b)) d(a, b);
        else for (e in b) Ab(a + "[" + e + "]", b[e], c, d);
    }
    (r.param = function (a, b) {
        var c,
            d = [],
            e = function (a, b) {
                var c = r.isFunction(b) ? b() : b;
                d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
            };
        if (Array.isArray(a) || (a.jquery && !r.isPlainObject(a)))
            r.each(a, function () {
                e(this.name, this.value);
            });
        else for (c in a) Ab(c, a[c], b, e);
        return d.join("&");
    }),
        r.fn.extend({
            serialize: function () {
                return r.param(this.serializeArray());
            },
            serializeArray: function () {
                return this.map(function () {
                    var a = r.prop(this, "elements");
                    return a ? r.makeArray(a) : this;
                })
                    .filter(function () {
                        var a = this.type;
                        return this.name && !r(this).is(":disabled") && zb.test(this.nodeName) && !yb.test(a) && (this.checked || !ja.test(a));
                    })
                    .map(function (a, b) {
                        var c = r(this).val();
                        return null == c
                            ? null
                            : Array.isArray(c)
                                ? r.map(c, function (a) {
                                    return { name: b.name, value: a.replace(xb, "\r\n") };
                                })
                                : { name: b.name, value: c.replace(xb, "\r\n") };
                    })
                    .get();
            },
        });
    var Bb = /%20/g,
        Cb = /#.*$/,
        Db = /([?&])_=[^&]*/,
        Eb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        Fb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Gb = /^(?:GET|HEAD)$/,
        Hb = /^\/\//,
        Ib = {},
        Jb = {},
        Kb = "*/".concat("*"),
        Lb = d.createElement("a");
    Lb.href = tb.href;
    function Mb(a) {
        return function (b, c) {
            "string" != typeof b && ((c = b), (b = "*"));
            var d,
                e = 0,
                f = b.toLowerCase().match(L) || [];
            if (r.isFunction(c)) while ((d = f[e++])) "+" === d[0] ? ((d = d.slice(1) || "*"), (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c);
        };
    }
    function Nb(a, b, c, d) {
        var e = {},
            f = a === Jb;
        function g(h) {
            var i;
            return (
                (e[h] = !0),
                r.each(a[h] || [], function (a, h) {
                    var j = h(b, c, d);
                    return "string" != typeof j || f || e[j] ? (f ? !(i = j) : void 0) : (b.dataTypes.unshift(j), g(j), !1);
                }),
                i
            );
        }
        return g(b.dataTypes[0]) || (!e["*"] && g("*"));
    }
    function Ob(a, b) {
        var c,
            d,
            e = r.ajaxSettings.flatOptions || {};
        for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
        return d && r.extend(!0, a, d), a;
    }
    function Pb(a, b, c) {
        var d,
            e,
            f,
            g,
            h = a.contents,
            i = a.dataTypes;
        while ("*" === i[0]) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
        if (d)
            for (e in h)
                if (h[e] && h[e].test(d)) {
                    i.unshift(e);
                    break;
                }
        if (i[0] in c) f = i[0];
        else {
            for (e in c) {
                if (!i[0] || a.converters[e + " " + i[0]]) {
                    f = e;
                    break;
                }
                g || (g = e);
            }
            f = f || g;
        }
        if (f) return f !== i[0] && i.unshift(f), c[f];
    }
    function Qb(a, b, c, d) {
        var e,
            f,
            g,
            h,
            i,
            j = {},
            k = a.dataTypes.slice();
        if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        f = k.shift();
        while (f)
            if ((a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), (i = f), (f = k.shift())))
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
                    if (((g = j[i + " " + f] || j["* " + f]), !g))
                        for (e in j)
                            if (((h = e.split(" ")), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]]))) {
                                g === !0 ? (g = j[e]) : j[e] !== !0 && ((f = h[0]), k.unshift(h[1]));
                                break;
                            }
                    if (g !== !0)
                        if (g && a["throws"]) b = g(b);
                        else
                            try {
                                b = g(b);
                            } catch (l) {
                                return { state: "parsererror", error: g ? l : "No conversion from " + i + " to " + f };
                            }
                }
        return { state: "success", data: b };
    }
    r.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: tb.href,
            type: "GET",
            isLocal: Fb.test(tb.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: { "*": Kb, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" },
            contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
            responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" },
            converters: { "* text": String, "text html": !0, "text json": JSON.parse, "text xml": r.parseXML },
            flatOptions: { url: !0, context: !0 },
        },
        ajaxSetup: function (a, b) {
            return b ? Ob(Ob(a, r.ajaxSettings), b) : Ob(r.ajaxSettings, a);
        },
        ajaxPrefilter: Mb(Ib),
        ajaxTransport: Mb(Jb),
        ajax: function (b, c) {
            "object" == typeof b && ((c = b), (b = void 0)), (c = c || {});
            var e,
                f,
                g,
                h,
                i,
                j,
                k,
                l,
                m,
                n,
                o = r.ajaxSetup({}, c),
                p = o.context || o,
                q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,
                s = r.Deferred(),
                t = r.Callbacks("once memory"),
                u = o.statusCode || {},
                v = {},
                w = {},
                x = "canceled",
                y = {
                    readyState: 0,
                    getResponseHeader: function (a) {
                        var b;
                        if (k) {
                            if (!h) {
                                h = {};
                                while ((b = Eb.exec(g))) h[b[1].toLowerCase()] = b[2];
                            }
                            b = h[a.toLowerCase()];
                        }
                        return null == b ? null : b;
                    },
                    getAllResponseHeaders: function () {
                        return k ? g : null;
                    },
                    setRequestHeader: function (a, b) {
                        return null == k && ((a = w[a.toLowerCase()] = w[a.toLowerCase()] || a), (v[a] = b)), this;
                    },
                    overrideMimeType: function (a) {
                        return null == k && (o.mimeType = a), this;
                    },
                    statusCode: function (a) {
                        var b;
                        if (a)
                            if (k) y.always(a[y.status]);
                            else for (b in a) u[b] = [u[b], a[b]];
                        return this;
                    },
                    abort: function (a) {
                        var b = a || x;
                        return e && e.abort(b), A(0, b), this;
                    },
                };
            if (
                (s.promise(y),
                    (o.url = ((b || o.url || tb.href) + "").replace(Hb, tb.protocol + "//")),
                    (o.type = c.method || c.type || o.method || o.type),
                    (o.dataTypes = (o.dataType || "*").toLowerCase().match(L) || [""]),
                    null == o.crossDomain)
            ) {
                j = d.createElement("a");
                try {
                    (j.href = o.url), (j.href = j.href), (o.crossDomain = Lb.protocol + "//" + Lb.host != j.protocol + "//" + j.host);
                } catch (z) {
                    o.crossDomain = !0;
                }
            }
            if ((o.data && o.processData && "string" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Nb(Ib, o, c, y), k)) return y;
            (l = r.event && o.global),
                l && 0 === r.active++ && r.event.trigger("ajaxStart"),
                (o.type = o.type.toUpperCase()),
                (o.hasContent = !Gb.test(o.type)),
                (f = o.url.replace(Cb, "")),
                o.hasContent
                    ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(Bb, "+"))
                    : ((n = o.url.slice(f.length)), o.data && ((f += (vb.test(f) ? "&" : "?") + o.data), delete o.data), o.cache === !1 && ((f = f.replace(Db, "$1")), (n = (vb.test(f) ? "&" : "?") + "_=" + ub++ + n)), (o.url = f + n)),
                o.ifModified && (r.lastModified[f] && y.setRequestHeader("If-Modified-Since", r.lastModified[f]), r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])),
                ((o.data && o.hasContent && o.contentType !== !1) || c.contentType) && y.setRequestHeader("Content-Type", o.contentType),
                y.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Kb + "; q=0.01" : "") : o.accepts["*"]);
            for (m in o.headers) y.setRequestHeader(m, o.headers[m]);
            if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();
            if (((x = "abort"), t.add(o.complete), y.done(o.success), y.fail(o.error), (e = Nb(Jb, o, c, y)))) {
                if (((y.readyState = 1), l && q.trigger("ajaxSend", [y, o]), k)) return y;
                o.async &&
                    o.timeout > 0 &&
                    (i = a.setTimeout(function () {
                        y.abort("timeout");
                    }, o.timeout));
                try {
                    (k = !1), e.send(v, A);
                } catch (z) {
                    if (k) throw z;
                    A(-1, z);
                }
            } else A(-1, "No Transport");
            function A(b, c, d, h) {
                var j,
                    m,
                    n,
                    v,
                    w,
                    x = c;
                k ||
                    ((k = !0),
                        i && a.clearTimeout(i),
                        (e = void 0),
                        (g = h || ""),
                        (y.readyState = b > 0 ? 4 : 0),
                        (j = (b >= 200 && b < 300) || 304 === b),
                        d && (v = Pb(o, y, d)),
                        (v = Qb(o, v, y, j)),
                        j
                            ? (o.ifModified && ((w = y.getResponseHeader("Last-Modified")), w && (r.lastModified[f] = w), (w = y.getResponseHeader("etag")), w && (r.etag[f] = w)),
                                204 === b || "HEAD" === o.type ? (x = "nocontent") : 304 === b ? (x = "notmodified") : ((x = v.state), (m = v.data), (n = v.error), (j = !n)))
                            : ((n = x), (!b && x) || ((x = "error"), b < 0 && (b = 0))),
                        (y.status = b),
                        (y.statusText = (c || x) + ""),
                        j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]),
                        y.statusCode(u),
                        (u = void 0),
                        l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]),
                        t.fireWith(p, [y, x]),
                        l && (q.trigger("ajaxComplete", [y, o]), --r.active || r.event.trigger("ajaxStop")));
            }
            return y;
        },
        getJSON: function (a, b, c) {
            return r.get(a, b, c, "json");
        },
        getScript: function (a, b) {
            return r.get(a, void 0, b, "script");
        },
    }),
        r.each(["get", "post"], function (a, b) {
            r[b] = function (a, c, d, e) {
                return r.isFunction(c) && ((e = e || d), (d = c), (c = void 0)), r.ajax(r.extend({ url: a, type: b, dataType: e, data: c, success: d }, r.isPlainObject(a) && a));
            };
        }),
        (r._evalUrl = function (a) {
            return r.ajax({ url: a, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0 });
        }),
        r.fn.extend({
            wrapAll: function (a) {
                var b;
                return (
                    this[0] &&
                    (r.isFunction(a) && (a = a.call(this[0])),
                        (b = r(a, this[0].ownerDocument).eq(0).clone(!0)),
                        this[0].parentNode && b.insertBefore(this[0]),
                        b
                            .map(function () {
                                var a = this;
                                while (a.firstElementChild) a = a.firstElementChild;
                                return a;
                            })
                            .append(this)),
                    this
                );
            },
            wrapInner: function (a) {
                return r.isFunction(a)
                    ? this.each(function (b) {
                        r(this).wrapInner(a.call(this, b));
                    })
                    : this.each(function () {
                        var b = r(this),
                            c = b.contents();
                        c.length ? c.wrapAll(a) : b.append(a);
                    });
            },
            wrap: function (a) {
                var b = r.isFunction(a);
                return this.each(function (c) {
                    r(this).wrapAll(b ? a.call(this, c) : a);
                });
            },
            unwrap: function (a) {
                return (
                    this.parent(a)
                        .not("body")
                        .each(function () {
                            r(this).replaceWith(this.childNodes);
                        }),
                    this
                );
            },
        }),
        (r.expr.pseudos.hidden = function (a) {
            return !r.expr.pseudos.visible(a);
        }),
        (r.expr.pseudos.visible = function (a) {
            return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
        }),
        (r.ajaxSettings.xhr = function () {
            try {
                return new a.XMLHttpRequest();
            } catch (b) { }
        });
    var Rb = { 0: 200, 1223: 204 },
        Sb = r.ajaxSettings.xhr();
    (o.cors = !!Sb && "withCredentials" in Sb),
        (o.ajax = Sb = !!Sb),
        r.ajaxTransport(function (b) {
            var c, d;
            if (o.cors || (Sb && !b.crossDomain))
                return {
                    send: function (e, f) {
                        var g,
                            h = b.xhr();
                        if ((h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)) for (g in b.xhrFields) h[g] = b.xhrFields[g];
                        b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
                        for (g in e) h.setRequestHeader(g, e[g]);
                        (c = function (a) {
                            return function () {
                                c &&
                                    ((c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null),
                                        "abort" === a
                                            ? h.abort()
                                            : "error" === a
                                                ? "number" != typeof h.status
                                                    ? f(0, "error")
                                                    : f(h.status, h.statusText)
                                                : f(Rb[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? { binary: h.response } : { text: h.responseText }, h.getAllResponseHeaders()));
                            };
                        }),
                            (h.onload = c()),
                            (d = h.onerror = c("error")),
                            void 0 !== h.onabort
                                ? (h.onabort = d)
                                : (h.onreadystatechange = function () {
                                    4 === h.readyState &&
                                        a.setTimeout(function () {
                                            c && d();
                                        });
                                }),
                            (c = c("abort"));
                        try {
                            h.send((b.hasContent && b.data) || null);
                        } catch (i) {
                            if (c) throw i;
                        }
                    },
                    abort: function () {
                        c && c();
                    },
                };
        }),
        r.ajaxPrefilter(function (a) {
            a.crossDomain && (a.contents.script = !1);
        }),
        r.ajaxSetup({
            accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" },
            contents: { script: /\b(?:java|ecma)script\b/ },
            converters: {
                "text script": function (a) {
                    return r.globalEval(a), a;
                },
            },
        }),
        r.ajaxPrefilter("script", function (a) {
            void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET");
        }),
        r.ajaxTransport("script", function (a) {
            if (a.crossDomain) {
                var b, c;
                return {
                    send: function (e, f) {
                        (b = r("<script>")
                            .prop({ charset: a.scriptCharset, src: a.url })
                            .on(
                                "load error",
                                (c = function (a) {
                                    b.remove(), (c = null), a && f("error" === a.type ? 404 : 200, a.type);
                                })
                            )),
                            d.head.appendChild(b[0]);
                    },
                    abort: function () {
                        c && c();
                    },
                };
            }
        });
    var Tb = [],
        Ub = /(=)\?(?=&|$)|\?\?/;
    r.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var a = Tb.pop() || r.expando + "_" + ub++;
            return (this[a] = !0), a;
        },
    }),
        r.ajaxPrefilter("json jsonp", function (b, c, d) {
            var e,
                f,
                g,
                h = b.jsonp !== !1 && (Ub.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Ub.test(b.data) && "data");
            if (h || "jsonp" === b.dataTypes[0])
                return (
                    (e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback),
                    h ? (b[h] = b[h].replace(Ub, "$1" + e)) : b.jsonp !== !1 && (b.url += (vb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e),
                    (b.converters["script json"] = function () {
                        return g || r.error(e + " was not called"), g[0];
                    }),
                    (b.dataTypes[0] = "json"),
                    (f = a[e]),
                    (a[e] = function () {
                        g = arguments;
                    }),
                    d.always(function () {
                        void 0 === f ? r(a).removeProp(e) : (a[e] = f), b[e] && ((b.jsonpCallback = c.jsonpCallback), Tb.push(e)), g && r.isFunction(f) && f(g[0]), (g = f = void 0);
                    }),
                    "script"
                );
        }),
        (o.createHTMLDocument = (function () {
            var a = d.implementation.createHTMLDocument("").body;
            return (a.innerHTML = "<form></form><form></form>"), 2 === a.childNodes.length;
        })()),
        (r.parseHTML = function (a, b, c) {
            if ("string" != typeof a) return [];
            "boolean" == typeof b && ((c = b), (b = !1));
            var e, f, g;
            return (
                b || (o.createHTMLDocument ? ((b = d.implementation.createHTMLDocument("")), (e = b.createElement("base")), (e.href = d.location.href), b.head.appendChild(e)) : (b = d)),
                (f = C.exec(a)),
                (g = !c && []),
                f ? [b.createElement(f[1])] : ((f = qa([a], b, g)), g && g.length && r(g).remove(), r.merge([], f.childNodes))
            );
        }),
        (r.fn.load = function (a, b, c) {
            var d,
                e,
                f,
                g = this,
                h = a.indexOf(" ");
            return (
                h > -1 && ((d = pb(a.slice(h))), (a = a.slice(0, h))),
                r.isFunction(b) ? ((c = b), (b = void 0)) : b && "object" == typeof b && (e = "POST"),
                g.length > 0 &&
                r
                    .ajax({ url: a, type: e || "GET", dataType: "html", data: b })
                    .done(function (a) {
                        (f = arguments), g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a);
                    })
                    .always(
                        c &&
                        function (a, b) {
                            g.each(function () {
                                c.apply(this, f || [a.responseText, b, a]);
                            });
                        }
                    ),
                this
            );
        }),
        r.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
            r.fn[b] = function (a) {
                return this.on(b, a);
            };
        }),
        (r.expr.pseudos.animated = function (a) {
            return r.grep(r.timers, function (b) {
                return a === b.elem;
            }).length;
        }),
        (r.offset = {
            setOffset: function (a, b, c) {
                var d,
                    e,
                    f,
                    g,
                    h,
                    i,
                    j,
                    k = r.css(a, "position"),
                    l = r(a),
                    m = {};
                "static" === k && (a.style.position = "relative"),
                    (h = l.offset()),
                    (f = r.css(a, "top")),
                    (i = r.css(a, "left")),
                    (j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1),
                    j ? ((d = l.position()), (g = d.top), (e = d.left)) : ((g = parseFloat(f) || 0), (e = parseFloat(i) || 0)),
                    r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))),
                    null != b.top && (m.top = b.top - h.top + g),
                    null != b.left && (m.left = b.left - h.left + e),
                    "using" in b ? b.using.call(a, m) : l.css(m);
            },
        }),
        r.fn.extend({
            offset: function (a) {
                if (arguments.length)
                    return void 0 === a
                        ? this
                        : this.each(function (b) {
                            r.offset.setOffset(this, a, b);
                        });
                var b,
                    c,
                    d,
                    e,
                    f = this[0];
                if (f)
                    return f.getClientRects().length
                        ? ((d = f.getBoundingClientRect()), (b = f.ownerDocument), (c = b.documentElement), (e = b.defaultView), { top: d.top + e.pageYOffset - c.clientTop, left: d.left + e.pageXOffset - c.clientLeft })
                        : { top: 0, left: 0 };
            },
            position: function () {
                if (this[0]) {
                    var a,
                        b,
                        c = this[0],
                        d = { top: 0, left: 0 };
                    return (
                        "fixed" === r.css(c, "position")
                            ? (b = c.getBoundingClientRect())
                            : ((a = this.offsetParent()), (b = this.offset()), B(a[0], "html") || (d = a.offset()), (d = { top: d.top + r.css(a[0], "borderTopWidth", !0), left: d.left + r.css(a[0], "borderLeftWidth", !0) })),
                        { top: b.top - d.top - r.css(c, "marginTop", !0), left: b.left - d.left - r.css(c, "marginLeft", !0) }
                    );
                }
            },
            offsetParent: function () {
                return this.map(function () {
                    var a = this.offsetParent;
                    while (a && "static" === r.css(a, "position")) a = a.offsetParent;
                    return a || ra;
                });
            },
        }),
        r.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (a, b) {
            var c = "pageYOffset" === b;
            r.fn[a] = function (d) {
                return T(
                    this,
                    function (a, d, e) {
                        var f;
                        return r.isWindow(a) ? (f = a) : 9 === a.nodeType && (f = a.defaultView), void 0 === e ? (f ? f[b] : a[d]) : void (f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : (a[d] = e));
                    },
                    a,
                    d,
                    arguments.length
                );
            };
        }),
        r.each(["top", "left"], function (a, b) {
            r.cssHooks[b] = Pa(o.pixelPosition, function (a, c) {
                if (c) return (c = Oa(a, b)), Ma.test(c) ? r(a).position()[b] + "px" : c;
            });
        }),
        r.each({ Height: "height", Width: "width" }, function (a, b) {
            r.each({ padding: "inner" + a, content: b, "": "outer" + a }, function (c, d) {
                r.fn[d] = function (e, f) {
                    var g = arguments.length && (c || "boolean" != typeof e),
                        h = c || (e === !0 || f === !0 ? "margin" : "border");
                    return T(
                        this,
                        function (b, c, e) {
                            var f;
                            return r.isWindow(b)
                                ? 0 === d.indexOf("outer")
                                    ? b["inner" + a]
                                    : b.document.documentElement["client" + a]
                                : 9 === b.nodeType
                                    ? ((f = b.documentElement), Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a]))
                                    : void 0 === e
                                        ? r.css(b, c, h)
                                        : r.style(b, c, e, h);
                        },
                        b,
                        g ? e : void 0,
                        g
                    );
                };
            });
        }),
        r.fn.extend({
            bind: function (a, b, c) {
                return this.on(a, null, b, c);
            },
            unbind: function (a, b) {
                return this.off(a, null, b);
            },
            delegate: function (a, b, c, d) {
                return this.on(b, a, c, d);
            },
            undelegate: function (a, b, c) {
                return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
            },
            holdReady: function (a) {
                a ? r.readyWait++ : r.ready(!0);
            },
        }),
        (r.isArray = Array.isArray),
        (r.parseJSON = JSON.parse),
        (r.nodeName = B),
        "function" == typeof define &&
        define.amd &&
        define("jquery", [], function () {
            return r;
        });
    var Vb = a.jQuery,
        Wb = a.$;
    return (
        (r.noConflict = function (b) {
            return a.$ === r && (a.$ = Wb), b && a.jQuery === r && (a.jQuery = Vb), r;
        }),
        b || (a.jQuery = a.$ = r),
        r
    );
});

(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else {
        factory(jQuery);
    }
})(function ($) {
    $.ui = $.ui || {};
    var version = ($.ui.version = "1.12.1");
    var widgetUuid = 0;
    var widgetSlice = Array.prototype.slice;
    $.cleanData = (function (orig) {
        return function (elems) {
            var events, elem, i;
            for (i = 0; (elem = elems[i]) != null; i++) {
                try {
                    events = $._data(elem, "events");
                    if (events && events.remove) {
                        $(elem).triggerHandler("remove");
                    }
                } catch (e) { }
            }
            orig(elems);
        };
    })($.cleanData);
    $.widget = function (name, base, prototype) {
        var existingConstructor, constructor, basePrototype;
        var proxiedPrototype = {};
        var namespace = name.split(".")[0];
        name = name.split(".")[1];
        var fullName = namespace + "-" + name;
        if (!prototype) {
            prototype = base;
            base = $.Widget;
        }
        if ($.isArray(prototype)) {
            prototype = $.extend.apply(null, [{}].concat(prototype));
        }
        $.expr[":"][fullName.toLowerCase()] = function (elem) {
            return !!$.data(elem, fullName);
        };
        $[namespace] = $[namespace] || {};
        existingConstructor = $[namespace][name];
        constructor = $[namespace][name] = function (options, element) {
            if (!this._createWidget) {
                return new constructor(options, element);
            }
            if (arguments.length) {
                this._createWidget(options, element);
            }
        };
        $.extend(constructor, existingConstructor, { version: prototype.version, _proto: $.extend({}, prototype), _childConstructors: [] });
        basePrototype = new base();
        basePrototype.options = $.widget.extend({}, basePrototype.options);
        $.each(prototype, function (prop, value) {
            if (!$.isFunction(value)) {
                proxiedPrototype[prop] = value;
                return;
            }
            proxiedPrototype[prop] = (function () {
                function _super() {
                    return base.prototype[prop].apply(this, arguments);
                }
                function _superApply(args) {
                    return base.prototype[prop].apply(this, args);
                }
                return function () {
                    var __super = this._super;
                    var __superApply = this._superApply;
                    var returnValue;
                    this._super = _super;
                    this._superApply = _superApply;
                    returnValue = value.apply(this, arguments);
                    this._super = __super;
                    this._superApply = __superApply;
                    return returnValue;
                };
            })();
        });
        constructor.prototype = $.widget.extend(basePrototype, { widgetEventPrefix: existingConstructor ? basePrototype.widgetEventPrefix || name : name }, proxiedPrototype, {
            constructor: constructor,
            namespace: namespace,
            widgetName: name,
            widgetFullName: fullName,
        });
        if (existingConstructor) {
            $.each(existingConstructor._childConstructors, function (i, child) {
                var childPrototype = child.prototype;
                $.widget(childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto);
            });
            delete existingConstructor._childConstructors;
        } else {
            base._childConstructors.push(constructor);
        }
        $.widget.bridge(name, constructor);
        return constructor;
    };
    $.widget.extend = function (target) {
        var input = widgetSlice.call(arguments, 1);
        var inputIndex = 0;
        var inputLength = input.length;
        var key;
        var value;
        for (; inputIndex < inputLength; inputIndex++) {
            for (key in input[inputIndex]) {
                value = input[inputIndex][key];
                if (input[inputIndex].hasOwnProperty(key) && value !== undefined) {
                    if ($.isPlainObject(value)) {
                        target[key] = $.isPlainObject(target[key]) ? $.widget.extend({}, target[key], value) : $.widget.extend({}, value);
                    } else {
                        target[key] = value;
                    }
                }
            }
        }
        return target;
    };
    $.widget.bridge = function (name, object) {
        var fullName = object.prototype.widgetFullName || name;
        $.fn[name] = function (options) {
            var isMethodCall = typeof options === "string";
            var args = widgetSlice.call(arguments, 1);
            var returnValue = this;
            if (isMethodCall) {
                if (!this.length && options === "instance") {
                    returnValue = undefined;
                } else {
                    this.each(function () {
                        var methodValue;
                        var instance = $.data(this, fullName);
                        if (options === "instance") {
                            returnValue = instance;
                            return false;
                        }
                        if (!instance) {
                            return $.error("cannot call methods on " + name + " prior to initialization; " + "attempted to call method '" + options + "'");
                        }
                        if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                            return $.error("no such method '" + options + "' for " + name + " widget instance");
                        }
                        methodValue = instance[options].apply(instance, args);
                        if (methodValue !== instance && methodValue !== undefined) {
                            returnValue = methodValue && methodValue.jquery ? returnValue.pushStack(methodValue.get()) : methodValue;
                            return false;
                        }
                    });
                }
            } else {
                if (args.length) {
                    options = $.widget.extend.apply(null, [options].concat(args));
                }
                this.each(function () {
                    var instance = $.data(this, fullName);
                    if (instance) {
                        instance.option(options || {});
                        if (instance._init) {
                            instance._init();
                        }
                    } else {
                        $.data(this, fullName, new object(options, this));
                    }
                });
            }
            return returnValue;
        };
    };
    $.Widget = function () { };
    $.Widget._childConstructors = [];
    $.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: { classes: {}, disabled: false, create: null },
        _createWidget: function (options, element) {
            element = $(element || this.defaultElement || this)[0];
            this.element = $(element);
            this.uuid = widgetUuid++;
            this.eventNamespace = "." + this.widgetName + this.uuid;
            this.bindings = $();
            this.hoverable = $();
            this.focusable = $();
            this.classesElementLookup = {};
            if (element !== this) {
                $.data(element, this.widgetFullName, this);
                this._on(true, this.element, {
                    remove: function (event) {
                        if (event.target === element) {
                            this.destroy();
                        }
                    },
                });
                this.document = $(element.style ? element.ownerDocument : element.document || element);
                this.window = $(this.document[0].defaultView || this.document[0].parentWindow);
            }
            this.options = $.widget.extend({}, this.options, this._getCreateOptions(), options);
            this._create();
            if (this.options.disabled) {
                this._setOptionDisabled(this.options.disabled);
            }
            this._trigger("create", null, this._getCreateEventData());
            this._init();
        },
        _getCreateOptions: function () {
            return {};
        },
        _getCreateEventData: $.noop,
        _create: $.noop,
        _init: $.noop,
        destroy: function () {
            var that = this;
            this._destroy();
            $.each(this.classesElementLookup, function (key, value) {
                that._removeClass(value, key);
            });
            this.element.off(this.eventNamespace).removeData(this.widgetFullName);
            this.widget().off(this.eventNamespace).removeAttr("aria-disabled");
            this.bindings.off(this.eventNamespace);
        },
        _destroy: $.noop,
        widget: function () {
            return this.element;
        },
        option: function (key, value) {
            var options = key;
            var parts;
            var curOption;
            var i;
            if (arguments.length === 0) {
                return $.widget.extend({}, this.options);
            }
            if (typeof key === "string") {
                options = {};
                parts = key.split(".");
                key = parts.shift();
                if (parts.length) {
                    curOption = options[key] = $.widget.extend({}, this.options[key]);
                    for (i = 0; i < parts.length - 1; i++) {
                        curOption[parts[i]] = curOption[parts[i]] || {};
                        curOption = curOption[parts[i]];
                    }
                    key = parts.pop();
                    if (arguments.length === 1) {
                        return curOption[key] === undefined ? null : curOption[key];
                    }
                    curOption[key] = value;
                } else {
                    if (arguments.length === 1) {
                        return this.options[key] === undefined ? null : this.options[key];
                    }
                    options[key] = value;
                }
            }
            this._setOptions(options);
            return this;
        },
        _setOptions: function (options) {
            var key;
            for (key in options) {
                this._setOption(key, options[key]);
            }
            return this;
        },
        _setOption: function (key, value) {
            if (key === "classes") {
                this._setOptionClasses(value);
            }
            this.options[key] = value;
            if (key === "disabled") {
                this._setOptionDisabled(value);
            }
            return this;
        },
        _setOptionClasses: function (value) {
            var classKey, elements, currentElements;
            for (classKey in value) {
                currentElements = this.classesElementLookup[classKey];
                if (value[classKey] === this.options.classes[classKey] || !currentElements || !currentElements.length) {
                    continue;
                }
                elements = $(currentElements.get());
                this._removeClass(currentElements, classKey);
                elements.addClass(this._classes({ element: elements, keys: classKey, classes: value, add: true }));
            }
        },
        _setOptionDisabled: function (value) {
            this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!value);
            if (value) {
                this._removeClass(this.hoverable, null, "ui-state-hover");
                this._removeClass(this.focusable, null, "ui-state-focus");
            }
        },
        enable: function () {
            return this._setOptions({ disabled: false });
        },
        disable: function () {
            return this._setOptions({ disabled: true });
        },
        _classes: function (options) {
            var full = [];
            var that = this;
            options = $.extend({ element: this.element, classes: this.options.classes || {} }, options);
            function processClassString(classes, checkOption) {
                var current, i;
                for (i = 0; i < classes.length; i++) {
                    current = that.classesElementLookup[classes[i]] || $();
                    if (options.add) {
                        current = $($.unique(current.get().concat(options.element.get())));
                    } else {
                        current = $(current.not(options.element).get());
                    }
                    that.classesElementLookup[classes[i]] = current;
                    full.push(classes[i]);
                    if (checkOption && options.classes[classes[i]]) {
                        full.push(options.classes[classes[i]]);
                    }
                }
            }
            this._on(options.element, { remove: "_untrackClassesElement" });
            if (options.keys) {
                processClassString(options.keys.match(/\S+/g) || [], true);
            }
            if (options.extra) {
                processClassString(options.extra.match(/\S+/g) || []);
            }
            return full.join(" ");
        },
        _untrackClassesElement: function (event) {
            var that = this;
            $.each(that.classesElementLookup, function (key, value) {
                if ($.inArray(event.target, value) !== -1) {
                    that.classesElementLookup[key] = $(value.not(event.target).get());
                }
            });
        },
        _removeClass: function (element, keys, extra) {
            return this._toggleClass(element, keys, extra, false);
        },
        _addClass: function (element, keys, extra) {
            return this._toggleClass(element, keys, extra, true);
        },
        _toggleClass: function (element, keys, extra, add) {
            add = typeof add === "boolean" ? add : extra;
            var shift = typeof element === "string" || element === null,
                options = { extra: shift ? keys : extra, keys: shift ? element : keys, element: shift ? this.element : element, add: add };
            options.element.toggleClass(this._classes(options), add);
            return this;
        },
        _on: function (suppressDisabledCheck, element, handlers) {
            var delegateElement;
            var instance = this;
            if (typeof suppressDisabledCheck !== "boolean") {
                handlers = element;
                element = suppressDisabledCheck;
                suppressDisabledCheck = false;
            }
            if (!handlers) {
                handlers = element;
                element = this.element;
                delegateElement = this.widget();
            } else {
                element = delegateElement = $(element);
                this.bindings = this.bindings.add(element);
            }
            $.each(handlers, function (event, handler) {
                function handlerProxy() {
                    if (!suppressDisabledCheck && (instance.options.disabled === true || $(this).hasClass("ui-state-disabled"))) {
                        return;
                    }
                    return (typeof handler === "string" ? instance[handler] : handler).apply(instance, arguments);
                }
                if (typeof handler !== "string") {
                    handlerProxy.guid = handler.guid = handler.guid || handlerProxy.guid || $.guid++;
                }
                var match = event.match(/^([\w:-]*)\s*(.*)$/);
                var eventName = match[1] + instance.eventNamespace;
                var selector = match[2];
                if (selector) {
                    delegateElement.on(eventName, selector, handlerProxy);
                } else {
                    element.on(eventName, handlerProxy);
                }
            });
        },
        _off: function (element, eventName) {
            eventName = (eventName || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace;
            element.off(eventName).off(eventName);
            this.bindings = $(this.bindings.not(element).get());
            this.focusable = $(this.focusable.not(element).get());
            this.hoverable = $(this.hoverable.not(element).get());
        },
        _delay: function (handler, delay) {
            function handlerProxy() {
                return (typeof handler === "string" ? instance[handler] : handler).apply(instance, arguments);
            }
            var instance = this;
            return setTimeout(handlerProxy, delay || 0);
        },
        _hoverable: function (element) {
            this.hoverable = this.hoverable.add(element);
            this._on(element, {
                mouseenter: function (event) {
                    this._addClass($(event.currentTarget), null, "ui-state-hover");
                },
                mouseleave: function (event) {
                    this._removeClass($(event.currentTarget), null, "ui-state-hover");
                },
            });
        },
        _focusable: function (element) {
            this.focusable = this.focusable.add(element);
            this._on(element, {
                focusin: function (event) {
                    this._addClass($(event.currentTarget), null, "ui-state-focus");
                },
                focusout: function (event) {
                    this._removeClass($(event.currentTarget), null, "ui-state-focus");
                },
            });
        },
        _trigger: function (type, event, data) {
            var prop, orig;
            var callback = this.options[type];
            data = data || {};
            event = $.Event(event);
            event.type = (type === this.widgetEventPrefix ? type : this.widgetEventPrefix + type).toLowerCase();
            event.target = this.element[0];
            orig = event.originalEvent;
            if (orig) {
                for (prop in orig) {
                    if (!(prop in event)) {
                        event[prop] = orig[prop];
                    }
                }
            }
            this.element.trigger(event, data);
            return !(($.isFunction(callback) && callback.apply(this.element[0], [event].concat(data)) === false) || event.isDefaultPrevented());
        },
    };
    $.each({ show: "fadeIn", hide: "fadeOut" }, function (method, defaultEffect) {
        $.Widget.prototype["_" + method] = function (element, options, callback) {
            if (typeof options === "string") {
                options = { effect: options };
            }
            var hasOptions;
            var effectName = !options ? method : options === true || typeof options === "number" ? defaultEffect : options.effect || defaultEffect;
            options = options || {};
            if (typeof options === "number") {
                options = { duration: options };
            }
            hasOptions = !$.isEmptyObject(options);
            options.complete = callback;
            if (options.delay) {
                element.delay(options.delay);
            }
            if (hasOptions && $.effects && $.effects.effect[effectName]) {
                element[method](options);
            } else if (effectName !== method && element[effectName]) {
                element[effectName](options.duration, options.easing, callback);
            } else {
                element.queue(function (next) {
                    $(this)[method]();
                    if (callback) {
                        callback.call(element[0]);
                    }
                    next();
                });
            }
        };
    });
    var widget = $.widget;
    var data = $.extend($.expr[":"], {
        data: $.expr.createPseudo
            ? $.expr.createPseudo(function (dataName) {
                return function (elem) {
                    return !!$.data(elem, dataName);
                };
            })
            : function (elem, i, match) {
                return !!$.data(elem, match[3]);
            },
    });
    var scrollParent = ($.fn.scrollParent = function (includeHidden) {
        var position = this.css("position"),
            excludeStaticParent = position === "absolute",
            overflowRegex = includeHidden ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
            scrollParent = this.parents()
                .filter(function () {
                    var parent = $(this);
                    if (excludeStaticParent && parent.css("position") === "static") {
                        return false;
                    }
                    return overflowRegex.test(parent.css("overflow") + parent.css("overflow-y") + parent.css("overflow-x"));
                })
                .eq(0);
        return position === "fixed" || !scrollParent.length ? $(this[0].ownerDocument || document) : scrollParent;
    });
    var ie = ($.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()));
    var mouseHandled = false;
    $(document).on("mouseup", function () {
        mouseHandled = false;
    });
    var widgetsMouse = $.widget("ui.mouse", {
        version: "1.12.1",
        options: { cancel: "input, textarea, button, select, option", distance: 1, delay: 0 },
        _mouseInit: function () {
            var that = this;
            this.element
                .on("mousedown." + this.widgetName, function (event) {
                    return that._mouseDown(event);
                })
                .on("click." + this.widgetName, function (event) {
                    if (true === $.data(event.target, that.widgetName + ".preventClickEvent")) {
                        $.removeData(event.target, that.widgetName + ".preventClickEvent");
                        event.stopImmediatePropagation();
                        return false;
                    }
                });
            this.started = false;
        },
        _mouseDestroy: function () {
            this.element.off("." + this.widgetName);
            if (this._mouseMoveDelegate) {
                this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate);
            }
        },
        _mouseDown: function (event) {
            if (mouseHandled) {
                return;
            }
            this._mouseMoved = false;
            this._mouseStarted && this._mouseUp(event);
            this._mouseDownEvent = event;
            var that = this,
                btnIsLeft = event.which === 1,
                elIsCancel = typeof this.options.cancel === "string" && event.target.nodeName ? $(event.target).closest(this.options.cancel).length : false;
            if (!btnIsLeft || elIsCancel || !this._mouseCapture(event)) {
                return true;
            }
            this.mouseDelayMet = !this.options.delay;
            if (!this.mouseDelayMet) {
                this._mouseDelayTimer = setTimeout(function () {
                    that.mouseDelayMet = true;
                }, this.options.delay);
            }
            if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                this._mouseStarted = this._mouseStart(event) !== false;
                if (!this._mouseStarted) {
                    event.preventDefault();
                    return true;
                }
            }
            if (true === $.data(event.target, this.widgetName + ".preventClickEvent")) {
                $.removeData(event.target, this.widgetName + ".preventClickEvent");
            }
            this._mouseMoveDelegate = function (event) {
                return that._mouseMove(event);
            };
            this._mouseUpDelegate = function (event) {
                return that._mouseUp(event);
            };
            this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate);
            event.preventDefault();
            mouseHandled = true;
            return true;
        },
        _mouseMove: function (event) {
            if (this._mouseMoved) {
                if ($.ui.ie && (!document.documentMode || document.documentMode < 9) && !event.button) {
                    return this._mouseUp(event);
                } else if (!event.which) {
                    if (event.originalEvent.altKey || event.originalEvent.ctrlKey || event.originalEvent.metaKey || event.originalEvent.shiftKey) {
                        this.ignoreMissingWhich = true;
                    } else if (!this.ignoreMissingWhich) {
                        return this._mouseUp(event);
                    }
                }
            }
            if (event.which || event.button) {
                this._mouseMoved = true;
            }
            if (this._mouseStarted) {
                this._mouseDrag(event);
                return event.preventDefault();
            }
            if (this._mouseDistanceMet(event) && this._mouseDelayMet(event)) {
                this._mouseStarted = this._mouseStart(this._mouseDownEvent, event) !== false;
                this._mouseStarted ? this._mouseDrag(event) : this._mouseUp(event);
            }
            return !this._mouseStarted;
        },
        _mouseUp: function (event) {
            this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate);
            if (this._mouseStarted) {
                this._mouseStarted = false;
                if (event.target === this._mouseDownEvent.target) {
                    $.data(event.target, this.widgetName + ".preventClickEvent", true);
                }
                this._mouseStop(event);
            }
            if (this._mouseDelayTimer) {
                clearTimeout(this._mouseDelayTimer);
                delete this._mouseDelayTimer;
            }
            this.ignoreMissingWhich = false;
            mouseHandled = false;
            event.preventDefault();
        },
        _mouseDistanceMet: function (event) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - event.pageX), Math.abs(this._mouseDownEvent.pageY - event.pageY)) >= this.options.distance;
        },
        _mouseDelayMet: function () {
            return this.mouseDelayMet;
        },
        _mouseStart: function () { },
        _mouseDrag: function () { },
        _mouseStop: function () { },
        _mouseCapture: function () {
            return true;
        },
    });
    var plugin = ($.ui.plugin = {
        add: function (module, option, set) {
            var i,
                proto = $.ui[module].prototype;
            for (i in set) {
                proto.plugins[i] = proto.plugins[i] || [];
                proto.plugins[i].push([option, set[i]]);
            }
        },
        call: function (instance, name, args, allowDisconnected) {
            var i,
                set = instance.plugins[name];
            if (!set) {
                return;
            }
            if (!allowDisconnected && (!instance.element[0].parentNode || instance.element[0].parentNode.nodeType === 11)) {
                return;
            }
            for (i = 0; i < set.length; i++) {
                if (instance.options[set[i][0]]) {
                    set[i][1].apply(instance.element, args);
                }
            }
        },
    });
    var safeActiveElement = ($.ui.safeActiveElement = function (document) {
        var activeElement;
        try {
            activeElement = document.activeElement;
        } catch (error) {
            activeElement = document.body;
        }
        if (!activeElement) {
            activeElement = document.body;
        }
        if (!activeElement.nodeName) {
            activeElement = document.body;
        }
        return activeElement;
    });
    var safeBlur = ($.ui.safeBlur = function (element) {
        if (element && element.nodeName.toLowerCase() !== "body") {
            $(element).trigger("blur");
        }
    });
    $.widget("ui.draggable", $.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "drag",
        options: {
            addClasses: true,
            appendTo: "parent",
            axis: false,
            connectToSortable: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            grid: false,
            handle: false,
            helper: "original",
            iframeFix: false,
            opacity: false,
            refreshPositions: false,
            revert: false,
            revertDuration: 500,
            scope: "default",
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: false,
            snapMode: "both",
            snapTolerance: 20,
            stack: false,
            zIndex: false,
            drag: null,
            start: null,
            stop: null,
        },
        _create: function () {
            if (this.options.helper === "original") {
                this._setPositionRelative();
            }
            if (this.options.addClasses) {
                this._addClass("ui-draggable");
            }
            this._setHandleClassName();
            this._mouseInit();
        },
        _setOption: function (key, value) {
            this._super(key, value);
            if (key === "handle") {
                this._removeHandleClassName();
                this._setHandleClassName();
            }
        },
        _destroy: function () {
            if ((this.helper || this.element).is(".ui-draggable-dragging")) {
                this.destroyOnClear = true;
                return;
            }
            this._removeHandleClassName();
            this._mouseDestroy();
        },
        _mouseCapture: function (event) {
            var o = this.options;
            if (this.helper || o.disabled || $(event.target).closest(".ui-resizable-handle").length > 0) {
                return false;
            }
            this.handle = this._getHandle(event);
            if (!this.handle) {
                return false;
            }
            this._blurActiveElement(event);
            this._blockFrames(o.iframeFix === true ? "iframe" : o.iframeFix);
            return true;
        },
        _blockFrames: function (selector) {
            this.iframeBlocks = this.document.find(selector).map(function () {
                var iframe = $(this);
                return $("<div>").css("position", "absolute").appendTo(iframe.parent()).outerWidth(iframe.outerWidth()).outerHeight(iframe.outerHeight()).offset(iframe.offset())[0];
            });
        },
        _unblockFrames: function () {
            if (this.iframeBlocks) {
                this.iframeBlocks.remove();
                delete this.iframeBlocks;
            }
        },
        _blurActiveElement: function (event) {
            var activeElement = $.ui.safeActiveElement(this.document[0]),
                target = $(event.target);
            if (target.closest(activeElement).length) {
                return;
            }
            $.ui.safeBlur(activeElement);
        },
        _mouseStart: function (event) {
            var o = this.options;
            this.helper = this._createHelper(event);
            this._addClass(this.helper, "ui-draggable-dragging");
            this._cacheHelperProportions();
            if ($.ui.ddmanager) {
                $.ui.ddmanager.current = this;
            }
            this._cacheMargins();
            this.cssPosition = this.helper.css("position");
            this.scrollParent = this.helper.scrollParent(true);
            this.offsetParent = this.helper.offsetParent();
            this.hasFixedAncestor =
                this.helper.parents().filter(function () {
                    return $(this).css("position") === "fixed";
                }).length > 0;
            this.positionAbs = this.element.offset();
            this._refreshOffsets(event);
            this.originalPosition = this.position = this._generatePosition(event, false);
            this.originalPageX = event.pageX;
            this.originalPageY = event.pageY;
            o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt);
            this._setContainment();
            if (this._trigger("start", event) === false) {
                this._clear();
                return false;
            }
            this._cacheHelperProportions();
            if ($.ui.ddmanager && !o.dropBehaviour) {
                $.ui.ddmanager.prepareOffsets(this, event);
            }
            this._mouseDrag(event, true);
            if ($.ui.ddmanager) {
                $.ui.ddmanager.dragStart(this, event);
            }
            return true;
        },
        _refreshOffsets: function (event) {
            this.offset = { top: this.positionAbs.top - this.margins.top, left: this.positionAbs.left - this.margins.left, scroll: false, parent: this._getParentOffset(), relative: this._getRelativeOffset() };
            this.offset.click = { left: event.pageX - this.offset.left, top: event.pageY - this.offset.top };
        },
        _mouseDrag: function (event, noPropagation) {
            if (this.hasFixedAncestor) {
                this.offset.parent = this._getParentOffset();
            }
            this.position = this._generatePosition(event, true);
            this.positionAbs = this._convertPositionTo("absolute");
            if (!noPropagation) {
                var ui = this._uiHash();
                if (this._trigger("drag", event, ui) === false) {
                    this._mouseUp(new $.Event("mouseup", event));
                    return false;
                }
                this.position = ui.position;
            }
            this.helper[0].style.left = this.position.left + "px";
            this.helper[0].style.top = this.position.top + "px";
            if ($.ui.ddmanager) {
                $.ui.ddmanager.drag(this, event);
            }
            return false;
        },
        _mouseStop: function (event) {
            var that = this,
                dropped = false;
            if ($.ui.ddmanager && !this.options.dropBehaviour) {
                dropped = $.ui.ddmanager.drop(this, event);
            }
            if (this.dropped) {
                dropped = this.dropped;
                this.dropped = false;
            }
            if ((this.options.revert === "invalid" && !dropped) || (this.options.revert === "valid" && dropped) || this.options.revert === true || ($.isFunction(this.options.revert) && this.options.revert.call(this.element, dropped))) {
                $(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
                    if (that._trigger("stop", event) !== false) {
                        that._clear();
                    }
                });
            } else {
                if (this._trigger("stop", event) !== false) {
                    this._clear();
                }
            }
            return false;
        },
        _mouseUp: function (event) {
            this._unblockFrames();
            if ($.ui.ddmanager) {
                $.ui.ddmanager.dragStop(this, event);
            }
            if (this.handleElement.is(event.target)) {
                this.element.trigger("focus");
            }
            return $.ui.mouse.prototype._mouseUp.call(this, event);
        },
        cancel: function () {
            if (this.helper.is(".ui-draggable-dragging")) {
                this._mouseUp(new $.Event("mouseup", { target: this.element[0] }));
            } else {
                this._clear();
            }
            return this;
        },
        _getHandle: function (event) {
            return this.options.handle ? !!$(event.target).closest(this.element.find(this.options.handle)).length : true;
        },
        _setHandleClassName: function () {
            this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element;
            this._addClass(this.handleElement, "ui-draggable-handle");
        },
        _removeHandleClassName: function () {
            this._removeClass(this.handleElement, "ui-draggable-handle");
        },
        _createHelper: function (event) {
            var o = this.options,
                helperIsFunction = $.isFunction(o.helper),
                helper = helperIsFunction ? $(o.helper.apply(this.element[0], [event])) : o.helper === "clone" ? this.element.clone().removeAttr("id") : this.element;
            if (!helper.parents("body").length) {
                helper.appendTo(o.appendTo === "parent" ? this.element[0].parentNode : o.appendTo);
            }
            if (helperIsFunction && helper[0] === this.element[0]) {
                this._setPositionRelative();
            }
            if (helper[0] !== this.element[0] && !/(fixed|absolute)/.test(helper.css("position"))) {
                helper.css("position", "absolute");
            }
            return helper;
        },
        _setPositionRelative: function () {
            if (!/^(?:r|a|f)/.test(this.element.css("position"))) {
                this.element[0].style.position = "relative";
            }
        },
        _adjustOffsetFromHelper: function (obj) {
            if (typeof obj === "string") {
                obj = obj.split(" ");
            }
            if ($.isArray(obj)) {
                obj = { left: +obj[0], top: +obj[1] || 0 };
            }
            if ("left" in obj) {
                this.offset.click.left = obj.left + this.margins.left;
            }
            if ("right" in obj) {
                this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left;
            }
            if ("top" in obj) {
                this.offset.click.top = obj.top + this.margins.top;
            }
            if ("bottom" in obj) {
                this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top;
            }
        },
        _isRootNode: function (element) {
            return /(html|body)/i.test(element.tagName) || element === this.document[0];
        },
        _getParentOffset: function () {
            var po = this.offsetParent.offset(),
                document = this.document[0];
            if (this.cssPosition === "absolute" && this.scrollParent[0] !== document && $.contains(this.scrollParent[0], this.offsetParent[0])) {
                po.left += this.scrollParent.scrollLeft();
                po.top += this.scrollParent.scrollTop();
            }
            if (this._isRootNode(this.offsetParent[0])) {
                po = { top: 0, left: 0 };
            }
            return { top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) };
        },
        _getRelativeOffset: function () {
            if (this.cssPosition !== "relative") {
                return { top: 0, left: 0 };
            }
            var p = this.element.position(),
                scrollIsRootNode = this._isRootNode(this.scrollParent[0]);
            return {
                top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + (!scrollIsRootNode ? this.scrollParent.scrollTop() : 0),
                left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + (!scrollIsRootNode ? this.scrollParent.scrollLeft() : 0),
            };
        },
        _cacheMargins: function () {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0,
            };
        },
        _cacheHelperProportions: function () {
            this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() };
        },
        _setContainment: function () {
            var isUserScrollable,
                c,
                ce,
                o = this.options,
                document = this.document[0];
            this.relativeContainer = null;
            if (!o.containment) {
                this.containment = null;
                return;
            }
            if (o.containment === "window") {
                this.containment = [
                    $(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left,
                    $(window).scrollTop() - this.offset.relative.top - this.offset.parent.top,
                    $(window).scrollLeft() + $(window).width() - this.helperProportions.width - this.margins.left,
                    $(window).scrollTop() + ($(window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top,
                ];
                return;
            }
            if (o.containment === "document") {
                this.containment = [0, 0, $(document).width() - this.helperProportions.width - this.margins.left, ($(document).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top];
                return;
            }
            if (o.containment.constructor === Array) {
                this.containment = o.containment;
                return;
            }
            if (o.containment === "parent") {
                o.containment = this.helper[0].parentNode;
            }
            c = $(o.containment);
            ce = c[0];
            if (!ce) {
                return;
            }
            isUserScrollable = /(scroll|auto)/.test(c.css("overflow"));
            this.containment = [
                (parseInt(c.css("borderLeftWidth"), 10) || 0) + (parseInt(c.css("paddingLeft"), 10) || 0),
                (parseInt(c.css("borderTopWidth"), 10) || 0) + (parseInt(c.css("paddingTop"), 10) || 0),
                (isUserScrollable ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) -
                (parseInt(c.css("borderRightWidth"), 10) || 0) -
                (parseInt(c.css("paddingRight"), 10) || 0) -
                this.helperProportions.width -
                this.margins.left -
                this.margins.right,
                (isUserScrollable ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) -
                (parseInt(c.css("borderBottomWidth"), 10) || 0) -
                (parseInt(c.css("paddingBottom"), 10) || 0) -
                this.helperProportions.height -
                this.margins.top -
                this.margins.bottom,
            ];
            this.relativeContainer = c;
        },
        _convertPositionTo: function (d, pos) {
            if (!pos) {
                pos = this.position;
            }
            var mod = d === "absolute" ? 1 : -1,
                scrollIsRootNode = this._isRootNode(this.scrollParent[0]);
            return {
                top: pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - (this.cssPosition === "fixed" ? -this.offset.scroll.top : scrollIsRootNode ? 0 : this.offset.scroll.top) * mod,
                left: pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - (this.cssPosition === "fixed" ? -this.offset.scroll.left : scrollIsRootNode ? 0 : this.offset.scroll.left) * mod,
            };
        },
        _generatePosition: function (event, constrainPosition) {
            var containment,
                co,
                top,
                left,
                o = this.options,
                scrollIsRootNode = this._isRootNode(this.scrollParent[0]),
                pageX = event.pageX,
                pageY = event.pageY;
            if (!scrollIsRootNode || !this.offset.scroll) {
                this.offset.scroll = { top: this.scrollParent.scrollTop(), left: this.scrollParent.scrollLeft() };
            }
            if (constrainPosition) {
                if (this.containment) {
                    if (this.relativeContainer) {
                        co = this.relativeContainer.offset();
                        containment = [this.containment[0] + co.left, this.containment[1] + co.top, this.containment[2] + co.left, this.containment[3] + co.top];
                    } else {
                        containment = this.containment;
                    }
                    if (event.pageX - this.offset.click.left < containment[0]) {
                        pageX = containment[0] + this.offset.click.left;
                    }
                    if (event.pageY - this.offset.click.top < containment[1]) {
                        pageY = containment[1] + this.offset.click.top;
                    }
                    if (event.pageX - this.offset.click.left > containment[2]) {
                        pageX = containment[2] + this.offset.click.left;
                    }
                    if (event.pageY - this.offset.click.top > containment[3]) {
                        pageY = containment[3] + this.offset.click.top;
                    }
                }
                if (o.grid) {
                    top = o.grid[1] ? this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1] : this.originalPageY;
                    pageY = containment ? (top - this.offset.click.top >= containment[1] || top - this.offset.click.top > containment[3] ? top : top - this.offset.click.top >= containment[1] ? top - o.grid[1] : top + o.grid[1]) : top;
                    left = o.grid[0] ? this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0] : this.originalPageX;
                    pageX = containment
                        ? left - this.offset.click.left >= containment[0] || left - this.offset.click.left > containment[2]
                            ? left
                            : left - this.offset.click.left >= containment[0]
                                ? left - o.grid[0]
                                : left + o.grid[0]
                        : left;
                }
                if (o.axis === "y") {
                    pageX = this.originalPageX;
                }
                if (o.axis === "x") {
                    pageY = this.originalPageY;
                }
            }
            return {
                top: pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + (this.cssPosition === "fixed" ? -this.offset.scroll.top : scrollIsRootNode ? 0 : this.offset.scroll.top),
                left: pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + (this.cssPosition === "fixed" ? -this.offset.scroll.left : scrollIsRootNode ? 0 : this.offset.scroll.left),
            };
        },
        _clear: function () {
            this._removeClass(this.helper, "ui-draggable-dragging");
            if (this.helper[0] !== this.element[0] && !this.cancelHelperRemoval) {
                this.helper.remove();
            }
            this.helper = null;
            this.cancelHelperRemoval = false;
            if (this.destroyOnClear) {
                this.destroy();
            }
        },
        _trigger: function (type, event, ui) {
            ui = ui || this._uiHash();
            $.ui.plugin.call(this, type, [event, ui, this], true);
            if (/^(drag|start|stop)/.test(type)) {
                this.positionAbs = this._convertPositionTo("absolute");
                ui.offset = this.positionAbs;
            }
            return $.Widget.prototype._trigger.call(this, type, event, ui);
        },
        plugins: {},
        _uiHash: function () {
            return { helper: this.helper, position: this.position, originalPosition: this.originalPosition, offset: this.positionAbs };
        },
    });
    $.ui.plugin.add("draggable", "connectToSortable", {
        start: function (event, ui, draggable) {
            var uiSortable = $.extend({}, ui, { item: draggable.element });
            draggable.sortables = [];
            $(draggable.options.connectToSortable).each(function () {
                var sortable = $(this).sortable("instance");
                if (sortable && !sortable.options.disabled) {
                    draggable.sortables.push(sortable);
                    sortable.refreshPositions();
                    sortable._trigger("activate", event, uiSortable);
                }
            });
        },
        stop: function (event, ui, draggable) {
            var uiSortable = $.extend({}, ui, { item: draggable.element });
            draggable.cancelHelperRemoval = false;
            $.each(draggable.sortables, function () {
                var sortable = this;
                if (sortable.isOver) {
                    sortable.isOver = 0;
                    draggable.cancelHelperRemoval = true;
                    sortable.cancelHelperRemoval = false;
                    sortable._storedCSS = { position: sortable.placeholder.css("position"), top: sortable.placeholder.css("top"), left: sortable.placeholder.css("left") };
                    sortable._mouseStop(event);
                    sortable.options.helper = sortable.options._helper;
                } else {
                    sortable.cancelHelperRemoval = true;
                    sortable._trigger("deactivate", event, uiSortable);
                }
            });
        },
        drag: function (event, ui, draggable) {
            $.each(draggable.sortables, function () {
                var innermostIntersecting = false,
                    sortable = this;
                sortable.positionAbs = draggable.positionAbs;
                sortable.helperProportions = draggable.helperProportions;
                sortable.offset.click = draggable.offset.click;
                if (sortable._intersectsWith(sortable.containerCache)) {
                    innermostIntersecting = true;
                    $.each(draggable.sortables, function () {
                        this.positionAbs = draggable.positionAbs;
                        this.helperProportions = draggable.helperProportions;
                        this.offset.click = draggable.offset.click;
                        if (this !== sortable && this._intersectsWith(this.containerCache) && $.contains(sortable.element[0], this.element[0])) {
                            innermostIntersecting = false;
                        }
                        return innermostIntersecting;
                    });
                }
                if (innermostIntersecting) {
                    if (!sortable.isOver) {
                        sortable.isOver = 1;
                        draggable._parent = ui.helper.parent();
                        sortable.currentItem = ui.helper.appendTo(sortable.element).data("ui-sortable-item", true);
                        sortable.options._helper = sortable.options.helper;
                        sortable.options.helper = function () {
                            return ui.helper[0];
                        };
                        event.target = sortable.currentItem[0];
                        sortable._mouseCapture(event, true);
                        sortable._mouseStart(event, true, true);
                        sortable.offset.click.top = draggable.offset.click.top;
                        sortable.offset.click.left = draggable.offset.click.left;
                        sortable.offset.parent.left -= draggable.offset.parent.left - sortable.offset.parent.left;
                        sortable.offset.parent.top -= draggable.offset.parent.top - sortable.offset.parent.top;
                        draggable._trigger("toSortable", event);
                        draggable.dropped = sortable.element;
                        $.each(draggable.sortables, function () {
                            this.refreshPositions();
                        });
                        draggable.currentItem = draggable.element;
                        sortable.fromOutside = draggable;
                    }
                    if (sortable.currentItem) {
                        sortable._mouseDrag(event);
                        ui.position = sortable.position;
                    }
                } else {
                    if (sortable.isOver) {
                        sortable.isOver = 0;
                        sortable.cancelHelperRemoval = true;
                        sortable.options._revert = sortable.options.revert;
                        sortable.options.revert = false;
                        sortable._trigger("out", event, sortable._uiHash(sortable));
                        sortable._mouseStop(event, true);
                        sortable.options.revert = sortable.options._revert;
                        sortable.options.helper = sortable.options._helper;
                        if (sortable.placeholder) {
                            sortable.placeholder.remove();
                        }
                        ui.helper.appendTo(draggable._parent);
                        draggable._refreshOffsets(event);
                        ui.position = draggable._generatePosition(event, true);
                        draggable._trigger("fromSortable", event);
                        draggable.dropped = false;
                        $.each(draggable.sortables, function () {
                            this.refreshPositions();
                        });
                    }
                }
            });
        },
    });
    $.ui.plugin.add("draggable", "cursor", {
        start: function (event, ui, instance) {
            var t = $("body"),
                o = instance.options;
            if (t.css("cursor")) {
                o._cursor = t.css("cursor");
            }
            t.css("cursor", o.cursor);
        },
        stop: function (event, ui, instance) {
            var o = instance.options;
            if (o._cursor) {
                $("body").css("cursor", o._cursor);
            }
        },
    });
    $.ui.plugin.add("draggable", "opacity", {
        start: function (event, ui, instance) {
            var t = $(ui.helper),
                o = instance.options;
            if (t.css("opacity")) {
                o._opacity = t.css("opacity");
            }
            t.css("opacity", o.opacity);
        },
        stop: function (event, ui, instance) {
            var o = instance.options;
            if (o._opacity) {
                $(ui.helper).css("opacity", o._opacity);
            }
        },
    });
    $.ui.plugin.add("draggable", "scroll", {
        start: function (event, ui, i) {
            if (!i.scrollParentNotHidden) {
                i.scrollParentNotHidden = i.helper.scrollParent(false);
            }
            if (i.scrollParentNotHidden[0] !== i.document[0] && i.scrollParentNotHidden[0].tagName !== "HTML") {
                i.overflowOffset = i.scrollParentNotHidden.offset();
            }
        },
        drag: function (event, ui, i) {
            var o = i.options,
                scrolled = false,
                scrollParent = i.scrollParentNotHidden[0],
                document = i.document[0];
            if (scrollParent !== document && scrollParent.tagName !== "HTML") {
                if (!o.axis || o.axis !== "x") {
                    if (i.overflowOffset.top + scrollParent.offsetHeight - event.pageY < o.scrollSensitivity) {
                        scrollParent.scrollTop = scrolled = scrollParent.scrollTop + o.scrollSpeed;
                    } else if (event.pageY - i.overflowOffset.top < o.scrollSensitivity) {
                        scrollParent.scrollTop = scrolled = scrollParent.scrollTop - o.scrollSpeed;
                    }
                }
                if (!o.axis || o.axis !== "y") {
                    if (i.overflowOffset.left + scrollParent.offsetWidth - event.pageX < o.scrollSensitivity) {
                        scrollParent.scrollLeft = scrolled = scrollParent.scrollLeft + o.scrollSpeed;
                    } else if (event.pageX - i.overflowOffset.left < o.scrollSensitivity) {
                        scrollParent.scrollLeft = scrolled = scrollParent.scrollLeft - o.scrollSpeed;
                    }
                }
            } else {
                if (!o.axis || o.axis !== "x") {
                    if (event.pageY - $(document).scrollTop() < o.scrollSensitivity) {
                        scrolled = $(document).scrollTop($(document).scrollTop() - o.scrollSpeed);
                    } else if ($(window).height() - (event.pageY - $(document).scrollTop()) < o.scrollSensitivity) {
                        scrolled = $(document).scrollTop($(document).scrollTop() + o.scrollSpeed);
                    }
                }
                if (!o.axis || o.axis !== "y") {
                    if (event.pageX - $(document).scrollLeft() < o.scrollSensitivity) {
                        scrolled = $(document).scrollLeft($(document).scrollLeft() - o.scrollSpeed);
                    } else if ($(window).width() - (event.pageX - $(document).scrollLeft()) < o.scrollSensitivity) {
                        scrolled = $(document).scrollLeft($(document).scrollLeft() + o.scrollSpeed);
                    }
                }
            }
            if (scrolled !== false && $.ui.ddmanager && !o.dropBehaviour) {
                $.ui.ddmanager.prepareOffsets(i, event);
            }
        },
    });
    $.ui.plugin.add("draggable", "snap", {
        start: function (event, ui, i) {
            var o = i.options;
            i.snapElements = [];
            $(o.snap.constructor !== String ? o.snap.items || ":data(ui-draggable)" : o.snap).each(function () {
                var $t = $(this),
                    $o = $t.offset();
                if (this !== i.element[0]) {
                    i.snapElements.push({ item: this, width: $t.outerWidth(), height: $t.outerHeight(), top: $o.top, left: $o.left });
                }
            });
        },
        drag: function (event, ui, inst) {
            var ts,
                bs,
                ls,
                rs,
                l,
                r,
                t,
                b,
                i,
                first,
                o = inst.options,
                d = o.snapTolerance,
                x1 = ui.offset.left,
                x2 = x1 + inst.helperProportions.width,
                y1 = ui.offset.top,
                y2 = y1 + inst.helperProportions.height;
            for (i = inst.snapElements.length - 1; i >= 0; i--) {
                l = inst.snapElements[i].left - inst.margins.left;
                r = l + inst.snapElements[i].width;
                t = inst.snapElements[i].top - inst.margins.top;
                b = t + inst.snapElements[i].height;
                if (x2 < l - d || x1 > r + d || y2 < t - d || y1 > b + d || !$.contains(inst.snapElements[i].item.ownerDocument, inst.snapElements[i].item)) {
                    if (inst.snapElements[i].snapping) {
                        inst.options.snap.release && inst.options.snap.release.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item }));
                    }
                    inst.snapElements[i].snapping = false;
                    continue;
                }
                if (o.snapMode !== "inner") {
                    ts = Math.abs(t - y2) <= d;
                    bs = Math.abs(b - y1) <= d;
                    ls = Math.abs(l - x2) <= d;
                    rs = Math.abs(r - x1) <= d;
                    if (ts) {
                        ui.position.top = inst._convertPositionTo("relative", { top: t - inst.helperProportions.height, left: 0 }).top;
                    }
                    if (bs) {
                        ui.position.top = inst._convertPositionTo("relative", { top: b, left: 0 }).top;
                    }
                    if (ls) {
                        ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l - inst.helperProportions.width }).left;
                    }
                    if (rs) {
                        ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r }).left;
                    }
                }
                first = ts || bs || ls || rs;
                if (o.snapMode !== "outer") {
                    ts = Math.abs(t - y1) <= d;
                    bs = Math.abs(b - y2) <= d;
                    ls = Math.abs(l - x1) <= d;
                    rs = Math.abs(r - x2) <= d;
                    if (ts) {
                        ui.position.top = inst._convertPositionTo("relative", { top: t, left: 0 }).top;
                    }
                    if (bs) {
                        ui.position.top = inst._convertPositionTo("relative", { top: b - inst.helperProportions.height, left: 0 }).top;
                    }
                    if (ls) {
                        ui.position.left = inst._convertPositionTo("relative", { top: 0, left: l }).left;
                    }
                    if (rs) {
                        ui.position.left = inst._convertPositionTo("relative", { top: 0, left: r - inst.helperProportions.width }).left;
                    }
                }
                if (!inst.snapElements[i].snapping && (ts || bs || ls || rs || first)) {
                    inst.options.snap.snap && inst.options.snap.snap.call(inst.element, event, $.extend(inst._uiHash(), { snapItem: inst.snapElements[i].item }));
                }
                inst.snapElements[i].snapping = ts || bs || ls || rs || first;
            }
        },
    });
    $.ui.plugin.add("draggable", "stack", {
        start: function (event, ui, instance) {
            var min,
                o = instance.options,
                group = $.makeArray($(o.stack)).sort(function (a, b) {
                    return (parseInt($(a).css("zIndex"), 10) || 0) - (parseInt($(b).css("zIndex"), 10) || 0);
                });
            if (!group.length) {
                return;
            }
            min = parseInt($(group[0]).css("zIndex"), 10) || 0;
            $(group).each(function (i) {
                $(this).css("zIndex", min + i);
            });
            this.css("zIndex", min + group.length);
        },
    });
    $.ui.plugin.add("draggable", "zIndex", {
        start: function (event, ui, instance) {
            var t = $(ui.helper),
                o = instance.options;
            if (t.css("zIndex")) {
                o._zIndex = t.css("zIndex");
            }
            t.css("zIndex", o.zIndex);
        },
        stop: function (event, ui, instance) {
            var o = instance.options;
            if (o._zIndex) {
                $(ui.helper).css("zIndex", o._zIndex);
            }
        },
    });
    var widgetsDraggable = $.ui.draggable;
    $.widget("ui.droppable", {
        version: "1.12.1",
        widgetEventPrefix: "drop",
        options: { accept: "*", addClasses: true, greedy: false, scope: "default", tolerance: "intersect", activate: null, deactivate: null, drop: null, out: null, over: null },
        _create: function () {
            var proportions,
                o = this.options,
                accept = o.accept;
            this.isover = false;
            this.isout = true;
            this.accept = $.isFunction(accept)
                ? accept
                : function (d) {
                    return d.is(accept);
                };
            this.proportions = function () {
                if (arguments.length) {
                    proportions = arguments[0];
                } else {
                    return proportions ? proportions : (proportions = { width: this.element[0].offsetWidth, height: this.element[0].offsetHeight });
                }
            };
            this._addToManager(o.scope);
            o.addClasses && this._addClass("ui-droppable");
        },
        _addToManager: function (scope) {
            $.ui.ddmanager.droppables[scope] = $.ui.ddmanager.droppables[scope] || [];
            $.ui.ddmanager.droppables[scope].push(this);
        },
        _splice: function (drop) {
            var i = 0;
            for (; i < drop.length; i++) {
                if (drop[i] === this) {
                    drop.splice(i, 1);
                }
            }
        },
        _destroy: function () {
            var drop = $.ui.ddmanager.droppables[this.options.scope];
            this._splice(drop);
        },
        _setOption: function (key, value) {
            if (key === "accept") {
                this.accept = $.isFunction(value)
                    ? value
                    : function (d) {
                        return d.is(value);
                    };
            } else if (key === "scope") {
                var drop = $.ui.ddmanager.droppables[this.options.scope];
                this._splice(drop);
                this._addToManager(value);
            }
            this._super(key, value);
        },
        _activate: function (event) {
            var draggable = $.ui.ddmanager.current;
            this._addActiveClass();
            if (draggable) {
                this._trigger("activate", event, this.ui(draggable));
            }
        },
        _deactivate: function (event) {
            var draggable = $.ui.ddmanager.current;
            this._removeActiveClass();
            if (draggable) {
                this._trigger("deactivate", event, this.ui(draggable));
            }
        },
        _over: function (event) {
            var draggable = $.ui.ddmanager.current;
            if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
                return;
            }
            if (this.accept.call(this.element[0], draggable.currentItem || draggable.element)) {
                this._addHoverClass();
                this._trigger("over", event, this.ui(draggable));
            }
        },
        _out: function (event) {
            var draggable = $.ui.ddmanager.current;
            if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
                return;
            }
            if (this.accept.call(this.element[0], draggable.currentItem || draggable.element)) {
                this._removeHoverClass();
                this._trigger("out", event, this.ui(draggable));
            }
        },
        _drop: function (event, custom) {
            var draggable = custom || $.ui.ddmanager.current,
                childrenIntersection = false;
            if (!draggable || (draggable.currentItem || draggable.element)[0] === this.element[0]) {
                return false;
            }
            this.element
                .find(":data(ui-droppable)")
                .not(".ui-draggable-dragging")
                .each(function () {
                    var inst = $(this).droppable("instance");
                    if (
                        inst.options.greedy &&
                        !inst.options.disabled &&
                        inst.options.scope === draggable.options.scope &&
                        inst.accept.call(inst.element[0], draggable.currentItem || draggable.element) &&
                        intersect(draggable, $.extend(inst, { offset: inst.element.offset() }), inst.options.tolerance, event)
                    ) {
                        childrenIntersection = true;
                        return false;
                    }
                });
            if (childrenIntersection) {
                return false;
            }
            if (this.accept.call(this.element[0], draggable.currentItem || draggable.element)) {
                this._removeActiveClass();
                this._removeHoverClass();
                this._trigger("drop", event, this.ui(draggable));
                return this.element;
            }
            return false;
        },
        ui: function (c) {
            return { draggable: c.currentItem || c.element, helper: c.helper, position: c.position, offset: c.positionAbs };
        },
        _addHoverClass: function () {
            this._addClass("ui-droppable-hover");
        },
        _removeHoverClass: function () {
            this._removeClass("ui-droppable-hover");
        },
        _addActiveClass: function () {
            this._addClass("ui-droppable-active");
        },
        _removeActiveClass: function () {
            this._removeClass("ui-droppable-active");
        },
    });
    var intersect = ($.ui.intersect = (function () {
        function isOverAxis(x, reference, size) {
            return x >= reference && x < reference + size;
        }
        return function (draggable, droppable, toleranceMode, event) {
            if (!droppable.offset) {
                return false;
            }
            var x1 = (draggable.positionAbs || draggable.position.absolute).left + draggable.margins.left,
                y1 = (draggable.positionAbs || draggable.position.absolute).top + draggable.margins.top,
                x2 = x1 + draggable.helperProportions.width,
                y2 = y1 + draggable.helperProportions.height,
                l = droppable.offset.left,
                t = droppable.offset.top,
                r = l + droppable.proportions().width,
                b = t + droppable.proportions().height;
            switch (toleranceMode) {
                case "fit":
                    return l <= x1 && x2 <= r && t <= y1 && y2 <= b;
                case "intersect":
                    return l < x1 + draggable.helperProportions.width / 2 && x2 - draggable.helperProportions.width / 2 < r && t < y1 + draggable.helperProportions.height / 2 && y2 - draggable.helperProportions.height / 2 < b;
                case "pointer":
                    return isOverAxis(event.pageY, t, droppable.proportions().height) && isOverAxis(event.pageX, l, droppable.proportions().width);
                case "touch":
                    return ((y1 >= t && y1 <= b) || (y2 >= t && y2 <= b) || (y1 < t && y2 > b)) && ((x1 >= l && x1 <= r) || (x2 >= l && x2 <= r) || (x1 < l && x2 > r));
                default:
                    return false;
            }
        };
    })());
    $.ui.ddmanager = {
        current: null,
        droppables: { default: [] },
        prepareOffsets: function (t, event) {
            var i,
                j,
                m = $.ui.ddmanager.droppables[t.options.scope] || [],
                type = event ? event.type : null,
                list = (t.currentItem || t.element).find(":data(ui-droppable)").addBack();
            droppablesLoop: for (i = 0; i < m.length; i++) {
                if (m[i].options.disabled || (t && !m[i].accept.call(m[i].element[0], t.currentItem || t.element))) {
                    continue;
                }
                for (j = 0; j < list.length; j++) {
                    if (list[j] === m[i].element[0]) {
                        m[i].proportions().height = 0;
                        continue droppablesLoop;
                    }
                }
                m[i].visible = m[i].element.css("display") !== "none";
                if (!m[i].visible) {
                    continue;
                }
                if (type === "mousedown") {
                    m[i]._activate.call(m[i], event);
                }
                m[i].offset = m[i].element.offset();
                m[i].proportions({ width: m[i].element[0].offsetWidth, height: m[i].element[0].offsetHeight });
            }
        },
        drop: function (draggable, event) {
            var dropped = false;
            $.each(($.ui.ddmanager.droppables[draggable.options.scope] || []).slice(), function () {
                if (!this.options) {
                    return;
                }
                if (!this.options.disabled && this.visible && intersect(draggable, this, this.options.tolerance, event)) {
                    dropped = this._drop.call(this, event) || dropped;
                }
                if (!this.options.disabled && this.visible && this.accept.call(this.element[0], draggable.currentItem || draggable.element)) {
                    this.isout = true;
                    this.isover = false;
                    this._deactivate.call(this, event);
                }
            });
            return dropped;
        },
        dragStart: function (draggable, event) {
            draggable.element.parentsUntil("body").on("scroll.droppable", function () {
                if (!draggable.options.refreshPositions) {
                    $.ui.ddmanager.prepareOffsets(draggable, event);
                }
            });
        },
        drag: function (draggable, event) {
            if (draggable.options.refreshPositions) {
                $.ui.ddmanager.prepareOffsets(draggable, event);
            }
            $.each($.ui.ddmanager.droppables[draggable.options.scope] || [], function () {
                if (this.options.disabled || this.greedyChild || !this.visible) {
                    return;
                }
                var parentInstance,
                    scope,
                    parent,
                    intersects = intersect(draggable, this, this.options.tolerance, event),
                    c = !intersects && this.isover ? "isout" : intersects && !this.isover ? "isover" : null;
                if (!c) {
                    return;
                }
                if (this.options.greedy) {
                    scope = this.options.scope;
                    parent = this.element.parents(":data(ui-droppable)").filter(function () {
                        return $(this).droppable("instance").options.scope === scope;
                    });
                    if (parent.length) {
                        parentInstance = $(parent[0]).droppable("instance");
                        parentInstance.greedyChild = c === "isover";
                    }
                }
                if (parentInstance && c === "isover") {
                    parentInstance.isover = false;
                    parentInstance.isout = true;
                    parentInstance._out.call(parentInstance, event);
                }
                this[c] = true;
                this[c === "isout" ? "isover" : "isout"] = false;
                this[c === "isover" ? "_over" : "_out"].call(this, event);
                if (parentInstance && c === "isout") {
                    parentInstance.isout = false;
                    parentInstance.isover = true;
                    parentInstance._over.call(parentInstance, event);
                }
            });
        },
        dragStop: function (draggable, event) {
            draggable.element.parentsUntil("body").off("scroll.droppable");
            if (!draggable.options.refreshPositions) {
                $.ui.ddmanager.prepareOffsets(draggable, event);
            }
        },
    };
    if ($.uiBackCompat !== false) {
        $.widget("ui.droppable", $.ui.droppable, {
            options: { hoverClass: false, activeClass: false },
            _addActiveClass: function () {
                this._super();
                if (this.options.activeClass) {
                    this.element.addClass(this.options.activeClass);
                }
            },
            _removeActiveClass: function () {
                this._super();
                if (this.options.activeClass) {
                    this.element.removeClass(this.options.activeClass);
                }
            },
            _addHoverClass: function () {
                this._super();
                if (this.options.hoverClass) {
                    this.element.addClass(this.options.hoverClass);
                }
            },
            _removeHoverClass: function () {
                this._super();
                if (this.options.hoverClass) {
                    this.element.removeClass(this.options.hoverClass);
                }
            },
        });
    }
    var widgetsDroppable = $.ui.droppable;
    var widgetsSortable = $.widget("ui.sortable", $.ui.mouse, {
        version: "1.12.1",
        widgetEventPrefix: "sort",
        ready: false,
        options: {
            appendTo: "parent",
            axis: false,
            connectWith: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            dropOnEmpty: true,
            forcePlaceholderSize: false,
            forceHelperSize: false,
            grid: false,
            handle: false,
            helper: "original",
            items: "> *",
            opacity: false,
            placeholder: false,
            revert: false,
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1e3,
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null,
        },
        _isOverAxis: function (x, reference, size) {
            return x >= reference && x < reference + size;
        },
        _isFloating: function (item) {
            return /left|right/.test(item.css("float")) || /inline|table-cell/.test(item.css("display"));
        },
        _create: function () {
            this.containerCache = {};
            this._addClass("ui-sortable");
            this.refresh();
            this.offset = this.element.offset();
            this._mouseInit();
            this._setHandleClassName();
            this.ready = true;
        },
        _setOption: function (key, value) {
            this._super(key, value);
            if (key === "handle") {
                this._setHandleClassName();
            }
        },
        _setHandleClassName: function () {
            var that = this;
            this._removeClass(this.element.find(".ui-sortable-handle"), "ui-sortable-handle");
            $.each(this.items, function () {
                that._addClass(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item, "ui-sortable-handle");
            });
        },
        _destroy: function () {
            this._mouseDestroy();
            for (var i = this.items.length - 1; i >= 0; i--) {
                this.items[i].item.removeData(this.widgetName + "-item");
            }
            return this;
        },
        _mouseCapture: function (event, overrideHandle) {
            var currentItem = null,
                validHandle = false,
                that = this;
            if (this.reverting) {
                return false;
            }
            if (this.options.disabled || this.options.type === "static") {
                return false;
            }
            this._refreshItems(event);
            $(event.target)
                .parents()
                .each(function () {
                    if ($.data(this, that.widgetName + "-item") === that) {
                        currentItem = $(this);
                        return false;
                    }
                });
            if ($.data(event.target, that.widgetName + "-item") === that) {
                currentItem = $(event.target);
            }
            if (!currentItem) {
                return false;
            }
            if (this.options.handle && !overrideHandle) {
                $(this.options.handle, currentItem)
                    .find("*")
                    .addBack()
                    .each(function () {
                        if (this === event.target) {
                            validHandle = true;
                        }
                    });
                if (!validHandle) {
                    return false;
                }
            }
            this.currentItem = currentItem;
            this._removeCurrentsFromItems();
            return true;
        },
        _mouseStart: function (event, overrideHandle, noActivation) {
            var i,
                body,
                o = this.options;
            this.currentContainer = this;
            this.refreshPositions();
            this.helper = this._createHelper(event);
            this._cacheHelperProportions();
            this._cacheMargins();
            this.scrollParent = this.helper.scrollParent();
            this.offset = this.currentItem.offset();
            this.offset = { top: this.offset.top - this.margins.top, left: this.offset.left - this.margins.left };
            $.extend(this.offset, { click: { left: event.pageX - this.offset.left, top: event.pageY - this.offset.top }, parent: this._getParentOffset(), relative: this._getRelativeOffset() });
            this.helper.css("position", "absolute");
            this.cssPosition = this.helper.css("position");
            this.originalPosition = this._generatePosition(event);
            this.originalPageX = event.pageX;
            this.originalPageY = event.pageY;
            o.cursorAt && this._adjustOffsetFromHelper(o.cursorAt);
            this.domPosition = { prev: this.currentItem.prev()[0], parent: this.currentItem.parent()[0] };
            if (this.helper[0] !== this.currentItem[0]) {
                this.currentItem.hide();
            }
            this._createPlaceholder();
            if (o.containment) {
                this._setContainment();
            }
            if (o.cursor && o.cursor !== "auto") {
                body = this.document.find("body");
                this.storedCursor = body.css("cursor");
                body.css("cursor", o.cursor);
                this.storedStylesheet = $("<style>*{ cursor: " + o.cursor + " !important; }</style>").appendTo(body);
            }
            if (o.opacity) {
                if (this.helper.css("opacity")) {
                    this._storedOpacity = this.helper.css("opacity");
                }
                this.helper.css("opacity", o.opacity);
            }
            if (o.zIndex) {
                if (this.helper.css("zIndex")) {
                    this._storedZIndex = this.helper.css("zIndex");
                }
                this.helper.css("zIndex", o.zIndex);
            }
            if (this.scrollParent[0] !== this.document[0] && this.scrollParent[0].tagName !== "HTML") {
                this.overflowOffset = this.scrollParent.offset();
            }
            this._trigger("start", event, this._uiHash());
            if (!this._preserveHelperProportions) {
                this._cacheHelperProportions();
            }
            if (!noActivation) {
                for (i = this.containers.length - 1; i >= 0; i--) {
                    this.containers[i]._trigger("activate", event, this._uiHash(this));
                }
            }
            if ($.ui.ddmanager) {
                $.ui.ddmanager.current = this;
            }
            if ($.ui.ddmanager && !o.dropBehaviour) {
                $.ui.ddmanager.prepareOffsets(this, event);
            }
            this.dragging = true;
            this._addClass(this.helper, "ui-sortable-helper");
            this._mouseDrag(event);
            return true;
        },
        _mouseDrag: function (event) {
            var i,
                item,
                itemElement,
                intersection,
                o = this.options,
                scrolled = false;
            this.position = this._generatePosition(event);
            this.positionAbs = this._convertPositionTo("absolute");
            if (!this.lastPositionAbs) {
                this.lastPositionAbs = this.positionAbs;
            }
            if (this.options.scroll) {
                if (this.scrollParent[0] !== this.document[0] && this.scrollParent[0].tagName !== "HTML") {
                    if (this.overflowOffset.top + this.scrollParent[0].offsetHeight - event.pageY < o.scrollSensitivity) {
                        this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop + o.scrollSpeed;
                    } else if (event.pageY - this.overflowOffset.top < o.scrollSensitivity) {
                        this.scrollParent[0].scrollTop = scrolled = this.scrollParent[0].scrollTop - o.scrollSpeed;
                    }
                    if (this.overflowOffset.left + this.scrollParent[0].offsetWidth - event.pageX < o.scrollSensitivity) {
                        this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft + o.scrollSpeed;
                    } else if (event.pageX - this.overflowOffset.left < o.scrollSensitivity) {
                        this.scrollParent[0].scrollLeft = scrolled = this.scrollParent[0].scrollLeft - o.scrollSpeed;
                    }
                } else {
                    if (event.pageY - this.document.scrollTop() < o.scrollSensitivity) {
                        scrolled = this.document.scrollTop(this.document.scrollTop() - o.scrollSpeed);
                    } else if (this.window.height() - (event.pageY - this.document.scrollTop()) < o.scrollSensitivity) {
                        scrolled = this.document.scrollTop(this.document.scrollTop() + o.scrollSpeed);
                    }
                    if (event.pageX - this.document.scrollLeft() < o.scrollSensitivity) {
                        scrolled = this.document.scrollLeft(this.document.scrollLeft() - o.scrollSpeed);
                    } else if (this.window.width() - (event.pageX - this.document.scrollLeft()) < o.scrollSensitivity) {
                        scrolled = this.document.scrollLeft(this.document.scrollLeft() + o.scrollSpeed);
                    }
                }
                if (scrolled !== false && $.ui.ddmanager && !o.dropBehaviour) {
                    $.ui.ddmanager.prepareOffsets(this, event);
                }
            }
            this.positionAbs = this._convertPositionTo("absolute");
            if (!this.options.axis || this.options.axis !== "y") {
                this.helper[0].style.left = this.position.left + "px";
            }
            if (!this.options.axis || this.options.axis !== "x") {
                this.helper[0].style.top = this.position.top + "px";
            }
            for (i = this.items.length - 1; i >= 0; i--) {
                item = this.items[i];
                itemElement = item.item[0];
                intersection = this._intersectsWithPointer(item);
                if (!intersection) {
                    continue;
                }
                if (item.instance !== this.currentContainer) {
                    continue;
                }
                if (
                    itemElement !== this.currentItem[0] &&
                    this.placeholder[intersection === 1 ? "next" : "prev"]()[0] !== itemElement &&
                    !$.contains(this.placeholder[0], itemElement) &&
                    (this.options.type === "semi-dynamic" ? !$.contains(this.element[0], itemElement) : true)
                ) {
                    this.direction = intersection === 1 ? "down" : "up";
                    if (this.options.tolerance === "pointer" || this._intersectsWithSides(item)) {
                        this._rearrange(event, item);
                    } else {
                        break;
                    }
                    this._trigger("change", event, this._uiHash());
                    break;
                }
            }
            this._contactContainers(event);
            if ($.ui.ddmanager) {
                $.ui.ddmanager.drag(this, event);
            }
            this._trigger("sort", event, this._uiHash());
            this.lastPositionAbs = this.positionAbs;
            return false;
        },
        _mouseStop: function (event, noPropagation) {
            if (!event) {
                return;
            }
            if ($.ui.ddmanager && !this.options.dropBehaviour) {
                $.ui.ddmanager.drop(this, event);
            }
            if (this.options.revert) {
                var that = this,
                    cur = this.placeholder.offset(),
                    axis = this.options.axis,
                    animation = {};
                if (!axis || axis === "x") {
                    animation.left = cur.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft);
                }
                if (!axis || axis === "y") {
                    animation.top = cur.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop);
                }
                this.reverting = true;
                $(this.helper).animate(animation, parseInt(this.options.revert, 10) || 500, function () {
                    that._clear(event);
                });
            } else {
                this._clear(event, noPropagation);
            }
            return false;
        },
        cancel: function () {
            if (this.dragging) {
                this._mouseUp(new $.Event("mouseup", { target: null }));
                if (this.options.helper === "original") {
                    this.currentItem.css(this._storedCSS);
                    this._removeClass(this.currentItem, "ui-sortable-helper");
                } else {
                    this.currentItem.show();
                }
                for (var i = this.containers.length - 1; i >= 0; i--) {
                    this.containers[i]._trigger("deactivate", null, this._uiHash(this));
                    if (this.containers[i].containerCache.over) {
                        this.containers[i]._trigger("out", null, this._uiHash(this));
                        this.containers[i].containerCache.over = 0;
                    }
                }
            }
            if (this.placeholder) {
                if (this.placeholder[0].parentNode) {
                    this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
                }
                if (this.options.helper !== "original" && this.helper && this.helper[0].parentNode) {
                    this.helper.remove();
                }
                $.extend(this, { helper: null, dragging: false, reverting: false, _noFinalSort: null });
                if (this.domPosition.prev) {
                    $(this.domPosition.prev).after(this.currentItem);
                } else {
                    $(this.domPosition.parent).prepend(this.currentItem);
                }
            }
            return this;
        },
        serialize: function (o) {
            var items = this._getItemsAsjQuery(o && o.connected),
                str = [];
            o = o || {};
            $(items).each(function () {
                var res = ($(o.item || this).attr(o.attribute || "id") || "").match(o.expression || /(.+)[\-=_](.+)/);
                if (res) {
                    str.push((o.key || res[1] + "[]") + "=" + (o.key && o.expression ? res[1] : res[2]));
                }
            });
            if (!str.length && o.key) {
                str.push(o.key + "=");
            }
            return str.join("&");
        },
        toArray: function (o) {
            var items = this._getItemsAsjQuery(o && o.connected),
                ret = [];
            o = o || {};
            items.each(function () {
                ret.push($(o.item || this).attr(o.attribute || "id") || "");
            });
            return ret;
        },
        _intersectsWith: function (item) {
            var x1 = this.positionAbs.left,
                x2 = x1 + this.helperProportions.width,
                y1 = this.positionAbs.top,
                y2 = y1 + this.helperProportions.height,
                l = item.left,
                r = l + item.width,
                t = item.top,
                b = t + item.height,
                dyClick = this.offset.click.top,
                dxClick = this.offset.click.left,
                isOverElementHeight = this.options.axis === "x" || (y1 + dyClick > t && y1 + dyClick < b),
                isOverElementWidth = this.options.axis === "y" || (x1 + dxClick > l && x1 + dxClick < r),
                isOverElement = isOverElementHeight && isOverElementWidth;
            if (
                this.options.tolerance === "pointer" ||
                this.options.forcePointerForContainers ||
                (this.options.tolerance !== "pointer" && this.helperProportions[this.floating ? "width" : "height"] > item[this.floating ? "width" : "height"])
            ) {
                return isOverElement;
            } else {
                return l < x1 + this.helperProportions.width / 2 && x2 - this.helperProportions.width / 2 < r && t < y1 + this.helperProportions.height / 2 && y2 - this.helperProportions.height / 2 < b;
            }
        },
        _intersectsWithPointer: function (item) {
            var verticalDirection,
                horizontalDirection,
                isOverElementHeight = this.options.axis === "x" || this._isOverAxis(this.positionAbs.top + this.offset.click.top, item.top, item.height),
                isOverElementWidth = this.options.axis === "y" || this._isOverAxis(this.positionAbs.left + this.offset.click.left, item.left, item.width),
                isOverElement = isOverElementHeight && isOverElementWidth;
            if (!isOverElement) {
                return false;
            }
            verticalDirection = this._getDragVerticalDirection();
            horizontalDirection = this._getDragHorizontalDirection();
            return this.floating ? (horizontalDirection === "right" || verticalDirection === "down" ? 2 : 1) : verticalDirection && (verticalDirection === "down" ? 2 : 1);
        },
        _intersectsWithSides: function (item) {
            var isOverBottomHalf = this._isOverAxis(this.positionAbs.top + this.offset.click.top, item.top + item.height / 2, item.height),
                isOverRightHalf = this._isOverAxis(this.positionAbs.left + this.offset.click.left, item.left + item.width / 2, item.width),
                verticalDirection = this._getDragVerticalDirection(),
                horizontalDirection = this._getDragHorizontalDirection();
            if (this.floating && horizontalDirection) {
                return (horizontalDirection === "right" && isOverRightHalf) || (horizontalDirection === "left" && !isOverRightHalf);
            } else {
                return verticalDirection && ((verticalDirection === "down" && isOverBottomHalf) || (verticalDirection === "up" && !isOverBottomHalf));
            }
        },
        _getDragVerticalDirection: function () {
            var delta = this.positionAbs.top - this.lastPositionAbs.top;
            return delta !== 0 && (delta > 0 ? "down" : "up");
        },
        _getDragHorizontalDirection: function () {
            var delta = this.positionAbs.left - this.lastPositionAbs.left;
            return delta !== 0 && (delta > 0 ? "right" : "left");
        },
        refresh: function (event) {
            this._refreshItems(event);
            this._setHandleClassName();
            this.refreshPositions();
            return this;
        },
        _connectWith: function () {
            var options = this.options;
            return options.connectWith.constructor === String ? [options.connectWith] : options.connectWith;
        },
        _getItemsAsjQuery: function (connected) {
            var i,
                j,
                cur,
                inst,
                items = [],
                queries = [],
                connectWith = this._connectWith();
            if (connectWith && connected) {
                for (i = connectWith.length - 1; i >= 0; i--) {
                    cur = $(connectWith[i], this.document[0]);
                    for (j = cur.length - 1; j >= 0; j--) {
                        inst = $.data(cur[j], this.widgetFullName);
                        if (inst && inst !== this && !inst.options.disabled) {
                            queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element) : $(inst.options.items, inst.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), inst]);
                        }
                    }
                }
            }
            queries.push([
                $.isFunction(this.options.items)
                    ? this.options.items.call(this.element, null, { options: this.options, item: this.currentItem })
                    : $(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),
                this,
            ]);
            function addItems() {
                items.push(this);
            }
            for (i = queries.length - 1; i >= 0; i--) {
                queries[i][0].each(addItems);
            }
            return $(items);
        },
        _removeCurrentsFromItems: function () {
            var list = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = $.grep(this.items, function (item) {
                for (var j = 0; j < list.length; j++) {
                    if (list[j] === item.item[0]) {
                        return false;
                    }
                }
                return true;
            });
        },
        _refreshItems: function (event) {
            this.items = [];
            this.containers = [this];
            var i,
                j,
                cur,
                inst,
                targetData,
                _queries,
                item,
                queriesLength,
                items = this.items,
                queries = [[$.isFunction(this.options.items) ? this.options.items.call(this.element[0], event, { item: this.currentItem }) : $(this.options.items, this.element), this]],
                connectWith = this._connectWith();
            if (connectWith && this.ready) {
                for (i = connectWith.length - 1; i >= 0; i--) {
                    cur = $(connectWith[i], this.document[0]);
                    for (j = cur.length - 1; j >= 0; j--) {
                        inst = $.data(cur[j], this.widgetFullName);
                        if (inst && inst !== this && !inst.options.disabled) {
                            queries.push([$.isFunction(inst.options.items) ? inst.options.items.call(inst.element[0], event, { item: this.currentItem }) : $(inst.options.items, inst.element), inst]);
                            this.containers.push(inst);
                        }
                    }
                }
            }
            for (i = queries.length - 1; i >= 0; i--) {
                targetData = queries[i][1];
                _queries = queries[i][0];
                for (j = 0, queriesLength = _queries.length; j < queriesLength; j++) {
                    item = $(_queries[j]);
                    item.data(this.widgetName + "-item", targetData);
                    items.push({ item: item, instance: targetData, width: 0, height: 0, left: 0, top: 0 });
                }
            }
        },
        refreshPositions: function (fast) {
            this.floating = this.items.length ? this.options.axis === "x" || this._isFloating(this.items[0].item) : false;
            if (this.offsetParent && this.helper) {
                this.offset.parent = this._getParentOffset();
            }
            var i, item, t, p;
            for (i = this.items.length - 1; i >= 0; i--) {
                item = this.items[i];
                if (item.instance !== this.currentContainer && this.currentContainer && item.item[0] !== this.currentItem[0]) {
                    continue;
                }
                t = this.options.toleranceElement ? $(this.options.toleranceElement, item.item) : item.item;
                if (!fast) {
                    item.width = t.outerWidth();
                    item.height = t.outerHeight();
                }
                p = t.offset();
                item.left = p.left;
                item.top = p.top;
            }
            if (this.options.custom && this.options.custom.refreshContainers) {
                this.options.custom.refreshContainers.call(this);
            } else {
                for (i = this.containers.length - 1; i >= 0; i--) {
                    p = this.containers[i].element.offset();
                    this.containers[i].containerCache.left = p.left;
                    this.containers[i].containerCache.top = p.top;
                    this.containers[i].containerCache.width = this.containers[i].element.outerWidth();
                    this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
                }
            }
            return this;
        },
        _createPlaceholder: function (that) {
            that = that || this;
            var className,
                o = that.options;
            if (!o.placeholder || o.placeholder.constructor === String) {
                className = o.placeholder;
                o.placeholder = {
                    element: function () {
                        var nodeName = that.currentItem[0].nodeName.toLowerCase(),
                            element = $("<" + nodeName + ">", that.document[0]);
                        that._addClass(element, "ui-sortable-placeholder", className || that.currentItem[0].className)._removeClass(element, "ui-sortable-helper");
                        if (nodeName === "tbody") {
                            that._createTrPlaceholder(that.currentItem.find("tr").eq(0), $("<tr>", that.document[0]).appendTo(element));
                        } else if (nodeName === "tr") {
                            that._createTrPlaceholder(that.currentItem, element);
                        } else if (nodeName === "img") {
                            element.attr("src", that.currentItem.attr("src"));
                        }
                        if (!className) {
                            element.css("visibility", "hidden");
                        }
                        return element;
                    },
                    update: function (container, p) {
                        if (className && !o.forcePlaceholderSize) {
                            return;
                        }
                        if (!p.height()) {
                            p.height(that.currentItem.innerHeight() - parseInt(that.currentItem.css("paddingTop") || 0, 10) - parseInt(that.currentItem.css("paddingBottom") || 0, 10));
                        }
                        if (!p.width()) {
                            p.width(that.currentItem.innerWidth() - parseInt(that.currentItem.css("paddingLeft") || 0, 10) - parseInt(that.currentItem.css("paddingRight") || 0, 10));
                        }
                    },
                };
            }
            that.placeholder = $(o.placeholder.element.call(that.element, that.currentItem));
            that.currentItem.after(that.placeholder);
            o.placeholder.update(that, that.placeholder);
        },
        _createTrPlaceholder: function (sourceTr, targetTr) {
            var that = this;
            sourceTr.children().each(function () {
                $("<td>&#160;</td>", that.document[0])
                    .attr("colspan", $(this).attr("colspan") || 1)
                    .appendTo(targetTr);
            });
        },
        _contactContainers: function (event) {
            var i,
                j,
                dist,
                itemWithLeastDistance,
                posProperty,
                sizeProperty,
                cur,
                nearBottom,
                floating,
                axis,
                innermostContainer = null,
                innermostIndex = null;
            for (i = this.containers.length - 1; i >= 0; i--) {
                if ($.contains(this.currentItem[0], this.containers[i].element[0])) {
                    continue;
                }
                if (this._intersectsWith(this.containers[i].containerCache)) {
                    if (innermostContainer && $.contains(this.containers[i].element[0], innermostContainer.element[0])) {
                        continue;
                    }
                    innermostContainer = this.containers[i];
                    innermostIndex = i;
                } else {
                    if (this.containers[i].containerCache.over) {
                        this.containers[i]._trigger("out", event, this._uiHash(this));
                        this.containers[i].containerCache.over = 0;
                    }
                }
            }
            if (!innermostContainer) {
                return;
            }
            if (this.containers.length === 1) {
                if (!this.containers[innermostIndex].containerCache.over) {
                    this.containers[innermostIndex]._trigger("over", event, this._uiHash(this));
                    this.containers[innermostIndex].containerCache.over = 1;
                }
            } else {
                dist = 1e4;
                itemWithLeastDistance = null;
                floating = innermostContainer.floating || this._isFloating(this.currentItem);
                posProperty = floating ? "left" : "top";
                sizeProperty = floating ? "width" : "height";
                axis = floating ? "pageX" : "pageY";
                for (j = this.items.length - 1; j >= 0; j--) {
                    if (!$.contains(this.containers[innermostIndex].element[0], this.items[j].item[0])) {
                        continue;
                    }
                    if (this.items[j].item[0] === this.currentItem[0]) {
                        continue;
                    }
                    cur = this.items[j].item.offset()[posProperty];
                    nearBottom = false;
                    if (event[axis] - cur > this.items[j][sizeProperty] / 2) {
                        nearBottom = true;
                    }
                    if (Math.abs(event[axis] - cur) < dist) {
                        dist = Math.abs(event[axis] - cur);
                        itemWithLeastDistance = this.items[j];
                        this.direction = nearBottom ? "up" : "down";
                    }
                }
                if (!itemWithLeastDistance && !this.options.dropOnEmpty) {
                    return;
                }
                if (this.currentContainer === this.containers[innermostIndex]) {
                    if (!this.currentContainer.containerCache.over) {
                        this.containers[innermostIndex]._trigger("over", event, this._uiHash());
                        this.currentContainer.containerCache.over = 1;
                    }
                    return;
                }
                itemWithLeastDistance ? this._rearrange(event, itemWithLeastDistance, null, true) : this._rearrange(event, null, this.containers[innermostIndex].element, true);
                this._trigger("change", event, this._uiHash());
                this.containers[innermostIndex]._trigger("change", event, this._uiHash(this));
                this.currentContainer = this.containers[innermostIndex];
                this.options.placeholder.update(this.currentContainer, this.placeholder);
                this.containers[innermostIndex]._trigger("over", event, this._uiHash(this));
                this.containers[innermostIndex].containerCache.over = 1;
            }
        },
        _createHelper: function (event) {
            var o = this.options,
                helper = $.isFunction(o.helper) ? $(o.helper.apply(this.element[0], [event, this.currentItem])) : o.helper === "clone" ? this.currentItem.clone() : this.currentItem;
            if (!helper.parents("body").length) {
                $(o.appendTo !== "parent" ? o.appendTo : this.currentItem[0].parentNode)[0].appendChild(helper[0]);
            }
            if (helper[0] === this.currentItem[0]) {
                this._storedCSS = { width: this.currentItem[0].style.width, height: this.currentItem[0].style.height, position: this.currentItem.css("position"), top: this.currentItem.css("top"), left: this.currentItem.css("left") };
            }
            if (!helper[0].style.width || o.forceHelperSize) {
                helper.width(this.currentItem.width());
            }
            if (!helper[0].style.height || o.forceHelperSize) {
                helper.height(this.currentItem.height());
            }
            return helper;
        },
        _adjustOffsetFromHelper: function (obj) {
            if (typeof obj === "string") {
                obj = obj.split(" ");
            }
            if ($.isArray(obj)) {
                obj = { left: +obj[0], top: +obj[1] || 0 };
            }
            if ("left" in obj) {
                this.offset.click.left = obj.left + this.margins.left;
            }
            if ("right" in obj) {
                this.offset.click.left = this.helperProportions.width - obj.right + this.margins.left;
            }
            if ("top" in obj) {
                this.offset.click.top = obj.top + this.margins.top;
            }
            if ("bottom" in obj) {
                this.offset.click.top = this.helperProportions.height - obj.bottom + this.margins.top;
            }
        },
        _getParentOffset: function () {
            this.offsetParent = this.helper.offsetParent();
            var po = this.offsetParent.offset();
            if (this.cssPosition === "absolute" && this.scrollParent[0] !== this.document[0] && $.contains(this.scrollParent[0], this.offsetParent[0])) {
                po.left += this.scrollParent.scrollLeft();
                po.top += this.scrollParent.scrollTop();
            }
            if (this.offsetParent[0] === this.document[0].body || (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() === "html" && $.ui.ie)) {
                po = { top: 0, left: 0 };
            }
            return { top: po.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left: po.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0) };
        },
        _getRelativeOffset: function () {
            if (this.cssPosition === "relative") {
                var p = this.currentItem.position();
                return { top: p.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(), left: p.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft() };
            } else {
                return { top: 0, left: 0 };
            }
        },
        _cacheMargins: function () {
            this.margins = { left: parseInt(this.currentItem.css("marginLeft"), 10) || 0, top: parseInt(this.currentItem.css("marginTop"), 10) || 0 };
        },
        _cacheHelperProportions: function () {
            this.helperProportions = { width: this.helper.outerWidth(), height: this.helper.outerHeight() };
        },
        _setContainment: function () {
            var ce,
                co,
                over,
                o = this.options;
            if (o.containment === "parent") {
                o.containment = this.helper[0].parentNode;
            }
            if (o.containment === "document" || o.containment === "window") {
                this.containment = [
                    0 - this.offset.relative.left - this.offset.parent.left,
                    0 - this.offset.relative.top - this.offset.parent.top,
                    o.containment === "document" ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left,
                    (o.containment === "document" ? this.document.height() || document.body.parentNode.scrollHeight : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top,
                ];
            }
            if (!/^(document|window|parent)$/.test(o.containment)) {
                ce = $(o.containment)[0];
                co = $(o.containment).offset();
                over = $(ce).css("overflow") !== "hidden";
                this.containment = [
                    co.left + (parseInt($(ce).css("borderLeftWidth"), 10) || 0) + (parseInt($(ce).css("paddingLeft"), 10) || 0) - this.margins.left,
                    co.top + (parseInt($(ce).css("borderTopWidth"), 10) || 0) + (parseInt($(ce).css("paddingTop"), 10) || 0) - this.margins.top,
                    co.left +
                    (over ? Math.max(ce.scrollWidth, ce.offsetWidth) : ce.offsetWidth) -
                    (parseInt($(ce).css("borderLeftWidth"), 10) || 0) -
                    (parseInt($(ce).css("paddingRight"), 10) || 0) -
                    this.helperProportions.width -
                    this.margins.left,
                    co.top +
                    (over ? Math.max(ce.scrollHeight, ce.offsetHeight) : ce.offsetHeight) -
                    (parseInt($(ce).css("borderTopWidth"), 10) || 0) -
                    (parseInt($(ce).css("paddingBottom"), 10) || 0) -
                    this.helperProportions.height -
                    this.margins.top,
                ];
            }
        },
        _convertPositionTo: function (d, pos) {
            if (!pos) {
                pos = this.position;
            }
            var mod = d === "absolute" ? 1 : -1,
                scroll = this.cssPosition === "absolute" && !(this.scrollParent[0] !== this.document[0] && $.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent,
                scrollIsRootNode = /(html|body)/i.test(scroll[0].tagName);
            return {
                top: pos.top + this.offset.relative.top * mod + this.offset.parent.top * mod - (this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : scrollIsRootNode ? 0 : scroll.scrollTop()) * mod,
                left: pos.left + this.offset.relative.left * mod + this.offset.parent.left * mod - (this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()) * mod,
            };
        },
        _generatePosition: function (event) {
            var top,
                left,
                o = this.options,
                pageX = event.pageX,
                pageY = event.pageY,
                scroll = this.cssPosition === "absolute" && !(this.scrollParent[0] !== this.document[0] && $.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent,
                scrollIsRootNode = /(html|body)/i.test(scroll[0].tagName);
            if (this.cssPosition === "relative" && !(this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0])) {
                this.offset.relative = this._getRelativeOffset();
            }
            if (this.originalPosition) {
                if (this.containment) {
                    if (event.pageX - this.offset.click.left < this.containment[0]) {
                        pageX = this.containment[0] + this.offset.click.left;
                    }
                    if (event.pageY - this.offset.click.top < this.containment[1]) {
                        pageY = this.containment[1] + this.offset.click.top;
                    }
                    if (event.pageX - this.offset.click.left > this.containment[2]) {
                        pageX = this.containment[2] + this.offset.click.left;
                    }
                    if (event.pageY - this.offset.click.top > this.containment[3]) {
                        pageY = this.containment[3] + this.offset.click.top;
                    }
                }
                if (o.grid) {
                    top = this.originalPageY + Math.round((pageY - this.originalPageY) / o.grid[1]) * o.grid[1];
                    pageY = this.containment
                        ? top - this.offset.click.top >= this.containment[1] && top - this.offset.click.top <= this.containment[3]
                            ? top
                            : top - this.offset.click.top >= this.containment[1]
                                ? top - o.grid[1]
                                : top + o.grid[1]
                        : top;
                    left = this.originalPageX + Math.round((pageX - this.originalPageX) / o.grid[0]) * o.grid[0];
                    pageX = this.containment
                        ? left - this.offset.click.left >= this.containment[0] && left - this.offset.click.left <= this.containment[2]
                            ? left
                            : left - this.offset.click.left >= this.containment[0]
                                ? left - o.grid[0]
                                : left + o.grid[0]
                        : left;
                }
            }
            return {
                top: pageY - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + (this.cssPosition === "fixed" ? -this.scrollParent.scrollTop() : scrollIsRootNode ? 0 : scroll.scrollTop()),
                left: pageX - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + (this.cssPosition === "fixed" ? -this.scrollParent.scrollLeft() : scrollIsRootNode ? 0 : scroll.scrollLeft()),
            };
        },
        _rearrange: function (event, i, a, hardRefresh) {
            a ? a[0].appendChild(this.placeholder[0]) : i.item[0].parentNode.insertBefore(this.placeholder[0], this.direction === "down" ? i.item[0] : i.item[0].nextSibling);
            this.counter = this.counter ? ++this.counter : 1;
            var counter = this.counter;
            this._delay(function () {
                if (counter === this.counter) {
                    this.refreshPositions(!hardRefresh);
                }
            });
        },
        _clear: function (event, noPropagation) {
            this.reverting = false;
            var i,
                delayedTriggers = [];
            if (!this._noFinalSort && this.currentItem.parent().length) {
                this.placeholder.before(this.currentItem);
            }
            this._noFinalSort = null;
            if (this.helper[0] === this.currentItem[0]) {
                for (i in this._storedCSS) {
                    if (this._storedCSS[i] === "auto" || this._storedCSS[i] === "static") {
                        this._storedCSS[i] = "";
                    }
                }
                this.currentItem.css(this._storedCSS);
                this._removeClass(this.currentItem, "ui-sortable-helper");
            } else {
                this.currentItem.show();
            }
            if (this.fromOutside && !noPropagation) {
                delayedTriggers.push(function (event) {
                    this._trigger("receive", event, this._uiHash(this.fromOutside));
                });
            }
            if ((this.fromOutside || this.domPosition.prev !== this.currentItem.prev().not(".ui-sortable-helper")[0] || this.domPosition.parent !== this.currentItem.parent()[0]) && !noPropagation) {
                delayedTriggers.push(function (event) {
                    this._trigger("update", event, this._uiHash());
                });
            }
            if (this !== this.currentContainer) {
                if (!noPropagation) {
                    delayedTriggers.push(function (event) {
                        this._trigger("remove", event, this._uiHash());
                    });
                    delayedTriggers.push(
                        function (c) {
                            return function (event) {
                                c._trigger("receive", event, this._uiHash(this));
                            };
                        }.call(this, this.currentContainer)
                    );
                    delayedTriggers.push(
                        function (c) {
                            return function (event) {
                                c._trigger("update", event, this._uiHash(this));
                            };
                        }.call(this, this.currentContainer)
                    );
                }
            }
            function delayEvent(type, instance, container) {
                return function (event) {
                    container._trigger(type, event, instance._uiHash(instance));
                };
            }
            for (i = this.containers.length - 1; i >= 0; i--) {
                if (!noPropagation) {
                    delayedTriggers.push(delayEvent("deactivate", this, this.containers[i]));
                }
                if (this.containers[i].containerCache.over) {
                    delayedTriggers.push(delayEvent("out", this, this.containers[i]));
                    this.containers[i].containerCache.over = 0;
                }
            }
            if (this.storedCursor) {
                this.document.find("body").css("cursor", this.storedCursor);
                this.storedStylesheet.remove();
            }
            if (this._storedOpacity) {
                this.helper.css("opacity", this._storedOpacity);
            }
            if (this._storedZIndex) {
                this.helper.css("zIndex", this._storedZIndex === "auto" ? "" : this._storedZIndex);
            }
            this.dragging = false;
            if (!noPropagation) {
                this._trigger("beforeStop", event, this._uiHash());
            }
            this.placeholder[0].parentNode.removeChild(this.placeholder[0]);
            if (!this.cancelHelperRemoval) {
                if (this.helper[0] !== this.currentItem[0]) {
                    this.helper.remove();
                }
                this.helper = null;
            }
            if (!noPropagation) {
                for (i = 0; i < delayedTriggers.length; i++) {
                    delayedTriggers[i].call(this, event);
                }
                this._trigger("stop", event, this._uiHash());
            }
            this.fromOutside = false;
            return !this.cancelHelperRemoval;
        },
        _trigger: function () {
            if ($.Widget.prototype._trigger.apply(this, arguments) === false) {
                this.cancel();
            }
        },
        _uiHash: function (_inst) {
            var inst = _inst || this;
            return { helper: inst.helper, placeholder: inst.placeholder || $([]), position: inst.position, originalPosition: inst.originalPosition, offset: inst.positionAbs, item: inst.currentItem, sender: _inst ? _inst.element : null };
        },
    });
});

var saveAs =
    saveAs ||
    (function (e) {
        "use strict";
        if (typeof e === "undefined" || (typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent))) {
            return;
        }
        var t = e.document,
            n = function () {
                return e.URL || e.webkitURL || e;
            },
            r = t.createElementNS("http://www.w3.org/1999/xhtml", "a"),
            o = "download" in r,
            i = function (e) {
                var t = new MouseEvent("click");
                e.dispatchEvent(t);
            },
            a = /constructor/i.test(e.HTMLElement),
            f = /CriOS\/[\d]+/.test(navigator.userAgent),
            u = function (t) {
                (e.setImmediate || e.setTimeout)(function () {
                    throw t;
                }, 0);
            },
            d = "application/octet-stream",
            s = 1e3 * 40,
            c = function (e) {
                var t = function () {
                    if (typeof e === "string") {
                        n().revokeObjectURL(e);
                    } else {
                        e.remove();
                    }
                };
                setTimeout(t, s);
            },
            l = function (e, t, n) {
                t = [].concat(t);
                var r = t.length;
                while (r--) {
                    var o = e["on" + t[r]];
                    if (typeof o === "function") {
                        try {
                            o.call(e, n || e);
                        } catch (i) {
                            u(i);
                        }
                    }
                }
            },
            p = function (e) {
                if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)) {
                    return new Blob([String.fromCharCode(65279), e], { type: e.type });
                }
                return e;
            },
            v = function (t, u, s) {
                if (!s) {
                    t = p(t);
                }
                var v = this,
                    w = t.type,
                    m = w === d,
                    y,
                    h = function () {
                        l(v, "writestart progress write writeend".split(" "));
                    },
                    S = function () {
                        if ((f || (m && a)) && e.FileReader) {
                            var r = new FileReader();
                            r.onloadend = function () {
                                var t = f ? r.result : r.result.replace(/^data:[^;]*;/, "data:attachment/file;");
                                var n = e.open(t, "_blank");
                                if (!n) e.location.href = t;
                                t = undefined;
                                v.readyState = v.DONE;
                                h();
                            };
                            r.readAsDataURL(t);
                            v.readyState = v.INIT;
                            return;
                        }
                        if (!y) {
                            y = n().createObjectURL(t);
                        }
                        if (m) {
                            e.location.href = y;
                        } else {
                            var o = e.open(y, "_blank");
                            if (!o) {
                                e.location.href = y;
                            }
                        }
                        v.readyState = v.DONE;
                        h();
                        c(y);
                    };
                v.readyState = v.INIT;
                if (o) {
                    y = n().createObjectURL(t);
                    setTimeout(function () {
                        r.href = y;
                        r.download = u;
                        i(r);
                        h();
                        c(y);
                        v.readyState = v.DONE;
                    });
                    return;
                }
                S();
            },
            w = v.prototype,
            m = function (e, t, n) {
                return new v(e, t || e.name || "download", n);
            };
        if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
            return function (e, t, n) {
                t = t || e.name || "download";
                if (!n) {
                    e = p(e);
                }
                return navigator.msSaveOrOpenBlob(e, t);
            };
        }
        w.abort = function () { };
        w.readyState = w.INIT = 0;
        w.WRITING = 1;
        w.DONE = 2;
        w.error = w.onwritestart = w.onprogress = w.onwrite = w.onabort = w.onerror = w.onwriteend = null;
        return m;
    })((typeof self !== "undefined" && self) || (typeof window !== "undefined" && window) || this.content);

if (typeof module !== "undefined" && module.exports) {
    module.exports.saveAs = saveAs;
} else if (typeof define !== "undefined" && define !== null && define.amd !== null) {
    define([], function () {
        return saveAs;
    });
}

var LZString = (function () {
    function o(o, r) {
        if (!t[o]) {
            t[o] = {};
            for (var n = 0; n < o.length; n++) t[o][o.charAt(n)] = n;
        }
        return t[o][r];
    }
    var r = String.fromCharCode,
        n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
        e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",
        t = {},
        i = {
            compressToBase64: function (o) {
                if (null == o) return "";
                var r = i._compress(o, 6, function (o) {
                    return n.charAt(o);
                });
                switch (r.length % 4) {
                    default:
                    case 0:
                        return r;
                    case 1:
                        return r + "===";
                    case 2:
                        return r + "==";
                    case 3:
                        return r + "=";
                }
            },
            decompressFromBase64: function (r) {
                return null == r
                    ? ""
                    : "" == r
                        ? null
                        : i._decompress(r.length, 32, function (e) {
                            return o(n, r.charAt(e));
                        });
            },
            compressToUTF16: function (o) {
                return null == o
                    ? ""
                    : i._compress(o, 15, function (o) {
                        return r(o + 32);
                    }) + " ";
            },
            decompressFromUTF16: function (o) {
                return null == o
                    ? ""
                    : "" == o
                        ? null
                        : i._decompress(o.length, 16384, function (r) {
                            return o.charCodeAt(r) - 32;
                        });
            },
            compressToUint8Array: function (o) {
                for (var r = i.compress(o), n = new Uint8Array(2 * r.length), e = 0, t = r.length; t > e; e++) {
                    var s = r.charCodeAt(e);
                    (n[2 * e] = s >>> 8), (n[2 * e + 1] = s % 256);
                }
                return n;
            },
            decompressFromUint8Array: function (o) {
                if (null === o || void 0 === o) return i.decompress(o);
                for (var n = new Array(o.length / 2), e = 0, t = n.length; t > e; e++) n[e] = 256 * o[2 * e] + o[2 * e + 1];
                var s = [];
                return (
                    n.forEach(function (o) {
                        s.push(r(o));
                    }),
                    i.decompress(s.join(""))
                );
            },
            compressToEncodedURIComponent: function (o) {
                return null == o
                    ? ""
                    : i._compress(o, 6, function (o) {
                        return e.charAt(o);
                    });
            },
            decompressFromEncodedURIComponent: function (r) {
                return null == r
                    ? ""
                    : "" == r
                        ? null
                        : ((r = r.replace(/ /g, "+")),
                            i._decompress(r.length, 32, function (n) {
                                return o(e, r.charAt(n));
                            }));
            },
            compress: function (o) {
                return i._compress(o, 16, function (o) {
                    return r(o);
                });
            },
            _compress: function (o, r, n) {
                if (null == o) return "";
                var e,
                    t,
                    i,
                    s = {},
                    p = {},
                    u = "",
                    c = "",
                    a = "",
                    l = 2,
                    f = 3,
                    h = 2,
                    d = [],
                    m = 0,
                    v = 0;
                for (i = 0; i < o.length; i += 1)
                    if (((u = o.charAt(i)), Object.prototype.hasOwnProperty.call(s, u) || ((s[u] = f++), (p[u] = !0)), (c = a + u), Object.prototype.hasOwnProperty.call(s, c))) a = c;
                    else {
                        if (Object.prototype.hasOwnProperty.call(p, a)) {
                            if (a.charCodeAt(0) < 256) {
                                for (e = 0; h > e; e++) (m <<= 1), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++;
                                for (t = a.charCodeAt(0), e = 0; 8 > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                            } else {
                                for (t = 1, e = 0; h > e; e++) (m = (m << 1) | t), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t = 0);
                                for (t = a.charCodeAt(0), e = 0; 16 > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                            }
                            l--, 0 == l && ((l = Math.pow(2, h)), h++), delete p[a];
                        } else for (t = s[a], e = 0; h > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                        l--, 0 == l && ((l = Math.pow(2, h)), h++), (s[c] = f++), (a = String(u));
                    }
                if ("" !== a) {
                    if (Object.prototype.hasOwnProperty.call(p, a)) {
                        if (a.charCodeAt(0) < 256) {
                            for (e = 0; h > e; e++) (m <<= 1), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++;
                            for (t = a.charCodeAt(0), e = 0; 8 > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                        } else {
                            for (t = 1, e = 0; h > e; e++) (m = (m << 1) | t), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t = 0);
                            for (t = a.charCodeAt(0), e = 0; 16 > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                        }
                        l--, 0 == l && ((l = Math.pow(2, h)), h++), delete p[a];
                    } else for (t = s[a], e = 0; h > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                    l--, 0 == l && ((l = Math.pow(2, h)), h++);
                }
                for (t = 2, e = 0; h > e; e++) (m = (m << 1) | (1 & t)), v == r - 1 ? ((v = 0), d.push(n(m)), (m = 0)) : v++, (t >>= 1);
                for (; ;) {
                    if (((m <<= 1), v == r - 1)) {
                        d.push(n(m));
                        break;
                    }
                    v++;
                }
                return d.join("");
            },
            decompress: function (o) {
                return null == o
                    ? ""
                    : "" == o
                        ? null
                        : i._decompress(o.length, 32768, function (r) {
                            return o.charCodeAt(r);
                        });
            },
            _decompress: function (o, n, e) {
                var t,
                    i,
                    s,
                    p,
                    u,
                    c,
                    a,
                    l,
                    f = [],
                    h = 4,
                    d = 4,
                    m = 3,
                    v = "",
                    w = [],
                    A = { val: e(0), position: n, index: 1 };
                for (i = 0; 3 > i; i += 1) f[i] = i;
                for (p = 0, c = Math.pow(2, 2), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                switch ((t = p)) {
                    case 0:
                        for (p = 0, c = Math.pow(2, 8), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                        l = r(p);
                        break;
                    case 1:
                        for (p = 0, c = Math.pow(2, 16), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                        l = r(p);
                        break;
                    case 2:
                        return "";
                }
                for (f[3] = l, s = l, w.push(l); ;) {
                    if (A.index > o) return "";
                    for (p = 0, c = Math.pow(2, m), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                    switch ((l = p)) {
                        case 0:
                            for (p = 0, c = Math.pow(2, 8), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                            (f[d++] = r(p)), (l = d - 1), h--;
                            break;
                        case 1:
                            for (p = 0, c = Math.pow(2, 16), a = 1; a != c;) (u = A.val & A.position), (A.position >>= 1), 0 == A.position && ((A.position = n), (A.val = e(A.index++))), (p |= (u > 0 ? 1 : 0) * a), (a <<= 1);
                            (f[d++] = r(p)), (l = d - 1), h--;
                            break;
                        case 2:
                            return w.join("");
                    }
                    if ((0 == h && ((h = Math.pow(2, m)), m++), f[l])) v = f[l];
                    else {
                        if (l !== d) return null;
                        v = s + s.charAt(0);
                    }
                    w.push(v), (f[d++] = s + v.charAt(0)), h--, (s = v), 0 == h && ((h = Math.pow(2, m)), m++);
                }
            },
        };
    return i;
})();

"function" == typeof define && define.amd ? 
define(function () { return LZString; }) : 
"undefined" != typeof module && null != module && (module.exports = LZString);
    
var elementTypes = { 
    Text: TextBox, 
    Image: Image, 
    Heading: Heading, 
    Pull: BlockQuote, 
    Block: TextQuote, 
    List: List, 
    Vimeo: Vimeo, 
    YouTube: YouTube, 
    Break: Break, 
    Carousel: Carousel, 
    Embed: Embed };
var elements = [];
var dropIndex;
var textBoxCount = 0;
var pageMetaData = { headline: "", byline: "", dateline: "", coverImgURL: "" };
var outputCode = "";

$(document).ready(_init);

function _init() {
    $("#sideBarNav div").click(function () {
        toggleTab($(this).attr("tab"));
    });
    $("#file-new").click(newPage);
    $("#file-open").click(function () {
        $("#importModalWrapper").toggle();
    });
    $("#file-export").click(function () {
        $("#outputModalWrapper").toggle();
    });
    setElementTypes();
    $("#settings input").on("input", handleSettingsInput);
    $("#importModal-confirm").click(function () {
        var inputText;
        if ($("#importModal-link").css("display") == "flex") {
            inputText = $("#importModal-link input").val();
            if (/^(https?:\/\/)?(www.)?[\w|\d|-]*\.org\//.test(inputText)) {
                importLongform(inputText);
            } else toggleModalMessage($("#importModal"), "Invalid Link", "#ec0404");
        } else if ($("#importModal-embed").css("display") == "flex") {
            if (newPage()) {
                inputText = $("#importModal-embed textarea").val();
                input(inputText);
            }
        }
    });
    $("#importModal-cancel").click(function () {
        $("#importModalWrapper").toggle();
    });
    $("#importModal h4").click(function () {
        $("#importModal .modalNav h4").removeClass("active");
        $("#importModal .modalContent div").hide();
        $(this).addClass("active");
        $("#importModal-" + this.getAttribute("toggle")).show();
    });
    $("#outputModal-save").click(savePage);
    $("#outputModal-close").click(function () {
        $("#outputModalWrapper").toggle();
    });
    var parsedURL = parseURL(window.location.href);
    if (parsedURL.query.content) input(decodeURI(window.atob(parsedURL.query.content)));
    initImportFile();
    window.addEventListener(
        "message",
        function (e) {
            if (e.data.type == "importFromCMS") {
                if (e.data.contentType == "content") {
                    if (newPage()) input(e.data.message);
                } else if (e.data.contentType == "headline") {
                    $("#headline").val(e.data.message);
                    handleSettingsInput();
                } else if (e.data.contentType == "dateline") {
                    $("#dateline").val(e.data.message);
                    handleSettingsInput();
                }
            }
        },
        false
    );
    if (parseURL(window.location.href).query.CMS == "1") {
        $('.modalNav h4[toggle="link"]').remove();
        $("#importModal-link").remove();
        $('.modalNav h4[toggle="embed"]').addClass("active");
        $("#importModal-embed").show();
    }
    window.onbeforeunload = function () {
        return "";
    };
    window.scrollTo(0, document.body.scrollHeight);
    resetDragDrop();
    output();
}
function handleDropEvent(event, ui) {
    if (ui.draggable.hasClass("elementIcon")) {
        addElement(ui.draggable.attr("id"));
        output();
    }
}
function handleDragEvent(event, ui) {
    $("#dropLocation").remove();
    dropIndex = -2;
    for (var i = elements.length - 1; i >= 0; i--) {
        if (elements[elements.length - 1].ele.offset().top + elements[elements.length - 1].ele.height() / 2 < event.pageY) dropIndex = elements.length - 1;
        else if (elements[0].ele.offset().top + elements[i].ele.height() / 2 > event.pageY) dropIndex = -1;
        else if (i != elements.length - 1 && elements[i + 1].ele.offset().top + elements[i + 1].ele.height() / 2 > event.pageY && elements[i].ele.offset().top + elements[i].ele.height() / 2 < event.pageY) dropIndex = i;
    }
    if (dropIndex == -1) $(elements[0].ele).before('<div id="dropLocation"></div>');
    else if (dropIndex > -1) $(elements[dropIndex].ele).after('<div id="dropLocation"></div>');
    if (dropIndex == elements.length - 1) $("html, body").scrollTop($(document).height());
}
function handleStopEvent() {
    $("#dropLocation").remove();
}
function handleSortEvent(event, ui) {
    var oldIndex = getElementIndex(ui.item);
    var prevIndex = getElementIndex(ui.item.prev());
    if (oldIndex < prevIndex) moveArrayItem(elements, oldIndex, prevIndex);
    else moveArrayItem(elements, oldIndex, prevIndex + 1);
    output();
}
function resetDragDrop() {
    $(".container").sortable({
        axis: "y",
        helper: function (event, elm) {
            var paddingX = ($(elm).outerWidth() - $(elm).width()) / 2;
            var marginY = ($(elm).parent().width() - 650) / 2;
            marginY = marginY > 0 ? marginY : 0;
            if ($(elm).hasClass("emptyDiv")) {
                return $(elm)
                    .clone()
                    .attr("id", "sortable-helper")
                    .width($(elm).outerWidth())
                    .css("padding", "0px " + paddingX + "px")
                    .css("margin", "0 " + marginY + "px");
            } else {
                return $(elm).clone().attr("id", "sortable-helper").height($(elm).outerHeight(true));
            }
        },
        update: handleSortEvent,
    });
    $(".elementIcon").draggable({
        cursor: "move",
        containtment: "window",
        helper: function () {
            return $(this).clone().appendTo("body").addClass("elementHelper").show();
        },
        scroll: true,
        drag: handleDragEvent,
        stop: handleStopEvent,
    });
    $(".container").droppable({ drop: handleDropEvent });
}
function output() {
    var id = LZString.compressToEncodedURIComponent(JSON.stringify(pageMetaData));
    var code = '<p class="metadata" style="display:none" id="' + id + '"></p>';
    for (var i = 0; i < elements.length; i++) {
        code += elements[i].ele.html();
    }
    var jCode = $(code);
    jCode.filter("figure").each(function () {
        var firstChild = $(this.children[0]);
        var src = firstChild.attr("src");
        var needsProcessing = $(this).hasClass("img") || $(this).hasClass("video");
        if (needsProcessing) {
            firstChild.addClass("lazyload");
            firstChild.attr("data-src", src);
            firstChild.attr("src", src);
            if ($(this).hasClass("img")) {
                firstChild.attr("data-sizes", "auto");
                var caption = $(this).find("figcaption").text();
                firstChild.attr("alt", caption);
            } else if ($(this).hasClass("video")) {
                firstChild.attr("width", "640");
                firstChild.attr("height", "360");
                firstChild.attr("frameborder", "0");
            }
        }
        if ($(this).hasClass("carousel")) {
            $(this)
                .find(".swiper-slide")
                .each(function () {
                    $(this).find("img").attr("src", "").removeClass("swiper-lazy-loading");
                    $(this).find("div.swiper-lazy-preloader").show();
                });
        }
    });
    code = $("<div>").html(jCode).html();
    outputCode = code;
    $("#outputModal-link input").val(window.location.href);
    $("#outputModal-embed textarea").val(code);
    postContentData();
}
function postMetaData() {
    var message = { type: "metadata", input: { headline: pageMetaData.headline === undefined ? "" : pageMetaData.headline, dateline: pageMetaData.dateline === undefined ? "" : pageMetaData.dateline } };
    parent.postMessage(message, "*");
}
function postContentData() {
    var message = { type: "content", input: outputCode };
    parent.postMessage(message, "*");
}
function input(str, metadata) {
    var page = $(str);
    if (metadata === undefined) {
        var pageMetaData = $(".metadata", page).attr("id");
        pageMetaData = JSON.parse(LZString.decompressFromEncodedURIComponent(page.attr("id")));
        $("#headline").val(pageMetaData.headline);
        $("#byline").val(pageMetaData.byline);
        $("#dateline").val(pageMetaData.dateline);
        $("#bkgndImgUrl").val(pageMetaData.coverImgURL);
        handleSettingsInput();
    } else {
        $("#headline").val(metadata.headline);
        $("#byline").val(metadata.byline);
        $("#dateline").val(metadata.dateline);
        $("#bkgndImgUrl").val(metadata.coverImgURL);
        handleSettingsInput();
    }
    var inputElements = [];
    for (var i = 0; i < page.length; i++) {
        var child = page[i];
        if (!$(child).hasClass("metadata") && $(child).attr("id") != "story"){
            inputElements.push(child);
        }
    }
    dropIndex = -1;
    var errorsOccured = false;
    for (var i = inputElements.length - 1; i >= 0; i--) {
        try {
            var added = false;
            for (var eleType in elementTypes) {
                if (elementTypes[eleType].instanceOf(inputElements[i])) {
                    addElement(eleType, inputElements[i]);
                    added = true;
                    break;
                }
            }
            if (!added) {
                if (inputElements[i].nodeName != "#comment" && inputElements[i].nodeName != "#text") {
                    console.warn('Warning: element "' + inputElements[i].outerHTML + '" could not be imported.');
                    errorsOccured = true;
                }
            }
        } catch (e) {
            console.warn('Warning: element "' + inputElements[i].outerHTML + '" could not be imported because an error occured.');
            console.warn(e);
            errorsOccured = true;
        }
    }
    if (errorsOccured) toggleModalMessage($("#importModal"), "Some elements could not be imported.", "#ec0404");
    else $("#importModalWrapper").hide();
    output();
}
function importLongform(url) {
    var dataString = "url=" + url;
    $.ajax({
        url: "js/get.php",
        type: "POST",
        data: dataString,
        success: function (html) {
            try {
                var page = $("<div></div>");
                page.html(html);
                var content = $(".longform", page);
                content = content[content.length - 1];
                var metadata = {};
                metadata.headline = $(".headline", page).length > 0 ? $(".headline", page)[0].innerText.trim() : "";
                metadata.byline = $(".byline", page).length > 0 ? $(".byline", page)[0].innerText.trim() : "";
                metadata.dateline = $(".dateline", page).length > 0 ? $(".dateline", page)[0].innerText.trim() : "";
                metadata.coverimgurl = /^(?:\s*\.longform .header{background-image: url\(\')([^\s|\']*)/.exec($(".journal-content-article style", page)[0].innerHTML)[1];
                if (newPage()) input(content.innerHTML, metadata);
            } catch (e) {
                toggleModalMessage($("#importModal"), "Link does not contain a longform", "#ec0404");
                console.log(e);
            }
        },
        error: function (jqXHR, status, err) {
            throw new Error("Error: get.php connect failure.");
        },
    });
}
function initImportFile() {
    var dndEnter = function (e) {
        this.style.opacity = 0.5;
        e.stopPropagation();
        e.preventDefault();
    };
    var dndOver = function (e) {
        e.stopPropagation();
        e.preventDefault();
    };
    var dndDrop = function (e) {
        this.style.opacity = 1;
        if (e.dataTransfer.files.length > 1) alert("Import is limited to a single file.");
        else
            importFile(e.dataTransfer.files[0], function (htmlCode) {
                if (newPage()) {
                    input(htmlCode);
                    $("#importModalWrapper").hide();
                }
            });
        e.stopPropagation();
        e.preventDefault();
    };
    var BG = document.getElementsByTagName("body")[0];
    if ("draggable" in document.createElement("span")) {
        BG.addEventListener("dragenter", dndEnter, false);
        BG.addEventListener("dragover", dndOver, false);
        BG.addEventListener("drop", dndDrop, false);
    } else {
        alert("Unsupported Browser.");
    }
}
function importFile(file, callback) {
    var htmlCode;
    if (typeof FileReader !== "undefined" && file.type === "text/html" && file.size <= 15e4) {
        var reader = new FileReader();
        reader.onload = function (e) {
            callback(reader.result);
        };
        reader.readAsText(file);
    } else {
        if (typeof FileReader == "undefined") alert("Unsupported Browser.");
        else if (file.type !== "text/html") alert("Invalid file type.");
        else if (file.size > 15e4) alert("File size too big.");
    }
}
function toggleTab(id) {
    $(".sideBarTab").removeClass("active");
    $("#" + id).addClass("active");
    $(".tabHeading").removeClass("active");
    $("#" + id + "Heading").addClass("active");
}
function inspect(Element) {
    $("#inspection").remove();
    $("#inspect").append(Element.inspect().attr("id", "inspection").addClass("sideBarContent"));
    toggleTab("inspect");
    if (Element instanceof TextBox || Element instanceof TextQuote) {
        Element.editor = new nicEditor({
            buttonList: ["save", "bold", "italic", "underline", "strikethrough", "left", "center", "right", "fontSize", "fontFamily", "fontFormat", "link", "unlink", "forecolor", "bgcolor"], 
                iconsPath: "https://www.wuft.org/longform/img/nicEditorIcons2.gif" }).panelInstance(Element.id);
        var updateElement = function () {
            try {
                if (translateFromRichText(nicEditors.findEditor(Element.id).getContent()) != Element.text) {
                    Element.update(nicEditors.findEditor(Element.id).getContent());
                }
            } catch (TypeError) { }
        };
        $(".nicEdit-main").on("keyup", updateElement);
        $(".nicEdit-main").on('DOMSubtreeModified', updateElement);
        $(".nicEdit-panel").on("update", updateElement);
        $(window).on("cleanPaste", updateElement);
        $(".nicEdit-button").click(updateElement);
    }
}
function handleSettingsInput() {
    if ($("#headline").val() !== "") {
        pageMetaData.headline = $("#headline").val();
        $("#cover-headline").text($("#headline").val());
    } else {
        pageMetaData.headline = "";
        $("#cover-headline").text($("#headline").attr("placeholder"));
    }
    if ($("#byline").val() !== "") {
        pageMetaData.byline = $("#byline").val();
        $("#cover-byline-name").text($("#byline").val());
    } else {
        pageMetaData.byline = "";
        $("#cover-byline-name").text($("#byline").attr("placeholder"));
    }
    if ($("#dateline").val() !== "") {
        pageMetaData.dateline = $("#dateline").val();
        $("#cover-dateline").text($("#dateline").val());
    } else {
        pageMetaData.dateline = "";
        $("#cover-dateline").text($("#dateline").attr("placeholder"));
    }
    pageMetaData.coverImgURL = $("#bkgndImgUrl").val();
    $("#cover .parallax-img").css("background-image", "url(" + $("#bkgndImgUrl").val() + ")");
    output();
    postMetaData();
}
function setElementTypes() {
    for (var eleType in elementTypes) {
        var draggable = $('<div class="element"><div id="' + eleType + '" class="elementIcon">' + elementTypes[eleType].icon + '</div><div class="elementLabel">' + eleType + "</div></div>");
        $("#elements .sideBarContent").append(draggable);
    }
}
function addElement(type, data) {
    $("#emptyDroppable").remove();
    var ele = new Element(type, data);
    if (elements.length === 0) $(".container").append(ele.ele);
    else if (dropIndex <= -1) elements[0].ele.before(ele.ele);
    else elements[dropIndex].ele.after(ele.ele);
    elements.splice(dropIndex + 1, 0, ele);
    resetDragDrop();
    inspect(ele);
}
function deleteElement(Element) {
    elements.splice(getElementIndex(Element.ele), 1);
    Element.ele.remove();
    $("#inspection").html("<em>No element selected. Please select an element on the page to inspect.</em>");
    if (elements.length === 0) $(".container").append($('<div id="emptyDroppable" class="deleteDiv emptyDiv"><p>Drag/Drop Elements From The Left Here To Begin</p></div>'));
    toggleTab("elements");
    output();
}
function duplicateElement(Element) {
    dropIndex = getElementIndex(Element.ele);
    for (var i = $(Element.ele[0].innerHTML).length - 1; i >= 0; i--) {
        var node = $(Element.ele[0].innerHTML)[i];
        for (var eleType in elementTypes) {
            if (elementTypes[eleType].instanceOf(node)) {
                addElement(eleType, node);
                break;
            }
        }
    }
}
function toggleModalMessage(modal, text, color) {
    var tooltip = $('<div class="modalTooltip"></div>');
    tooltip.text(text);
    tooltip.css("color", color);
    modal.append(tooltip);
    setInterval(function () {
        tooltip.fadeOut(500);
        setInterval(function () {
            tooltip.remove();
        }, 500);
    }, 3e3);
}
function newPage() {
    proceed = true;
    if (elements.length > 0) proceed = confirm("Are you sure you want start over? All unsaved data will be lost.");
    if (proceed) {
        for (var i = 0; i < elements.length;) {
            deleteElement(elements[i]);
        }
        pageMetaData = { headline: "", byline: "", dateline: "", coverImgURL: "" };
        $("#headline").val(pageMetaData.headline);
        $("#byline").val(pageMetaData.byline);
        $("#dateline").val(pageMetaData.dateline);
        $("#bkgndImgUrl").val(pageMetaData.coverimgurl);
        handleSettingsInput();
    }
    return proceed;
}
function savePage() {
    var name = pageMetaData.headline !== "" ? pageMetaData.headline : "longform";
    var filename = name + ".html";
    var blob = new Blob([outputCode], { type: "text/html;charset=utf-8" });
    saveAs(blob, filename);
}
function getElementIndex(ele) {
    for (var i = elements.length - 1; i >= 0; i--) {
        if (elements[i].ele[0] == ele[0]) return i;
    }
    return -1;
}
function moveArrayItem(arr, oldIndex, newIndex) {
    if (newIndex >= arr.length) {
        var k = newIndex - arr.length;
        while (k-- + 1) {
            arr.push(undefined);
        }
    }
    arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
    return arr;
}
function translateFromRichText(text) {
    var output = text
        .replace(/<div><br><\/div>/g, "")
        .replace(/<p><\/p>/g, "")
        .replace(/<div>/, "</p><p>")
        .replace(/<div>/g, "<p>")
        .replace(/<\/div>/g, "</p>");
    if (output.substring(0, 3) != "<p>") output = "<p>" + output;
    if (output.substring(output.length - 4, output.length) != "</p>") output = output + "</p>";
    return output;
}
function parseURL(url) {
    var parser = document.createElement("a");
    parser.href = url;
    var query_string = {};
    var query = parser.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return { protocol: parser.protocol, hostname: parser.hostname, port: parser.port, pathname: parser.pathname, hash: parser.hash, host: parser.host, query: query_string };
}
function Element(type, data) {
    var ele;
    for (var eleType in elementTypes) {
        if (type == eleType) {
            ele = new elementTypes[eleType](data);
            break;
        }
    }
    return ele;
}
//#region Textbox
function TextBox(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.text = "<p></p>";
    this.id = "richText" + textBoxCount;
    textBoxCount++;
    this.ele.html(TextBox.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
TextBox.prototype.update = function (text) {
    text = translateFromRichText(text);
    this.text = text;
    this.ele.html(this.text);
    if (this.text == "<p></p>" || this.text == "<p><br></p>" || this.text == "<p>") {
        this.ele.addClass("emptyDiv").html(TextBox.icon);
    } else this.ele.removeClass("emptyDiv");
    output();
};
TextBox.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Text Box</h6>");
    var textLabel = $('<label for="elementText">Content</label>');
    var textArea = $('<textarea name="elementText"></textarea>').val(translateFromRichText(this.text)).attr("id", self.id);
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    var breakupButton = $(
        '<button title="Break Up By Paragraphs" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 96.83"><path d="M84,56.17L73.76,66l-9.85-9.83L52.3,66l-9.4-9.83L30.42,66l-11-9.83L6.33,66v41.17h88V66Z" transform="translate(-6.33 -10.33)"/><polygon points="0 45.83 13.14 36 24.08 45.83 36.56 36 45.97 45.83 57.57 36 67.42 45.83 77.71 36 88 45.83 88 12.18 71.75 0 0 0 0 45.83"/></svg></button>'
    ).click(function () {
        TextBox.breakup(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(breakupButton).append(textLabel).append(textArea);
    return inspection;
};
TextBox.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("");
        else this.update(data.innerHTML);
    }
};
TextBox.instanceOf = function (ele) {
    if ((ele.nodeName == "P" && !$(ele).hasClass("blockquote")) || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
TextBox.breakup = function (text) {
    var paragraphs = $(text.ele).children();
    dropIndex = getElementIndex(text.ele);
    for (var i = paragraphs.length - 1; i >= 0; i--) {
        if (paragraphs[i].outerHTML != "<p></p>") addElement("Text", paragraphs[i]);
    }
    deleteElement(text);
};
Object.defineProperty(TextBox, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM127.55,254a14.87,14.87,0,0,1,14.87-14.87H350.61A14.87,14.87,0,0,1,365.48,254v14.87a14.87,14.87,0,0,1-14.87,14.87H142.42a14.87,14.87,0,0,1-14.87-14.87V254ZM365.48,447.27a14.87,14.87,0,0,1-14.87,14.87H142.42a14.87,14.87,0,0,1-14.87-14.87V432.4a14.87,14.87,0,0,1,14.87-14.87H350.61a14.87,14.87,0,0,1,14.87,14.87v14.87Zm119-89.22a14.87,14.87,0,0,1-14.87,14.87H142.42A14.87,14.87,0,0,1,127.55,358V343.18a14.87,14.87,0,0,1,14.87-14.87H469.58a14.87,14.87,0,0,1,14.87,14.87V358Zm0-178.45a14.87,14.87,0,0,1-14.87,14.87H142.42a14.87,14.87,0,0,1-14.87-14.87V164.73a14.87,14.87,0,0,1,14.87-14.87H469.58a14.87,14.87,0,0,1,14.87,14.87V179.6Z"/></svg>';
    },
    set: function () {
        throw new Error("TextBox: icon is read only.");
    },
});
//#endregion

function BlockQuote(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.text = "";
    this.ele.html(BlockQuote.icon);
    this.type = "center";
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
BlockQuote.prototype.update = function (text, type) {
    this.text = text;
    this.type = type;
    this.ele.html('<blockquote class="' + this.type + '"><p>' + this.text + "</p></blockquote>");
    if (this.text == "<p></p>") this.ele.addClass("emptyDiv").html(BlockQuote.icon);
    else this.ele.removeClass("emptyDiv");
    output();
};
BlockQuote.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Pull Quote</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="left">Left<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="right">Right<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="center">Center<br>'));
    var textLabel = $('<label for="elementText">Content</label>');
    var textArea = $('<textarea id="elementText" name="elementText"></textarea>').val(self.text);
    textArea.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(textArea.val(), type);
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(textArea.val(), type);
            output();
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(textLabel).append(textArea);
    return inspection;
};
BlockQuote.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", "info");
        else {
            var text = data.innerHTML.replace(/<p>/g, "").replace(/<\/p>/g, "\n");
            if ($(data).hasClass("left")) this.update(text, "left");
            else if ($(data).hasClass("center")) this.update(text, "center");
            else if ($(data).hasClass("right")) this.update(text, "right");
        }
    }
};
BlockQuote.instanceOf = function (ele) {
    if (ele.nodeName == "BLOCKQUOTE" || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(BlockQuote, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM220.68,416.84c-38.87,5.55-81.7-15.47-92-70.2-9.92-78.52,47.19-143.17,132.07-152.29,3.17-.4,3.57,1.59,3.57,1.59l3.57,24.58a2.89,2.89,0,0,1-2,3.18c-24.19,7.14-45.21,19.43-53.14,50.37a70.78,70.78,0,0,1,67.82,60.67C286.91,374,259.94,410.49,220.68,416.84Zm202.66,0c-39.26,5.55-81.7-15.47-91.61-69.8C321,268.51,378.53,203.87,463.4,194.35c3.17-.4,3.57,1.59,3.57,1.59l4,24.58a2.88,2.88,0,0,1-2,3.18c-24.59,7.14-44.82,19.43-53.14,50.37,33.31,1.58,62.66,26.17,67.81,60.67C489.57,374,462.6,410.49,423.34,416.84Z"/></svg>';
    },
    set: function () {
        throw new Error("BlockQuote: icon is read only.");
    },
});
function TextQuote(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.text = '<p class="blockquote"></p>';
    this.id = "richText" + textBoxCount;
    textBoxCount++;
    this.ele.html(TextQuote.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
TextQuote.prototype.update = function (text) {
    text = translateFromRichText(text);
    var container = $("<div>" + text + "</div>");
    $("p", container).each(function () {
        $(this).addClass("blockquote");
    });
    $("p:empty", container).remove();
    this.text = container.html();
    this.ele.html(this.text);
    if (this.text == '<p class="blockquote"></p>' || this.text == '<p class="blockquote"><br></p>' || this.text == '<p class="blockquote">') {
        this.ele.addClass("emptyDiv").html(TextQuote.icon);
    } else this.ele.removeClass("emptyDiv");
    output();
};
TextQuote.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Block Quote</h6>");
    var textLabel = $('<label for="elementText">Content</label>');
    var textArea = $('<textarea name="elementText"></textarea>').val(translateFromRichText(this.text)).attr("id", self.id);
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    var breakupButton = $(
        '<button title="Break Up By Paragraphs" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 96.83"><path d="M84,56.17L73.76,66l-9.85-9.83L52.3,66l-9.4-9.83L30.42,66l-11-9.83L6.33,66v41.17h88V66Z" transform="translate(-6.33 -10.33)"/><polygon points="0 45.83 13.14 36 24.08 45.83 36.56 36 45.97 45.83 57.57 36 67.42 45.83 77.71 36 88 45.83 88 12.18 71.75 0 0 0 0 45.83"/></svg></button>'
    ).click(function () {
        TextQuote.breakup(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(breakupButton).append(textLabel).append(textArea);
    return inspection;
};
TextQuote.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("");
        else this.update(data.innerHTML);
    }
};
TextQuote.instanceOf = function (ele) {
    if ((ele.nodeName == "P" && $(ele).hasClass("blockquote")) || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
TextQuote.breakup = function (text) {
    var paragraphs = $(text.ele).children();
    dropIndex = getElementIndex(text.ele);
    for (var i = paragraphs.length - 1; i >= 0; i--) {
        if (paragraphs[i].outerHTML != '<p class="blockquote"></p>') addElement("Block", paragraphs[i]);
    }
    deleteElement(text);
};
Object.defineProperty(TextQuote, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM279.47,127.58c1.43-.18,1.61.72,1.61,0.72l1.81,11.12a1.3,1.3,0,0,1-.9,1.43c-11.13,3.23-20.28,8.79-24,22.79a32.49,32.49,0,0,1,3.4,64.58c-17.76,2.51-37-7-41.44-31.57C215,161.12,241.08,131.88,279.47,127.58Zm-91.67,0c1.43-.18,1.61.72,1.61,0.72L191,139.42a1.3,1.3,0,0,1-.91,1.43c-10.94,3.23-20.45,8.79-24,22.79a32,32,0,0,1,30.67,27.44,32.46,32.46,0,0,1-27.09,37.14c-17.58,2.51-37-7-41.61-31.76C123.57,161,149.4,131.7,187.8,127.58ZM127.54,276.31c0-8.21,10-14.87,22.3-14.87h312.3c12.32,0,22.31,6.66,22.31,14.87v14.87c0,8.21-10,14.87-22.31,14.87H149.84c-12.31,0-22.3-6.66-22.3-14.87V276.31ZM365.46,469.58a14.86,14.86,0,0,1-14.86,14.87H142.4a14.87,14.87,0,0,1-14.86-14.87V454.71a14.87,14.87,0,0,1,14.86-14.87H350.6a14.86,14.86,0,0,1,14.86,14.87v14.87ZM469.6,395.23H142.4a14.87,14.87,0,0,1-14.86-14.87V365.49a14.87,14.87,0,0,1,14.86-14.87H469.57a14.88,14.88,0,0,1,14.87,14.87v14.82l0,0A14.86,14.86,0,0,1,469.6,395.23ZM477,216.78H313.14c-4.11,0-7.45-6.66-7.45-14.87V187c0-8.21,3.34-14.87,7.45-14.87H477c4.11,0,7.45,6.66,7.45,14.87v14.87h0C484.46,210.12,481.13,216.78,477,216.78Z"/></svg>';
    },
    set: function () {
        throw new Error("TextQuote: icon is read only.");
    },
});
function Heading(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.text = "";
    this.ele.html(Heading.icon);
    this.type = "h2";
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
Heading.prototype.update = function (text, type) {
    this.type = type;
    this.prefix = "<" + this.type + ">";
    this.suffix = "</" + this.type + ">";
    this.text = text;
    this.ele.html(this.prefix + this.text + this.suffix);
    if (this.text === "") this.ele.addClass("emptyDiv").html(Heading.icon);
    else this.ele.removeClass("emptyDiv");
};
Heading.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Heading</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="h3">Minor Title<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="h2">Chapter/Section Title<br>'));
    var textLabel = $('<label for="elementText">Content</label>');
    var textInput = $('<input type="text" id="elementText" name="elementText"></input>').val(self.text);
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    textInput.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(textInput.val(), type);
        output();
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(textInput.val(), type);
            output();
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(textLabel).append(textInput);
    return inspection;
};
Heading.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", "h2");
        else this.update(data.innerHTML, data.nodeName.toLowerCase());
    }
};
Heading.instanceOf = function (ele) {
    if (ele.nodeName == "H2" || ele.nodeName == "H3" || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(Heading, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M226.58,157.23v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238ZM306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM184.14,194.13L144.47,233.8a10.57,10.57,0,0,1-14.29,0,10.44,10.44,0,0,1,.4-14.28L163.11,187l-32.53-32.53a9.82,9.82,0,0,1,13.89-13.89l39.67,39.67A9.6,9.6,0,0,1,184.14,194.13ZM434.86,474.6H216.67a9.92,9.92,0,1,1,0-19.83H434.86A9.92,9.92,0,1,1,434.86,474.6Zm39.67-59.5H216.67a9.92,9.92,0,0,1,0-19.84H474.53A9.92,9.92,0,0,1,474.53,415.1ZM206.75,345.67a9.82,9.82,0,0,1,9.92-9.92H434.86a9.92,9.92,0,1,1,0,19.84H216.67A9.82,9.82,0,0,1,206.75,345.67Zm267.78-49.59H216.67a9.91,9.91,0,1,1,0-19.83H474.53A9.91,9.91,0,1,1,474.53,296.08Zm9.92-69.42a9.83,9.83,0,0,1-9.92,9.92H216.67a9.82,9.82,0,0,1-9.92-9.92V147.32a9.82,9.82,0,0,1,9.92-9.92H474.53a9.82,9.82,0,0,1,9.92,9.92v79.34Zm-257.87-9.92h238V157.23h-238v59.51Zm0-59.51v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238Zm0,0v59.51h238V157.23h-238Z"/></svg>';
    },
    set: function () {
        throw new Error("Heading: icon is read only.");
    },
});
function Image(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.src = "";
    this.caption = "";
    this.type = "";
    this.ele.html(Image.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
Image.prototype.update = function (type, src, caption) {
    this.type = type;
    this.src = src;
    this.caption = caption;
    this.ele.html('<figure class="img ' + this.type + '"><img src="' + this.src + '"><figcaption>' + this.caption + "</figcaption></figure>");
    if (this.src === "") this.ele.addClass("emptyDiv").html(Image.icon);
    else this.ele.removeClass("emptyDiv");
    output();
};
Image.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Image</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="fitContent">Fit Content<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="full">Full Width<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="left">Left Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="right">Right Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="">Center<br>'));
    var srcLabel = $('<label for="imgSrc">Image URL</label>');
    var imgSrc = $('<input type="text" id="imgSrc" name="imgSrc"></input>').val(self.src);
    var captionLabel = $('<label for="caption">Caption</label>');
    var caption = $('<input type="text" id="caption" name="caption"></input>').val(self.caption);
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    imgSrc.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, imgSrc.val(), caption.val());
    });
    caption.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, imgSrc.val(), caption.val());
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(type, imgSrc.val(), caption.val());
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(srcLabel).append(imgSrc).append(captionLabel).append(caption);
    return inspection;
};
Image.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", "", "");
        else if (data.children[0].nodeName == "IMG") {
            var type = data.className.replace(/img|\s/g, "");
            this.update(type, data.children[0].getAttribute("data-src"), data.children[1].innerHTML);
        }
    }
};
Image.instanceOf = function (ele) {
    if (ele.nodeName == "FIGURE" && ele.children[0].nodeName == "IMG") return true;
    else if (ele.nodeName == "svg" && ele.outerHTML == this.icon) return true;
    else return false;
};
Object.defineProperty(Image, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM408.07,163.72A49.24,49.24,0,1,1,358.83,213,49.24,49.24,0,0,1,408.07,163.72ZM470,448.28H141.94a14,14,0,0,1-12.12-21.67L237.77,261a14,14,0,0,1,23.08-.66L368.79,406.93l46.5-44.26a14.37,14.37,0,0,1,1.59-1.33,14,14,0,0,1,19.59,3.16l45.25,61.44A14,14,0,0,1,470,448.28Z"/></svg>';
    },
    set: function () {
        throw new Error("Image: icon is read only.");
    },
});
function Vimeo(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.src = "";
    this.caption = "";
    this.type = "";
    this.ele.html(Vimeo.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
    window.longform.rv.resize();
    setTimeout(function () {
        window.longform.rv.resize();
    }, 500);
}
Vimeo.prototype.update = function (type, src, caption) {
    this.type = type;
    this.src = src;
    this.caption = caption;
    this.ele.html('<figure class="' + this.type + ' video"><iframe src="https://player.vimeo.com/video/' + this.src + '" width="640" height="360" frameborder="0" allowfull></iframe><figcaption>' + this.caption + "</figcaption></figure>");
    if (this.src === "") this.ele.addClass("emptyDiv").html(Vimeo.icon);
    else this.ele.removeClass("emptyDiv");
    output();
    window.longform.rv.resize();
};
Vimeo.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Vimeo Video</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="full">Full Width<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="left">Left Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="right">Right Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="">Center<br>'));
    var srcLabel = $('<label for="videoSrc">Vimeo Video Id</label>');
    var videoSrc = $('<input type="text" id="videoSrc" name="videoSrc" placeholder="Last digits of video URL"></input>').val(self.src);
    var captionLabel = $('<label for="caption">Caption</label>');
    var caption = $('<input type="text" id="caption" name="caption"></input>').val(self.caption);
    var disclaimer = $("<br><em>Videos are not clickable in this preview.</em>");
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    videoSrc.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, videoSrc.val(), caption.val());
    });
    caption.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, videoSrc.val(), caption.val());
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(type, videoSrc.val(), caption.val());
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(srcLabel).append(videoSrc).append(captionLabel).append(caption).append(disclaimer);
    return inspection;
};
Vimeo.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("big", "", "");
        else {
            var srcRegex = /(?:^https:\/\/player\.vimeo\.com\/video\/)(.*$)/g;
            var src = srcRegex.exec(data.children[0].getAttribute("data-src"))[1];
            var type = data.className.replace(/video|\s/g, "");
            this.update(type, src, data.children[1].innerHTML);
        }
    }
};
Vimeo.instanceOf = function (ele) {
    var srcRegex = /(?:^https:\/\/player\.vimeo\.com\/video\/)(.*$)/g;
    if ((ele.nodeName == "FIGURE" && $(ele).hasClass("video") && ele.children[0].nodeName == "IFRAME" && srcRegex.test(ele.children[0].getAttribute("data-src"))) || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(Vimeo, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM482.28,209.59c-4.29,18-10.84,35.65-18,52.76-21.65,51.59-48.77,100.14-87.74,140.77a294.63,294.63,0,0,1-52.14,43.54c-37.93,24.48-66.45,15.62-82.74-26.4-11-28.39-17.24-58.64-25.84-88q-8.91-30.42-18.5-60.65a89,89,0,0,0-6.46-14.71c-4.4-8.3-11.41-11.44-20.64-9.18q-5.05,1.25-10.05,2.71c-18.8,5.51-22,4.23-32.61-13.91,29.22-23.53,54.36-52.65,89-69.41,26-12.56,45.77-5.85,55.59,21.26,7.88,21.72,11.26,45.16,15.6,68,3.41,18,3.82,36.58,8.3,54.22,2.92,11.47,9.37,22.76,16.73,32.18,8,10.29,19.45,10.38,27.88.39,19.8-23.47,35.22-49.55,36.38-81.37,0.73-20-8.41-28.65-28.75-29.18-4.72-.12-9.44,0-14.76,0,7.89-35.72,57.1-84.83,105.49-80.54C474.7,155.28,490.6,174.68,482.28,209.59Z"/></svg>';
    },
    set: function () {
        throw new Error("Vimeo: icon is read only.");
    },
});
function YouTube(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.src = "";
    this.caption = "";
    this.type = "";
    this.ele.html(YouTube.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
    window.longform.rv.resize();
    setTimeout(function () {
        window.longform.rv.resize();
    }, 500);
}
YouTube.prototype.update = function (type, src, caption) {
    this.type = type;
    this.src = src;
    this.caption = caption;
    this.ele.html('<figure class="' + this.type + ' video"><iframe src="https://www.youtube.com/embed/' + this.src + '" width="640" height="360" frameborder="0" allowfull></iframe><figcaption>' + this.caption + "</figcaption></figure>");
    if (this.src === "") this.ele.addClass("emptyDiv").html(YouTube.icon);
    else this.ele.removeClass("emptyDiv");
    output();
    window.longform.rv.resize();
};
YouTube.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>YouTube Video</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="full">Full Width<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="left">Left Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="right">Right Align<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="">Center<br>'));
    var srcLabel = $('<label for="videoSrc">YouTube Video Id</label>');
    var videoSrc = $('<input type="text" id="videoSrc" name="videoSrc" placeholder="Last digits of video URL"></input>').val(self.src);
    var captionLabel = $('<label for="caption">Caption</label>');
    var caption = $('<input type="text" id="caption" name="caption"></input>').val(self.caption);
    var disclaimer = $("<br><em>Videos are not clickable in this preview.</em>");
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    videoSrc.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, videoSrc.val(), caption.val());
    });
    caption.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, videoSrc.val(), caption.val());
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(type, videoSrc.val(), caption.val());
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(srcLabel).append(videoSrc).append(captionLabel).append(caption).append(disclaimer);
    return inspection;
};
YouTube.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("big", "", "");
        else {
            var srcRegex = /(?:^https:\/\/www\.youtube\.com\/embed\/)(.*$)/g;
            var src = srcRegex.exec(data.children[0].getAttribute("data-src"))[1];
            var type = data.className.replace(/video|\s/g, "");
            this.update(type, src, data.children[1].innerHTML);
        }
    }
};
YouTube.instanceOf = function (ele) {
    var srcRegex = /(?:^https:\/\/www\.youtube\.com\/embed\/)(.*$)/g;
    if ((ele.nodeName == "FIGURE" && $(ele).hasClass("video") && ele.children[0].nodeName == "IFRAME" && srcRegex.test(ele.children[0].getAttribute("data-src"))) || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(YouTube, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M269.8,252.55l96.11,50.15L269.8,352.52v-100Z"/><path d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM483.42,349.55c-0.72,9.32-1.38,18.65-2.54,27.91a79,79,0,0,1-7.77,26.06c-6.46,12.75-17.2,20.06-31,22.77a144.39,144.39,0,0,1-20.05,2.12c-16.4.86-32.81,1.49-49.22,2.09-8.64.32-17.29,0.42-25.94,0.56-13.47.22-26.93,0.4-38.27,0.56-18.62-.28-35.1-0.44-51.58-0.8q-23.76-.51-47.51-1.39C197,429,184.42,428.33,172,426c-10.14-1.88-19.12-6-26.21-13.72-4.66-5.06-7.29-11.24-9.5-17.64a101.51,101.51,0,0,1-5.16-26c-0.88-12.56-2.14-25.12-2.36-37.69-0.36-21.82-.78-43.67.67-65.47,0.68-10.13,1.49-20.27,2.75-30.34A76.77,76.77,0,0,1,140.52,208c6.45-12,16.81-18.89,29.89-21.57a131.67,131.67,0,0,1,19.35-2.13c15.93-.86,31.87-1.48,47.82-2.09,7.71-.3,15.43-0.43,23.15-0.52,17.71-.21,35.41-0.42,53.12-0.48,11.2,0,22.41.12,33.61,0.34,13.58,0.26,27.16.52,40.72,1.07,15,0.6,29.91,1.44,44.84,2.37,8.36,0.51,16.38,2.41,23.71,6.74,9.89,5.84,16.09,14.44,19.7,25.2a109.16,109.16,0,0,1,5.5,27.2c0.88,12.56,2.17,25.12,2.34,37.7C484.59,304.4,485.15,327,483.42,349.55Z"/><path d="M365.91,302.7L269.8,352.52v-100Z"/></svg>';
    },
    set: function () {
        throw new Error("YouTube: icon is read only.");
    },
});
function Embed(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.content = "";
    this.caption = "";
    this.type = "";
    this.ele.html(Embed.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
Embed.prototype.update = function (type, content, caption) {
    this.type = type;
    this.content = content;
    this.caption = caption;
    this.ele.html('<figure class="' + this.type + '">' + this.content + "</iframe><figcaption>" + this.caption + "</figcaption></figure>");
    if (this.content === "") this.ele.addClass("emptyDiv").html(Embed.icon);
    else this.ele.removeClass("emptyDiv");
    output();
};
Embed.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Embed</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="full">Full Width<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="left">Left<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="right">Right<br>'));
    typeInputs.push($('<input class="radio" type="radio" name="type" value="">Center<br>'));
    var contentLabel = $('<label for="content">Content</label>');
    var content = $('<input type="text" id="content" name="content"></input>').val(self.content);
    var captionLabel = $('<label for="caption">Caption</label>');
    var caption = $('<input type="text" id="caption" name="caption"></input>').val(self.caption);
    var disclaimer = $("<br><em>Embeds are not clickable in this preview.</em>");
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append(typeLabel);
    content.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, content.val(), caption.val());
    });
    caption.on("input", function () {
        var type = $('input[type="radio"][name="type"]:checked').val();
        self.update(type, content.val(), caption.val());
    });
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(type, content.val(), caption.val());
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    inspection.append(contentLabel).append(content).append(captionLabel).append(caption).append(disclaimer);
    return inspection;
};
Embed.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", "", "");
        else {
            var content = "";
            for (var i = 0; i < data.children.length - 1; i++) {
                content += data.children[i].outerHTML;
            }
            var type;
            if ($(data).hasClass("full")) type = "full";
            else if ($(data).hasClass("left")) type = "left";
            else if ($(data).hasClass("right")) type = "right";
            else type = "";
            this.update(type, content, data.children[data.children.length - 1].innerHTML);
        }
    }
};
Embed.instanceOf = function (ele) {
    if (ele.nodeName == "FIGURE") return true;
    else if (ele.nodeName == "svg" && ele.outerHTML == this.icon) return true;
    else return false;
};
Object.defineProperty(Embed, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM231.24,361.08l-21.83,22.84L124.8,302.34l84.61-81.92,21.83,23.16-62.12,59.1Zm46.31,92H249.66l74.07-294.12h27.59Zm119.86-69.14-22.16-23.51,62.12-58.74-62.12-58.75,22.16-22.5L481.69,302Z"/></svg>';
    },
    set: function () {
        throw new Error("Embed: icon is read only.");
    },
});
function Break(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.ele.html(Break.icon);
    this.type = "doubleLine";
    this.update(this.type);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
Break.prototype.update = function (type) {
    this.type = type;
    this.ele.html('<hr class="' + this.type + '">');
    if (this.text === "") this.ele.addClass("emptyDiv").html(Break.icon);
    else this.ele.removeClass("emptyDiv");
};
Break.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var Break = $("<h6>Break</h6>");
    var typeLabel = $('<label for="type">Type</label>');
    var typeInputs = [];
    typeInputs.push($('<input class="radio" type="radio" name="type" value="doubleLine">Double Line<br>'));
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(Break).append(deleteButton).append(duplicateButton).append(typeLabel);
    for (var i = typeInputs.length - 1; i >= 0; i--) {
        typeInputs[i].on("click", function () {
            var type = $('input[type="radio"][name="type"]:checked').val();
            self.update(type);
            output();
        });
        if (typeInputs[i].val() == self.type) typeInputs[i].attr("checked", true);
        inspection.append(typeInputs[i]);
    }
    return inspection;
};
Break.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", "doubleLine");
        else this.update(data.className);
    }
};
Break.instanceOf = function (ele) {
    if (ele.nodeName == "HR" || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(Break, "icon", {
    get: function () {
        return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM158.35,322.8h-14.7a16.09,16.09,0,1,1,0-32.18h14.7A16.09,16.09,0,1,1,158.35,322.8ZM175.13,144a16.09,16.09,0,0,1,16.09-16.09H415.29A16.09,16.09,0,0,1,431.38,144v96.31a16.09,16.09,0,0,1-16.09,16.09H191.22a16.09,16.09,0,0,1-16.09-16.09V144ZM407,306.71a16.09,16.09,0,0,1-16.09,16.09h-14.7a16.09,16.09,0,0,1,0-32.18h14.7A16.1,16.1,0,0,1,407,306.71Zm-77.5,0a16.09,16.09,0,0,1-16.09,16.09h-14.7a16.09,16.09,0,0,1,0-32.18h14.7A16.1,16.1,0,0,1,329.45,306.71Zm-77.51,0a16.09,16.09,0,0,1-16.09,16.09h-14.7a16.09,16.09,0,1,1,0-32.18h14.7A16.1,16.1,0,0,1,251.94,306.71Zm181,162a16.09,16.09,0,0,1-16.09,16.09H192.76a16.09,16.09,0,0,1-16.09-16.09V372.4a16.1,16.1,0,0,1,16.09-16.09H416.84a16.1,16.1,0,0,1,16.09,16.09v96.31ZM468.37,322.8h-14.7a16.09,16.09,0,0,1,0-32.18h14.7A16.09,16.09,0,1,1,468.37,322.8Z"/></svg>';
    },
    set: function () {
        throw new Error("Break: icon is read only.");
    },
});
function List(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.items = [""];
    this.ele.html(List.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
List.prototype.update = function (text, index) {
    if (text !== undefined) this.items[index] = text;
    var html = "<ul>";
    for (var i = 0; i < this.items.length; i++) {
        html += "<li>" + this.items[i] + "</li>";
    }
    html += "</ul>";
    this.ele.html(html);
    var allEmpty = true;
    for (var i = 0; i < this.items.length; i++) {
        if (this.items[i] !== "") {
            allEmpty = false;
            break;
        }
    }
    if (allEmpty) this.ele.addClass("emptyDiv").html(List.icon);
    else this.ele.removeClass("emptyDiv");
    output();
};
List.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>List</h6>");
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append($("<br />"));
    var handleInput = function (textInput, index) {
        "use strict";
        return function () {
            self.update(textInput.val(), index);
        };
    };
    var handleDelete = function (index) {
        "use strict";
        return function () {
            self.items.splice(index, 1);
            if (self.items.length <= 0) self.items.push("");
            self.update();
            inspect(self);
        };
    };
    var handleTab = function (index) {
        "use strict";
        return function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode == 9) {
                e.preventDefault();
                self.items.splice(index + 1, 0, "");
                self.update();
                inspect(self);
            }
        };
    };
    for (var i = 0; i < this.items.length; i++) {
        var index = i;
        var textInput = $('<input class="listInput" type="text"></input>').val(self.items[i]);
        var deleteItem = $('<button class="deleteLiButton">X</button>');
        textInput.on("input", handleInput(textInput, i));
        textInput.on("keydown", handleTab(i));
        deleteItem.click(handleDelete(i));
        inspection.append(textInput).append(deleteItem);
        if (i == this.items.length - 1)
            setTimeout(function () {
                textInput[0].focus();
            }, 10);
    }
    var addButton = $("<button>Add</button>").click(function () {
        self.items.push("");
        inspect(self);
    });
    inspection.append(addButton);
    return inspection;
};
List.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update("", 0);
        else {
            for (var i = 1; i < data.children.length; i++) {
                this.items.push(data.children[i].innerHTML);
            }
            this.update(data.children[0].innerHTML, 0);
        }
    }
};
List.instanceOf = function (ele) {
    if (ele.nodeName == "UL" || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(List, "icon", {
    get: function () {
        return '<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM158.32,461.33a30.76,30.76,0,1,1,30.77-30.76A30.77,30.77,0,0,1,158.32,461.33Zm0-83.06a30.77,30.77,0,1,1,30.77-30.77A30.77,30.77,0,0,1,158.32,378.27Zm0-83.07a30.77,30.77,0,1,1,30.77-30.77A30.77,30.77,0,0,1,158.32,295.2Zm0-83a30.77,30.77,0,1,1,30.77-30.77A30.77,30.77,0,0,1,158.32,212.2ZM466,449H232.16a18.45,18.45,0,0,1,0-36.91H466A18.45,18.45,0,1,1,466,449ZM466,366H232.16a18.46,18.46,0,0,1,0-36.92H466A18.46,18.46,0,1,1,466,366Zm0-83.07H232.16a18.46,18.46,0,0,1,0-36.92H466A18.46,18.46,0,1,1,466,282.89Zm0-83H232.16a18.46,18.46,0,0,1,0-36.92H466A18.46,18.46,0,1,1,466,199.89Z"/></svg>';
    },
    set: function () {
        throw new Error("List: icon is read only.");
    },
});
function Carousel(data) {
    this.ele = $('<div class="emptyDiv"></div>');
    this.emptyItem = function () {
        return { img: "", caption: "" };
    };
    this.items = [this.emptyItem()];
    this.ele.html(Carousel.icon);
    this.parseInput(data);
    var self = this;
    $(this.ele).click(function () {
        inspect(self);
    });
}
Carousel.prototype.update = function (img, caption, index) {
    if (img !== undefined) {
        this.items[index].img = img;
        this.items[index].caption = caption;
    }
    var html = '<figure class="carousel"><div class="swiper-container"><div class="swiper-wrapper">';
    for (var i = 0; i < this.items.length; i++) {
        html += '<div class="swiper-slide"><img data-src="' + this.items[i].img + '" class="swiper-lazy"><div class="swiper-lazy-preloader"></div><figcaption>' + this.items[i].caption + "</figcaption></div>";
    }
    html += '</div><div class="swiper-pagination swiper-pagination-white"></div><div class="swiper-button-next swiper-button-white"></div><div class="swiper-button-prev swiper-button-white"></div></div>';
    this.ele.html(html);
    var allEmpty = true;
    for (var i = 0; i < this.items.length; i++) {
        if (this.items[i] !== this.emptyItem()) {
            allEmpty = false;
            break;
        }
    }
    if (allEmpty) this.ele.addClass("emptyDiv").html(Carousel.icon);
    else {
        this.ele.removeClass("emptyDiv");
    }
    $(this.ele)
        .find(".swiper-slide")
        .each(function () {
            longform.init_swiper();
            var img = $(this).find("img");
            var imgSrc = $(img).attr("data-src");
            $(img).attr("src", imgSrc);
            $(this).find("div.swiper-lazy-preloader").hide();
        });
    output();
};
Carousel.prototype.inspect = function () {
    var self = this;
    var inspection = $("<div></div>");
    var heading = $("<h6>Carousel</h6>");
    var deleteButton = $(
        '<button title="Delete" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 22"><path d="M0.5,1S0,1,0,2,0.5,3,.5,3h18S19,3,19,2s-0.5-1-.5-1H0.5ZM1,4H18L16,22H3ZM8,0A1,1,0,0,0,7,1h5a1,1,0,0,0-1-1H8ZM6,20H7L6,4H5ZM13,4L12,20h1L14,4H13ZM9,4V20h1V4H9Z"/></svg></button>'
    ).click(function () {
        deleteElement(self);
    });
    var duplicateButton = $(
        '<button title="Duplicate" class="elementButton"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 269 306"><path d="M269,12V254a12,12,0,0,1-12,12H247V33a12,12,0,0,0-12-12H40V12A12,12,0,0,1,52,0H257A12,12,0,0,1,269,12Z"/><rect y="40" width="229" height="266" rx="12" ry="12"/></svg></button>'
    ).click(function () {
        duplicateElement(self);
    });
    inspection.append(heading).append(deleteButton).append(duplicateButton).append($("<br />"));
    var handleInput = function (srcInput, captionInput, index) {
        "use strict";
        return function () {
            self.update(srcInput.val(), captionInput.val(), index);
        };
    };
    var handleDelete = function (index) {
        "use strict";
        return function () {
            self.items.splice(index, 1);
            if (self.items.length <= 0) self.items.push("");
            self.update();
            inspect(self);
        };
    };
    for (var i = 0; i < this.items.length; i++) {
        var index = i;
        var slideContainer = $('<div class="carouselSlideContainer"></div>');
        var slideInfo = $('<div class="carouselSlide"></div>');
        var srcInput = $('<input placeholder="Image URL" type="text"></input>').val(self.items[i].img);
        var captionInput = $('<input placeholder="Caption" type="text"></input>').val(self.items[i].caption);
        var deleteItem = $('<button class="deleteSlide">X</button>');
        slideInfo.append(srcInput, captionInput);
        slideContainer.append(slideInfo, deleteItem);
        srcInput.on("input", handleInput(srcInput, captionInput, i));
        captionInput.on("input", handleInput(srcInput, captionInput, i));
        deleteItem.click(handleDelete(i));
        inspection.append(slideContainer);
        if (i == this.items.length - 1)
            setTimeout(function () {
                srcInput[0].focus();
            }, 10);
    }
    var addButton = $("<button>Add</button>").click(function () {
        self.items.push(self.emptyItem());
        inspect(self);
    });
    inspection.append(addButton);
    return inspection;
};
Carousel.prototype.parseInput = function (data) {
    if (data !== undefined) {
        if (data.nodeName == "svg") this.update(this.emptyItem(), 0);
        else {
            dataSlides = $(data).find(".swiper-wrapper")[0];
            for (var i = 0; i < dataSlides.children.length; i++) {
                if (i > 0) this.items.push(this.emptyItem());
                var imgSrc = $(dataSlides.children[i]).find("img").attr("data-src");
                var caption = $(dataSlides.children[i]).find("figcaption").text();
                this.update(imgSrc, caption, i);
            }
            var self = this;
            setTimeout(function () {
                self.update();
            }, 100);
        }
    }
};
Carousel.instanceOf = function (ele) {
    if ((ele.nodeName == "FIGURE" && ele.className == "carousel") || (ele.nodeName == "svg" && ele.outerHTML == this.icon)) return true;
    else return false;
};
Object.defineProperty(Carousel, "icon", {
    get: function () {
        return '<svg id="Layer_2" data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path class="cls-1" d="M182.22,254.2v104a10.13,10.13,0,0,1-11.7,10l-17.21-2.69a10.14,10.14,0,0,1-8.58-10V256.89a10.14,10.14,0,0,1,8.58-10l17.21-2.69A10.13,10.13,0,0,1,182.22,254.2Z"/><path class="cls-1" d="M371.22,192.37H240.78a10.14,10.14,0,0,0-10.14,10.14v207a10.14,10.14,0,0,0,10.14,10.14H371.22a10.14,10.14,0,0,0,10.14-10.14v-207A10.14,10.14,0,0,0,371.22,192.37Zm0,0H240.78a10.14,10.14,0,0,0-10.14,10.14v207a10.14,10.14,0,0,0,10.14,10.14H371.22a10.14,10.14,0,0,0,10.14-10.14v-207A10.14,10.14,0,0,0,371.22,192.37ZM306,0C137,0,0,137,0,306S137,612,306,612,612,475,612,306,475,0,306,0ZM199,378.08a10.13,10.13,0,0,1-11.72,10L136.1,380a10.14,10.14,0,0,1-8.55-10V242.08a10.13,10.13,0,0,1,8.61-10l51.18-7.84a10.15,10.15,0,0,1,11.67,10V378.08Zm199.53,48.59a10.14,10.14,0,0,1-10.13,10.14H224a10.14,10.14,0,0,1-10.14-10.14V185.33A10.14,10.14,0,0,1,224,175.19H388.41a10.14,10.14,0,0,1,10.13,10.14V426.67ZM484.45,370a10.15,10.15,0,0,1-8.52,10l-49.62,8a10.14,10.14,0,0,1-11.76-10V234.28a10.14,10.14,0,0,1,11.71-10L475.88,232a10.14,10.14,0,0,1,8.57,10V370ZM458.76,246.82l-15.65-2.55a10.14,10.14,0,0,0-11.77,10V357.72a10.14,10.14,0,0,0,11.77,10l15.65-2.55a10.15,10.15,0,0,0,8.51-10V256.83A10.15,10.15,0,0,0,458.76,246.82Zm-87.54-54.45H240.78a10.14,10.14,0,0,0-10.14,10.14v207a10.14,10.14,0,0,0,10.14,10.14H371.22a10.14,10.14,0,0,0,10.14-10.14v-207A10.14,10.14,0,0,0,371.22,192.37Z"/></svg>';
    },
    set: function () {
        throw new Error("Carousel: icon is read only.");
    },
});