(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                var a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                var f = new Error("Cannot find module '" + o + "'");
                throw ((f.code = "MODULE_NOT_FOUND"), f);
            }
            var l = (n[o] = { exports: {} });
            t[o][0].call(
                l.exports,
                function (e) {
                    var n = t[o][1][e];
                    return s(n ? n : e);
                },
                l,
                l.exports,
                e,
                t,
                n,
                r
            );
        }
        return n[o].exports;
    }
    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s;
})(
    {
        1: [
            function (require, module, exports) {
                "use strict";
                var $ = require("jquery");
                module.exports = {
                    init: function init() {
                        $(".hamburger").click(function () {
                            $(this).toggleClass("is-active");
                            $("#wuft-banner").toggleClass("scrollable");
                            var targetId = "#" + $(this).attr("data-toggle");
                            $(targetId).slideToggle(260);
                        });
                        var dropdown = $("#wuft-banner");
                        dropdown.bind("wheel", function (e) {
                            var height = dropdown.height(),
                                scrollHeight = dropdown.get(0).scrollHeight;
                            var d = e.originalEvent.wheelDelta;
                            if ((this.scrollTop === scrollHeight - height && d < 0) || (this.scrollTop === 0 && d > 0)) {
                                e.preventDefault();
                            }
                        });
                    },
                };
            },
            { jquery: 5 },
        ],
        2: [
            function (require, module, exports) {
                "use strict";
                window.longform = {
                    ss: require("./smoothScrollAnchorLinks.js"),
                    $: require("jquery"),
                    ls: require("lazySizes"),
                    px: require("parallaxify"),
                    rv: require("./responsiveVideo.js"),
                    sw: require("swiper"),
                    ha: require("./hamburger.js"),
                    init: function init() {
                        var $ = this.$;
                        $("#cover").height($(window).height());
                        $(window).resize(function () {
                            $("#cover").height($(window).height());
                        });
                        this.init_breakouts();
                        this.ls.init();
                        this.ss.init(500);
                        new this.px({ elements: document.getElementsByClassName("parallax-img") }).registerUpdate();
                        this.rv.init();
                        this.init_swiper();
                        this.ha.init();
                    },
                    init_breakouts: function init_breakouts() {
                        var $ = this.$;
                        var figures = $("figure.breakout");
                        figures.each(function () {
                            var heading = $(this).find(".heading");
                            var dropArrowContainer = $('<div class="toggleArrow"></div>');
                            var dropArrowSVG = $(
                                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">' +
                                '<path d="M75.49,44.78a2.88,2.88,0,0,0-4.08-4.08L50,62.12,28.58,40.7a2.88,2.88,0,0,0-4.08,4.08L48,68.24a2.88,2.88,0,0,0,4.08,0Z"/>' +
                                "</svg>"
                            );
                            var content = $(this).find(".content");
                            dropArrowSVG.click(function () {
                                content.slideToggle(1e3, function () {
                                    dropArrowSVG.toggleClass("toggled");
                                });
                            });
                            dropArrowContainer.append(dropArrowSVG);
                            $(this).append(dropArrowContainer);
                        });
                    },
                    init_swiper: function init_swiper() {
                        var self = this;
                        var $ = this.$;
                        $(".swiper-container").each(function () {
                            var captions = $(".swiper-slide figcaption");
                            captions.each(function () {
                                $(this).hide();
                                var caption = this;
                                $(this)
                                    .siblings("img")
                                    .on("load", function () {
                                        $(caption).show();
                                    });
                            });
                            var swiper = new self.sw(this, {
                                nextButton: ".swiper-button-next",
                                prevButton: ".swiper-button-prev",
                                pagination: ".swiper-pagination",
                                keyboardControl: true,
                                mousewheelControl: true,
                                mousewheelForceToAxis: true,
                                paginationClickable: true,
                                preloadImages: false,
                                lazyLoading: true,
                                lazyLoadingInPrevNext: true,
                                lazyLoadingInPrevNextAmount: 2,
                            });
                        });
                    },
                };
                window.longform.init();
            },
            { "./hamburger.js": 1, "./responsiveVideo.js": 3, "./smoothScrollAnchorLinks.js": 4, jquery: 5, lazySizes: 6, parallaxify: 7, swiper: 8 },
        ],
        3: [
            function (require, module, exports) {
                "use strict";
                module.exports = {
                    init: function init() {
                        var self = this;
                        window.addEventListener("resize", function () {
                            self.resize();
                        });
                        self.resize();
                    },
                    resize: function resize() {
                        var videos = document.querySelectorAll("figure.video iframe");
                        if (videos !== null) {
                            var i;
                            for (i = 0; i < videos.length; i++) {
                                videos[i].setAttribute("data-aspectRatio", videos[i].height / videos[i].width);
                                videos[i].removeAttribute("height");
                                videos[i].removeAttribute("width");
                            }
                            for (i = 0; i < videos.length; i++) {
                                var parentWidth = videos[i].parentElement.clientWidth;
                                videos[i].width = parentWidth;
                                videos[i].height = parentWidth * videos[i].getAttribute("data-aspectRatio");
                            }
                        }
                    },
                };
            },
            {},
        ],
        4: [
            function (require, module, exports) {
                "use strict";
                module.exports = {
                    $: require("jquery"),
                    init: function init(speed) {
                        var $ = this.$;
                        $('a[href*="#"]:not([href="#"])').click(function () {
                            if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
                                var target = $(this.hash);
                                target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
                                if (target.length) {
                                    $("html, body").animate({ scrollTop: target.offset().top }, speed);
                                    return false;
                                }
                            }
                        });
                    },
                };
            },
            { jquery: 5 },
        ],
        5: [
            function (require, module, exports) {
                (function (global, factory) {
                    "use strict";
                    if (typeof module === "object" && typeof module.exports === "object") {
                        module.exports = global.document
                            ? factory(global, true)
                            : function (w) {
                                if (!w.document) {
                                    throw new Error("jQuery requires a window with a document");
                                }
                                return factory(w);
                            };
                    } else {
                        factory(global);
                    }
                })(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
                    "use strict";
                    var arr = [];
                    var document = window.document;
                    var getProto = Object.getPrototypeOf;
                    var slice = arr.slice;
                    var concat = arr.concat;
                    var push = arr.push;
                    var indexOf = arr.indexOf;
                    var class2type = {};
                    var toString = class2type.toString;
                    var hasOwn = class2type.hasOwnProperty;
                    var fnToString = hasOwn.toString;
                    var ObjectFunctionString = fnToString.call(Object);
                    var support = {};
                    function DOMEval(code, doc) {
                        doc = doc || document;
                        var script = doc.createElement("script");
                        script.text = code;
                        doc.head.appendChild(script).parentNode.removeChild(script);
                    }
                    var version = "3.2.1",
                        jQuery = function (selector, context) {
                            return new jQuery.fn.init(selector, context);
                        },
                        rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
                        rmsPrefix = /^-ms-/,
                        rdashAlpha = /-([a-z])/g,
                        fcamelCase = function (all, letter) {
                            return letter.toUpperCase();
                        };
                    jQuery.fn = jQuery.prototype = {
                        jquery: version,
                        constructor: jQuery,
                        length: 0,
                        toArray: function () {
                            return slice.call(this);
                        },
                        get: function (num) {
                            if (num == null) {
                                return slice.call(this);
                            }
                            return num < 0 ? this[num + this.length] : this[num];
                        },
                        pushStack: function (elems) {
                            var ret = jQuery.merge(this.constructor(), elems);
                            ret.prevObject = this;
                            return ret;
                        },
                        each: function (callback) {
                            return jQuery.each(this, callback);
                        },
                        map: function (callback) {
                            return this.pushStack(
                                jQuery.map(this, function (elem, i) {
                                    return callback.call(elem, i, elem);
                                })
                            );
                        },
                        slice: function () {
                            return this.pushStack(slice.apply(this, arguments));
                        },
                        first: function () {
                            return this.eq(0);
                        },
                        last: function () {
                            return this.eq(-1);
                        },
                        eq: function (i) {
                            var len = this.length,
                                j = +i + (i < 0 ? len : 0);
                            return this.pushStack(j >= 0 && j < len ? [this[j]] : []);
                        },
                        end: function () {
                            return this.prevObject || this.constructor();
                        },
                        push: push,
                        sort: arr.sort,
                        splice: arr.splice,
                    };
                    jQuery.extend = jQuery.fn.extend = function () {
                        var options,
                            name,
                            src,
                            copy,
                            copyIsArray,
                            clone,
                            target = arguments[0] || {},
                            i = 1,
                            length = arguments.length,
                            deep = false;
                        if (typeof target === "boolean") {
                            deep = target;
                            target = arguments[i] || {};
                            i++;
                        }
                        if (typeof target !== "object" && !jQuery.isFunction(target)) {
                            target = {};
                        }
                        if (i === length) {
                            target = this;
                            i--;
                        }
                        for (; i < length; i++) {
                            if ((options = arguments[i]) != null) {
                                for (name in options) {
                                    src = target[name];
                                    copy = options[name];
                                    if (target === copy) {
                                        continue;
                                    }
                                    if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = Array.isArray(copy)))) {
                                        if (copyIsArray) {
                                            copyIsArray = false;
                                            clone = src && Array.isArray(src) ? src : [];
                                        } else {
                                            clone = src && jQuery.isPlainObject(src) ? src : {};
                                        }
                                        target[name] = jQuery.extend(deep, clone, copy);
                                    } else if (copy !== undefined) {
                                        target[name] = copy;
                                    }
                                }
                            }
                        }
                        return target;
                    };
                    jQuery.extend({
                        expando: "jQuery" + (version + Math.random()).replace(/\D/g, ""),
                        isReady: true,
                        error: function (msg) {
                            throw new Error(msg);
                        },
                        noop: function () { },
                        isFunction: function (obj) {
                            return jQuery.type(obj) === "function";
                        },
                        isWindow: function (obj) {
                            return obj != null && obj === obj.window;
                        },
                        isNumeric: function (obj) {
                            var type = jQuery.type(obj);
                            return (type === "number" || type === "string") && !isNaN(obj - parseFloat(obj));
                        },
                        isPlainObject: function (obj) {
                            var proto, Ctor;
                            if (!obj || toString.call(obj) !== "[object Object]") {
                                return false;
                            }
                            proto = getProto(obj);
                            if (!proto) {
                                return true;
                            }
                            Ctor = hasOwn.call(proto, "constructor") && proto.constructor;
                            return typeof Ctor === "function" && fnToString.call(Ctor) === ObjectFunctionString;
                        },
                        isEmptyObject: function (obj) {
                            var name;
                            for (name in obj) {
                                return false;
                            }
                            return true;
                        },
                        type: function (obj) {
                            if (obj == null) {
                                return obj + "";
                            }
                            return typeof obj === "object" || typeof obj === "function" ? class2type[toString.call(obj)] || "object" : typeof obj;
                        },
                        globalEval: function (code) {
                            DOMEval(code);
                        },
                        camelCase: function (string) {
                            return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
                        },
                        each: function (obj, callback) {
                            var length,
                                i = 0;
                            if (isArrayLike(obj)) {
                                length = obj.length;
                                for (; i < length; i++) {
                                    if (callback.call(obj[i], i, obj[i]) === false) {
                                        break;
                                    }
                                }
                            } else {
                                for (i in obj) {
                                    if (callback.call(obj[i], i, obj[i]) === false) {
                                        break;
                                    }
                                }
                            }
                            return obj;
                        },
                        trim: function (text) {
                            return text == null ? "" : (text + "").replace(rtrim, "");
                        },
                        makeArray: function (arr, results) {
                            var ret = results || [];
                            if (arr != null) {
                                if (isArrayLike(Object(arr))) {
                                    jQuery.merge(ret, typeof arr === "string" ? [arr] : arr);
                                } else {
                                    push.call(ret, arr);
                                }
                            }
                            return ret;
                        },
                        inArray: function (elem, arr, i) {
                            return arr == null ? -1 : indexOf.call(arr, elem, i);
                        },
                        merge: function (first, second) {
                            var len = +second.length,
                                j = 0,
                                i = first.length;
                            for (; j < len; j++) {
                                first[i++] = second[j];
                            }
                            first.length = i;
                            return first;
                        },
                        grep: function (elems, callback, invert) {
                            var callbackInverse,
                                matches = [],
                                i = 0,
                                length = elems.length,
                                callbackExpect = !invert;
                            for (; i < length; i++) {
                                callbackInverse = !callback(elems[i], i);
                                if (callbackInverse !== callbackExpect) {
                                    matches.push(elems[i]);
                                }
                            }
                            return matches;
                        },
                        map: function (elems, callback, arg) {
                            var length,
                                value,
                                i = 0,
                                ret = [];
                            if (isArrayLike(elems)) {
                                length = elems.length;
                                for (; i < length; i++) {
                                    value = callback(elems[i], i, arg);
                                    if (value != null) {
                                        ret.push(value);
                                    }
                                }
                            } else {
                                for (i in elems) {
                                    value = callback(elems[i], i, arg);
                                    if (value != null) {
                                        ret.push(value);
                                    }
                                }
                            }
                            return concat.apply([], ret);
                        },
                        guid: 1,
                        proxy: function (fn, context) {
                            var tmp, args, proxy;
                            if (typeof context === "string") {
                                tmp = fn[context];
                                context = fn;
                                fn = tmp;
                            }
                            if (!jQuery.isFunction(fn)) {
                                return undefined;
                            }
                            args = slice.call(arguments, 2);
                            proxy = function () {
                                return fn.apply(context || this, args.concat(slice.call(arguments)));
                            };
                            proxy.guid = fn.guid = fn.guid || jQuery.guid++;
                            return proxy;
                        },
                        now: Date.now,
                        support: support,
                    });
                    if (typeof Symbol === "function") {
                        jQuery.fn[Symbol.iterator] = arr[Symbol.iterator];
                    }
                    jQuery.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (i, name) {
                        class2type["[object " + name + "]"] = name.toLowerCase();
                    });
                    function isArrayLike(obj) {
                        var length = !!obj && "length" in obj && obj.length,
                            type = jQuery.type(obj);
                        if (type === "function" || jQuery.isWindow(obj)) {
                            return false;
                        }
                        return type === "array" || length === 0 || (typeof length === "number" && length > 0 && length - 1 in obj);
                    }
                    var Sizzle = (function (window) {
                        var i,
                            support,
                            Expr,
                            getText,
                            isXML,
                            tokenize,
                            compile,
                            select,
                            outermostContext,
                            sortInput,
                            hasDuplicate,
                            setDocument,
                            document,
                            docElem,
                            documentIsHTML,
                            rbuggyQSA,
                            rbuggyMatches,
                            matches,
                            contains,
                            expando = "sizzle" + 1 * new Date(),
                            preferredDoc = window.document,
                            dirruns = 0,
                            done = 0,
                            classCache = createCache(),
                            tokenCache = createCache(),
                            compilerCache = createCache(),
                            sortOrder = function (a, b) {
                                if (a === b) {
                                    hasDuplicate = true;
                                }
                                return 0;
                            },
                            hasOwn = {}.hasOwnProperty,
                            arr = [],
                            pop = arr.pop,
                            push_native = arr.push,
                            push = arr.push,
                            slice = arr.slice,
                            indexOf = function (list, elem) {
                                var i = 0,
                                    len = list.length;
                                for (; i < len; i++) {
                                    if (list[i] === elem) {
                                        return i;
                                    }
                                }
                                return -1;
                            },
                            booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                            whitespace = "[\\x20\\t\\r\\n\\f]",
                            identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
                            attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace + "*([*^$|!~]?=)" + whitespace + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace + "*\\]",
                            pseudos = ":(" + identifier + ")(?:\\((" + "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" + ".*" + ")\\)|)",
                            rwhitespace = new RegExp(whitespace + "+", "g"),
                            rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"),
                            rcomma = new RegExp("^" + whitespace + "*," + whitespace + "*"),
                            rcombinators = new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"),
                            rattributeQuotes = new RegExp("=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g"),
                            rpseudo = new RegExp(pseudos),
                            ridentifier = new RegExp("^" + identifier + "$"),
                            matchExpr = {
                                ID: new RegExp("^#(" + identifier + ")"),
                                CLASS: new RegExp("^\\.(" + identifier + ")"),
                                TAG: new RegExp("^(" + identifier + "|[*])"),
                                ATTR: new RegExp("^" + attributes),
                                PSEUDO: new RegExp("^" + pseudos),
                                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"),
                                bool: new RegExp("^(?:" + booleans + ")$", "i"),
                                needsContext: new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i"),
                            },
                            rinputs = /^(?:input|select|textarea|button)$/i,
                            rheader = /^h\d$/i,
                            rnative = /^[^{]+\{\s*\[native \w/,
                            rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                            rsibling = /[+~]/,
                            runescape = new RegExp("\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig"),
                            funescape = function (_, escaped, escapedWhitespace) {
                                var high = "0x" + escaped - 65536;
                                return high !== high || escapedWhitespace ? escaped : high < 0 ? String.fromCharCode(high + 65536) : String.fromCharCode((high >> 10) | 55296, (high & 1023) | 56320);
                            },
                            rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
                            fcssescape = function (ch, asCodePoint) {
                                if (asCodePoint) {
                                    if (ch === "\0") {
                                        return "�";
                                    }
                                    return ch.slice(0, -1) + "\\" + ch.charCodeAt(ch.length - 1).toString(16) + " ";
                                }
                                return "\\" + ch;
                            },
                            unloadHandler = function () {
                                setDocument();
                            },
                            disabledAncestor = addCombinator(
                                function (elem) {
                                    return elem.disabled === true && ("form" in elem || "label" in elem);
                                },
                                { dir: "parentNode", next: "legend" }
                            );
                        try {
                            push.apply((arr = slice.call(preferredDoc.childNodes)), preferredDoc.childNodes);
                            arr[preferredDoc.childNodes.length].nodeType;
                        } catch (e) {
                            push = {
                                apply: arr.length
                                    ? function (target, els) {
                                        push_native.apply(target, slice.call(els));
                                    }
                                    : function (target, els) {
                                        var j = target.length,
                                            i = 0;
                                        while ((target[j++] = els[i++])) { }
                                        target.length = j - 1;
                                    },
                            };
                        }
                        function Sizzle(selector, context, results, seed) {
                            var m,
                                i,
                                elem,
                                nid,
                                match,
                                groups,
                                newSelector,
                                newContext = context && context.ownerDocument,
                                nodeType = context ? context.nodeType : 9;
                            results = results || [];
                            if (typeof selector !== "string" || !selector || (nodeType !== 1 && nodeType !== 9 && nodeType !== 11)) {
                                return results;
                            }
                            if (!seed) {
                                if ((context ? context.ownerDocument || context : preferredDoc) !== document) {
                                    setDocument(context);
                                }
                                context = context || document;
                                if (documentIsHTML) {
                                    if (nodeType !== 11 && (match = rquickExpr.exec(selector))) {
                                        if ((m = match[1])) {
                                            if (nodeType === 9) {
                                                if ((elem = context.getElementById(m))) {
                                                    if (elem.id === m) {
                                                        results.push(elem);
                                                        return results;
                                                    }
                                                } else {
                                                    return results;
                                                }
                                            } else {
                                                if (newContext && (elem = newContext.getElementById(m)) && contains(context, elem) && elem.id === m) {
                                                    results.push(elem);
                                                    return results;
                                                }
                                            }
                                        } else if (match[2]) {
                                            push.apply(results, context.getElementsByTagName(selector));
                                            return results;
                                        } else if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
                                            push.apply(results, context.getElementsByClassName(m));
                                            return results;
                                        }
                                    }
                                    if (support.qsa && !compilerCache[selector + " "] && (!rbuggyQSA || !rbuggyQSA.test(selector))) {
                                        if (nodeType !== 1) {
                                            newContext = context;
                                            newSelector = selector;
                                        } else if (context.nodeName.toLowerCase() !== "object") {
                                            if ((nid = context.getAttribute("id"))) {
                                                nid = nid.replace(rcssescape, fcssescape);
                                            } else {
                                                context.setAttribute("id", (nid = expando));
                                            }
                                            groups = tokenize(selector);
                                            i = groups.length;
                                            while (i--) {
                                                groups[i] = "#" + nid + " " + toSelector(groups[i]);
                                            }
                                            newSelector = groups.join(",");
                                            newContext = (rsibling.test(selector) && testContext(context.parentNode)) || context;
                                        }
                                        if (newSelector) {
                                            try {
                                                push.apply(results, newContext.querySelectorAll(newSelector));
                                                return results;
                                            } catch (qsaError) {
                                            } finally {
                                                if (nid === expando) {
                                                    context.removeAttribute("id");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            return select(selector.replace(rtrim, "$1"), context, results, seed);
                        }
                        function createCache() {
                            var keys = [];
                            function cache(key, value) {
                                if (keys.push(key + " ") > Expr.cacheLength) {
                                    delete cache[keys.shift()];
                                }
                                return (cache[key + " "] = value);
                            }
                            return cache;
                        }
                        function markFunction(fn) {
                            fn[expando] = true;
                            return fn;
                        }
                        function assert(fn) {
                            var el = document.createElement("fieldset");
                            try {
                                return !!fn(el);
                            } catch (e) {
                                return false;
                            } finally {
                                if (el.parentNode) {
                                    el.parentNode.removeChild(el);
                                }
                                el = null;
                            }
                        }
                        function addHandle(attrs, handler) {
                            var arr = attrs.split("|"),
                                i = arr.length;
                            while (i--) {
                                Expr.attrHandle[arr[i]] = handler;
                            }
                        }
                        function siblingCheck(a, b) {
                            var cur = b && a,
                                diff = cur && a.nodeType === 1 && b.nodeType === 1 && a.sourceIndex - b.sourceIndex;
                            if (diff) {
                                return diff;
                            }
                            if (cur) {
                                while ((cur = cur.nextSibling)) {
                                    if (cur === b) {
                                        return -1;
                                    }
                                }
                            }
                            return a ? 1 : -1;
                        }
                        function createInputPseudo(type) {
                            return function (elem) {
                                var name = elem.nodeName.toLowerCase();
                                return name === "input" && elem.type === type;
                            };
                        }
                        function createButtonPseudo(type) {
                            return function (elem) {
                                var name = elem.nodeName.toLowerCase();
                                return (name === "input" || name === "button") && elem.type === type;
                            };
                        }
                        function createDisabledPseudo(disabled) {
                            return function (elem) {
                                if ("form" in elem) {
                                    if (elem.parentNode && elem.disabled === false) {
                                        if ("label" in elem) {
                                            if ("label" in elem.parentNode) {
                                                return elem.parentNode.disabled === disabled;
                                            } else {
                                                return elem.disabled === disabled;
                                            }
                                        }
                                        return elem.isDisabled === disabled || (elem.isDisabled !== !disabled && disabledAncestor(elem) === disabled);
                                    }
                                    return elem.disabled === disabled;
                                } else if ("label" in elem) {
                                    return elem.disabled === disabled;
                                }
                                return false;
                            };
                        }
                        function createPositionalPseudo(fn) {
                            return markFunction(function (argument) {
                                argument = +argument;
                                return markFunction(function (seed, matches) {
                                    var j,
                                        matchIndexes = fn([], seed.length, argument),
                                        i = matchIndexes.length;
                                    while (i--) {
                                        if (seed[(j = matchIndexes[i])]) {
                                            seed[j] = !(matches[j] = seed[j]);
                                        }
                                    }
                                });
                            });
                        }
                        function testContext(context) {
                            return context && typeof context.getElementsByTagName !== "undefined" && context;
                        }
                        support = Sizzle.support = {};
                        isXML = Sizzle.isXML = function (elem) {
                            var documentElement = elem && (elem.ownerDocument || elem).documentElement;
                            return documentElement ? documentElement.nodeName !== "HTML" : false;
                        };
                        setDocument = Sizzle.setDocument = function (node) {
                            var hasCompare,
                                subWindow,
                                doc = node ? node.ownerDocument || node : preferredDoc;
                            if (doc === document || doc.nodeType !== 9 || !doc.documentElement) {
                                return document;
                            }
                            document = doc;
                            docElem = document.documentElement;
                            documentIsHTML = !isXML(document);
                            if (preferredDoc !== document && (subWindow = document.defaultView) && subWindow.top !== subWindow) {
                                if (subWindow.addEventListener) {
                                    subWindow.addEventListener("unload", unloadHandler, false);
                                } else if (subWindow.attachEvent) {
                                    subWindow.attachEvent("onunload", unloadHandler);
                                }
                            }
                            support.attributes = assert(function (el) {
                                el.className = "i";
                                return !el.getAttribute("className");
                            });
                            support.getElementsByTagName = assert(function (el) {
                                el.appendChild(document.createComment(""));
                                return !el.getElementsByTagName("*").length;
                            });
                            support.getElementsByClassName = rnative.test(document.getElementsByClassName);
                            support.getById = assert(function (el) {
                                docElem.appendChild(el).id = expando;
                                return !document.getElementsByName || !document.getElementsByName(expando).length;
                            });
                            if (support.getById) {
                                Expr.filter["ID"] = function (id) {
                                    var attrId = id.replace(runescape, funescape);
                                    return function (elem) {
                                        return elem.getAttribute("id") === attrId;
                                    };
                                };
                                Expr.find["ID"] = function (id, context) {
                                    if (typeof context.getElementById !== "undefined" && documentIsHTML) {
                                        var elem = context.getElementById(id);
                                        return elem ? [elem] : [];
                                    }
                                };
                            } else {
                                Expr.filter["ID"] = function (id) {
                                    var attrId = id.replace(runescape, funescape);
                                    return function (elem) {
                                        var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");
                                        return node && node.value === attrId;
                                    };
                                };
                                Expr.find["ID"] = function (id, context) {
                                    if (typeof context.getElementById !== "undefined" && documentIsHTML) {
                                        var node,
                                            i,
                                            elems,
                                            elem = context.getElementById(id);
                                        if (elem) {
                                            node = elem.getAttributeNode("id");
                                            if (node && node.value === id) {
                                                return [elem];
                                            }
                                            elems = context.getElementsByName(id);
                                            i = 0;
                                            while ((elem = elems[i++])) {
                                                node = elem.getAttributeNode("id");
                                                if (node && node.value === id) {
                                                    return [elem];
                                                }
                                            }
                                        }
                                        return [];
                                    }
                                };
                            }
                            Expr.find["TAG"] = support.getElementsByTagName
                                ? function (tag, context) {
                                    if (typeof context.getElementsByTagName !== "undefined") {
                                        return context.getElementsByTagName(tag);
                                    } else if (support.qsa) {
                                        return context.querySelectorAll(tag);
                                    }
                                }
                                : function (tag, context) {
                                    var elem,
                                        tmp = [],
                                        i = 0,
                                        results = context.getElementsByTagName(tag);
                                    if (tag === "*") {
                                        while ((elem = results[i++])) {
                                            if (elem.nodeType === 1) {
                                                tmp.push(elem);
                                            }
                                        }
                                        return tmp;
                                    }
                                    return results;
                                };
                            Expr.find["CLASS"] =
                                support.getElementsByClassName &&
                                function (className, context) {
                                    if (typeof context.getElementsByClassName !== "undefined" && documentIsHTML) {
                                        return context.getElementsByClassName(className);
                                    }
                                };
                            rbuggyMatches = [];
                            rbuggyQSA = [];
                            if ((support.qsa = rnative.test(document.querySelectorAll))) {
                                assert(function (el) {
                                    docElem.appendChild(el).innerHTML = "<a id='" + expando + "'></a>" + "<select id='" + expando + "-\r\\' msallowcapture=''>" + "<option selected=''></option></select>";
                                    if (el.querySelectorAll("[msallowcapture^='']").length) {
                                        rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
                                    }
                                    if (!el.querySelectorAll("[selected]").length) {
                                        rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
                                    }
                                    if (!el.querySelectorAll("[id~=" + expando + "-]").length) {
                                        rbuggyQSA.push("~=");
                                    }
                                    if (!el.querySelectorAll(":checked").length) {
                                        rbuggyQSA.push(":checked");
                                    }
                                    if (!el.querySelectorAll("a#" + expando + "+*").length) {
                                        rbuggyQSA.push(".#.+[+~]");
                                    }
                                });
                                assert(function (el) {
                                    el.innerHTML = "<a href='' disabled='disabled'></a>" + "<select disabled='disabled'><option/></select>";
                                    var input = document.createElement("input");
                                    input.setAttribute("type", "hidden");
                                    el.appendChild(input).setAttribute("name", "D");
                                    if (el.querySelectorAll("[name=d]").length) {
                                        rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
                                    }
                                    if (el.querySelectorAll(":enabled").length !== 2) {
                                        rbuggyQSA.push(":enabled", ":disabled");
                                    }
                                    docElem.appendChild(el).disabled = true;
                                    if (el.querySelectorAll(":disabled").length !== 2) {
                                        rbuggyQSA.push(":enabled", ":disabled");
                                    }
                                    el.querySelectorAll("*,:x");
                                    rbuggyQSA.push(",.*:");
                                });
                            }
                            if ((support.matchesSelector = rnative.test((matches = docElem.matches || docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)))) {
                                assert(function (el) {
                                    support.disconnectedMatch = matches.call(el, "*");
                                    matches.call(el, "[s!='']:x");
                                    rbuggyMatches.push("!=", pseudos);
                                });
                            }
                            rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|"));
                            rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join("|"));
                            hasCompare = rnative.test(docElem.compareDocumentPosition);
                            contains =
                                hasCompare || rnative.test(docElem.contains)
                                    ? function (a, b) {
                                        var adown = a.nodeType === 9 ? a.documentElement : a,
                                            bup = b && b.parentNode;
                                        return a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
                                    }
                                    : function (a, b) {
                                        if (b) {
                                            while ((b = b.parentNode)) {
                                                if (b === a) {
                                                    return true;
                                                }
                                            }
                                        }
                                        return false;
                                    };
                            sortOrder = hasCompare
                                ? function (a, b) {
                                    if (a === b) {
                                        hasDuplicate = true;
                                        return 0;
                                    }
                                    var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
                                    if (compare) {
                                        return compare;
                                    }
                                    compare = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;
                                    if (compare & 1 || (!support.sortDetached && b.compareDocumentPosition(a) === compare)) {
                                        if (a === document || (a.ownerDocument === preferredDoc && contains(preferredDoc, a))) {
                                            return -1;
                                        }
                                        if (b === document || (b.ownerDocument === preferredDoc && contains(preferredDoc, b))) {
                                            return 1;
                                        }
                                        return sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0;
                                    }
                                    return compare & 4 ? -1 : 1;
                                }
                                : function (a, b) {
                                    if (a === b) {
                                        hasDuplicate = true;
                                        return 0;
                                    }
                                    var cur,
                                        i = 0,
                                        aup = a.parentNode,
                                        bup = b.parentNode,
                                        ap = [a],
                                        bp = [b];
                                    if (!aup || !bup) {
                                        return a === document ? -1 : b === document ? 1 : aup ? -1 : bup ? 1 : sortInput ? indexOf(sortInput, a) - indexOf(sortInput, b) : 0;
                                    } else if (aup === bup) {
                                        return siblingCheck(a, b);
                                    }
                                    cur = a;
                                    while ((cur = cur.parentNode)) {
                                        ap.unshift(cur);
                                    }
                                    cur = b;
                                    while ((cur = cur.parentNode)) {
                                        bp.unshift(cur);
                                    }
                                    while (ap[i] === bp[i]) {
                                        i++;
                                    }
                                    return i ? siblingCheck(ap[i], bp[i]) : ap[i] === preferredDoc ? -1 : bp[i] === preferredDoc ? 1 : 0;
                                };
                            return document;
                        };
                        Sizzle.matches = function (expr, elements) {
                            return Sizzle(expr, null, null, elements);
                        };
                        Sizzle.matchesSelector = function (elem, expr) {
                            if ((elem.ownerDocument || elem) !== document) {
                                setDocument(elem);
                            }
                            expr = expr.replace(rattributeQuotes, "='$1']");
                            if (support.matchesSelector && documentIsHTML && !compilerCache[expr + " "] && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
                                try {
                                    var ret = matches.call(elem, expr);
                                    if (ret || support.disconnectedMatch || (elem.document && elem.document.nodeType !== 11)) {
                                        return ret;
                                    }
                                } catch (e) { }
                            }
                            return Sizzle(expr, document, null, [elem]).length > 0;
                        };
                        Sizzle.contains = function (context, elem) {
                            if ((context.ownerDocument || context) !== document) {
                                setDocument(context);
                            }
                            return contains(context, elem);
                        };
                        Sizzle.attr = function (elem, name) {
                            if ((elem.ownerDocument || elem) !== document) {
                                setDocument(elem);
                            }
                            var fn = Expr.attrHandle[name.toLowerCase()],
                                val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
                            return val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                        };
                        Sizzle.escape = function (sel) {
                            return (sel + "").replace(rcssescape, fcssescape);
                        };
                        Sizzle.error = function (msg) {
                            throw new Error("Syntax error, unrecognized expression: " + msg);
                        };
                        Sizzle.uniqueSort = function (results) {
                            var elem,
                                duplicates = [],
                                j = 0,
                                i = 0;
                            hasDuplicate = !support.detectDuplicates;
                            sortInput = !support.sortStable && results.slice(0);
                            results.sort(sortOrder);
                            if (hasDuplicate) {
                                while ((elem = results[i++])) {
                                    if (elem === results[i]) {
                                        j = duplicates.push(i);
                                    }
                                }
                                while (j--) {
                                    results.splice(duplicates[j], 1);
                                }
                            }
                            sortInput = null;
                            return results;
                        };
                        getText = Sizzle.getText = function (elem) {
                            var node,
                                ret = "",
                                i = 0,
                                nodeType = elem.nodeType;
                            if (!nodeType) {
                                while ((node = elem[i++])) {
                                    ret += getText(node);
                                }
                            } else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                                if (typeof elem.textContent === "string") {
                                    return elem.textContent;
                                } else {
                                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                                        ret += getText(elem);
                                    }
                                }
                            } else if (nodeType === 3 || nodeType === 4) {
                                return elem.nodeValue;
                            }
                            return ret;
                        };
                        Expr = Sizzle.selectors = {
                            cacheLength: 50,
                            createPseudo: markFunction,
                            match: matchExpr,
                            attrHandle: {},
                            find: {},
                            relative: { ">": { dir: "parentNode", first: true }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: true }, "~": { dir: "previousSibling" } },
                            preFilter: {
                                ATTR: function (match) {
                                    match[1] = match[1].replace(runescape, funescape);
                                    match[3] = (match[3] || match[4] || match[5] || "").replace(runescape, funescape);
                                    if (match[2] === "~=") {
                                        match[3] = " " + match[3] + " ";
                                    }
                                    return match.slice(0, 4);
                                },
                                CHILD: function (match) {
                                    match[1] = match[1].toLowerCase();
                                    if (match[1].slice(0, 3) === "nth") {
                                        if (!match[3]) {
                                            Sizzle.error(match[0]);
                                        }
                                        match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === "even" || match[3] === "odd"));
                                        match[5] = +(match[7] + match[8] || match[3] === "odd");
                                    } else if (match[3]) {
                                        Sizzle.error(match[0]);
                                    }
                                    return match;
                                },
                                PSEUDO: function (match) {
                                    var excess,
                                        unquoted = !match[6] && match[2];
                                    if (matchExpr["CHILD"].test(match[0])) {
                                        return null;
                                    }
                                    if (match[3]) {
                                        match[2] = match[4] || match[5] || "";
                                    } else if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)) {
                                        match[0] = match[0].slice(0, excess);
                                        match[2] = unquoted.slice(0, excess);
                                    }
                                    return match.slice(0, 3);
                                },
                            },
                            filter: {
                                TAG: function (nodeNameSelector) {
                                    var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
                                    return nodeNameSelector === "*"
                                        ? function () {
                                            return true;
                                        }
                                        : function (elem) {
                                            return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
                                        };
                                },
                                CLASS: function (className) {
                                    var pattern = classCache[className + " "];
                                    return (
                                        pattern ||
                                        ((pattern = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) &&
                                            classCache(className, function (elem) {
                                                return pattern.test((typeof elem.className === "string" && elem.className) || (typeof elem.getAttribute !== "undefined" && elem.getAttribute("class")) || "");
                                            }))
                                    );
                                },
                                ATTR: function (name, operator, check) {
                                    return function (elem) {
                                        var result = Sizzle.attr(elem, name);
                                        if (result == null) {
                                            return operator === "!=";
                                        }
                                        if (!operator) {
                                            return true;
                                        }
                                        result += "";
                                        return operator === "="
                                            ? result === check
                                            : operator === "!="
                                                ? result !== check
                                                : operator === "^="
                                                    ? check && result.indexOf(check) === 0
                                                    : operator === "*="
                                                        ? check && result.indexOf(check) > -1
                                                        : operator === "$="
                                                            ? check && result.slice(-check.length) === check
                                                            : operator === "~="
                                                                ? (" " + result.replace(rwhitespace, " ") + " ").indexOf(check) > -1
                                                                : operator === "|="
                                                                    ? result === check || result.slice(0, check.length + 1) === check + "-"
                                                                    : false;
                                    };
                                },
                                CHILD: function (type, what, argument, first, last) {
                                    var simple = type.slice(0, 3) !== "nth",
                                        forward = type.slice(-4) !== "last",
                                        ofType = what === "of-type";
                                    return first === 1 && last === 0
                                        ? function (elem) {
                                            return !!elem.parentNode;
                                        }
                                        : function (elem, context, xml) {
                                            var cache,
                                                uniqueCache,
                                                outerCache,
                                                node,
                                                nodeIndex,
                                                start,
                                                dir = simple !== forward ? "nextSibling" : "previousSibling",
                                                parent = elem.parentNode,
                                                name = ofType && elem.nodeName.toLowerCase(),
                                                useCache = !xml && !ofType,
                                                diff = false;
                                            if (parent) {
                                                if (simple) {
                                                    while (dir) {
                                                        node = elem;
                                                        while ((node = node[dir])) {
                                                            if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                                                                return false;
                                                            }
                                                        }
                                                        start = dir = type === "only" && !start && "nextSibling";
                                                    }
                                                    return true;
                                                }
                                                start = [forward ? parent.firstChild : parent.lastChild];
                                                if (forward && useCache) {
                                                    node = parent;
                                                    outerCache = node[expando] || (node[expando] = {});
                                                    uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                                                    cache = uniqueCache[type] || [];
                                                    nodeIndex = cache[0] === dirruns && cache[1];
                                                    diff = nodeIndex && cache[2];
                                                    node = nodeIndex && parent.childNodes[nodeIndex];
                                                    while ((node = (++nodeIndex && node && node[dir]) || (diff = nodeIndex = 0) || start.pop())) {
                                                        if (node.nodeType === 1 && ++diff && node === elem) {
                                                            uniqueCache[type] = [dirruns, nodeIndex, diff];
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    if (useCache) {
                                                        node = elem;
                                                        outerCache = node[expando] || (node[expando] = {});
                                                        uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                                                        cache = uniqueCache[type] || [];
                                                        nodeIndex = cache[0] === dirruns && cache[1];
                                                        diff = nodeIndex;
                                                    }
                                                    if (diff === false) {
                                                        while ((node = (++nodeIndex && node && node[dir]) || (diff = nodeIndex = 0) || start.pop())) {
                                                            if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                                                                if (useCache) {
                                                                    outerCache = node[expando] || (node[expando] = {});
                                                                    uniqueCache = outerCache[node.uniqueID] || (outerCache[node.uniqueID] = {});
                                                                    uniqueCache[type] = [dirruns, diff];
                                                                }
                                                                if (node === elem) {
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                diff -= last;
                                                return diff === first || (diff % first === 0 && diff / first >= 0);
                                            }
                                        };
                                },
                                PSEUDO: function (pseudo, argument) {
                                    var args,
                                        fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error("unsupported pseudo: " + pseudo);
                                    if (fn[expando]) {
                                        return fn(argument);
                                    }
                                    if (fn.length > 1) {
                                        args = [pseudo, pseudo, "", argument];
                                        return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase())
                                            ? markFunction(function (seed, matches) {
                                                var idx,
                                                    matched = fn(seed, argument),
                                                    i = matched.length;
                                                while (i--) {
                                                    idx = indexOf(seed, matched[i]);
                                                    seed[idx] = !(matches[idx] = matched[i]);
                                                }
                                            })
                                            : function (elem) {
                                                return fn(elem, 0, args);
                                            };
                                    }
                                    return fn;
                                },
                            },
                            pseudos: {
                                not: markFunction(function (selector) {
                                    var input = [],
                                        results = [],
                                        matcher = compile(selector.replace(rtrim, "$1"));
                                    return matcher[expando]
                                        ? markFunction(function (seed, matches, context, xml) {
                                            var elem,
                                                unmatched = matcher(seed, null, xml, []),
                                                i = seed.length;
                                            while (i--) {
                                                if ((elem = unmatched[i])) {
                                                    seed[i] = !(matches[i] = elem);
                                                }
                                            }
                                        })
                                        : function (elem, context, xml) {
                                            input[0] = elem;
                                            matcher(input, null, xml, results);
                                            input[0] = null;
                                            return !results.pop();
                                        };
                                }),
                                has: markFunction(function (selector) {
                                    return function (elem) {
                                        return Sizzle(selector, elem).length > 0;
                                    };
                                }),
                                contains: markFunction(function (text) {
                                    text = text.replace(runescape, funescape);
                                    return function (elem) {
                                        return (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1;
                                    };
                                }),
                                lang: markFunction(function (lang) {
                                    if (!ridentifier.test(lang || "")) {
                                        Sizzle.error("unsupported lang: " + lang);
                                    }
                                    lang = lang.replace(runescape, funescape).toLowerCase();
                                    return function (elem) {
                                        var elemLang;
                                        do {
                                            if ((elemLang = documentIsHTML ? elem.lang : elem.getAttribute("xml:lang") || elem.getAttribute("lang"))) {
                                                elemLang = elemLang.toLowerCase();
                                                return elemLang === lang || elemLang.indexOf(lang + "-") === 0;
                                            }
                                        } while ((elem = elem.parentNode) && elem.nodeType === 1);
                                        return false;
                                    };
                                }),
                                target: function (elem) {
                                    var hash = window.location && window.location.hash;
                                    return hash && hash.slice(1) === elem.id;
                                },
                                root: function (elem) {
                                    return elem === docElem;
                                },
                                focus: function (elem) {
                                    return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
                                },
                                enabled: createDisabledPseudo(false),
                                disabled: createDisabledPseudo(true),
                                checked: function (elem) {
                                    var nodeName = elem.nodeName.toLowerCase();
                                    return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
                                },
                                selected: function (elem) {
                                    if (elem.parentNode) {
                                        elem.parentNode.selectedIndex;
                                    }
                                    return elem.selected === true;
                                },
                                empty: function (elem) {
                                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                                        if (elem.nodeType < 6) {
                                            return false;
                                        }
                                    }
                                    return true;
                                },
                                parent: function (elem) {
                                    return !Expr.pseudos["empty"](elem);
                                },
                                header: function (elem) {
                                    return rheader.test(elem.nodeName);
                                },
                                input: function (elem) {
                                    return rinputs.test(elem.nodeName);
                                },
                                button: function (elem) {
                                    var name = elem.nodeName.toLowerCase();
                                    return (name === "input" && elem.type === "button") || name === "button";
                                },
                                text: function (elem) {
                                    var attr;
                                    return elem.nodeName.toLowerCase() === "input" && elem.type === "text" && ((attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text");
                                },
                                first: createPositionalPseudo(function () {
                                    return [0];
                                }),
                                last: createPositionalPseudo(function (matchIndexes, length) {
                                    return [length - 1];
                                }),
                                eq: createPositionalPseudo(function (matchIndexes, length, argument) {
                                    return [argument < 0 ? argument + length : argument];
                                }),
                                even: createPositionalPseudo(function (matchIndexes, length) {
                                    var i = 0;
                                    for (; i < length; i += 2) {
                                        matchIndexes.push(i);
                                    }
                                    return matchIndexes;
                                }),
                                odd: createPositionalPseudo(function (matchIndexes, length) {
                                    var i = 1;
                                    for (; i < length; i += 2) {
                                        matchIndexes.push(i);
                                    }
                                    return matchIndexes;
                                }),
                                lt: createPositionalPseudo(function (matchIndexes, length, argument) {
                                    var i = argument < 0 ? argument + length : argument;
                                    for (; --i >= 0;) {
                                        matchIndexes.push(i);
                                    }
                                    return matchIndexes;
                                }),
                                gt: createPositionalPseudo(function (matchIndexes, length, argument) {
                                    var i = argument < 0 ? argument + length : argument;
                                    for (; ++i < length;) {
                                        matchIndexes.push(i);
                                    }
                                    return matchIndexes;
                                }),
                            },
                        };
                        Expr.pseudos["nth"] = Expr.pseudos["eq"];
                        for (i in { radio: true, checkbox: true, file: true, password: true, image: true }) {
                            Expr.pseudos[i] = createInputPseudo(i);
                        }
                        for (i in { submit: true, reset: true }) {
                            Expr.pseudos[i] = createButtonPseudo(i);
                        }
                        function setFilters() { }
                        setFilters.prototype = Expr.filters = Expr.pseudos;
                        Expr.setFilters = new setFilters();
                        tokenize = Sizzle.tokenize = function (selector, parseOnly) {
                            var matched,
                                match,
                                tokens,
                                type,
                                soFar,
                                groups,
                                preFilters,
                                cached = tokenCache[selector + " "];
                            if (cached) {
                                return parseOnly ? 0 : cached.slice(0);
                            }
                            soFar = selector;
                            groups = [];
                            preFilters = Expr.preFilter;
                            while (soFar) {
                                if (!matched || (match = rcomma.exec(soFar))) {
                                    if (match) {
                                        soFar = soFar.slice(match[0].length) || soFar;
                                    }
                                    groups.push((tokens = []));
                                }
                                matched = false;
                                if ((match = rcombinators.exec(soFar))) {
                                    matched = match.shift();
                                    tokens.push({ value: matched, type: match[0].replace(rtrim, " ") });
                                    soFar = soFar.slice(matched.length);
                                }
                                for (type in Expr.filter) {
                                    if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
                                        matched = match.shift();
                                        tokens.push({ value: matched, type: type, matches: match });
                                        soFar = soFar.slice(matched.length);
                                    }
                                }
                                if (!matched) {
                                    break;
                                }
                            }
                            return parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
                        };
                        function toSelector(tokens) {
                            var i = 0,
                                len = tokens.length,
                                selector = "";
                            for (; i < len; i++) {
                                selector += tokens[i].value;
                            }
                            return selector;
                        }
                        function addCombinator(matcher, combinator, base) {
                            var dir = combinator.dir,
                                skip = combinator.next,
                                key = skip || dir,
                                checkNonElements = base && key === "parentNode",
                                doneName = done++;
                            return combinator.first
                                ? function (elem, context, xml) {
                                    while ((elem = elem[dir])) {
                                        if (elem.nodeType === 1 || checkNonElements) {
                                            return matcher(elem, context, xml);
                                        }
                                    }
                                    return false;
                                }
                                : function (elem, context, xml) {
                                    var oldCache,
                                        uniqueCache,
                                        outerCache,
                                        newCache = [dirruns, doneName];
                                    if (xml) {
                                        while ((elem = elem[dir])) {
                                            if (elem.nodeType === 1 || checkNonElements) {
                                                if (matcher(elem, context, xml)) {
                                                    return true;
                                                }
                                            }
                                        }
                                    } else {
                                        while ((elem = elem[dir])) {
                                            if (elem.nodeType === 1 || checkNonElements) {
                                                outerCache = elem[expando] || (elem[expando] = {});
                                                uniqueCache = outerCache[elem.uniqueID] || (outerCache[elem.uniqueID] = {});
                                                if (skip && skip === elem.nodeName.toLowerCase()) {
                                                    elem = elem[dir] || elem;
                                                } else if ((oldCache = uniqueCache[key]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                                                    return (newCache[2] = oldCache[2]);
                                                } else {
                                                    uniqueCache[key] = newCache;
                                                    if ((newCache[2] = matcher(elem, context, xml))) {
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return false;
                                };
                        }
                        function elementMatcher(matchers) {
                            return matchers.length > 1
                                ? function (elem, context, xml) {
                                    var i = matchers.length;
                                    while (i--) {
                                        if (!matchers[i](elem, context, xml)) {
                                            return false;
                                        }
                                    }
                                    return true;
                                }
                                : matchers[0];
                        }
                        function multipleContexts(selector, contexts, results) {
                            var i = 0,
                                len = contexts.length;
                            for (; i < len; i++) {
                                Sizzle(selector, contexts[i], results);
                            }
                            return results;
                        }
                        function condense(unmatched, map, filter, context, xml) {
                            var elem,
                                newUnmatched = [],
                                i = 0,
                                len = unmatched.length,
                                mapped = map != null;
                            for (; i < len; i++) {
                                if ((elem = unmatched[i])) {
                                    if (!filter || filter(elem, context, xml)) {
                                        newUnmatched.push(elem);
                                        if (mapped) {
                                            map.push(i);
                                        }
                                    }
                                }
                            }
                            return newUnmatched;
                        }
                        function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
                            if (postFilter && !postFilter[expando]) {
                                postFilter = setMatcher(postFilter);
                            }
                            if (postFinder && !postFinder[expando]) {
                                postFinder = setMatcher(postFinder, postSelector);
                            }
                            return markFunction(function (seed, results, context, xml) {
                                var temp,
                                    i,
                                    elem,
                                    preMap = [],
                                    postMap = [],
                                    preexisting = results.length,
                                    elems = seed || multipleContexts(selector || "*", context.nodeType ? [context] : context, []),
                                    matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems,
                                    matcherOut = matcher ? (postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results) : matcherIn;
                                if (matcher) {
                                    matcher(matcherIn, matcherOut, context, xml);
                                }
                                if (postFilter) {
                                    temp = condense(matcherOut, postMap);
                                    postFilter(temp, [], context, xml);
                                    i = temp.length;
                                    while (i--) {
                                        if ((elem = temp[i])) {
                                            matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
                                        }
                                    }
                                }
                                if (seed) {
                                    if (postFinder || preFilter) {
                                        if (postFinder) {
                                            temp = [];
                                            i = matcherOut.length;
                                            while (i--) {
                                                if ((elem = matcherOut[i])) {
                                                    temp.push((matcherIn[i] = elem));
                                                }
                                            }
                                            postFinder(null, (matcherOut = []), temp, xml);
                                        }
                                        i = matcherOut.length;
                                        while (i--) {
                                            if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf(seed, elem) : preMap[i]) > -1) {
                                                seed[temp] = !(results[temp] = elem);
                                            }
                                        }
                                    }
                                } else {
                                    matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);
                                    if (postFinder) {
                                        postFinder(null, results, matcherOut, xml);
                                    } else {
                                        push.apply(results, matcherOut);
                                    }
                                }
                            });
                        }
                        function matcherFromTokens(tokens) {
                            var checkContext,
                                matcher,
                                j,
                                len = tokens.length,
                                leadingRelative = Expr.relative[tokens[0].type],
                                implicitRelative = leadingRelative || Expr.relative[" "],
                                i = leadingRelative ? 1 : 0,
                                matchContext = addCombinator(
                                    function (elem) {
                                        return elem === checkContext;
                                    },
                                    implicitRelative,
                                    true
                                ),
                                matchAnyContext = addCombinator(
                                    function (elem) {
                                        return indexOf(checkContext, elem) > -1;
                                    },
                                    implicitRelative,
                                    true
                                ),
                                matchers = [
                                    function (elem, context, xml) {
                                        var ret = (!leadingRelative && (xml || context !== outermostContext)) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
                                        checkContext = null;
                                        return ret;
                                    },
                                ];
                            for (; i < len; i++) {
                                if ((matcher = Expr.relative[tokens[i].type])) {
                                    matchers = [addCombinator(elementMatcher(matchers), matcher)];
                                } else {
                                    matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);
                                    if (matcher[expando]) {
                                        j = ++i;
                                        for (; j < len; j++) {
                                            if (Expr.relative[tokens[j].type]) {
                                                break;
                                            }
                                        }
                                        return setMatcher(
                                            i > 1 && elementMatcher(matchers),
                                            i > 1 && toSelector(tokens.slice(0, i - 1).concat({ value: tokens[i - 2].type === " " ? "*" : "" })).replace(rtrim, "$1"),
                                            matcher,
                                            i < j && matcherFromTokens(tokens.slice(i, j)),
                                            j < len && matcherFromTokens((tokens = tokens.slice(j))),
                                            j < len && toSelector(tokens)
                                        );
                                    }
                                    matchers.push(matcher);
                                }
                            }
                            return elementMatcher(matchers);
                        }
                        function matcherFromGroupMatchers(elementMatchers, setMatchers) {
                            var bySet = setMatchers.length > 0,
                                byElement = elementMatchers.length > 0,
                                superMatcher = function (seed, context, xml, results, outermost) {
                                    var elem,
                                        j,
                                        matcher,
                                        matchedCount = 0,
                                        i = "0",
                                        unmatched = seed && [],
                                        setMatched = [],
                                        contextBackup = outermostContext,
                                        elems = seed || (byElement && Expr.find["TAG"]("*", outermost)),
                                        dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
                                        len = elems.length;
                                    if (outermost) {
                                        outermostContext = context === document || context || outermost;
                                    }
                                    for (; i !== len && (elem = elems[i]) != null; i++) {
                                        if (byElement && elem) {
                                            j = 0;
                                            if (!context && elem.ownerDocument !== document) {
                                                setDocument(elem);
                                                xml = !documentIsHTML;
                                            }
                                            while ((matcher = elementMatchers[j++])) {
                                                if (matcher(elem, context || document, xml)) {
                                                    results.push(elem);
                                                    break;
                                                }
                                            }
                                            if (outermost) {
                                                dirruns = dirrunsUnique;
                                            }
                                        }
                                        if (bySet) {
                                            if ((elem = !matcher && elem)) {
                                                matchedCount--;
                                            }
                                            if (seed) {
                                                unmatched.push(elem);
                                            }
                                        }
                                    }
                                    matchedCount += i;
                                    if (bySet && i !== matchedCount) {
                                        j = 0;
                                        while ((matcher = setMatchers[j++])) {
                                            matcher(unmatched, setMatched, context, xml);
                                        }
                                        if (seed) {
                                            if (matchedCount > 0) {
                                                while (i--) {
                                                    if (!(unmatched[i] || setMatched[i])) {
                                                        setMatched[i] = pop.call(results);
                                                    }
                                                }
                                            }
                                            setMatched = condense(setMatched);
                                        }
                                        push.apply(results, setMatched);
                                        if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
                                            Sizzle.uniqueSort(results);
                                        }
                                    }
                                    if (outermost) {
                                        dirruns = dirrunsUnique;
                                        outermostContext = contextBackup;
                                    }
                                    return unmatched;
                                };
                            return bySet ? markFunction(superMatcher) : superMatcher;
                        }
                        compile = Sizzle.compile = function (selector, match) {
                            var i,
                                setMatchers = [],
                                elementMatchers = [],
                                cached = compilerCache[selector + " "];
                            if (!cached) {
                                if (!match) {
                                    match = tokenize(selector);
                                }
                                i = match.length;
                                while (i--) {
                                    cached = matcherFromTokens(match[i]);
                                    if (cached[expando]) {
                                        setMatchers.push(cached);
                                    } else {
                                        elementMatchers.push(cached);
                                    }
                                }
                                cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
                                cached.selector = selector;
                            }
                            return cached;
                        };
                        select = Sizzle.select = function (selector, context, results, seed) {
                            var i,
                                tokens,
                                token,
                                type,
                                find,
                                compiled = typeof selector === "function" && selector,
                                match = !seed && tokenize((selector = compiled.selector || selector));
                            results = results || [];
                            if (match.length === 1) {
                                tokens = match[0] = match[0].slice(0);
                                if (tokens.length > 2 && (token = tokens[0]).type === "ID" && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
                                    context = (Expr.find["ID"](token.matches[0].replace(runescape, funescape), context) || [])[0];
                                    if (!context) {
                                        return results;
                                    } else if (compiled) {
                                        context = context.parentNode;
                                    }
                                    selector = selector.slice(tokens.shift().value.length);
                                }
                                i = matchExpr["needsContext"].test(selector) ? 0 : tokens.length;
                                while (i--) {
                                    token = tokens[i];
                                    if (Expr.relative[(type = token.type)]) {
                                        break;
                                    }
                                    if ((find = Expr.find[type])) {
                                        if ((seed = find(token.matches[0].replace(runescape, funescape), (rsibling.test(tokens[0].type) && testContext(context.parentNode)) || context))) {
                                            tokens.splice(i, 1);
                                            selector = seed.length && toSelector(tokens);
                                            if (!selector) {
                                                push.apply(results, seed);
                                                return results;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            (compiled || compile(selector, match))(seed, context, !documentIsHTML, results, !context || (rsibling.test(selector) && testContext(context.parentNode)) || context);
                            return results;
                        };
                        support.sortStable = expando.split("").sort(sortOrder).join("") === expando;
                        support.detectDuplicates = !!hasDuplicate;
                        setDocument();
                        support.sortDetached = assert(function (el) {
                            return el.compareDocumentPosition(document.createElement("fieldset")) & 1;
                        });
                        if (
                            !assert(function (el) {
                                el.innerHTML = "<a href='#'></a>";
                                return el.firstChild.getAttribute("href") === "#";
                            })
                        ) {
                            addHandle("type|href|height|width", function (elem, name, isXML) {
                                if (!isXML) {
                                    return elem.getAttribute(name, name.toLowerCase() === "type" ? 1 : 2);
                                }
                            });
                        }
                        if (
                            !support.attributes ||
                            !assert(function (el) {
                                el.innerHTML = "<input/>";
                                el.firstChild.setAttribute("value", "");
                                return el.firstChild.getAttribute("value") === "";
                            })
                        ) {
                            addHandle("value", function (elem, name, isXML) {
                                if (!isXML && elem.nodeName.toLowerCase() === "input") {
                                    return elem.defaultValue;
                                }
                            });
                        }
                        if (
                            !assert(function (el) {
                                return el.getAttribute("disabled") == null;
                            })
                        ) {
                            addHandle(booleans, function (elem, name, isXML) {
                                var val;
                                if (!isXML) {
                                    return elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                                }
                            });
                        }
                        return Sizzle;
                    })(window);
                    jQuery.find = Sizzle;
                    jQuery.expr = Sizzle.selectors;
                    jQuery.expr[":"] = jQuery.expr.pseudos;
                    jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
                    jQuery.text = Sizzle.getText;
                    jQuery.isXMLDoc = Sizzle.isXML;
                    jQuery.contains = Sizzle.contains;
                    jQuery.escapeSelector = Sizzle.escape;
                    var dir = function (elem, dir, until) {
                        var matched = [],
                            truncate = until !== undefined;
                        while ((elem = elem[dir]) && elem.nodeType !== 9) {
                            if (elem.nodeType === 1) {
                                if (truncate && jQuery(elem).is(until)) {
                                    break;
                                }
                                matched.push(elem);
                            }
                        }
                        return matched;
                    };
                    var siblings = function (n, elem) {
                        var matched = [];
                        for (; n; n = n.nextSibling) {
                            if (n.nodeType === 1 && n !== elem) {
                                matched.push(n);
                            }
                        }
                        return matched;
                    };
                    var rneedsContext = jQuery.expr.match.needsContext;
                    function nodeName(elem, name) {
                        return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
                    }
                    var rsingleTag = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;
                    var risSimple = /^.[^:#\[\.,]*$/;
                    function winnow(elements, qualifier, not) {
                        if (jQuery.isFunction(qualifier)) {
                            return jQuery.grep(elements, function (elem, i) {
                                return !!qualifier.call(elem, i, elem) !== not;
                            });
                        }
                        if (qualifier.nodeType) {
                            return jQuery.grep(elements, function (elem) {
                                return (elem === qualifier) !== not;
                            });
                        }
                        if (typeof qualifier !== "string") {
                            return jQuery.grep(elements, function (elem) {
                                return indexOf.call(qualifier, elem) > -1 !== not;
                            });
                        }
                        if (risSimple.test(qualifier)) {
                            return jQuery.filter(qualifier, elements, not);
                        }
                        qualifier = jQuery.filter(qualifier, elements);
                        return jQuery.grep(elements, function (elem) {
                            return indexOf.call(qualifier, elem) > -1 !== not && elem.nodeType === 1;
                        });
                    }
                    jQuery.filter = function (expr, elems, not) {
                        var elem = elems[0];
                        if (not) {
                            expr = ":not(" + expr + ")";
                        }
                        if (elems.length === 1 && elem.nodeType === 1) {
                            return jQuery.find.matchesSelector(elem, expr) ? [elem] : [];
                        }
                        return jQuery.find.matches(
                            expr,
                            jQuery.grep(elems, function (elem) {
                                return elem.nodeType === 1;
                            })
                        );
                    };
                    jQuery.fn.extend({
                        find: function (selector) {
                            var i,
                                ret,
                                len = this.length,
                                self = this;
                            if (typeof selector !== "string") {
                                return this.pushStack(
                                    jQuery(selector).filter(function () {
                                        for (i = 0; i < len; i++) {
                                            if (jQuery.contains(self[i], this)) {
                                                return true;
                                            }
                                        }
                                    })
                                );
                            }
                            ret = this.pushStack([]);
                            for (i = 0; i < len; i++) {
                                jQuery.find(selector, self[i], ret);
                            }
                            return len > 1 ? jQuery.uniqueSort(ret) : ret;
                        },
                        filter: function (selector) {
                            return this.pushStack(winnow(this, selector || [], false));
                        },
                        not: function (selector) {
                            return this.pushStack(winnow(this, selector || [], true));
                        },
                        is: function (selector) {
                            return !!winnow(this, typeof selector === "string" && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
                        },
                    });
                    var rootjQuery,
                        rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
                        init = (jQuery.fn.init = function (selector, context, root) {
                            var match, elem;
                            if (!selector) {
                                return this;
                            }
                            root = root || rootjQuery;
                            if (typeof selector === "string") {
                                if (selector[0] === "<" && selector[selector.length - 1] === ">" && selector.length >= 3) {
                                    match = [null, selector, null];
                                } else {
                                    match = rquickExpr.exec(selector);
                                }
                                if (match && (match[1] || !context)) {
                                    if (match[1]) {
                                        context = context instanceof jQuery ? context[0] : context;
                                        jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));
                                        if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                                            for (match in context) {
                                                if (jQuery.isFunction(this[match])) {
                                                    this[match](context[match]);
                                                } else {
                                                    this.attr(match, context[match]);
                                                }
                                            }
                                        }
                                        return this;
                                    } else {
                                        elem = document.getElementById(match[2]);
                                        if (elem) {
                                            this[0] = elem;
                                            this.length = 1;
                                        }
                                        return this;
                                    }
                                } else if (!context || context.jquery) {
                                    return (context || root).find(selector);
                                } else {
                                    return this.constructor(context).find(selector);
                                }
                            } else if (selector.nodeType) {
                                this[0] = selector;
                                this.length = 1;
                                return this;
                            } else if (jQuery.isFunction(selector)) {
                                return root.ready !== undefined ? root.ready(selector) : selector(jQuery);
                            }
                            return jQuery.makeArray(selector, this);
                        });
                    init.prototype = jQuery.fn;
                    rootjQuery = jQuery(document);
                    var rparentsprev = /^(?:parents|prev(?:Until|All))/,
                        guaranteedUnique = { children: true, contents: true, next: true, prev: true };
                    jQuery.fn.extend({
                        has: function (target) {
                            var targets = jQuery(target, this),
                                l = targets.length;
                            return this.filter(function () {
                                var i = 0;
                                for (; i < l; i++) {
                                    if (jQuery.contains(this, targets[i])) {
                                        return true;
                                    }
                                }
                            });
                        },
                        closest: function (selectors, context) {
                            var cur,
                                i = 0,
                                l = this.length,
                                matched = [],
                                targets = typeof selectors !== "string" && jQuery(selectors);
                            if (!rneedsContext.test(selectors)) {
                                for (; i < l; i++) {
                                    for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
                                        if (cur.nodeType < 11 && (targets ? targets.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
                                            matched.push(cur);
                                            break;
                                        }
                                    }
                                }
                            }
                            return this.pushStack(matched.length > 1 ? jQuery.uniqueSort(matched) : matched);
                        },
                        index: function (elem) {
                            if (!elem) {
                                return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
                            }
                            if (typeof elem === "string") {
                                return indexOf.call(jQuery(elem), this[0]);
                            }
                            return indexOf.call(this, elem.jquery ? elem[0] : elem);
                        },
                        add: function (selector, context) {
                            return this.pushStack(jQuery.uniqueSort(jQuery.merge(this.get(), jQuery(selector, context))));
                        },
                        addBack: function (selector) {
                            return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
                        },
                    });
                    function sibling(cur, dir) {
                        while ((cur = cur[dir]) && cur.nodeType !== 1) { }
                        return cur;
                    }
                    jQuery.each(
                        {
                            parent: function (elem) {
                                var parent = elem.parentNode;
                                return parent && parent.nodeType !== 11 ? parent : null;
                            },
                            parents: function (elem) {
                                return dir(elem, "parentNode");
                            },
                            parentsUntil: function (elem, i, until) {
                                return dir(elem, "parentNode", until);
                            },
                            next: function (elem) {
                                return sibling(elem, "nextSibling");
                            },
                            prev: function (elem) {
                                return sibling(elem, "previousSibling");
                            },
                            nextAll: function (elem) {
                                return dir(elem, "nextSibling");
                            },
                            prevAll: function (elem) {
                                return dir(elem, "previousSibling");
                            },
                            nextUntil: function (elem, i, until) {
                                return dir(elem, "nextSibling", until);
                            },
                            prevUntil: function (elem, i, until) {
                                return dir(elem, "previousSibling", until);
                            },
                            siblings: function (elem) {
                                return siblings((elem.parentNode || {}).firstChild, elem);
                            },
                            children: function (elem) {
                                return siblings(elem.firstChild);
                            },
                            contents: function (elem) {
                                if (nodeName(elem, "iframe")) {
                                    return elem.contentDocument;
                                }
                                if (nodeName(elem, "template")) {
                                    elem = elem.content || elem;
                                }
                                return jQuery.merge([], elem.childNodes);
                            },
                        },
                        function (name, fn) {
                            jQuery.fn[name] = function (until, selector) {
                                var matched = jQuery.map(this, fn, until);
                                if (name.slice(-5) !== "Until") {
                                    selector = until;
                                }
                                if (selector && typeof selector === "string") {
                                    matched = jQuery.filter(selector, matched);
                                }
                                if (this.length > 1) {
                                    if (!guaranteedUnique[name]) {
                                        jQuery.uniqueSort(matched);
                                    }
                                    if (rparentsprev.test(name)) {
                                        matched.reverse();
                                    }
                                }
                                return this.pushStack(matched);
                            };
                        }
                    );
                    var rnothtmlwhite = /[^\x20\t\r\n\f]+/g;
                    function createOptions(options) {
                        var object = {};
                        jQuery.each(options.match(rnothtmlwhite) || [], function (_, flag) {
                            object[flag] = true;
                        });
                        return object;
                    }
                    jQuery.Callbacks = function (options) {
                        options = typeof options === "string" ? createOptions(options) : jQuery.extend({}, options);
                        var firing,
                            memory,
                            fired,
                            locked,
                            list = [],
                            queue = [],
                            firingIndex = -1,
                            fire = function () {
                                locked = locked || options.once;
                                fired = firing = true;
                                for (; queue.length; firingIndex = -1) {
                                    memory = queue.shift();
                                    while (++firingIndex < list.length) {
                                        if (list[firingIndex].apply(memory[0], memory[1]) === false && options.stopOnFalse) {
                                            firingIndex = list.length;
                                            memory = false;
                                        }
                                    }
                                }
                                if (!options.memory) {
                                    memory = false;
                                }
                                firing = false;
                                if (locked) {
                                    if (memory) {
                                        list = [];
                                    } else {
                                        list = "";
                                    }
                                }
                            },
                            self = {
                                add: function () {
                                    if (list) {
                                        if (memory && !firing) {
                                            firingIndex = list.length - 1;
                                            queue.push(memory);
                                        }
                                        (function add(args) {
                                            jQuery.each(args, function (_, arg) {
                                                if (jQuery.isFunction(arg)) {
                                                    if (!options.unique || !self.has(arg)) {
                                                        list.push(arg);
                                                    }
                                                } else if (arg && arg.length && jQuery.type(arg) !== "string") {
                                                    add(arg);
                                                }
                                            });
                                        })(arguments);
                                        if (memory && !firing) {
                                            fire();
                                        }
                                    }
                                    return this;
                                },
                                remove: function () {
                                    jQuery.each(arguments, function (_, arg) {
                                        var index;
                                        while ((index = jQuery.inArray(arg, list, index)) > -1) {
                                            list.splice(index, 1);
                                            if (index <= firingIndex) {
                                                firingIndex--;
                                            }
                                        }
                                    });
                                    return this;
                                },
                                has: function (fn) {
                                    return fn ? jQuery.inArray(fn, list) > -1 : list.length > 0;
                                },
                                empty: function () {
                                    if (list) {
                                        list = [];
                                    }
                                    return this;
                                },
                                disable: function () {
                                    locked = queue = [];
                                    list = memory = "";
                                    return this;
                                },
                                disabled: function () {
                                    return !list;
                                },
                                lock: function () {
                                    locked = queue = [];
                                    if (!memory && !firing) {
                                        list = memory = "";
                                    }
                                    return this;
                                },
                                locked: function () {
                                    return !!locked;
                                },
                                fireWith: function (context, args) {
                                    if (!locked) {
                                        args = args || [];
                                        args = [context, args.slice ? args.slice() : args];
                                        queue.push(args);
                                        if (!firing) {
                                            fire();
                                        }
                                    }
                                    return this;
                                },
                                fire: function () {
                                    self.fireWith(this, arguments);
                                    return this;
                                },
                                fired: function () {
                                    return !!fired;
                                },
                            };
                        return self;
                    };
                    function Identity(v) {
                        return v;
                    }
                    function Thrower(ex) {
                        throw ex;
                    }
                    function adoptValue(value, resolve, reject, noValue) {
                        var method;
                        try {
                            if (value && jQuery.isFunction((method = value.promise))) {
                                method.call(value).done(resolve).fail(reject);
                            } else if (value && jQuery.isFunction((method = value.then))) {
                                method.call(value, resolve, reject);
                            } else {
                                resolve.apply(undefined, [value].slice(noValue));
                            }
                        } catch (value) {
                            reject.apply(undefined, [value]);
                        }
                    }
                    jQuery.extend({
                        Deferred: function (func) {
                            var tuples = [
                                ["notify", "progress", jQuery.Callbacks("memory"), jQuery.Callbacks("memory"), 2],
                                ["resolve", "done", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), 0, "resolved"],
                                ["reject", "fail", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), 1, "rejected"],
                            ],
                                state = "pending",
                                promise = {
                                    state: function () {
                                        return state;
                                    },
                                    always: function () {
                                        deferred.done(arguments).fail(arguments);
                                        return this;
                                    },
                                    catch: function (fn) {
                                        return promise.then(null, fn);
                                    },
                                    pipe: function () {
                                        var fns = arguments;
                                        return jQuery
                                            .Deferred(function (newDefer) {
                                                jQuery.each(tuples, function (i, tuple) {
                                                    var fn = jQuery.isFunction(fns[tuple[4]]) && fns[tuple[4]];
                                                    deferred[tuple[1]](function () {
                                                        var returned = fn && fn.apply(this, arguments);
                                                        if (returned && jQuery.isFunction(returned.promise)) {
                                                            returned.promise().progress(newDefer.notify).done(newDefer.resolve).fail(newDefer.reject);
                                                        } else {
                                                            newDefer[tuple[0] + "With"](this, fn ? [returned] : arguments);
                                                        }
                                                    });
                                                });
                                                fns = null;
                                            })
                                            .promise();
                                    },
                                    then: function (onFulfilled, onRejected, onProgress) {
                                        var maxDepth = 0;
                                        function resolve(depth, deferred, handler, special) {
                                            return function () {
                                                var that = this,
                                                    args = arguments,
                                                    mightThrow = function () {
                                                        var returned, then;
                                                        if (depth < maxDepth) {
                                                            return;
                                                        }
                                                        returned = handler.apply(that, args);
                                                        if (returned === deferred.promise()) {
                                                            throw new TypeError("Thenable self-resolution");
                                                        }
                                                        then = returned && (typeof returned === "object" || typeof returned === "function") && returned.then;
                                                        if (jQuery.isFunction(then)) {
                                                            if (special) {
                                                                then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special));
                                                            } else {
                                                                maxDepth++;
                                                                then.call(returned, resolve(maxDepth, deferred, Identity, special), resolve(maxDepth, deferred, Thrower, special), resolve(maxDepth, deferred, Identity, deferred.notifyWith));
                                                            }
                                                        } else {
                                                            if (handler !== Identity) {
                                                                that = undefined;
                                                                args = [returned];
                                                            }
                                                            (special || deferred.resolveWith)(that, args);
                                                        }
                                                    },
                                                    process = special
                                                        ? mightThrow
                                                        : function () {
                                                            try {
                                                                mightThrow();
                                                            } catch (e) {
                                                                if (jQuery.Deferred.exceptionHook) {
                                                                    jQuery.Deferred.exceptionHook(e, process.stackTrace);
                                                                }
                                                                if (depth + 1 >= maxDepth) {
                                                                    if (handler !== Thrower) {
                                                                        that = undefined;
                                                                        args = [e];
                                                                    }
                                                                    deferred.rejectWith(that, args);
                                                                }
                                                            }
                                                        };
                                                if (depth) {
                                                    process();
                                                } else {
                                                    if (jQuery.Deferred.getStackHook) {
                                                        process.stackTrace = jQuery.Deferred.getStackHook();
                                                    }
                                                    window.setTimeout(process);
                                                }
                                            };
                                        }
                                        return jQuery
                                            .Deferred(function (newDefer) {
                                                tuples[0][3].add(resolve(0, newDefer, jQuery.isFunction(onProgress) ? onProgress : Identity, newDefer.notifyWith));
                                                tuples[1][3].add(resolve(0, newDefer, jQuery.isFunction(onFulfilled) ? onFulfilled : Identity));
                                                tuples[2][3].add(resolve(0, newDefer, jQuery.isFunction(onRejected) ? onRejected : Thrower));
                                            })
                                            .promise();
                                    },
                                    promise: function (obj) {
                                        return obj != null ? jQuery.extend(obj, promise) : promise;
                                    },
                                },
                                deferred = {};
                            jQuery.each(tuples, function (i, tuple) {
                                var list = tuple[2],
                                    stateString = tuple[5];
                                promise[tuple[1]] = list.add;
                                if (stateString) {
                                    list.add(
                                        function () {
                                            state = stateString;
                                        },
                                        tuples[3 - i][2].disable,
                                        tuples[0][2].lock
                                    );
                                }
                                list.add(tuple[3].fire);
                                deferred[tuple[0]] = function () {
                                    deferred[tuple[0] + "With"](this === deferred ? undefined : this, arguments);
                                    return this;
                                };
                                deferred[tuple[0] + "With"] = list.fireWith;
                            });
                            promise.promise(deferred);
                            if (func) {
                                func.call(deferred, deferred);
                            }
                            return deferred;
                        },
                        when: function (singleValue) {
                            var remaining = arguments.length,
                                i = remaining,
                                resolveContexts = Array(i),
                                resolveValues = slice.call(arguments),
                                master = jQuery.Deferred(),
                                updateFunc = function (i) {
                                    return function (value) {
                                        resolveContexts[i] = this;
                                        resolveValues[i] = arguments.length > 1 ? slice.call(arguments) : value;
                                        if (!--remaining) {
                                            master.resolveWith(resolveContexts, resolveValues);
                                        }
                                    };
                                };
                            if (remaining <= 1) {
                                adoptValue(singleValue, master.done(updateFunc(i)).resolve, master.reject, !remaining);
                                if (master.state() === "pending" || jQuery.isFunction(resolveValues[i] && resolveValues[i].then)) {
                                    return master.then();
                                }
                            }
                            while (i--) {
                                adoptValue(resolveValues[i], updateFunc(i), master.reject);
                            }
                            return master.promise();
                        },
                    });
                    var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
                    jQuery.Deferred.exceptionHook = function (error, stack) {
                        if (window.console && window.console.warn && error && rerrorNames.test(error.name)) {
                            window.console.warn("jQuery.Deferred exception: " + error.message, error.stack, stack);
                        }
                    };
                    jQuery.readyException = function (error) {
                        window.setTimeout(function () {
                            throw error;
                        });
                    };
                    var readyList = jQuery.Deferred();
                    jQuery.fn.ready = function (fn) {
                        readyList.then(fn).catch(function (error) {
                            jQuery.readyException(error);
                        });
                        return this;
                    };
                    jQuery.extend({
                        isReady: false,
                        readyWait: 1,
                        ready: function (wait) {
                            if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
                                return;
                            }
                            jQuery.isReady = true;
                            if (wait !== true && --jQuery.readyWait > 0) {
                                return;
                            }
                            readyList.resolveWith(document, [jQuery]);
                        },
                    });
                    jQuery.ready.then = readyList.then;
                    function completed() {
                        document.removeEventListener("DOMContentLoaded", completed);
                        window.removeEventListener("load", completed);
                        jQuery.ready();
                    }
                    if (document.readyState === "complete" || (document.readyState !== "loading" && !document.documentElement.doScroll)) {
                        window.setTimeout(jQuery.ready);
                    } else {
                        document.addEventListener("DOMContentLoaded", completed);
                        window.addEventListener("load", completed);
                    }
                    var access = function (elems, fn, key, value, chainable, emptyGet, raw) {
                        var i = 0,
                            len = elems.length,
                            bulk = key == null;
                        if (jQuery.type(key) === "object") {
                            chainable = true;
                            for (i in key) {
                                access(elems, fn, i, key[i], true, emptyGet, raw);
                            }
                        } else if (value !== undefined) {
                            chainable = true;
                            if (!jQuery.isFunction(value)) {
                                raw = true;
                            }
                            if (bulk) {
                                if (raw) {
                                    fn.call(elems, value);
                                    fn = null;
                                } else {
                                    bulk = fn;
                                    fn = function (elem, key, value) {
                                        return bulk.call(jQuery(elem), value);
                                    };
                                }
                            }
                            if (fn) {
                                for (; i < len; i++) {
                                    fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
                                }
                            }
                        }
                        if (chainable) {
                            return elems;
                        }
                        if (bulk) {
                            return fn.call(elems);
                        }
                        return len ? fn(elems[0], key) : emptyGet;
                    };
                    var acceptData = function (owner) {
                        return owner.nodeType === 1 || owner.nodeType === 9 || !+owner.nodeType;
                    };
                    function Data() {
                        this.expando = jQuery.expando + Data.uid++;
                    }
                    Data.uid = 1;
                    Data.prototype = {
                        cache: function (owner) {
                            var value = owner[this.expando];
                            if (!value) {
                                value = {};
                                if (acceptData(owner)) {
                                    if (owner.nodeType) {
                                        owner[this.expando] = value;
                                    } else {
                                        Object.defineProperty(owner, this.expando, { value: value, configurable: true });
                                    }
                                }
                            }
                            return value;
                        },
                        set: function (owner, data, value) {
                            var prop,
                                cache = this.cache(owner);
                            if (typeof data === "string") {
                                cache[jQuery.camelCase(data)] = value;
                            } else {
                                for (prop in data) {
                                    cache[jQuery.camelCase(prop)] = data[prop];
                                }
                            }
                            return cache;
                        },
                        get: function (owner, key) {
                            return key === undefined ? this.cache(owner) : owner[this.expando] && owner[this.expando][jQuery.camelCase(key)];
                        },
                        access: function (owner, key, value) {
                            if (key === undefined || (key && typeof key === "string" && value === undefined)) {
                                return this.get(owner, key);
                            }
                            this.set(owner, key, value);
                            return value !== undefined ? value : key;
                        },
                        remove: function (owner, key) {
                            var i,
                                cache = owner[this.expando];
                            if (cache === undefined) {
                                return;
                            }
                            if (key !== undefined) {
                                if (Array.isArray(key)) {
                                    key = key.map(jQuery.camelCase);
                                } else {
                                    key = jQuery.camelCase(key);
                                    key = key in cache ? [key] : key.match(rnothtmlwhite) || [];
                                }
                                i = key.length;
                                while (i--) {
                                    delete cache[key[i]];
                                }
                            }
                            if (key === undefined || jQuery.isEmptyObject(cache)) {
                                if (owner.nodeType) {
                                    owner[this.expando] = undefined;
                                } else {
                                    delete owner[this.expando];
                                }
                            }
                        },
                        hasData: function (owner) {
                            var cache = owner[this.expando];
                            return cache !== undefined && !jQuery.isEmptyObject(cache);
                        },
                    };
                    var dataPriv = new Data();
                    var dataUser = new Data();
                    var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
                        rmultiDash = /[A-Z]/g;
                    function getData(data) {
                        if (data === "true") {
                            return true;
                        }
                        if (data === "false") {
                            return false;
                        }
                        if (data === "null") {
                            return null;
                        }
                        if (data === +data + "") {
                            return +data;
                        }
                        if (rbrace.test(data)) {
                            return JSON.parse(data);
                        }
                        return data;
                    }
                    function dataAttr(elem, key, data) {
                        var name;
                        if (data === undefined && elem.nodeType === 1) {
                            name = "data-" + key.replace(rmultiDash, "-$&").toLowerCase();
                            data = elem.getAttribute(name);
                            if (typeof data === "string") {
                                try {
                                    data = getData(data);
                                } catch (e) { }
                                dataUser.set(elem, key, data);
                            } else {
                                data = undefined;
                            }
                        }
                        return data;
                    }
                    jQuery.extend({
                        hasData: function (elem) {
                            return dataUser.hasData(elem) || dataPriv.hasData(elem);
                        },
                        data: function (elem, name, data) {
                            return dataUser.access(elem, name, data);
                        },
                        removeData: function (elem, name) {
                            dataUser.remove(elem, name);
                        },
                        _data: function (elem, name, data) {
                            return dataPriv.access(elem, name, data);
                        },
                        _removeData: function (elem, name) {
                            dataPriv.remove(elem, name);
                        },
                    });
                    jQuery.fn.extend({
                        data: function (key, value) {
                            var i,
                                name,
                                data,
                                elem = this[0],
                                attrs = elem && elem.attributes;
                            if (key === undefined) {
                                if (this.length) {
                                    data = dataUser.get(elem);
                                    if (elem.nodeType === 1 && !dataPriv.get(elem, "hasDataAttrs")) {
                                        i = attrs.length;
                                        while (i--) {
                                            if (attrs[i]) {
                                                name = attrs[i].name;
                                                if (name.indexOf("data-") === 0) {
                                                    name = jQuery.camelCase(name.slice(5));
                                                    dataAttr(elem, name, data[name]);
                                                }
                                            }
                                        }
                                        dataPriv.set(elem, "hasDataAttrs", true);
                                    }
                                }
                                return data;
                            }
                            if (typeof key === "object") {
                                return this.each(function () {
                                    dataUser.set(this, key);
                                });
                            }
                            return access(
                                this,
                                function (value) {
                                    var data;
                                    if (elem && value === undefined) {
                                        data = dataUser.get(elem, key);
                                        if (data !== undefined) {
                                            return data;
                                        }
                                        data = dataAttr(elem, key);
                                        if (data !== undefined) {
                                            return data;
                                        }
                                        return;
                                    }
                                    this.each(function () {
                                        dataUser.set(this, key, value);
                                    });
                                },
                                null,
                                value,
                                arguments.length > 1,
                                null,
                                true
                            );
                        },
                        removeData: function (key) {
                            return this.each(function () {
                                dataUser.remove(this, key);
                            });
                        },
                    });
                    jQuery.extend({
                        queue: function (elem, type, data) {
                            var queue;
                            if (elem) {
                                type = (type || "fx") + "queue";
                                queue = dataPriv.get(elem, type);
                                if (data) {
                                    if (!queue || Array.isArray(data)) {
                                        queue = dataPriv.access(elem, type, jQuery.makeArray(data));
                                    } else {
                                        queue.push(data);
                                    }
                                }
                                return queue || [];
                            }
                        },
                        dequeue: function (elem, type) {
                            type = type || "fx";
                            var queue = jQuery.queue(elem, type),
                                startLength = queue.length,
                                fn = queue.shift(),
                                hooks = jQuery._queueHooks(elem, type),
                                next = function () {
                                    jQuery.dequeue(elem, type);
                                };
                            if (fn === "inprogress") {
                                fn = queue.shift();
                                startLength--;
                            }
                            if (fn) {
                                if (type === "fx") {
                                    queue.unshift("inprogress");
                                }
                                delete hooks.stop;
                                fn.call(elem, next, hooks);
                            }
                            if (!startLength && hooks) {
                                hooks.empty.fire();
                            }
                        },
                        _queueHooks: function (elem, type) {
                            var key = type + "queueHooks";
                            return (
                                dataPriv.get(elem, key) ||
                                dataPriv.access(elem, key, {
                                    empty: jQuery.Callbacks("once memory").add(function () {
                                        dataPriv.remove(elem, [type + "queue", key]);
                                    }),
                                })
                            );
                        },
                    });
                    jQuery.fn.extend({
                        queue: function (type, data) {
                            var setter = 2;
                            if (typeof type !== "string") {
                                data = type;
                                type = "fx";
                                setter--;
                            }
                            if (arguments.length < setter) {
                                return jQuery.queue(this[0], type);
                            }
                            return data === undefined
                                ? this
                                : this.each(function () {
                                    var queue = jQuery.queue(this, type, data);
                                    jQuery._queueHooks(this, type);
                                    if (type === "fx" && queue[0] !== "inprogress") {
                                        jQuery.dequeue(this, type);
                                    }
                                });
                        },
                        dequeue: function (type) {
                            return this.each(function () {
                                jQuery.dequeue(this, type);
                            });
                        },
                        clearQueue: function (type) {
                            return this.queue(type || "fx", []);
                        },
                        promise: function (type, obj) {
                            var tmp,
                                count = 1,
                                defer = jQuery.Deferred(),
                                elements = this,
                                i = this.length,
                                resolve = function () {
                                    if (!--count) {
                                        defer.resolveWith(elements, [elements]);
                                    }
                                };
                            if (typeof type !== "string") {
                                obj = type;
                                type = undefined;
                            }
                            type = type || "fx";
                            while (i--) {
                                tmp = dataPriv.get(elements[i], type + "queueHooks");
                                if (tmp && tmp.empty) {
                                    count++;
                                    tmp.empty.add(resolve);
                                }
                            }
                            resolve();
                            return defer.promise(obj);
                        },
                    });
                    var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
                    var rcssNum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
                    var cssExpand = ["Top", "Right", "Bottom", "Left"];
                    var isHiddenWithinTree = function (elem, el) {
                        elem = el || elem;
                        return elem.style.display === "none" || (elem.style.display === "" && jQuery.contains(elem.ownerDocument, elem) && jQuery.css(elem, "display") === "none");
                    };
                    var swap = function (elem, options, callback, args) {
                        var ret,
                            name,
                            old = {};
                        for (name in options) {
                            old[name] = elem.style[name];
                            elem.style[name] = options[name];
                        }
                        ret = callback.apply(elem, args || []);
                        for (name in options) {
                            elem.style[name] = old[name];
                        }
                        return ret;
                    };
                    function adjustCSS(elem, prop, valueParts, tween) {
                        var adjusted,
                            scale = 1,
                            maxIterations = 20,
                            currentValue = tween
                                ? function () {
                                    return tween.cur();
                                }
                                : function () {
                                    return jQuery.css(elem, prop, "");
                                },
                            initial = currentValue(),
                            unit = (valueParts && valueParts[3]) || (jQuery.cssNumber[prop] ? "" : "px"),
                            initialInUnit = (jQuery.cssNumber[prop] || (unit !== "px" && +initial)) && rcssNum.exec(jQuery.css(elem, prop));
                        if (initialInUnit && initialInUnit[3] !== unit) {
                            unit = unit || initialInUnit[3];
                            valueParts = valueParts || [];
                            initialInUnit = +initial || 1;
                            do {
                                scale = scale || ".5";
                                initialInUnit = initialInUnit / scale;
                                jQuery.style(elem, prop, initialInUnit + unit);
                            } while (scale !== (scale = currentValue() / initial) && scale !== 1 && --maxIterations);
                        }
                        if (valueParts) {
                            initialInUnit = +initialInUnit || +initial || 0;
                            adjusted = valueParts[1] ? initialInUnit + (valueParts[1] + 1) * valueParts[2] : +valueParts[2];
                            if (tween) {
                                tween.unit = unit;
                                tween.start = initialInUnit;
                                tween.end = adjusted;
                            }
                        }
                        return adjusted;
                    }
                    var defaultDisplayMap = {};
                    function getDefaultDisplay(elem) {
                        var temp,
                            doc = elem.ownerDocument,
                            nodeName = elem.nodeName,
                            display = defaultDisplayMap[nodeName];
                        if (display) {
                            return display;
                        }
                        temp = doc.body.appendChild(doc.createElement(nodeName));
                        display = jQuery.css(temp, "display");
                        temp.parentNode.removeChild(temp);
                        if (display === "none") {
                            display = "block";
                        }
                        defaultDisplayMap[nodeName] = display;
                        return display;
                    }
                    function showHide(elements, show) {
                        var display,
                            elem,
                            values = [],
                            index = 0,
                            length = elements.length;
                        for (; index < length; index++) {
                            elem = elements[index];
                            if (!elem.style) {
                                continue;
                            }
                            display = elem.style.display;
                            if (show) {
                                if (display === "none") {
                                    values[index] = dataPriv.get(elem, "display") || null;
                                    if (!values[index]) {
                                        elem.style.display = "";
                                    }
                                }
                                if (elem.style.display === "" && isHiddenWithinTree(elem)) {
                                    values[index] = getDefaultDisplay(elem);
                                }
                            } else {
                                if (display !== "none") {
                                    values[index] = "none";
                                    dataPriv.set(elem, "display", display);
                                }
                            }
                        }
                        for (index = 0; index < length; index++) {
                            if (values[index] != null) {
                                elements[index].style.display = values[index];
                            }
                        }
                        return elements;
                    }
                    jQuery.fn.extend({
                        show: function () {
                            return showHide(this, true);
                        },
                        hide: function () {
                            return showHide(this);
                        },
                        toggle: function (state) {
                            if (typeof state === "boolean") {
                                return state ? this.show() : this.hide();
                            }
                            return this.each(function () {
                                if (isHiddenWithinTree(this)) {
                                    jQuery(this).show();
                                } else {
                                    jQuery(this).hide();
                                }
                            });
                        },
                    });
                    var rcheckableType = /^(?:checkbox|radio)$/i;
                    var rtagName = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i;
                    var rscriptType = /^$|\/(?:java|ecma)script/i;
                    var wrapMap = {
                        option: [1, "<select multiple='multiple'>", "</select>"],
                        thead: [1, "<table>", "</table>"],
                        col: [2, "<table><colgroup>", "</colgroup></table>"],
                        tr: [2, "<table><tbody>", "</tbody></table>"],
                        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                        _default: [0, "", ""],
                    };
                    wrapMap.optgroup = wrapMap.option;
                    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
                    wrapMap.th = wrapMap.td;
                    function getAll(context, tag) {
                        var ret;
                        if (typeof context.getElementsByTagName !== "undefined") {
                            ret = context.getElementsByTagName(tag || "*");
                        } else if (typeof context.querySelectorAll !== "undefined") {
                            ret = context.querySelectorAll(tag || "*");
                        } else {
                            ret = [];
                        }
                        if (tag === undefined || (tag && nodeName(context, tag))) {
                            return jQuery.merge([context], ret);
                        }
                        return ret;
                    }
                    function setGlobalEval(elems, refElements) {
                        var i = 0,
                            l = elems.length;
                        for (; i < l; i++) {
                            dataPriv.set(elems[i], "globalEval", !refElements || dataPriv.get(refElements[i], "globalEval"));
                        }
                    }
                    var rhtml = /<|&#?\w+;/;
                    function buildFragment(elems, context, scripts, selection, ignored) {
                        var elem,
                            tmp,
                            tag,
                            wrap,
                            contains,
                            j,
                            fragment = context.createDocumentFragment(),
                            nodes = [],
                            i = 0,
                            l = elems.length;
                        for (; i < l; i++) {
                            elem = elems[i];
                            if (elem || elem === 0) {
                                if (jQuery.type(elem) === "object") {
                                    jQuery.merge(nodes, elem.nodeType ? [elem] : elem);
                                } else if (!rhtml.test(elem)) {
                                    nodes.push(context.createTextNode(elem));
                                } else {
                                    tmp = tmp || fragment.appendChild(context.createElement("div"));
                                    tag = (rtagName.exec(elem) || ["", ""])[1].toLowerCase();
                                    wrap = wrapMap[tag] || wrapMap._default;
                                    tmp.innerHTML = wrap[1] + jQuery.htmlPrefilter(elem) + wrap[2];
                                    j = wrap[0];
                                    while (j--) {
                                        tmp = tmp.lastChild;
                                    }
                                    jQuery.merge(nodes, tmp.childNodes);
                                    tmp = fragment.firstChild;
                                    tmp.textContent = "";
                                }
                            }
                        }
                        fragment.textContent = "";
                        i = 0;
                        while ((elem = nodes[i++])) {
                            if (selection && jQuery.inArray(elem, selection) > -1) {
                                if (ignored) {
                                    ignored.push(elem);
                                }
                                continue;
                            }
                            contains = jQuery.contains(elem.ownerDocument, elem);
                            tmp = getAll(fragment.appendChild(elem), "script");
                            if (contains) {
                                setGlobalEval(tmp);
                            }
                            if (scripts) {
                                j = 0;
                                while ((elem = tmp[j++])) {
                                    if (rscriptType.test(elem.type || "")) {
                                        scripts.push(elem);
                                    }
                                }
                            }
                        }
                        return fragment;
                    }
                    (function () {
                        var fragment = document.createDocumentFragment(),
                            div = fragment.appendChild(document.createElement("div")),
                            input = document.createElement("input");
                        input.setAttribute("type", "radio");
                        input.setAttribute("checked", "checked");
                        input.setAttribute("name", "t");
                        div.appendChild(input);
                        support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
                        div.innerHTML = "<textarea>x</textarea>";
                        support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
                    })();
                    var documentElement = document.documentElement;
                    var rkeyEvent = /^key/,
                        rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
                        rtypenamespace = /^([^.]*)(?:\.(.+)|)/;
                    function returnTrue() {
                        return true;
                    }
                    function returnFalse() {
                        return false;
                    }
                    function safeActiveElement() {
                        try {
                            return document.activeElement;
                        } catch (err) { }
                    }
                    function on(elem, types, selector, data, fn, one) {
                        var origFn, type;
                        if (typeof types === "object") {
                            if (typeof selector !== "string") {
                                data = data || selector;
                                selector = undefined;
                            }
                            for (type in types) {
                                on(elem, type, selector, data, types[type], one);
                            }
                            return elem;
                        }
                        if (data == null && fn == null) {
                            fn = selector;
                            data = selector = undefined;
                        } else if (fn == null) {
                            if (typeof selector === "string") {
                                fn = data;
                                data = undefined;
                            } else {
                                fn = data;
                                data = selector;
                                selector = undefined;
                            }
                        }
                        if (fn === false) {
                            fn = returnFalse;
                        } else if (!fn) {
                            return elem;
                        }
                        if (one === 1) {
                            origFn = fn;
                            fn = function (event) {
                                jQuery().off(event);
                                return origFn.apply(this, arguments);
                            };
                            fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
                        }
                        return elem.each(function () {
                            jQuery.event.add(this, types, fn, data, selector);
                        });
                    }
                    jQuery.event = {
                        global: {},
                        add: function (elem, types, handler, data, selector) {
                            var handleObjIn,
                                eventHandle,
                                tmp,
                                events,
                                t,
                                handleObj,
                                special,
                                handlers,
                                type,
                                namespaces,
                                origType,
                                elemData = dataPriv.get(elem);
                            if (!elemData) {
                                return;
                            }
                            if (handler.handler) {
                                handleObjIn = handler;
                                handler = handleObjIn.handler;
                                selector = handleObjIn.selector;
                            }
                            if (selector) {
                                jQuery.find.matchesSelector(documentElement, selector);
                            }
                            if (!handler.guid) {
                                handler.guid = jQuery.guid++;
                            }
                            if (!(events = elemData.events)) {
                                events = elemData.events = {};
                            }
                            if (!(eventHandle = elemData.handle)) {
                                eventHandle = elemData.handle = function (e) {
                                    return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ? jQuery.event.dispatch.apply(elem, arguments) : undefined;
                                };
                            }
                            types = (types || "").match(rnothtmlwhite) || [""];
                            t = types.length;
                            while (t--) {
                                tmp = rtypenamespace.exec(types[t]) || [];
                                type = origType = tmp[1];
                                namespaces = (tmp[2] || "").split(".").sort();
                                if (!type) {
                                    continue;
                                }
                                special = jQuery.event.special[type] || {};
                                type = (selector ? special.delegateType : special.bindType) || type;
                                special = jQuery.event.special[type] || {};
                                handleObj = jQuery.extend(
                                    {
                                        type: type,
                                        origType: origType,
                                        data: data,
                                        handler: handler,
                                        guid: handler.guid,
                                        selector: selector,
                                        needsContext: selector && jQuery.expr.match.needsContext.test(selector),
                                        namespace: namespaces.join("."),
                                    },
                                    handleObjIn
                                );
                                if (!(handlers = events[type])) {
                                    handlers = events[type] = [];
                                    handlers.delegateCount = 0;
                                    if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                                        if (elem.addEventListener) {
                                            elem.addEventListener(type, eventHandle);
                                        }
                                    }
                                }
                                if (special.add) {
                                    special.add.call(elem, handleObj);
                                    if (!handleObj.handler.guid) {
                                        handleObj.handler.guid = handler.guid;
                                    }
                                }
                                if (selector) {
                                    handlers.splice(handlers.delegateCount++, 0, handleObj);
                                } else {
                                    handlers.push(handleObj);
                                }
                                jQuery.event.global[type] = true;
                            }
                        },
                        remove: function (elem, types, handler, selector, mappedTypes) {
                            var j,
                                origCount,
                                tmp,
                                events,
                                t,
                                handleObj,
                                special,
                                handlers,
                                type,
                                namespaces,
                                origType,
                                elemData = dataPriv.hasData(elem) && dataPriv.get(elem);
                            if (!elemData || !(events = elemData.events)) {
                                return;
                            }
                            types = (types || "").match(rnothtmlwhite) || [""];
                            t = types.length;
                            while (t--) {
                                tmp = rtypenamespace.exec(types[t]) || [];
                                type = origType = tmp[1];
                                namespaces = (tmp[2] || "").split(".").sort();
                                if (!type) {
                                    for (type in events) {
                                        jQuery.event.remove(elem, type + types[t], handler, selector, true);
                                    }
                                    continue;
                                }
                                special = jQuery.event.special[type] || {};
                                type = (selector ? special.delegateType : special.bindType) || type;
                                handlers = events[type] || [];
                                tmp = tmp[2] && new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
                                origCount = j = handlers.length;
                                while (j--) {
                                    handleObj = handlers[j];
                                    if (
                                        (mappedTypes || origType === handleObj.origType) &&
                                        (!handler || handler.guid === handleObj.guid) &&
                                        (!tmp || tmp.test(handleObj.namespace)) &&
                                        (!selector || selector === handleObj.selector || (selector === "**" && handleObj.selector))
                                    ) {
                                        handlers.splice(j, 1);
                                        if (handleObj.selector) {
                                            handlers.delegateCount--;
                                        }
                                        if (special.remove) {
                                            special.remove.call(elem, handleObj);
                                        }
                                    }
                                }
                                if (origCount && !handlers.length) {
                                    if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
                                        jQuery.removeEvent(elem, type, elemData.handle);
                                    }
                                    delete events[type];
                                }
                            }
                            if (jQuery.isEmptyObject(events)) {
                                dataPriv.remove(elem, "handle events");
                            }
                        },
                        dispatch: function (nativeEvent) {
                            var event = jQuery.event.fix(nativeEvent);
                            var i,
                                j,
                                ret,
                                matched,
                                handleObj,
                                handlerQueue,
                                args = new Array(arguments.length),
                                handlers = (dataPriv.get(this, "events") || {})[event.type] || [],
                                special = jQuery.event.special[event.type] || {};
                            args[0] = event;
                            for (i = 1; i < arguments.length; i++) {
                                args[i] = arguments[i];
                            }
                            event.delegateTarget = this;
                            if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                                return;
                            }
                            handlerQueue = jQuery.event.handlers.call(this, event, handlers);
                            i = 0;
                            while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
                                event.currentTarget = matched.elem;
                                j = 0;
                                while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
                                    if (!event.rnamespace || event.rnamespace.test(handleObj.namespace)) {
                                        event.handleObj = handleObj;
                                        event.data = handleObj.data;
                                        ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);
                                        if (ret !== undefined) {
                                            if ((event.result = ret) === false) {
                                                event.preventDefault();
                                                event.stopPropagation();
                                            }
                                        }
                                    }
                                }
                            }
                            if (special.postDispatch) {
                                special.postDispatch.call(this, event);
                            }
                            return event.result;
                        },
                        handlers: function (event, handlers) {
                            var i,
                                handleObj,
                                sel,
                                matchedHandlers,
                                matchedSelectors,
                                handlerQueue = [],
                                delegateCount = handlers.delegateCount,
                                cur = event.target;
                            if (delegateCount && cur.nodeType && !(event.type === "click" && event.button >= 1)) {
                                for (; cur !== this; cur = cur.parentNode || this) {
                                    if (cur.nodeType === 1 && !(event.type === "click" && cur.disabled === true)) {
                                        matchedHandlers = [];
                                        matchedSelectors = {};
                                        for (i = 0; i < delegateCount; i++) {
                                            handleObj = handlers[i];
                                            sel = handleObj.selector + " ";
                                            if (matchedSelectors[sel] === undefined) {
                                                matchedSelectors[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) > -1 : jQuery.find(sel, this, null, [cur]).length;
                                            }
                                            if (matchedSelectors[sel]) {
                                                matchedHandlers.push(handleObj);
                                            }
                                        }
                                        if (matchedHandlers.length) {
                                            handlerQueue.push({ elem: cur, handlers: matchedHandlers });
                                        }
                                    }
                                }
                            }
                            cur = this;
                            if (delegateCount < handlers.length) {
                                handlerQueue.push({ elem: cur, handlers: handlers.slice(delegateCount) });
                            }
                            return handlerQueue;
                        },
                        addProp: function (name, hook) {
                            Object.defineProperty(jQuery.Event.prototype, name, {
                                enumerable: true,
                                configurable: true,
                                get: jQuery.isFunction(hook)
                                    ? function () {
                                        if (this.originalEvent) {
                                            return hook(this.originalEvent);
                                        }
                                    }
                                    : function () {
                                        if (this.originalEvent) {
                                            return this.originalEvent[name];
                                        }
                                    },
                                set: function (value) {
                                    Object.defineProperty(this, name, { enumerable: true, configurable: true, writable: true, value: value });
                                },
                            });
                        },
                        fix: function (originalEvent) {
                            return originalEvent[jQuery.expando] ? originalEvent : new jQuery.Event(originalEvent);
                        },
                        special: {
                            load: { noBubble: true },
                            focus: {
                                trigger: function () {
                                    if (this !== safeActiveElement() && this.focus) {
                                        this.focus();
                                        return false;
                                    }
                                },
                                delegateType: "focusin",
                            },
                            blur: {
                                trigger: function () {
                                    if (this === safeActiveElement() && this.blur) {
                                        this.blur();
                                        return false;
                                    }
                                },
                                delegateType: "focusout",
                            },
                            click: {
                                trigger: function () {
                                    if (this.type === "checkbox" && this.click && nodeName(this, "input")) {
                                        this.click();
                                        return false;
                                    }
                                },
                                _default: function (event) {
                                    return nodeName(event.target, "a");
                                },
                            },
                            beforeunload: {
                                postDispatch: function (event) {
                                    if (event.result !== undefined && event.originalEvent) {
                                        event.originalEvent.returnValue = event.result;
                                    }
                                },
                            },
                        },
                    };
                    jQuery.removeEvent = function (elem, type, handle) {
                        if (elem.removeEventListener) {
                            elem.removeEventListener(type, handle);
                        }
                    };
                    jQuery.Event = function (src, props) {
                        if (!(this instanceof jQuery.Event)) {
                            return new jQuery.Event(src, props);
                        }
                        if (src && src.type) {
                            this.originalEvent = src;
                            this.type = src.type;
                            this.isDefaultPrevented = src.defaultPrevented || (src.defaultPrevented === undefined && src.returnValue === false) ? returnTrue : returnFalse;
                            this.target = src.target && src.target.nodeType === 3 ? src.target.parentNode : src.target;
                            this.currentTarget = src.currentTarget;
                            this.relatedTarget = src.relatedTarget;
                        } else {
                            this.type = src;
                        }
                        if (props) {
                            jQuery.extend(this, props);
                        }
                        this.timeStamp = (src && src.timeStamp) || jQuery.now();
                        this[jQuery.expando] = true;
                    };
                    jQuery.Event.prototype = {
                        constructor: jQuery.Event,
                        isDefaultPrevented: returnFalse,
                        isPropagationStopped: returnFalse,
                        isImmediatePropagationStopped: returnFalse,
                        isSimulated: false,
                        preventDefault: function () {
                            var e = this.originalEvent;
                            this.isDefaultPrevented = returnTrue;
                            if (e && !this.isSimulated) {
                                e.preventDefault();
                            }
                        },
                        stopPropagation: function () {
                            var e = this.originalEvent;
                            this.isPropagationStopped = returnTrue;
                            if (e && !this.isSimulated) {
                                e.stopPropagation();
                            }
                        },
                        stopImmediatePropagation: function () {
                            var e = this.originalEvent;
                            this.isImmediatePropagationStopped = returnTrue;
                            if (e && !this.isSimulated) {
                                e.stopImmediatePropagation();
                            }
                            this.stopPropagation();
                        },
                    };
                    jQuery.each(
                        {
                            altKey: true,
                            bubbles: true,
                            cancelable: true,
                            changedTouches: true,
                            ctrlKey: true,
                            detail: true,
                            eventPhase: true,
                            metaKey: true,
                            pageX: true,
                            pageY: true,
                            shiftKey: true,
                            view: true,
                            char: true,
                            charCode: true,
                            key: true,
                            keyCode: true,
                            button: true,
                            buttons: true,
                            clientX: true,
                            clientY: true,
                            offsetX: true,
                            offsetY: true,
                            pointerId: true,
                            pointerType: true,
                            screenX: true,
                            screenY: true,
                            targetTouches: true,
                            toElement: true,
                            touches: true,
                            which: function (event) {
                                var button = event.button;
                                if (event.which == null && rkeyEvent.test(event.type)) {
                                    return event.charCode != null ? event.charCode : event.keyCode;
                                }
                                if (!event.which && button !== undefined && rmouseEvent.test(event.type)) {
                                    if (button & 1) {
                                        return 1;
                                    }
                                    if (button & 2) {
                                        return 3;
                                    }
                                    if (button & 4) {
                                        return 2;
                                    }
                                    return 0;
                                }
                                return event.which;
                            },
                        },
                        jQuery.event.addProp
                    );
                    jQuery.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (orig, fix) {
                        jQuery.event.special[orig] = {
                            delegateType: fix,
                            bindType: fix,
                            handle: function (event) {
                                var ret,
                                    target = this,
                                    related = event.relatedTarget,
                                    handleObj = event.handleObj;
                                if (!related || (related !== target && !jQuery.contains(target, related))) {
                                    event.type = handleObj.origType;
                                    ret = handleObj.handler.apply(this, arguments);
                                    event.type = fix;
                                }
                                return ret;
                            },
                        };
                    });
                    jQuery.fn.extend({
                        on: function (types, selector, data, fn) {
                            return on(this, types, selector, data, fn);
                        },
                        one: function (types, selector, data, fn) {
                            return on(this, types, selector, data, fn, 1);
                        },
                        off: function (types, selector, fn) {
                            var handleObj, type;
                            if (types && types.preventDefault && types.handleObj) {
                                handleObj = types.handleObj;
                                jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                                return this;
                            }
                            if (typeof types === "object") {
                                for (type in types) {
                                    this.off(type, selector, types[type]);
                                }
                                return this;
                            }
                            if (selector === false || typeof selector === "function") {
                                fn = selector;
                                selector = undefined;
                            }
                            if (fn === false) {
                                fn = returnFalse;
                            }
                            return this.each(function () {
                                jQuery.event.remove(this, types, fn, selector);
                            });
                        },
                    });
                    var rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
                        rnoInnerhtml = /<script|<style|<link/i,
                        rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
                        rscriptTypeMasked = /^true\/(.*)/,
                        rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
                    function manipulationTarget(elem, content) {
                        if (nodeName(elem, "table") && nodeName(content.nodeType !== 11 ? content : content.firstChild, "tr")) {
                            return jQuery(">tbody", elem)[0] || elem;
                        }
                        return elem;
                    }
                    function disableScript(elem) {
                        elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
                        return elem;
                    }
                    function restoreScript(elem) {
                        var match = rscriptTypeMasked.exec(elem.type);
                        if (match) {
                            elem.type = match[1];
                        } else {
                            elem.removeAttribute("type");
                        }
                        return elem;
                    }
                    function cloneCopyEvent(src, dest) {
                        var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
                        if (dest.nodeType !== 1) {
                            return;
                        }
                        if (dataPriv.hasData(src)) {
                            pdataOld = dataPriv.access(src);
                            pdataCur = dataPriv.set(dest, pdataOld);
                            events = pdataOld.events;
                            if (events) {
                                delete pdataCur.handle;
                                pdataCur.events = {};
                                for (type in events) {
                                    for (i = 0, l = events[type].length; i < l; i++) {
                                        jQuery.event.add(dest, type, events[type][i]);
                                    }
                                }
                            }
                        }
                        if (dataUser.hasData(src)) {
                            udataOld = dataUser.access(src);
                            udataCur = jQuery.extend({}, udataOld);
                            dataUser.set(dest, udataCur);
                        }
                    }
                    function fixInput(src, dest) {
                        var nodeName = dest.nodeName.toLowerCase();
                        if (nodeName === "input" && rcheckableType.test(src.type)) {
                            dest.checked = src.checked;
                        } else if (nodeName === "input" || nodeName === "textarea") {
                            dest.defaultValue = src.defaultValue;
                        }
                    }
                    function domManip(collection, args, callback, ignored) {
                        args = concat.apply([], args);
                        var fragment,
                            first,
                            scripts,
                            hasScripts,
                            node,
                            doc,
                            i = 0,
                            l = collection.length,
                            iNoClone = l - 1,
                            value = args[0],
                            isFunction = jQuery.isFunction(value);
                        if (isFunction || (l > 1 && typeof value === "string" && !support.checkClone && rchecked.test(value))) {
                            return collection.each(function (index) {
                                var self = collection.eq(index);
                                if (isFunction) {
                                    args[0] = value.call(this, index, self.html());
                                }
                                domManip(self, args, callback, ignored);
                            });
                        }
                        if (l) {
                            fragment = buildFragment(args, collection[0].ownerDocument, false, collection, ignored);
                            first = fragment.firstChild;
                            if (fragment.childNodes.length === 1) {
                                fragment = first;
                            }
                            if (first || ignored) {
                                scripts = jQuery.map(getAll(fragment, "script"), disableScript);
                                hasScripts = scripts.length;
                                for (; i < l; i++) {
                                    node = fragment;
                                    if (i !== iNoClone) {
                                        node = jQuery.clone(node, true, true);
                                        if (hasScripts) {
                                            jQuery.merge(scripts, getAll(node, "script"));
                                        }
                                    }
                                    callback.call(collection[i], node, i);
                                }
                                if (hasScripts) {
                                    doc = scripts[scripts.length - 1].ownerDocument;
                                    jQuery.map(scripts, restoreScript);
                                    for (i = 0; i < hasScripts; i++) {
                                        node = scripts[i];
                                        if (rscriptType.test(node.type || "") && !dataPriv.access(node, "globalEval") && jQuery.contains(doc, node)) {
                                            if (node.src) {
                                                if (jQuery._evalUrl) {
                                                    jQuery._evalUrl(node.src);
                                                }
                                            } else {
                                                DOMEval(node.textContent.replace(rcleanScript, ""), doc);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return collection;
                    }
                    function remove(elem, selector, keepData) {
                        var node,
                            nodes = selector ? jQuery.filter(selector, elem) : elem,
                            i = 0;
                        for (; (node = nodes[i]) != null; i++) {
                            if (!keepData && node.nodeType === 1) {
                                jQuery.cleanData(getAll(node));
                            }
                            if (node.parentNode) {
                                if (keepData && jQuery.contains(node.ownerDocument, node)) {
                                    setGlobalEval(getAll(node, "script"));
                                }
                                node.parentNode.removeChild(node);
                            }
                        }
                        return elem;
                    }
                    jQuery.extend({
                        htmlPrefilter: function (html) {
                            return html.replace(rxhtmlTag, "<$1></$2>");
                        },
                        clone: function (elem, dataAndEvents, deepDataAndEvents) {
                            var i,
                                l,
                                srcElements,
                                destElements,
                                clone = elem.cloneNode(true),
                                inPage = jQuery.contains(elem.ownerDocument, elem);
                            if (!support.noCloneChecked && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                                destElements = getAll(clone);
                                srcElements = getAll(elem);
                                for (i = 0, l = srcElements.length; i < l; i++) {
                                    fixInput(srcElements[i], destElements[i]);
                                }
                            }
                            if (dataAndEvents) {
                                if (deepDataAndEvents) {
                                    srcElements = srcElements || getAll(elem);
                                    destElements = destElements || getAll(clone);
                                    for (i = 0, l = srcElements.length; i < l; i++) {
                                        cloneCopyEvent(srcElements[i], destElements[i]);
                                    }
                                } else {
                                    cloneCopyEvent(elem, clone);
                                }
                            }
                            destElements = getAll(clone, "script");
                            if (destElements.length > 0) {
                                setGlobalEval(destElements, !inPage && getAll(elem, "script"));
                            }
                            return clone;
                        },
                        cleanData: function (elems) {
                            var data,
                                elem,
                                type,
                                special = jQuery.event.special,
                                i = 0;
                            for (; (elem = elems[i]) !== undefined; i++) {
                                if (acceptData(elem)) {
                                    if ((data = elem[dataPriv.expando])) {
                                        if (data.events) {
                                            for (type in data.events) {
                                                if (special[type]) {
                                                    jQuery.event.remove(elem, type);
                                                } else {
                                                    jQuery.removeEvent(elem, type, data.handle);
                                                }
                                            }
                                        }
                                        elem[dataPriv.expando] = undefined;
                                    }
                                    if (elem[dataUser.expando]) {
                                        elem[dataUser.expando] = undefined;
                                    }
                                }
                            }
                        },
                    });
                    jQuery.fn.extend({
                        detach: function (selector) {
                            return remove(this, selector, true);
                        },
                        remove: function (selector) {
                            return remove(this, selector);
                        },
                        text: function (value) {
                            return access(
                                this,
                                function (value) {
                                    return value === undefined
                                        ? jQuery.text(this)
                                        : this.empty().each(function () {
                                            if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                                                this.textContent = value;
                                            }
                                        });
                                },
                                null,
                                value,
                                arguments.length
                            );
                        },
                        append: function () {
                            return domManip(this, arguments, function (elem) {
                                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                                    var target = manipulationTarget(this, elem);
                                    target.appendChild(elem);
                                }
                            });
                        },
                        prepend: function () {
                            return domManip(this, arguments, function (elem) {
                                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                                    var target = manipulationTarget(this, elem);
                                    target.insertBefore(elem, target.firstChild);
                                }
                            });
                        },
                        before: function () {
                            return domManip(this, arguments, function (elem) {
                                if (this.parentNode) {
                                    this.parentNode.insertBefore(elem, this);
                                }
                            });
                        },
                        after: function () {
                            return domManip(this, arguments, function (elem) {
                                if (this.parentNode) {
                                    this.parentNode.insertBefore(elem, this.nextSibling);
                                }
                            });
                        },
                        empty: function () {
                            var elem,
                                i = 0;
                            for (; (elem = this[i]) != null; i++) {
                                if (elem.nodeType === 1) {
                                    jQuery.cleanData(getAll(elem, false));
                                    elem.textContent = "";
                                }
                            }
                            return this;
                        },
                        clone: function (dataAndEvents, deepDataAndEvents) {
                            dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
                            deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
                            return this.map(function () {
                                return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
                            });
                        },
                        html: function (value) {
                            return access(
                                this,
                                function (value) {
                                    var elem = this[0] || {},
                                        i = 0,
                                        l = this.length;
                                    if (value === undefined && elem.nodeType === 1) {
                                        return elem.innerHTML;
                                    }
                                    if (typeof value === "string" && !rnoInnerhtml.test(value) && !wrapMap[(rtagName.exec(value) || ["", ""])[1].toLowerCase()]) {
                                        value = jQuery.htmlPrefilter(value);
                                        try {
                                            for (; i < l; i++) {
                                                elem = this[i] || {};
                                                if (elem.nodeType === 1) {
                                                    jQuery.cleanData(getAll(elem, false));
                                                    elem.innerHTML = value;
                                                }
                                            }
                                            elem = 0;
                                        } catch (e) { }
                                    }
                                    if (elem) {
                                        this.empty().append(value);
                                    }
                                },
                                null,
                                value,
                                arguments.length
                            );
                        },
                        replaceWith: function () {
                            var ignored = [];
                            return domManip(
                                this,
                                arguments,
                                function (elem) {
                                    var parent = this.parentNode;
                                    if (jQuery.inArray(this, ignored) < 0) {
                                        jQuery.cleanData(getAll(this));
                                        if (parent) {
                                            parent.replaceChild(elem, this);
                                        }
                                    }
                                },
                                ignored
                            );
                        },
                    });
                    jQuery.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (name, original) {
                        jQuery.fn[name] = function (selector) {
                            var elems,
                                ret = [],
                                insert = jQuery(selector),
                                last = insert.length - 1,
                                i = 0;
                            for (; i <= last; i++) {
                                elems = i === last ? this : this.clone(true);
                                jQuery(insert[i])[original](elems);
                                push.apply(ret, elems.get());
                            }
                            return this.pushStack(ret);
                        };
                    });
                    var rmargin = /^margin/;
                    var rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
                    var getStyles = function (elem) {
                        var view = elem.ownerDocument.defaultView;
                        if (!view || !view.opener) {
                            view = window;
                        }
                        return view.getComputedStyle(elem);
                    };
                    (function () {
                        function computeStyleTests() {
                            if (!div) {
                                return;
                            }
                            div.style.cssText = "box-sizing:border-box;" + "position:relative;display:block;" + "margin:auto;border:1px;padding:1px;" + "top:1%;width:50%";
                            div.innerHTML = "";
                            documentElement.appendChild(container);
                            var divStyle = window.getComputedStyle(div);
                            pixelPositionVal = divStyle.top !== "1%";
                            reliableMarginLeftVal = divStyle.marginLeft === "2px";
                            boxSizingReliableVal = divStyle.width === "4px";
                            div.style.marginRight = "50%";
                            pixelMarginRightVal = divStyle.marginRight === "4px";
                            documentElement.removeChild(container);
                            div = null;
                        }
                        var pixelPositionVal,
                            boxSizingReliableVal,
                            pixelMarginRightVal,
                            reliableMarginLeftVal,
                            container = document.createElement("div"),
                            div = document.createElement("div");
                        if (!div.style) {
                            return;
                        }
                        div.style.backgroundClip = "content-box";
                        div.cloneNode(true).style.backgroundClip = "";
                        support.clearCloneStyle = div.style.backgroundClip === "content-box";
                        container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" + "padding:0;margin-top:1px;position:absolute";
                        container.appendChild(div);
                        jQuery.extend(support, {
                            pixelPosition: function () {
                                computeStyleTests();
                                return pixelPositionVal;
                            },
                            boxSizingReliable: function () {
                                computeStyleTests();
                                return boxSizingReliableVal;
                            },
                            pixelMarginRight: function () {
                                computeStyleTests();
                                return pixelMarginRightVal;
                            },
                            reliableMarginLeft: function () {
                                computeStyleTests();
                                return reliableMarginLeftVal;
                            },
                        });
                    })();
                    function curCSS(elem, name, computed) {
                        var width,
                            minWidth,
                            maxWidth,
                            ret,
                            style = elem.style;
                        computed = computed || getStyles(elem);
                        if (computed) {
                            ret = computed.getPropertyValue(name) || computed[name];
                            if (ret === "" && !jQuery.contains(elem.ownerDocument, elem)) {
                                ret = jQuery.style(elem, name);
                            }
                            if (!support.pixelMarginRight() && rnumnonpx.test(ret) && rmargin.test(name)) {
                                width = style.width;
                                minWidth = style.minWidth;
                                maxWidth = style.maxWidth;
                                style.minWidth = style.maxWidth = style.width = ret;
                                ret = computed.width;
                                style.width = width;
                                style.minWidth = minWidth;
                                style.maxWidth = maxWidth;
                            }
                        }
                        return ret !== undefined ? ret + "" : ret;
                    }
                    function addGetHookIf(conditionFn, hookFn) {
                        return {
                            get: function () {
                                if (conditionFn()) {
                                    delete this.get;
                                    return;
                                }
                                return (this.get = hookFn).apply(this, arguments);
                            },
                        };
                    }
                    var rdisplayswap = /^(none|table(?!-c[ea]).+)/,
                        rcustomProp = /^--/,
                        cssShow = { position: "absolute", visibility: "hidden", display: "block" },
                        cssNormalTransform = { letterSpacing: "0", fontWeight: "400" },
                        cssPrefixes = ["Webkit", "Moz", "ms"],
                        emptyStyle = document.createElement("div").style;
                    function vendorPropName(name) {
                        if (name in emptyStyle) {
                            return name;
                        }
                        var capName = name[0].toUpperCase() + name.slice(1),
                            i = cssPrefixes.length;
                        while (i--) {
                            name = cssPrefixes[i] + capName;
                            if (name in emptyStyle) {
                                return name;
                            }
                        }
                    }
                    function finalPropName(name) {
                        var ret = jQuery.cssProps[name];
                        if (!ret) {
                            ret = jQuery.cssProps[name] = vendorPropName(name) || name;
                        }
                        return ret;
                    }
                    function setPositiveNumber(elem, value, subtract) {
                        var matches = rcssNum.exec(value);
                        return matches ? Math.max(0, matches[2] - (subtract || 0)) + (matches[3] || "px") : value;
                    }
                    function augmentWidthOrHeight(elem, name, extra, isBorderBox, styles) {
                        var i,
                            val = 0;
                        if (extra === (isBorderBox ? "border" : "content")) {
                            i = 4;
                        } else {
                            i = name === "width" ? 1 : 0;
                        }
                        for (; i < 4; i += 2) {
                            if (extra === "margin") {
                                val += jQuery.css(elem, extra + cssExpand[i], true, styles);
                            }
                            if (isBorderBox) {
                                if (extra === "content") {
                                    val -= jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                                }
                                if (extra !== "margin") {
                                    val -= jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                                }
                            } else {
                                val += jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                                if (extra !== "padding") {
                                    val += jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                                }
                            }
                        }
                        return val;
                    }
                    function getWidthOrHeight(elem, name, extra) {
                        var valueIsBorderBox,
                            styles = getStyles(elem),
                            val = curCSS(elem, name, styles),
                            isBorderBox = jQuery.css(elem, "boxSizing", false, styles) === "border-box";
                        if (rnumnonpx.test(val)) {
                            return val;
                        }
                        valueIsBorderBox = isBorderBox && (support.boxSizingReliable() || val === elem.style[name]);
                        if (val === "auto") {
                            val = elem["offset" + name[0].toUpperCase() + name.slice(1)];
                        }
                        val = parseFloat(val) || 0;
                        return val + augmentWidthOrHeight(elem, name, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox, styles) + "px";
                    }
                    jQuery.extend({
                        cssHooks: {
                            opacity: {
                                get: function (elem, computed) {
                                    if (computed) {
                                        var ret = curCSS(elem, "opacity");
                                        return ret === "" ? "1" : ret;
                                    }
                                },
                            },
                        },
                        cssNumber: {
                            animationIterationCount: true,
                            columnCount: true,
                            fillOpacity: true,
                            flexGrow: true,
                            flexShrink: true,
                            fontWeight: true,
                            lineHeight: true,
                            opacity: true,
                            order: true,
                            orphans: true,
                            widows: true,
                            zIndex: true,
                            zoom: true,
                        },
                        cssProps: { float: "cssFloat" },
                        style: function (elem, name, value, extra) {
                            if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                                return;
                            }
                            var ret,
                                type,
                                hooks,
                                origName = jQuery.camelCase(name),
                                isCustomProp = rcustomProp.test(name),
                                style = elem.style;
                            if (!isCustomProp) {
                                name = finalPropName(origName);
                            }
                            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
                            if (value !== undefined) {
                                type = typeof value;
                                if (type === "string" && (ret = rcssNum.exec(value)) && ret[1]) {
                                    value = adjustCSS(elem, name, ret);
                                    type = "number";
                                }
                                if (value == null || value !== value) {
                                    return;
                                }
                                if (type === "number") {
                                    value += (ret && ret[3]) || (jQuery.cssNumber[origName] ? "" : "px");
                                }
                                if (!support.clearCloneStyle && value === "" && name.indexOf("background") === 0) {
                                    style[name] = "inherit";
                                }
                                if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
                                    if (isCustomProp) {
                                        style.setProperty(name, value);
                                    } else {
                                        style[name] = value;
                                    }
                                }
                            } else {
                                if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                                    return ret;
                                }
                                return style[name];
                            }
                        },
                        css: function (elem, name, extra, styles) {
                            var val,
                                num,
                                hooks,
                                origName = jQuery.camelCase(name),
                                isCustomProp = rcustomProp.test(name);
                            if (!isCustomProp) {
                                name = finalPropName(origName);
                            }
                            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
                            if (hooks && "get" in hooks) {
                                val = hooks.get(elem, true, extra);
                            }
                            if (val === undefined) {
                                val = curCSS(elem, name, styles);
                            }
                            if (val === "normal" && name in cssNormalTransform) {
                                val = cssNormalTransform[name];
                            }
                            if (extra === "" || extra) {
                                num = parseFloat(val);
                                return extra === true || isFinite(num) ? num || 0 : val;
                            }
                            return val;
                        },
                    });
                    jQuery.each(["height", "width"], function (i, name) {
                        jQuery.cssHooks[name] = {
                            get: function (elem, computed, extra) {
                                if (computed) {
                                    return rdisplayswap.test(jQuery.css(elem, "display")) && (!elem.getClientRects().length || !elem.getBoundingClientRect().width)
                                        ? swap(elem, cssShow, function () {
                                            return getWidthOrHeight(elem, name, extra);
                                        })
                                        : getWidthOrHeight(elem, name, extra);
                                }
                            },
                            set: function (elem, value, extra) {
                                var matches,
                                    styles = extra && getStyles(elem),
                                    subtract = extra && augmentWidthOrHeight(elem, name, extra, jQuery.css(elem, "boxSizing", false, styles) === "border-box", styles);
                                if (subtract && (matches = rcssNum.exec(value)) && (matches[3] || "px") !== "px") {
                                    elem.style[name] = value;
                                    value = jQuery.css(elem, name);
                                }
                                return setPositiveNumber(elem, value, subtract);
                            },
                        };
                    });
                    jQuery.cssHooks.marginLeft = addGetHookIf(support.reliableMarginLeft, function (elem, computed) {
                        if (computed) {
                            return (
                                (parseFloat(curCSS(elem, "marginLeft")) ||
                                    elem.getBoundingClientRect().left -
                                    swap(elem, { marginLeft: 0 }, function () {
                                        return elem.getBoundingClientRect().left;
                                    })) + "px"
                            );
                        }
                    });
                    jQuery.each({ margin: "", padding: "", border: "Width" }, function (prefix, suffix) {
                        jQuery.cssHooks[prefix + suffix] = {
                            expand: function (value) {
                                var i = 0,
                                    expanded = {},
                                    parts = typeof value === "string" ? value.split(" ") : [value];
                                for (; i < 4; i++) {
                                    expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                                }
                                return expanded;
                            },
                        };
                        if (!rmargin.test(prefix)) {
                            jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
                        }
                    });
                    jQuery.fn.extend({
                        css: function (name, value) {
                            return access(
                                this,
                                function (elem, name, value) {
                                    var styles,
                                        len,
                                        map = {},
                                        i = 0;
                                    if (Array.isArray(name)) {
                                        styles = getStyles(elem);
                                        len = name.length;
                                        for (; i < len; i++) {
                                            map[name[i]] = jQuery.css(elem, name[i], false, styles);
                                        }
                                        return map;
                                    }
                                    return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
                                },
                                name,
                                value,
                                arguments.length > 1
                            );
                        },
                    });
                    function Tween(elem, options, prop, end, easing) {
                        return new Tween.prototype.init(elem, options, prop, end, easing);
                    }
                    jQuery.Tween = Tween;
                    Tween.prototype = {
                        constructor: Tween,
                        init: function (elem, options, prop, end, easing, unit) {
                            this.elem = elem;
                            this.prop = prop;
                            this.easing = easing || jQuery.easing._default;
                            this.options = options;
                            this.start = this.now = this.cur();
                            this.end = end;
                            this.unit = unit || (jQuery.cssNumber[prop] ? "" : "px");
                        },
                        cur: function () {
                            var hooks = Tween.propHooks[this.prop];
                            return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
                        },
                        run: function (percent) {
                            var eased,
                                hooks = Tween.propHooks[this.prop];
                            if (this.options.duration) {
                                this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
                            } else {
                                this.pos = eased = percent;
                            }
                            this.now = (this.end - this.start) * eased + this.start;
                            if (this.options.step) {
                                this.options.step.call(this.elem, this.now, this);
                            }
                            if (hooks && hooks.set) {
                                hooks.set(this);
                            } else {
                                Tween.propHooks._default.set(this);
                            }
                            return this;
                        },
                    };
                    Tween.prototype.init.prototype = Tween.prototype;
                    Tween.propHooks = {
                        _default: {
                            get: function (tween) {
                                var result;
                                if (tween.elem.nodeType !== 1 || (tween.elem[tween.prop] != null && tween.elem.style[tween.prop] == null)) {
                                    return tween.elem[tween.prop];
                                }
                                result = jQuery.css(tween.elem, tween.prop, "");
                                return !result || result === "auto" ? 0 : result;
                            },
                            set: function (tween) {
                                if (jQuery.fx.step[tween.prop]) {
                                    jQuery.fx.step[tween.prop](tween);
                                } else if (tween.elem.nodeType === 1 && (tween.elem.style[jQuery.cssProps[tween.prop]] != null || jQuery.cssHooks[tween.prop])) {
                                    jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
                                } else {
                                    tween.elem[tween.prop] = tween.now;
                                }
                            },
                        },
                    };
                    Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
                        set: function (tween) {
                            if (tween.elem.nodeType && tween.elem.parentNode) {
                                tween.elem[tween.prop] = tween.now;
                            }
                        },
                    };
                    jQuery.easing = {
                        linear: function (p) {
                            return p;
                        },
                        swing: function (p) {
                            return 0.5 - Math.cos(p * Math.PI) / 2;
                        },
                        _default: "swing",
                    };
                    jQuery.fx = Tween.prototype.init;
                    jQuery.fx.step = {};
                    var fxNow,
                        inProgress,
                        rfxtypes = /^(?:toggle|show|hide)$/,
                        rrun = /queueHooks$/;
                    function schedule() {
                        if (inProgress) {
                            if (document.hidden === false && window.requestAnimationFrame) {
                                window.requestAnimationFrame(schedule);
                            } else {
                                window.setTimeout(schedule, jQuery.fx.interval);
                            }
                            jQuery.fx.tick();
                        }
                    }
                    function createFxNow() {
                        window.setTimeout(function () {
                            fxNow = undefined;
                        });
                        return (fxNow = jQuery.now());
                    }
                    function genFx(type, includeWidth) {
                        var which,
                            i = 0,
                            attrs = { height: type };
                        includeWidth = includeWidth ? 1 : 0;
                        for (; i < 4; i += 2 - includeWidth) {
                            which = cssExpand[i];
                            attrs["margin" + which] = attrs["padding" + which] = type;
                        }
                        if (includeWidth) {
                            attrs.opacity = attrs.width = type;
                        }
                        return attrs;
                    }
                    function createTween(value, prop, animation) {
                        var tween,
                            collection = (Animation.tweeners[prop] || []).concat(Animation.tweeners["*"]),
                            index = 0,
                            length = collection.length;
                        for (; index < length; index++) {
                            if ((tween = collection[index].call(animation, prop, value))) {
                                return tween;
                            }
                        }
                    }
                    function defaultPrefilter(elem, props, opts) {
                        var prop,
                            value,
                            toggle,
                            hooks,
                            oldfire,
                            propTween,
                            restoreDisplay,
                            display,
                            isBox = "width" in props || "height" in props,
                            anim = this,
                            orig = {},
                            style = elem.style,
                            hidden = elem.nodeType && isHiddenWithinTree(elem),
                            dataShow = dataPriv.get(elem, "fxshow");
                        if (!opts.queue) {
                            hooks = jQuery._queueHooks(elem, "fx");
                            if (hooks.unqueued == null) {
                                hooks.unqueued = 0;
                                oldfire = hooks.empty.fire;
                                hooks.empty.fire = function () {
                                    if (!hooks.unqueued) {
                                        oldfire();
                                    }
                                };
                            }
                            hooks.unqueued++;
                            anim.always(function () {
                                anim.always(function () {
                                    hooks.unqueued--;
                                    if (!jQuery.queue(elem, "fx").length) {
                                        hooks.empty.fire();
                                    }
                                });
                            });
                        }
                        for (prop in props) {
                            value = props[prop];
                            if (rfxtypes.test(value)) {
                                delete props[prop];
                                toggle = toggle || value === "toggle";
                                if (value === (hidden ? "hide" : "show")) {
                                    if (value === "show" && dataShow && dataShow[prop] !== undefined) {
                                        hidden = true;
                                    } else {
                                        continue;
                                    }
                                }
                                orig[prop] = (dataShow && dataShow[prop]) || jQuery.style(elem, prop);
                            }
                        }
                        propTween = !jQuery.isEmptyObject(props);
                        if (!propTween && jQuery.isEmptyObject(orig)) {
                            return;
                        }
                        if (isBox && elem.nodeType === 1) {
                            opts.overflow = [style.overflow, style.overflowX, style.overflowY];
                            restoreDisplay = dataShow && dataShow.display;
                            if (restoreDisplay == null) {
                                restoreDisplay = dataPriv.get(elem, "display");
                            }
                            display = jQuery.css(elem, "display");
                            if (display === "none") {
                                if (restoreDisplay) {
                                    display = restoreDisplay;
                                } else {
                                    showHide([elem], true);
                                    restoreDisplay = elem.style.display || restoreDisplay;
                                    display = jQuery.css(elem, "display");
                                    showHide([elem]);
                                }
                            }
                            if (display === "inline" || (display === "inline-block" && restoreDisplay != null)) {
                                if (jQuery.css(elem, "float") === "none") {
                                    if (!propTween) {
                                        anim.done(function () {
                                            style.display = restoreDisplay;
                                        });
                                        if (restoreDisplay == null) {
                                            display = style.display;
                                            restoreDisplay = display === "none" ? "" : display;
                                        }
                                    }
                                    style.display = "inline-block";
                                }
                            }
                        }
                        if (opts.overflow) {
                            style.overflow = "hidden";
                            anim.always(function () {
                                style.overflow = opts.overflow[0];
                                style.overflowX = opts.overflow[1];
                                style.overflowY = opts.overflow[2];
                            });
                        }
                        propTween = false;
                        for (prop in orig) {
                            if (!propTween) {
                                if (dataShow) {
                                    if ("hidden" in dataShow) {
                                        hidden = dataShow.hidden;
                                    }
                                } else {
                                    dataShow = dataPriv.access(elem, "fxshow", { display: restoreDisplay });
                                }
                                if (toggle) {
                                    dataShow.hidden = !hidden;
                                }
                                if (hidden) {
                                    showHide([elem], true);
                                }
                                anim.done(function () {
                                    if (!hidden) {
                                        showHide([elem]);
                                    }
                                    dataPriv.remove(elem, "fxshow");
                                    for (prop in orig) {
                                        jQuery.style(elem, prop, orig[prop]);
                                    }
                                });
                            }
                            propTween = createTween(hidden ? dataShow[prop] : 0, prop, anim);
                            if (!(prop in dataShow)) {
                                dataShow[prop] = propTween.start;
                                if (hidden) {
                                    propTween.end = propTween.start;
                                    propTween.start = 0;
                                }
                            }
                        }
                    }
                    function propFilter(props, specialEasing) {
                        var index, name, easing, value, hooks;
                        for (index in props) {
                            name = jQuery.camelCase(index);
                            easing = specialEasing[name];
                            value = props[index];
                            if (Array.isArray(value)) {
                                easing = value[1];
                                value = props[index] = value[0];
                            }
                            if (index !== name) {
                                props[name] = value;
                                delete props[index];
                            }
                            hooks = jQuery.cssHooks[name];
                            if (hooks && "expand" in hooks) {
                                value = hooks.expand(value);
                                delete props[name];
                                for (index in value) {
                                    if (!(index in props)) {
                                        props[index] = value[index];
                                        specialEasing[index] = easing;
                                    }
                                }
                            } else {
                                specialEasing[name] = easing;
                            }
                        }
                    }
                    function Animation(elem, properties, options) {
                        var result,
                            stopped,
                            index = 0,
                            length = Animation.prefilters.length,
                            deferred = jQuery.Deferred().always(function () {
                                delete tick.elem;
                            }),
                            tick = function () {
                                if (stopped) {
                                    return false;
                                }
                                var currentTime = fxNow || createFxNow(),
                                    remaining = Math.max(0, animation.startTime + animation.duration - currentTime),
                                    temp = remaining / animation.duration || 0,
                                    percent = 1 - temp,
                                    index = 0,
                                    length = animation.tweens.length;
                                for (; index < length; index++) {
                                    animation.tweens[index].run(percent);
                                }
                                deferred.notifyWith(elem, [animation, percent, remaining]);
                                if (percent < 1 && length) {
                                    return remaining;
                                }
                                if (!length) {
                                    deferred.notifyWith(elem, [animation, 1, 0]);
                                }
                                deferred.resolveWith(elem, [animation]);
                                return false;
                            },
                            animation = deferred.promise({
                                elem: elem,
                                props: jQuery.extend({}, properties),
                                opts: jQuery.extend(true, { specialEasing: {}, easing: jQuery.easing._default }, options),
                                originalProperties: properties,
                                originalOptions: options,
                                startTime: fxNow || createFxNow(),
                                duration: options.duration,
                                tweens: [],
                                createTween: function (prop, end) {
                                    var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                                    animation.tweens.push(tween);
                                    return tween;
                                },
                                stop: function (gotoEnd) {
                                    var index = 0,
                                        length = gotoEnd ? animation.tweens.length : 0;
                                    if (stopped) {
                                        return this;
                                    }
                                    stopped = true;
                                    for (; index < length; index++) {
                                        animation.tweens[index].run(1);
                                    }
                                    if (gotoEnd) {
                                        deferred.notifyWith(elem, [animation, 1, 0]);
                                        deferred.resolveWith(elem, [animation, gotoEnd]);
                                    } else {
                                        deferred.rejectWith(elem, [animation, gotoEnd]);
                                    }
                                    return this;
                                },
                            }),
                            props = animation.props;
                        propFilter(props, animation.opts.specialEasing);
                        for (; index < length; index++) {
                            result = Animation.prefilters[index].call(animation, elem, props, animation.opts);
                            if (result) {
                                if (jQuery.isFunction(result.stop)) {
                                    jQuery._queueHooks(animation.elem, animation.opts.queue).stop = jQuery.proxy(result.stop, result);
                                }
                                return result;
                            }
                        }
                        jQuery.map(props, createTween, animation);
                        if (jQuery.isFunction(animation.opts.start)) {
                            animation.opts.start.call(elem, animation);
                        }
                        animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
                        jQuery.fx.timer(jQuery.extend(tick, { elem: elem, anim: animation, queue: animation.opts.queue }));
                        return animation;
                    }
                    jQuery.Animation = jQuery.extend(Animation, {
                        tweeners: {
                            "*": [
                                function (prop, value) {
                                    var tween = this.createTween(prop, value);
                                    adjustCSS(tween.elem, prop, rcssNum.exec(value), tween);
                                    return tween;
                                },
                            ],
                        },
                        tweener: function (props, callback) {
                            if (jQuery.isFunction(props)) {
                                callback = props;
                                props = ["*"];
                            } else {
                                props = props.match(rnothtmlwhite);
                            }
                            var prop,
                                index = 0,
                                length = props.length;
                            for (; index < length; index++) {
                                prop = props[index];
                                Animation.tweeners[prop] = Animation.tweeners[prop] || [];
                                Animation.tweeners[prop].unshift(callback);
                            }
                        },
                        prefilters: [defaultPrefilter],
                        prefilter: function (callback, prepend) {
                            if (prepend) {
                                Animation.prefilters.unshift(callback);
                            } else {
                                Animation.prefilters.push(callback);
                            }
                        },
                    });
                    jQuery.speed = function (speed, easing, fn) {
                        var opt =
                            speed && typeof speed === "object"
                                ? jQuery.extend({}, speed)
                                : { complete: fn || (!fn && easing) || (jQuery.isFunction(speed) && speed), duration: speed, easing: (fn && easing) || (easing && !jQuery.isFunction(easing) && easing) };
                        if (jQuery.fx.off) {
                            opt.duration = 0;
                        } else {
                            if (typeof opt.duration !== "number") {
                                if (opt.duration in jQuery.fx.speeds) {
                                    opt.duration = jQuery.fx.speeds[opt.duration];
                                } else {
                                    opt.duration = jQuery.fx.speeds._default;
                                }
                            }
                        }
                        if (opt.queue == null || opt.queue === true) {
                            opt.queue = "fx";
                        }
                        opt.old = opt.complete;
                        opt.complete = function () {
                            if (jQuery.isFunction(opt.old)) {
                                opt.old.call(this);
                            }
                            if (opt.queue) {
                                jQuery.dequeue(this, opt.queue);
                            }
                        };
                        return opt;
                    };
                    jQuery.fn.extend({
                        fadeTo: function (speed, to, easing, callback) {
                            return this.filter(isHiddenWithinTree).css("opacity", 0).show().end().animate({ opacity: to }, speed, easing, callback);
                        },
                        animate: function (prop, speed, easing, callback) {
                            var empty = jQuery.isEmptyObject(prop),
                                optall = jQuery.speed(speed, easing, callback),
                                doAnimation = function () {
                                    var anim = Animation(this, jQuery.extend({}, prop), optall);
                                    if (empty || dataPriv.get(this, "finish")) {
                                        anim.stop(true);
                                    }
                                };
                            doAnimation.finish = doAnimation;
                            return empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
                        },
                        stop: function (type, clearQueue, gotoEnd) {
                            var stopQueue = function (hooks) {
                                var stop = hooks.stop;
                                delete hooks.stop;
                                stop(gotoEnd);
                            };
                            if (typeof type !== "string") {
                                gotoEnd = clearQueue;
                                clearQueue = type;
                                type = undefined;
                            }
                            if (clearQueue && type !== false) {
                                this.queue(type || "fx", []);
                            }
                            return this.each(function () {
                                var dequeue = true,
                                    index = type != null && type + "queueHooks",
                                    timers = jQuery.timers,
                                    data = dataPriv.get(this);
                                if (index) {
                                    if (data[index] && data[index].stop) {
                                        stopQueue(data[index]);
                                    }
                                } else {
                                    for (index in data) {
                                        if (data[index] && data[index].stop && rrun.test(index)) {
                                            stopQueue(data[index]);
                                        }
                                    }
                                }
                                for (index = timers.length; index--;) {
                                    if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                                        timers[index].anim.stop(gotoEnd);
                                        dequeue = false;
                                        timers.splice(index, 1);
                                    }
                                }
                                if (dequeue || !gotoEnd) {
                                    jQuery.dequeue(this, type);
                                }
                            });
                        },
                        finish: function (type) {
                            if (type !== false) {
                                type = type || "fx";
                            }
                            return this.each(function () {
                                var index,
                                    data = dataPriv.get(this),
                                    queue = data[type + "queue"],
                                    hooks = data[type + "queueHooks"],
                                    timers = jQuery.timers,
                                    length = queue ? queue.length : 0;
                                data.finish = true;
                                jQuery.queue(this, type, []);
                                if (hooks && hooks.stop) {
                                    hooks.stop.call(this, true);
                                }
                                for (index = timers.length; index--;) {
                                    if (timers[index].elem === this && timers[index].queue === type) {
                                        timers[index].anim.stop(true);
                                        timers.splice(index, 1);
                                    }
                                }
                                for (index = 0; index < length; index++) {
                                    if (queue[index] && queue[index].finish) {
                                        queue[index].finish.call(this);
                                    }
                                }
                                delete data.finish;
                            });
                        },
                    });
                    jQuery.each(["toggle", "show", "hide"], function (i, name) {
                        var cssFn = jQuery.fn[name];
                        jQuery.fn[name] = function (speed, easing, callback) {
                            return speed == null || typeof speed === "boolean" ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
                        };
                    });
                    jQuery.each({ slideDown: genFx("show"), slideUp: genFx("hide"), slideToggle: genFx("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (name, props) {
                        jQuery.fn[name] = function (speed, easing, callback) {
                            return this.animate(props, speed, easing, callback);
                        };
                    });
                    jQuery.timers = [];
                    jQuery.fx.tick = function () {
                        var timer,
                            i = 0,
                            timers = jQuery.timers;
                        fxNow = jQuery.now();
                        for (; i < timers.length; i++) {
                            timer = timers[i];
                            if (!timer() && timers[i] === timer) {
                                timers.splice(i--, 1);
                            }
                        }
                        if (!timers.length) {
                            jQuery.fx.stop();
                        }
                        fxNow = undefined;
                    };
                    jQuery.fx.timer = function (timer) {
                        jQuery.timers.push(timer);
                        jQuery.fx.start();
                    };
                    jQuery.fx.interval = 13;
                    jQuery.fx.start = function () {
                        if (inProgress) {
                            return;
                        }
                        inProgress = true;
                        schedule();
                    };
                    jQuery.fx.stop = function () {
                        inProgress = null;
                    };
                    jQuery.fx.speeds = { slow: 600, fast: 200, _default: 400 };
                    jQuery.fn.delay = function (time, type) {
                        time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
                        type = type || "fx";
                        return this.queue(type, function (next, hooks) {
                            var timeout = window.setTimeout(next, time);
                            hooks.stop = function () {
                                window.clearTimeout(timeout);
                            };
                        });
                    };
                    (function () {
                        var input = document.createElement("input"),
                            select = document.createElement("select"),
                            opt = select.appendChild(document.createElement("option"));
                        input.type = "checkbox";
                        support.checkOn = input.value !== "";
                        support.optSelected = opt.selected;
                        input = document.createElement("input");
                        input.value = "t";
                        input.type = "radio";
                        support.radioValue = input.value === "t";
                    })();
                    var boolHook,
                        attrHandle = jQuery.expr.attrHandle;
                    jQuery.fn.extend({
                        attr: function (name, value) {
                            return access(this, jQuery.attr, name, value, arguments.length > 1);
                        },
                        removeAttr: function (name) {
                            return this.each(function () {
                                jQuery.removeAttr(this, name);
                            });
                        },
                    });
                    jQuery.extend({
                        attr: function (elem, name, value) {
                            var ret,
                                hooks,
                                nType = elem.nodeType;
                            if (nType === 3 || nType === 8 || nType === 2) {
                                return;
                            }
                            if (typeof elem.getAttribute === "undefined") {
                                return jQuery.prop(elem, name, value);
                            }
                            if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                                hooks = jQuery.attrHooks[name.toLowerCase()] || (jQuery.expr.match.bool.test(name) ? boolHook : undefined);
                            }
                            if (value !== undefined) {
                                if (value === null) {
                                    jQuery.removeAttr(elem, name);
                                    return;
                                }
                                if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                                    return ret;
                                }
                                elem.setAttribute(name, value + "");
                                return value;
                            }
                            if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
                                return ret;
                            }
                            ret = jQuery.find.attr(elem, name);
                            return ret == null ? undefined : ret;
                        },
                        attrHooks: {
                            type: {
                                set: function (elem, value) {
                                    if (!support.radioValue && value === "radio" && nodeName(elem, "input")) {
                                        var val = elem.value;
                                        elem.setAttribute("type", value);
                                        if (val) {
                                            elem.value = val;
                                        }
                                        return value;
                                    }
                                },
                            },
                        },
                        removeAttr: function (elem, value) {
                            var name,
                                i = 0,
                                attrNames = value && value.match(rnothtmlwhite);
                            if (attrNames && elem.nodeType === 1) {
                                while ((name = attrNames[i++])) {
                                    elem.removeAttribute(name);
                                }
                            }
                        },
                    });
                    boolHook = {
                        set: function (elem, value, name) {
                            if (value === false) {
                                jQuery.removeAttr(elem, name);
                            } else {
                                elem.setAttribute(name, name);
                            }
                            return name;
                        },
                    };
                    jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function (i, name) {
                        var getter = attrHandle[name] || jQuery.find.attr;
                        attrHandle[name] = function (elem, name, isXML) {
                            var ret,
                                handle,
                                lowercaseName = name.toLowerCase();
                            if (!isXML) {
                                handle = attrHandle[lowercaseName];
                                attrHandle[lowercaseName] = ret;
                                ret = getter(elem, name, isXML) != null ? lowercaseName : null;
                                attrHandle[lowercaseName] = handle;
                            }
                            return ret;
                        };
                    });
                    var rfocusable = /^(?:input|select|textarea|button)$/i,
                        rclickable = /^(?:a|area)$/i;
                    jQuery.fn.extend({
                        prop: function (name, value) {
                            return access(this, jQuery.prop, name, value, arguments.length > 1);
                        },
                        removeProp: function (name) {
                            return this.each(function () {
                                delete this[jQuery.propFix[name] || name];
                            });
                        },
                    });
                    jQuery.extend({
                        prop: function (elem, name, value) {
                            var ret,
                                hooks,
                                nType = elem.nodeType;
                            if (nType === 3 || nType === 8 || nType === 2) {
                                return;
                            }
                            if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                                name = jQuery.propFix[name] || name;
                                hooks = jQuery.propHooks[name];
                            }
                            if (value !== undefined) {
                                if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                                    return ret;
                                }
                                return (elem[name] = value);
                            }
                            if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
                                return ret;
                            }
                            return elem[name];
                        },
                        propHooks: {
                            tabIndex: {
                                get: function (elem) {
                                    var tabindex = jQuery.find.attr(elem, "tabindex");
                                    if (tabindex) {
                                        return parseInt(tabindex, 10);
                                    }
                                    if (rfocusable.test(elem.nodeName) || (rclickable.test(elem.nodeName) && elem.href)) {
                                        return 0;
                                    }
                                    return -1;
                                },
                            },
                        },
                        propFix: { for: "htmlFor", class: "className" },
                    });
                    if (!support.optSelected) {
                        jQuery.propHooks.selected = {
                            get: function (elem) {
                                var parent = elem.parentNode;
                                if (parent && parent.parentNode) {
                                    parent.parentNode.selectedIndex;
                                }
                                return null;
                            },
                            set: function (elem) {
                                var parent = elem.parentNode;
                                if (parent) {
                                    parent.selectedIndex;
                                    if (parent.parentNode) {
                                        parent.parentNode.selectedIndex;
                                    }
                                }
                            },
                        };
                    }
                    jQuery.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
                        jQuery.propFix[this.toLowerCase()] = this;
                    });
                    function stripAndCollapse(value) {
                        var tokens = value.match(rnothtmlwhite) || [];
                        return tokens.join(" ");
                    }
                    function getClass(elem) {
                        return (elem.getAttribute && elem.getAttribute("class")) || "";
                    }
                    jQuery.fn.extend({
                        addClass: function (value) {
                            var classes,
                                elem,
                                cur,
                                curValue,
                                clazz,
                                j,
                                finalValue,
                                i = 0;
                            if (jQuery.isFunction(value)) {
                                return this.each(function (j) {
                                    jQuery(this).addClass(value.call(this, j, getClass(this)));
                                });
                            }
                            if (typeof value === "string" && value) {
                                classes = value.match(rnothtmlwhite) || [];
                                while ((elem = this[i++])) {
                                    curValue = getClass(elem);
                                    cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";
                                    if (cur) {
                                        j = 0;
                                        while ((clazz = classes[j++])) {
                                            if (cur.indexOf(" " + clazz + " ") < 0) {
                                                cur += clazz + " ";
                                            }
                                        }
                                        finalValue = stripAndCollapse(cur);
                                        if (curValue !== finalValue) {
                                            elem.setAttribute("class", finalValue);
                                        }
                                    }
                                }
                            }
                            return this;
                        },
                        removeClass: function (value) {
                            var classes,
                                elem,
                                cur,
                                curValue,
                                clazz,
                                j,
                                finalValue,
                                i = 0;
                            if (jQuery.isFunction(value)) {
                                return this.each(function (j) {
                                    jQuery(this).removeClass(value.call(this, j, getClass(this)));
                                });
                            }
                            if (!arguments.length) {
                                return this.attr("class", "");
                            }
                            if (typeof value === "string" && value) {
                                classes = value.match(rnothtmlwhite) || [];
                                while ((elem = this[i++])) {
                                    curValue = getClass(elem);
                                    cur = elem.nodeType === 1 && " " + stripAndCollapse(curValue) + " ";
                                    if (cur) {
                                        j = 0;
                                        while ((clazz = classes[j++])) {
                                            while (cur.indexOf(" " + clazz + " ") > -1) {
                                                cur = cur.replace(" " + clazz + " ", " ");
                                            }
                                        }
                                        finalValue = stripAndCollapse(cur);
                                        if (curValue !== finalValue) {
                                            elem.setAttribute("class", finalValue);
                                        }
                                    }
                                }
                            }
                            return this;
                        },
                        toggleClass: function (value, stateVal) {
                            var type = typeof value;
                            if (typeof stateVal === "boolean" && type === "string") {
                                return stateVal ? this.addClass(value) : this.removeClass(value);
                            }
                            if (jQuery.isFunction(value)) {
                                return this.each(function (i) {
                                    jQuery(this).toggleClass(value.call(this, i, getClass(this), stateVal), stateVal);
                                });
                            }
                            return this.each(function () {
                                var className, i, self, classNames;
                                if (type === "string") {
                                    i = 0;
                                    self = jQuery(this);
                                    classNames = value.match(rnothtmlwhite) || [];
                                    while ((className = classNames[i++])) {
                                        if (self.hasClass(className)) {
                                            self.removeClass(className);
                                        } else {
                                            self.addClass(className);
                                        }
                                    }
                                } else if (value === undefined || type === "boolean") {
                                    className = getClass(this);
                                    if (className) {
                                        dataPriv.set(this, "__className__", className);
                                    }
                                    if (this.setAttribute) {
                                        this.setAttribute("class", className || value === false ? "" : dataPriv.get(this, "__className__") || "");
                                    }
                                }
                            });
                        },
                        hasClass: function (selector) {
                            var className,
                                elem,
                                i = 0;
                            className = " " + selector + " ";
                            while ((elem = this[i++])) {
                                if (elem.nodeType === 1 && (" " + stripAndCollapse(getClass(elem)) + " ").indexOf(className) > -1) {
                                    return true;
                                }
                            }
                            return false;
                        },
                    });
                    var rreturn = /\r/g;
                    jQuery.fn.extend({
                        val: function (value) {
                            var hooks,
                                ret,
                                isFunction,
                                elem = this[0];
                            if (!arguments.length) {
                                if (elem) {
                                    hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];
                                    if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
                                        return ret;
                                    }
                                    ret = elem.value;
                                    if (typeof ret === "string") {
                                        return ret.replace(rreturn, "");
                                    }
                                    return ret == null ? "" : ret;
                                }
                                return;
                            }
                            isFunction = jQuery.isFunction(value);
                            return this.each(function (i) {
                                var val;
                                if (this.nodeType !== 1) {
                                    return;
                                }
                                if (isFunction) {
                                    val = value.call(this, i, jQuery(this).val());
                                } else {
                                    val = value;
                                }
                                if (val == null) {
                                    val = "";
                                } else if (typeof val === "number") {
                                    val += "";
                                } else if (Array.isArray(val)) {
                                    val = jQuery.map(val, function (value) {
                                        return value == null ? "" : value + "";
                                    });
                                }
                                hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];
                                if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
                                    this.value = val;
                                }
                            });
                        },
                    });
                    jQuery.extend({
                        valHooks: {
                            option: {
                                get: function (elem) {
                                    var val = jQuery.find.attr(elem, "value");
                                    return val != null ? val : stripAndCollapse(jQuery.text(elem));
                                },
                            },
                            select: {
                                get: function (elem) {
                                    var value,
                                        option,
                                        i,
                                        options = elem.options,
                                        index = elem.selectedIndex,
                                        one = elem.type === "select-one",
                                        values = one ? null : [],
                                        max = one ? index + 1 : options.length;
                                    if (index < 0) {
                                        i = max;
                                    } else {
                                        i = one ? index : 0;
                                    }
                                    for (; i < max; i++) {
                                        option = options[i];
                                        if ((option.selected || i === index) && !option.disabled && (!option.parentNode.disabled || !nodeName(option.parentNode, "optgroup"))) {
                                            value = jQuery(option).val();
                                            if (one) {
                                                return value;
                                            }
                                            values.push(value);
                                        }
                                    }
                                    return values;
                                },
                                set: function (elem, value) {
                                    var optionSet,
                                        option,
                                        options = elem.options,
                                        values = jQuery.makeArray(value),
                                        i = options.length;
                                    while (i--) {
                                        option = options[i];
                                        if ((option.selected = jQuery.inArray(jQuery.valHooks.option.get(option), values) > -1)) {
                                            optionSet = true;
                                        }
                                    }
                                    if (!optionSet) {
                                        elem.selectedIndex = -1;
                                    }
                                    return values;
                                },
                            },
                        },
                    });
                    jQuery.each(["radio", "checkbox"], function () {
                        jQuery.valHooks[this] = {
                            set: function (elem, value) {
                                if (Array.isArray(value)) {
                                    return (elem.checked = jQuery.inArray(jQuery(elem).val(), value) > -1);
                                }
                            },
                        };
                        if (!support.checkOn) {
                            jQuery.valHooks[this].get = function (elem) {
                                return elem.getAttribute("value") === null ? "on" : elem.value;
                            };
                        }
                    });
                    var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;
                    jQuery.extend(jQuery.event, {
                        trigger: function (event, data, elem, onlyHandlers) {
                            var i,
                                cur,
                                tmp,
                                bubbleType,
                                ontype,
                                handle,
                                special,
                                eventPath = [elem || document],
                                type = hasOwn.call(event, "type") ? event.type : event,
                                namespaces = hasOwn.call(event, "namespace") ? event.namespace.split(".") : [];
                            cur = tmp = elem = elem || document;
                            if (elem.nodeType === 3 || elem.nodeType === 8) {
                                return;
                            }
                            if (rfocusMorph.test(type + jQuery.event.triggered)) {
                                return;
                            }
                            if (type.indexOf(".") > -1) {
                                namespaces = type.split(".");
                                type = namespaces.shift();
                                namespaces.sort();
                            }
                            ontype = type.indexOf(":") < 0 && "on" + type;
                            event = event[jQuery.expando] ? event : new jQuery.Event(type, typeof event === "object" && event);
                            event.isTrigger = onlyHandlers ? 2 : 3;
                            event.namespace = namespaces.join(".");
                            event.rnamespace = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
                            event.result = undefined;
                            if (!event.target) {
                                event.target = elem;
                            }
                            data = data == null ? [event] : jQuery.makeArray(data, [event]);
                            special = jQuery.event.special[type] || {};
                            if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
                                return;
                            }
                            if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                                bubbleType = special.delegateType || type;
                                if (!rfocusMorph.test(bubbleType + type)) {
                                    cur = cur.parentNode;
                                }
                                for (; cur; cur = cur.parentNode) {
                                    eventPath.push(cur);
                                    tmp = cur;
                                }
                                if (tmp === (elem.ownerDocument || document)) {
                                    eventPath.push(tmp.defaultView || tmp.parentWindow || window);
                                }
                            }
                            i = 0;
                            while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
                                event.type = i > 1 ? bubbleType : special.bindType || type;
                                handle = (dataPriv.get(cur, "events") || {})[event.type] && dataPriv.get(cur, "handle");
                                if (handle) {
                                    handle.apply(cur, data);
                                }
                                handle = ontype && cur[ontype];
                                if (handle && handle.apply && acceptData(cur)) {
                                    event.result = handle.apply(cur, data);
                                    if (event.result === false) {
                                        event.preventDefault();
                                    }
                                }
                            }
                            event.type = type;
                            if (!onlyHandlers && !event.isDefaultPrevented()) {
                                if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && acceptData(elem)) {
                                    if (ontype && jQuery.isFunction(elem[type]) && !jQuery.isWindow(elem)) {
                                        tmp = elem[ontype];
                                        if (tmp) {
                                            elem[ontype] = null;
                                        }
                                        jQuery.event.triggered = type;
                                        elem[type]();
                                        jQuery.event.triggered = undefined;
                                        if (tmp) {
                                            elem[ontype] = tmp;
                                        }
                                    }
                                }
                            }
                            return event.result;
                        },
                        simulate: function (type, elem, event) {
                            var e = jQuery.extend(new jQuery.Event(), event, { type: type, isSimulated: true });
                            jQuery.event.trigger(e, null, elem);
                        },
                    });
                    jQuery.fn.extend({
                        trigger: function (type, data) {
                            return this.each(function () {
                                jQuery.event.trigger(type, data, this);
                            });
                        },
                        triggerHandler: function (type, data) {
                            var elem = this[0];
                            if (elem) {
                                return jQuery.event.trigger(type, data, elem, true);
                            }
                        },
                    });
                    jQuery.each(
                        ("blur focus focusin focusout resize scroll click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup contextmenu").split(" "),
                        function (i, name) {
                            jQuery.fn[name] = function (data, fn) {
                                return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
                            };
                        }
                    );
                    jQuery.fn.extend({
                        hover: function (fnOver, fnOut) {
                            return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
                        },
                    });
                    support.focusin = "onfocusin" in window;
                    if (!support.focusin) {
                        jQuery.each({ focus: "focusin", blur: "focusout" }, function (orig, fix) {
                            var handler = function (event) {
                                jQuery.event.simulate(fix, event.target, jQuery.event.fix(event));
                            };
                            jQuery.event.special[fix] = {
                                setup: function () {
                                    var doc = this.ownerDocument || this,
                                        attaches = dataPriv.access(doc, fix);
                                    if (!attaches) {
                                        doc.addEventListener(orig, handler, true);
                                    }
                                    dataPriv.access(doc, fix, (attaches || 0) + 1);
                                },
                                teardown: function () {
                                    var doc = this.ownerDocument || this,
                                        attaches = dataPriv.access(doc, fix) - 1;
                                    if (!attaches) {
                                        doc.removeEventListener(orig, handler, true);
                                        dataPriv.remove(doc, fix);
                                    } else {
                                        dataPriv.access(doc, fix, attaches);
                                    }
                                },
                            };
                        });
                    }
                    var location = window.location;
                    var nonce = jQuery.now();
                    var rquery = /\?/;
                    jQuery.parseXML = function (data) {
                        var xml;
                        if (!data || typeof data !== "string") {
                            return null;
                        }
                        try {
                            xml = new window.DOMParser().parseFromString(data, "text/xml");
                        } catch (e) {
                            xml = undefined;
                        }
                        if (!xml || xml.getElementsByTagName("parsererror").length) {
                            jQuery.error("Invalid XML: " + data);
                        }
                        return xml;
                    };
                    var rbracket = /\[\]$/,
                        rCRLF = /\r?\n/g,
                        rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
                        rsubmittable = /^(?:input|select|textarea|keygen)/i;
                    function buildParams(prefix, obj, traditional, add) {
                        var name;
                        if (Array.isArray(obj)) {
                            jQuery.each(obj, function (i, v) {
                                if (traditional || rbracket.test(prefix)) {
                                    add(prefix, v);
                                } else {
                                    buildParams(prefix + "[" + (typeof v === "object" && v != null ? i : "") + "]", v, traditional, add);
                                }
                            });
                        } else if (!traditional && jQuery.type(obj) === "object") {
                            for (name in obj) {
                                buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
                            }
                        } else {
                            add(prefix, obj);
                        }
                    }
                    jQuery.param = function (a, traditional) {
                        var prefix,
                            s = [],
                            add = function (key, valueOrFunction) {
                                var value = jQuery.isFunction(valueOrFunction) ? valueOrFunction() : valueOrFunction;
                                s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value == null ? "" : value);
                            };
                        if (Array.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) {
                            jQuery.each(a, function () {
                                add(this.name, this.value);
                            });
                        } else {
                            for (prefix in a) {
                                buildParams(prefix, a[prefix], traditional, add);
                            }
                        }
                        return s.join("&");
                    };
                    jQuery.fn.extend({
                        serialize: function () {
                            return jQuery.param(this.serializeArray());
                        },
                        serializeArray: function () {
                            return this.map(function () {
                                var elements = jQuery.prop(this, "elements");
                                return elements ? jQuery.makeArray(elements) : this;
                            })
                                .filter(function () {
                                    var type = this.type;
                                    return this.name && !jQuery(this).is(":disabled") && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
                                })
                                .map(function (i, elem) {
                                    var val = jQuery(this).val();
                                    if (val == null) {
                                        return null;
                                    }
                                    if (Array.isArray(val)) {
                                        return jQuery.map(val, function (val) {
                                            return { name: elem.name, value: val.replace(rCRLF, "\r\n") };
                                        });
                                    }
                                    return { name: elem.name, value: val.replace(rCRLF, "\r\n") };
                                })
                                .get();
                        },
                    });
                    var r20 = /%20/g,
                        rhash = /#.*$/,
                        rantiCache = /([?&])_=[^&]*/,
                        rheaders = /^(.*?):[ \t]*([^\r\n]*)$/gm,
                        rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
                        rnoContent = /^(?:GET|HEAD)$/,
                        rprotocol = /^\/\//,
                        prefilters = {},
                        transports = {},
                        allTypes = "*/".concat("*"),
                        originAnchor = document.createElement("a");
                    originAnchor.href = location.href;
                    function addToPrefiltersOrTransports(structure) {
                        return function (dataTypeExpression, func) {
                            if (typeof dataTypeExpression !== "string") {
                                func = dataTypeExpression;
                                dataTypeExpression = "*";
                            }
                            var dataType,
                                i = 0,
                                dataTypes = dataTypeExpression.toLowerCase().match(rnothtmlwhite) || [];
                            if (jQuery.isFunction(func)) {
                                while ((dataType = dataTypes[i++])) {
                                    if (dataType[0] === "+") {
                                        dataType = dataType.slice(1) || "*";
                                        (structure[dataType] = structure[dataType] || []).unshift(func);
                                    } else {
                                        (structure[dataType] = structure[dataType] || []).push(func);
                                    }
                                }
                            }
                        };
                    }
                    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
                        var inspected = {},
                            seekingTransport = structure === transports;
                        function inspect(dataType) {
                            var selected;
                            inspected[dataType] = true;
                            jQuery.each(structure[dataType] || [], function (_, prefilterOrFactory) {
                                var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                                if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[dataTypeOrTransport]) {
                                    options.dataTypes.unshift(dataTypeOrTransport);
                                    inspect(dataTypeOrTransport);
                                    return false;
                                } else if (seekingTransport) {
                                    return !(selected = dataTypeOrTransport);
                                }
                            });
                            return selected;
                        }
                        return inspect(options.dataTypes[0]) || (!inspected["*"] && inspect("*"));
                    }
                    function ajaxExtend(target, src) {
                        var key,
                            deep,
                            flatOptions = jQuery.ajaxSettings.flatOptions || {};
                        for (key in src) {
                            if (src[key] !== undefined) {
                                (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
                            }
                        }
                        if (deep) {
                            jQuery.extend(true, target, deep);
                        }
                        return target;
                    }
                    function ajaxHandleResponses(s, jqXHR, responses) {
                        var ct,
                            type,
                            finalDataType,
                            firstDataType,
                            contents = s.contents,
                            dataTypes = s.dataTypes;
                        while (dataTypes[0] === "*") {
                            dataTypes.shift();
                            if (ct === undefined) {
                                ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
                            }
                        }
                        if (ct) {
                            for (type in contents) {
                                if (contents[type] && contents[type].test(ct)) {
                                    dataTypes.unshift(type);
                                    break;
                                }
                            }
                        }
                        if (dataTypes[0] in responses) {
                            finalDataType = dataTypes[0];
                        } else {
                            for (type in responses) {
                                if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
                                    finalDataType = type;
                                    break;
                                }
                                if (!firstDataType) {
                                    firstDataType = type;
                                }
                            }
                            finalDataType = finalDataType || firstDataType;
                        }
                        if (finalDataType) {
                            if (finalDataType !== dataTypes[0]) {
                                dataTypes.unshift(finalDataType);
                            }
                            return responses[finalDataType];
                        }
                    }
                    function ajaxConvert(s, response, jqXHR, isSuccess) {
                        var conv2,
                            current,
                            conv,
                            tmp,
                            prev,
                            converters = {},
                            dataTypes = s.dataTypes.slice();
                        if (dataTypes[1]) {
                            for (conv in s.converters) {
                                converters[conv.toLowerCase()] = s.converters[conv];
                            }
                        }
                        current = dataTypes.shift();
                        while (current) {
                            if (s.responseFields[current]) {
                                jqXHR[s.responseFields[current]] = response;
                            }
                            if (!prev && isSuccess && s.dataFilter) {
                                response = s.dataFilter(response, s.dataType);
                            }
                            prev = current;
                            current = dataTypes.shift();
                            if (current) {
                                if (current === "*") {
                                    current = prev;
                                } else if (prev !== "*" && prev !== current) {
                                    conv = converters[prev + " " + current] || converters["* " + current];
                                    if (!conv) {
                                        for (conv2 in converters) {
                                            tmp = conv2.split(" ");
                                            if (tmp[1] === current) {
                                                conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]];
                                                if (conv) {
                                                    if (conv === true) {
                                                        conv = converters[conv2];
                                                    } else if (converters[conv2] !== true) {
                                                        current = tmp[0];
                                                        dataTypes.unshift(tmp[1]);
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (conv !== true) {
                                        if (conv && s.throws) {
                                            response = conv(response);
                                        } else {
                                            try {
                                                response = conv(response);
                                            } catch (e) {
                                                return { state: "parsererror", error: conv ? e : "No conversion from " + prev + " to " + current };
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return { state: "success", data: response };
                    }
                    jQuery.extend({
                        active: 0,
                        lastModified: {},
                        etag: {},
                        ajaxSettings: {
                            url: location.href,
                            type: "GET",
                            isLocal: rlocalProtocol.test(location.protocol),
                            global: true,
                            processData: true,
                            async: true,
                            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                            accepts: { "*": allTypes, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" },
                            contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
                            responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" },
                            converters: { "* text": String, "text html": true, "text json": JSON.parse, "text xml": jQuery.parseXML },
                            flatOptions: { url: true, context: true },
                        },
                        ajaxSetup: function (target, settings) {
                            return settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
                        },
                        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
                        ajaxTransport: addToPrefiltersOrTransports(transports),
                        ajax: function (url, options) {
                            if (typeof url === "object") {
                                options = url;
                                url = undefined;
                            }
                            options = options || {};
                            var transport,
                                cacheURL,
                                responseHeadersString,
                                responseHeaders,
                                timeoutTimer,
                                urlAnchor,
                                completed,
                                fireGlobals,
                                i,
                                uncached,
                                s = jQuery.ajaxSetup({}, options),
                                callbackContext = s.context || s,
                                globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event,
                                deferred = jQuery.Deferred(),
                                completeDeferred = jQuery.Callbacks("once memory"),
                                statusCode = s.statusCode || {},
                                requestHeaders = {},
                                requestHeadersNames = {},
                                strAbort = "canceled",
                                jqXHR = {
                                    readyState: 0,
                                    getResponseHeader: function (key) {
                                        var match;
                                        if (completed) {
                                            if (!responseHeaders) {
                                                responseHeaders = {};
                                                while ((match = rheaders.exec(responseHeadersString))) {
                                                    responseHeaders[match[1].toLowerCase()] = match[2];
                                                }
                                            }
                                            match = responseHeaders[key.toLowerCase()];
                                        }
                                        return match == null ? null : match;
                                    },
                                    getAllResponseHeaders: function () {
                                        return completed ? responseHeadersString : null;
                                    },
                                    setRequestHeader: function (name, value) {
                                        if (completed == null) {
                                            name = requestHeadersNames[name.toLowerCase()] = requestHeadersNames[name.toLowerCase()] || name;
                                            requestHeaders[name] = value;
                                        }
                                        return this;
                                    },
                                    overrideMimeType: function (type) {
                                        if (completed == null) {
                                            s.mimeType = type;
                                        }
                                        return this;
                                    },
                                    statusCode: function (map) {
                                        var code;
                                        if (map) {
                                            if (completed) {
                                                jqXHR.always(map[jqXHR.status]);
                                            } else {
                                                for (code in map) {
                                                    statusCode[code] = [statusCode[code], map[code]];
                                                }
                                            }
                                        }
                                        return this;
                                    },
                                    abort: function (statusText) {
                                        var finalText = statusText || strAbort;
                                        if (transport) {
                                            transport.abort(finalText);
                                        }
                                        done(0, finalText);
                                        return this;
                                    },
                                };
                            deferred.promise(jqXHR);
                            s.url = ((url || s.url || location.href) + "").replace(rprotocol, location.protocol + "//");
                            s.type = options.method || options.type || s.method || s.type;
                            s.dataTypes = (s.dataType || "*").toLowerCase().match(rnothtmlwhite) || [""];
                            if (s.crossDomain == null) {
                                urlAnchor = document.createElement("a");
                                try {
                                    urlAnchor.href = s.url;
                                    urlAnchor.href = urlAnchor.href;
                                    s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !== urlAnchor.protocol + "//" + urlAnchor.host;
                                } catch (e) {
                                    s.crossDomain = true;
                                }
                            }
                            if (s.data && s.processData && typeof s.data !== "string") {
                                s.data = jQuery.param(s.data, s.traditional);
                            }
                            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
                            if (completed) {
                                return jqXHR;
                            }
                            fireGlobals = jQuery.event && s.global;
                            if (fireGlobals && jQuery.active++ === 0) {
                                jQuery.event.trigger("ajaxStart");
                            }
                            s.type = s.type.toUpperCase();
                            s.hasContent = !rnoContent.test(s.type);
                            cacheURL = s.url.replace(rhash, "");
                            if (!s.hasContent) {
                                uncached = s.url.slice(cacheURL.length);
                                if (s.data) {
                                    cacheURL += (rquery.test(cacheURL) ? "&" : "?") + s.data;
                                    delete s.data;
                                }
                                if (s.cache === false) {
                                    cacheURL = cacheURL.replace(rantiCache, "$1");
                                    uncached = (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce++ + uncached;
                                }
                                s.url = cacheURL + uncached;
                            } else if (s.data && s.processData && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0) {
                                s.data = s.data.replace(r20, "+");
                            }
                            if (s.ifModified) {
                                if (jQuery.lastModified[cacheURL]) {
                                    jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[cacheURL]);
                                }
                                if (jQuery.etag[cacheURL]) {
                                    jqXHR.setRequestHeader("If-None-Match", jQuery.etag[cacheURL]);
                                }
                            }
                            if ((s.data && s.hasContent && s.contentType !== false) || options.contentType) {
                                jqXHR.setRequestHeader("Content-Type", s.contentType);
                            }
                            jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);
                            for (i in s.headers) {
                                jqXHR.setRequestHeader(i, s.headers[i]);
                            }
                            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || completed)) {
                                return jqXHR.abort();
                            }
                            strAbort = "abort";
                            completeDeferred.add(s.complete);
                            jqXHR.done(s.success);
                            jqXHR.fail(s.error);
                            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
                            if (!transport) {
                                done(-1, "No Transport");
                            } else {
                                jqXHR.readyState = 1;
                                if (fireGlobals) {
                                    globalEventContext.trigger("ajaxSend", [jqXHR, s]);
                                }
                                if (completed) {
                                    return jqXHR;
                                }
                                if (s.async && s.timeout > 0) {
                                    timeoutTimer = window.setTimeout(function () {
                                        jqXHR.abort("timeout");
                                    }, s.timeout);
                                }
                                try {
                                    completed = false;
                                    transport.send(requestHeaders, done);
                                } catch (e) {
                                    if (completed) {
                                        throw e;
                                    }
                                    done(-1, e);
                                }
                            }
                            function done(status, nativeStatusText, responses, headers) {
                                var isSuccess,
                                    success,
                                    error,
                                    response,
                                    modified,
                                    statusText = nativeStatusText;
                                if (completed) {
                                    return;
                                }
                                completed = true;
                                if (timeoutTimer) {
                                    window.clearTimeout(timeoutTimer);
                                }
                                transport = undefined;
                                responseHeadersString = headers || "";
                                jqXHR.readyState = status > 0 ? 4 : 0;
                                isSuccess = (status >= 200 && status < 300) || status === 304;
                                if (responses) {
                                    response = ajaxHandleResponses(s, jqXHR, responses);
                                }
                                response = ajaxConvert(s, response, jqXHR, isSuccess);
                                if (isSuccess) {
                                    if (s.ifModified) {
                                        modified = jqXHR.getResponseHeader("Last-Modified");
                                        if (modified) {
                                            jQuery.lastModified[cacheURL] = modified;
                                        }
                                        modified = jqXHR.getResponseHeader("etag");
                                        if (modified) {
                                            jQuery.etag[cacheURL] = modified;
                                        }
                                    }
                                    if (status === 204 || s.type === "HEAD") {
                                        statusText = "nocontent";
                                    } else if (status === 304) {
                                        statusText = "notmodified";
                                    } else {
                                        statusText = response.state;
                                        success = response.data;
                                        error = response.error;
                                        isSuccess = !error;
                                    }
                                } else {
                                    error = statusText;
                                    if (status || !statusText) {
                                        statusText = "error";
                                        if (status < 0) {
                                            status = 0;
                                        }
                                    }
                                }
                                jqXHR.status = status;
                                jqXHR.statusText = (nativeStatusText || statusText) + "";
                                if (isSuccess) {
                                    deferred.resolveWith(callbackContext, [success, statusText, jqXHR]);
                                } else {
                                    deferred.rejectWith(callbackContext, [jqXHR, statusText, error]);
                                }
                                jqXHR.statusCode(statusCode);
                                statusCode = undefined;
                                if (fireGlobals) {
                                    globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError", [jqXHR, s, isSuccess ? success : error]);
                                }
                                completeDeferred.fireWith(callbackContext, [jqXHR, statusText]);
                                if (fireGlobals) {
                                    globalEventContext.trigger("ajaxComplete", [jqXHR, s]);
                                    if (!--jQuery.active) {
                                        jQuery.event.trigger("ajaxStop");
                                    }
                                }
                            }
                            return jqXHR;
                        },
                        getJSON: function (url, data, callback) {
                            return jQuery.get(url, data, callback, "json");
                        },
                        getScript: function (url, callback) {
                            return jQuery.get(url, undefined, callback, "script");
                        },
                    });
                    jQuery.each(["get", "post"], function (i, method) {
                        jQuery[method] = function (url, data, callback, type) {
                            if (jQuery.isFunction(data)) {
                                type = type || callback;
                                callback = data;
                                data = undefined;
                            }
                            return jQuery.ajax(jQuery.extend({ url: url, type: method, dataType: type, data: data, success: callback }, jQuery.isPlainObject(url) && url));
                        };
                    });
                    jQuery._evalUrl = function (url) {
                        return jQuery.ajax({ url: url, type: "GET", dataType: "script", cache: true, async: false, global: false, throws: true });
                    };
                    jQuery.fn.extend({
                        wrapAll: function (html) {
                            var wrap;
                            if (this[0]) {
                                if (jQuery.isFunction(html)) {
                                    html = html.call(this[0]);
                                }
                                wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);
                                if (this[0].parentNode) {
                                    wrap.insertBefore(this[0]);
                                }
                                wrap.map(function () {
                                    var elem = this;
                                    while (elem.firstElementChild) {
                                        elem = elem.firstElementChild;
                                    }
                                    return elem;
                                }).append(this);
                            }
                            return this;
                        },
                        wrapInner: function (html) {
                            if (jQuery.isFunction(html)) {
                                return this.each(function (i) {
                                    jQuery(this).wrapInner(html.call(this, i));
                                });
                            }
                            return this.each(function () {
                                var self = jQuery(this),
                                    contents = self.contents();
                                if (contents.length) {
                                    contents.wrapAll(html);
                                } else {
                                    self.append(html);
                                }
                            });
                        },
                        wrap: function (html) {
                            var isFunction = jQuery.isFunction(html);
                            return this.each(function (i) {
                                jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
                            });
                        },
                        unwrap: function (selector) {
                            this.parent(selector)
                                .not("body")
                                .each(function () {
                                    jQuery(this).replaceWith(this.childNodes);
                                });
                            return this;
                        },
                    });
                    jQuery.expr.pseudos.hidden = function (elem) {
                        return !jQuery.expr.pseudos.visible(elem);
                    };
                    jQuery.expr.pseudos.visible = function (elem) {
                        return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
                    };
                    jQuery.ajaxSettings.xhr = function () {
                        try {
                            return new window.XMLHttpRequest();
                        } catch (e) { }
                    };
                    var xhrSuccessStatus = { 0: 200, 1223: 204 },
                        xhrSupported = jQuery.ajaxSettings.xhr();
                    support.cors = !!xhrSupported && "withCredentials" in xhrSupported;
                    support.ajax = xhrSupported = !!xhrSupported;
                    jQuery.ajaxTransport(function (options) {
                        var callback, errorCallback;
                        if (support.cors || (xhrSupported && !options.crossDomain)) {
                            return {
                                send: function (headers, complete) {
                                    var i,
                                        xhr = options.xhr();
                                    xhr.open(options.type, options.url, options.async, options.username, options.password);
                                    if (options.xhrFields) {
                                        for (i in options.xhrFields) {
                                            xhr[i] = options.xhrFields[i];
                                        }
                                    }
                                    if (options.mimeType && xhr.overrideMimeType) {
                                        xhr.overrideMimeType(options.mimeType);
                                    }
                                    if (!options.crossDomain && !headers["X-Requested-With"]) {
                                        headers["X-Requested-With"] = "XMLHttpRequest";
                                    }
                                    for (i in headers) {
                                        xhr.setRequestHeader(i, headers[i]);
                                    }
                                    callback = function (type) {
                                        return function () {
                                            if (callback) {
                                                callback = errorCallback = xhr.onload = xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;
                                                if (type === "abort") {
                                                    xhr.abort();
                                                } else if (type === "error") {
                                                    if (typeof xhr.status !== "number") {
                                                        complete(0, "error");
                                                    } else {
                                                        complete(xhr.status, xhr.statusText);
                                                    }
                                                } else {
                                                    complete(
                                                        xhrSuccessStatus[xhr.status] || xhr.status,
                                                        xhr.statusText,
                                                        (xhr.responseType || "text") !== "text" || typeof xhr.responseText !== "string" ? { binary: xhr.response } : { text: xhr.responseText },
                                                        xhr.getAllResponseHeaders()
                                                    );
                                                }
                                            }
                                        };
                                    };
                                    xhr.onload = callback();
                                    errorCallback = xhr.onerror = callback("error");
                                    if (xhr.onabort !== undefined) {
                                        xhr.onabort = errorCallback;
                                    } else {
                                        xhr.onreadystatechange = function () {
                                            if (xhr.readyState === 4) {
                                                window.setTimeout(function () {
                                                    if (callback) {
                                                        errorCallback();
                                                    }
                                                });
                                            }
                                        };
                                    }
                                    callback = callback("abort");
                                    try {
                                        xhr.send((options.hasContent && options.data) || null);
                                    } catch (e) {
                                        if (callback) {
                                            throw e;
                                        }
                                    }
                                },
                                abort: function () {
                                    if (callback) {
                                        callback();
                                    }
                                },
                            };
                        }
                    });
                    jQuery.ajaxPrefilter(function (s) {
                        if (s.crossDomain) {
                            s.contents.script = false;
                        }
                    });
                    jQuery.ajaxSetup({
                        accepts: { script: "text/javascript, application/javascript, " + "application/ecmascript, application/x-ecmascript" },
                        contents: { script: /\b(?:java|ecma)script\b/ },
                        converters: {
                            "text script": function (text) {
                                jQuery.globalEval(text);
                                return text;
                            },
                        },
                    });
                    jQuery.ajaxPrefilter("script", function (s) {
                        if (s.cache === undefined) {
                            s.cache = false;
                        }
                        if (s.crossDomain) {
                            s.type = "GET";
                        }
                    });
                    jQuery.ajaxTransport("script", function (s) {
                        if (s.crossDomain) {
                            var script, callback;
                            return {
                                send: function (_, complete) {
                                    script = jQuery("<script>")
                                        .prop({ charset: s.scriptCharset, src: s.url })
                                        .on(
                                            "load error",
                                            (callback = function (evt) {
                                                script.remove();
                                                callback = null;
                                                if (evt) {
                                                    complete(evt.type === "error" ? 404 : 200, evt.type);
                                                }
                                            })
                                        );
                                    document.head.appendChild(script[0]);
                                },
                                abort: function () {
                                    if (callback) {
                                        callback();
                                    }
                                },
                            };
                        }
                    });
                    var oldCallbacks = [],
                        rjsonp = /(=)\?(?=&|$)|\?\?/;
                    jQuery.ajaxSetup({
                        jsonp: "callback",
                        jsonpCallback: function () {
                            var callback = oldCallbacks.pop() || jQuery.expando + "_" + nonce++;
                            this[callback] = true;
                            return callback;
                        },
                    });
                    jQuery.ajaxPrefilter("json jsonp", function (s, originalSettings, jqXHR) {
                        var callbackName,
                            overwritten,
                            responseContainer,
                            jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? "url" : typeof s.data === "string" && (s.contentType || "").indexOf("application/x-www-form-urlencoded") === 0 && rjsonp.test(s.data) && "data");
                        if (jsonProp || s.dataTypes[0] === "jsonp") {
                            callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;
                            if (jsonProp) {
                                s[jsonProp] = s[jsonProp].replace(rjsonp, "$1" + callbackName);
                            } else if (s.jsonp !== false) {
                                s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
                            }
                            s.converters["script json"] = function () {
                                if (!responseContainer) {
                                    jQuery.error(callbackName + " was not called");
                                }
                                return responseContainer[0];
                            };
                            s.dataTypes[0] = "json";
                            overwritten = window[callbackName];
                            window[callbackName] = function () {
                                responseContainer = arguments;
                            };
                            jqXHR.always(function () {
                                if (overwritten === undefined) {
                                    jQuery(window).removeProp(callbackName);
                                } else {
                                    window[callbackName] = overwritten;
                                }
                                if (s[callbackName]) {
                                    s.jsonpCallback = originalSettings.jsonpCallback;
                                    oldCallbacks.push(callbackName);
                                }
                                if (responseContainer && jQuery.isFunction(overwritten)) {
                                    overwritten(responseContainer[0]);
                                }
                                responseContainer = overwritten = undefined;
                            });
                            return "script";
                        }
                    });
                    support.createHTMLDocument = (function () {
                        var body = document.implementation.createHTMLDocument("").body;
                        body.innerHTML = "<form></form><form></form>";
                        return body.childNodes.length === 2;
                    })();
                    jQuery.parseHTML = function (data, context, keepScripts) {
                        if (typeof data !== "string") {
                            return [];
                        }
                        if (typeof context === "boolean") {
                            keepScripts = context;
                            context = false;
                        }
                        var base, parsed, scripts;
                        if (!context) {
                            if (support.createHTMLDocument) {
                                context = document.implementation.createHTMLDocument("");
                                base = context.createElement("base");
                                base.href = document.location.href;
                                context.head.appendChild(base);
                            } else {
                                context = document;
                            }
                        }
                        parsed = rsingleTag.exec(data);
                        scripts = !keepScripts && [];
                        if (parsed) {
                            return [context.createElement(parsed[1])];
                        }
                        parsed = buildFragment([data], context, scripts);
                        if (scripts && scripts.length) {
                            jQuery(scripts).remove();
                        }
                        return jQuery.merge([], parsed.childNodes);
                    };
                    jQuery.fn.load = function (url, params, callback) {
                        var selector,
                            type,
                            response,
                            self = this,
                            off = url.indexOf(" ");
                        if (off > -1) {
                            selector = stripAndCollapse(url.slice(off));
                            url = url.slice(0, off);
                        }
                        if (jQuery.isFunction(params)) {
                            callback = params;
                            params = undefined;
                        } else if (params && typeof params === "object") {
                            type = "POST";
                        }
                        if (self.length > 0) {
                            jQuery
                                .ajax({ url: url, type: type || "GET", dataType: "html", data: params })
                                .done(function (responseText) {
                                    response = arguments;
                                    self.html(selector ? jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) : responseText);
                                })
                                .always(
                                    callback &&
                                    function (jqXHR, status) {
                                        self.each(function () {
                                            callback.apply(this, response || [jqXHR.responseText, status, jqXHR]);
                                        });
                                    }
                                );
                        }
                        return this;
                    };
                    jQuery.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (i, type) {
                        jQuery.fn[type] = function (fn) {
                            return this.on(type, fn);
                        };
                    });
                    jQuery.expr.pseudos.animated = function (elem) {
                        return jQuery.grep(jQuery.timers, function (fn) {
                            return elem === fn.elem;
                        }).length;
                    };
                    jQuery.offset = {
                        setOffset: function (elem, options, i) {
                            var curPosition,
                                curLeft,
                                curCSSTop,
                                curTop,
                                curOffset,
                                curCSSLeft,
                                calculatePosition,
                                position = jQuery.css(elem, "position"),
                                curElem = jQuery(elem),
                                props = {};
                            if (position === "static") {
                                elem.style.position = "relative";
                            }
                            curOffset = curElem.offset();
                            curCSSTop = jQuery.css(elem, "top");
                            curCSSLeft = jQuery.css(elem, "left");
                            calculatePosition = (position === "absolute" || position === "fixed") && (curCSSTop + curCSSLeft).indexOf("auto") > -1;
                            if (calculatePosition) {
                                curPosition = curElem.position();
                                curTop = curPosition.top;
                                curLeft = curPosition.left;
                            } else {
                                curTop = parseFloat(curCSSTop) || 0;
                                curLeft = parseFloat(curCSSLeft) || 0;
                            }
                            if (jQuery.isFunction(options)) {
                                options = options.call(elem, i, jQuery.extend({}, curOffset));
                            }
                            if (options.top != null) {
                                props.top = options.top - curOffset.top + curTop;
                            }
                            if (options.left != null) {
                                props.left = options.left - curOffset.left + curLeft;
                            }
                            if ("using" in options) {
                                options.using.call(elem, props);
                            } else {
                                curElem.css(props);
                            }
                        },
                    };
                    jQuery.fn.extend({
                        offset: function (options) {
                            if (arguments.length) {
                                return options === undefined
                                    ? this
                                    : this.each(function (i) {
                                        jQuery.offset.setOffset(this, options, i);
                                    });
                            }
                            var doc,
                                docElem,
                                rect,
                                win,
                                elem = this[0];
                            if (!elem) {
                                return;
                            }
                            if (!elem.getClientRects().length) {
                                return { top: 0, left: 0 };
                            }
                            rect = elem.getBoundingClientRect();
                            doc = elem.ownerDocument;
                            docElem = doc.documentElement;
                            win = doc.defaultView;
                            return { top: rect.top + win.pageYOffset - docElem.clientTop, left: rect.left + win.pageXOffset - docElem.clientLeft };
                        },
                        position: function () {
                            if (!this[0]) {
                                return;
                            }
                            var offsetParent,
                                offset,
                                elem = this[0],
                                parentOffset = { top: 0, left: 0 };
                            if (jQuery.css(elem, "position") === "fixed") {
                                offset = elem.getBoundingClientRect();
                            } else {
                                offsetParent = this.offsetParent();
                                offset = this.offset();
                                if (!nodeName(offsetParent[0], "html")) {
                                    parentOffset = offsetParent.offset();
                                }
                                parentOffset = { top: parentOffset.top + jQuery.css(offsetParent[0], "borderTopWidth", true), left: parentOffset.left + jQuery.css(offsetParent[0], "borderLeftWidth", true) };
                            }
                            return { top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true), left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true) };
                        },
                        offsetParent: function () {
                            return this.map(function () {
                                var offsetParent = this.offsetParent;
                                while (offsetParent && jQuery.css(offsetParent, "position") === "static") {
                                    offsetParent = offsetParent.offsetParent;
                                }
                                return offsetParent || documentElement;
                            });
                        },
                    });
                    jQuery.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (method, prop) {
                        var top = "pageYOffset" === prop;
                        jQuery.fn[method] = function (val) {
                            return access(
                                this,
                                function (elem, method, val) {
                                    var win;
                                    if (jQuery.isWindow(elem)) {
                                        win = elem;
                                    } else if (elem.nodeType === 9) {
                                        win = elem.defaultView;
                                    }
                                    if (val === undefined) {
                                        return win ? win[prop] : elem[method];
                                    }
                                    if (win) {
                                        win.scrollTo(!top ? val : win.pageXOffset, top ? val : win.pageYOffset);
                                    } else {
                                        elem[method] = val;
                                    }
                                },
                                method,
                                val,
                                arguments.length
                            );
                        };
                    });
                    jQuery.each(["top", "left"], function (i, prop) {
                        jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function (elem, computed) {
                            if (computed) {
                                computed = curCSS(elem, prop);
                                return rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + "px" : computed;
                            }
                        });
                    });
                    jQuery.each({ Height: "height", Width: "width" }, function (name, type) {
                        jQuery.each({ padding: "inner" + name, content: type, "": "outer" + name }, function (defaultExtra, funcName) {
                            jQuery.fn[funcName] = function (margin, value) {
                                var chainable = arguments.length && (defaultExtra || typeof margin !== "boolean"),
                                    extra = defaultExtra || (margin === true || value === true ? "margin" : "border");
                                return access(
                                    this,
                                    function (elem, type, value) {
                                        var doc;
                                        if (jQuery.isWindow(elem)) {
                                            return funcName.indexOf("outer") === 0 ? elem["inner" + name] : elem.document.documentElement["client" + name];
                                        }
                                        if (elem.nodeType === 9) {
                                            doc = elem.documentElement;
                                            return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
                                        }
                                        return value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
                                    },
                                    type,
                                    chainable ? margin : undefined,
                                    chainable
                                );
                            };
                        });
                    });
                    jQuery.fn.extend({
                        bind: function (types, data, fn) {
                            return this.on(types, null, data, fn);
                        },
                        unbind: function (types, fn) {
                            return this.off(types, null, fn);
                        },
                        delegate: function (selector, types, data, fn) {
                            return this.on(types, selector, data, fn);
                        },
                        undelegate: function (selector, types, fn) {
                            return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
                        },
                    });
                    jQuery.holdReady = function (hold) {
                        if (hold) {
                            jQuery.readyWait++;
                        } else {
                            jQuery.ready(true);
                        }
                    };
                    jQuery.isArray = Array.isArray;
                    jQuery.parseJSON = JSON.parse;
                    jQuery.nodeName = nodeName;
                    if (typeof define === "function" && define.amd) {
                        define("jquery", [], function () {
                            return jQuery;
                        });
                    }
                    var _jQuery = window.jQuery,
                        _$ = window.$;
                    jQuery.noConflict = function (deep) {
                        if (window.$ === jQuery) {
                            window.$ = _$;
                        }
                        if (deep && window.jQuery === jQuery) {
                            window.jQuery = _jQuery;
                        }
                        return jQuery;
                    };
                    if (!noGlobal) {
                        window.jQuery = window.$ = jQuery;
                    }
                    return jQuery;
                });
            },
            {},
        ],
        6: [
            function (require, module, exports) {
                (function (window, factory) {
                    var lazySizes = factory(window, window.document);
                    window.lazySizes = lazySizes;
                    if (typeof module == "object" && module.exports) {
                        module.exports = lazySizes;
                    }
                })(window, function l(window, document) {
                    "use strict";
                    if (!document.getElementsByClassName) {
                        return;
                    }
                    var lazysizes, lazySizesConfig;
                    var docElem = document.documentElement;
                    var Date = window.Date;
                    var supportPicture = window.HTMLPictureElement;
                    var _addEventListener = "addEventListener";
                    var _getAttribute = "getAttribute";
                    var addEventListener = window[_addEventListener];
                    var setTimeout = window.setTimeout;
                    var requestAnimationFrame = window.requestAnimationFrame || setTimeout;
                    var requestIdleCallback = window.requestIdleCallback;
                    var regPicture = /^picture$/i;
                    var loadEvents = ["load", "error", "lazyincluded", "_lazyloaded"];
                    var regClassCache = {};
                    var forEach = Array.prototype.forEach;
                    var hasClass = function (ele, cls) {
                        if (!regClassCache[cls]) {
                            regClassCache[cls] = new RegExp("(\\s|^)" + cls + "(\\s|$)");
                        }
                        return regClassCache[cls].test(ele[_getAttribute]("class") || "") && regClassCache[cls];
                    };
                    var addClass = function (ele, cls) {
                        if (!hasClass(ele, cls)) {
                            ele.setAttribute("class", (ele[_getAttribute]("class") || "").trim() + " " + cls);
                        }
                    };
                    var removeClass = function (ele, cls) {
                        var reg;
                        if ((reg = hasClass(ele, cls))) {
                            ele.setAttribute("class", (ele[_getAttribute]("class") || "").replace(reg, " "));
                        }
                    };
                    var addRemoveLoadEvents = function (dom, fn, add) {
                        var action = add ? _addEventListener : "removeEventListener";
                        if (add) {
                            addRemoveLoadEvents(dom, fn);
                        }
                        loadEvents.forEach(function (evt) {
                            dom[action](evt, fn);
                        });
                    };
                    var triggerEvent = function (elem, name, detail, noBubbles, noCancelable) {
                        var event = document.createEvent("CustomEvent");
                        if (!detail) {
                            detail = {};
                        }
                        detail.instance = lazysizes;
                        event.initCustomEvent(name, !noBubbles, !noCancelable, detail);
                        elem.dispatchEvent(event);
                        return event;
                    };
                    var updatePolyfill = function (el, full) {
                        var polyfill;
                        if (!supportPicture && (polyfill = window.picturefill || lazySizesConfig.pf)) {
                            polyfill({ reevaluate: true, elements: [el] });
                        } else if (full && full.src) {
                            el.src = full.src;
                        }
                    };
                    var getCSS = function (elem, style) {
                        return (getComputedStyle(elem, null) || {})[style];
                    };
                    var getWidth = function (elem, parent, width) {
                        width = width || elem.offsetWidth;
                        while (width < lazySizesConfig.minSize && parent && !elem._lazysizesWidth) {
                            width = parent.offsetWidth;
                            parent = parent.parentNode;
                        }
                        return width;
                    };
                    var rAF = (function () {
                        var running, waiting;
                        var firstFns = [];
                        var secondFns = [];
                        var fns = firstFns;
                        var run = function () {
                            var runFns = fns;
                            fns = firstFns.length ? secondFns : firstFns;
                            running = true;
                            waiting = false;
                            while (runFns.length) {
                                runFns.shift()();
                            }
                            running = false;
                        };
                        var rafBatch = function (fn, queue) {
                            if (running && !queue) {
                                fn.apply(this, arguments);
                            } else {
                                fns.push(fn);
                                if (!waiting) {
                                    waiting = true;
                                    (document.hidden ? setTimeout : requestAnimationFrame)(run);
                                }
                            }
                        };
                        rafBatch._lsFlush = run;
                        return rafBatch;
                    })();
                    var rAFIt = function (fn, simple) {
                        return simple
                            ? function () {
                                rAF(fn);
                            }
                            : function () {
                                var that = this;
                                var args = arguments;
                                rAF(function () {
                                    fn.apply(that, args);
                                });
                            };
                    };
                    var throttle = function (fn) {
                        var running;
                        var lastTime = 0;
                        var gDelay = 125;
                        var rICTimeout = lazySizesConfig.ricTimeout;
                        var run = function () {
                            running = false;
                            lastTime = Date.now();
                            fn();
                        };
                        var idleCallback =
                            requestIdleCallback && lazySizesConfig.ricTimeout
                                ? function () {
                                    requestIdleCallback(run, { timeout: rICTimeout });
                                    if (rICTimeout !== lazySizesConfig.ricTimeout) {
                                        rICTimeout = lazySizesConfig.ricTimeout;
                                    }
                                }
                                : rAFIt(function () {
                                    setTimeout(run);
                                }, true);
                        return function (isPriority) {
                            var delay;
                            if ((isPriority = isPriority === true)) {
                                rICTimeout = 33;
                            }
                            if (running) {
                                return;
                            }
                            running = true;
                            delay = gDelay - (Date.now() - lastTime);
                            if (delay < 0) {
                                delay = 0;
                            }
                            if (isPriority || (delay < 9 && requestIdleCallback)) {
                                idleCallback();
                            } else {
                                setTimeout(idleCallback, delay);
                            }
                        };
                    };
                    var debounce = function (func) {
                        var timeout, timestamp;
                        var wait = 99;
                        var run = function () {
                            timeout = null;
                            func();
                        };
                        var later = function () {
                            var last = Date.now() - timestamp;
                            if (last < wait) {
                                setTimeout(later, wait - last);
                            } else {
                                (requestIdleCallback || run)(run);
                            }
                        };
                        return function () {
                            timestamp = Date.now();
                            if (!timeout) {
                                timeout = setTimeout(later, wait);
                            }
                        };
                    };
                    (function () {
                        var prop;
                        var lazySizesDefaults = {
                            lazyClass: "lazyload",
                            loadedClass: "lazyloaded",
                            loadingClass: "lazyloading",
                            preloadClass: "lazypreload",
                            errorClass: "lazyerror",
                            autosizesClass: "lazyautosizes",
                            srcAttr: "data-src",
                            srcsetAttr: "data-srcset",
                            sizesAttr: "data-sizes",
                            minSize: 40,
                            customMedia: {},
                            init: true,
                            expFactor: 1.5,
                            hFac: 0.8,
                            loadMode: 2,
                            loadHidden: true,
                            ricTimeout: 300,
                        };
                        lazySizesConfig = window.lazySizesConfig || window.lazysizesConfig || {};
                        for (prop in lazySizesDefaults) {
                            if (!(prop in lazySizesConfig)) {
                                lazySizesConfig[prop] = lazySizesDefaults[prop];
                            }
                        }
                        window.lazySizesConfig = lazySizesConfig;
                        setTimeout(function () {
                            if (lazySizesConfig.init) {
                                init();
                            }
                        });
                    })();
                    var loader = (function () {
                        var preloadElems, isCompleted, resetPreloadingTimer, loadMode, started;
                        var eLvW, elvH, eLtop, eLleft, eLright, eLbottom;
                        var defaultExpand, preloadExpand, hFac;
                        var regImg = /^img$/i;
                        var regIframe = /^iframe$/i;
                        var supportScroll = "onscroll" in window && !/glebot/.test(navigator.userAgent);
                        var shrinkExpand = 0;
                        var currentExpand = 0;
                        var isLoading = 0;
                        var lowRuns = -1;
                        var resetPreloading = function (e) {
                            isLoading--;
                            if (e && e.target) {
                                addRemoveLoadEvents(e.target, resetPreloading);
                            }
                            if (!e || isLoading < 0 || !e.target) {
                                isLoading = 0;
                            }
                        };
                        var isNestedVisible = function (elem, elemExpand) {
                            var outerRect;
                            var parent = elem;
                            var visible = getCSS(document.body, "visibility") == "hidden" || getCSS(elem, "visibility") != "hidden";
                            eLtop -= elemExpand;
                            eLbottom += elemExpand;
                            eLleft -= elemExpand;
                            eLright += elemExpand;
                            while (visible && (parent = parent.offsetParent) && parent != document.body && parent != docElem) {
                                visible = (getCSS(parent, "opacity") || 1) > 0;
                                if (visible && getCSS(parent, "overflow") != "visible") {
                                    outerRect = parent.getBoundingClientRect();
                                    visible = eLright > outerRect.left && eLleft < outerRect.right && eLbottom > outerRect.top - 1 && eLtop < outerRect.bottom + 1;
                                }
                            }
                            return visible;
                        };
                        var checkElements = function () {
                            var eLlen, i, rect, autoLoadElem, loadedSomething, elemExpand, elemNegativeExpand, elemExpandVal, beforeExpandVal;
                            var lazyloadElems = lazysizes.elements;
                            if ((loadMode = lazySizesConfig.loadMode) && isLoading < 8 && (eLlen = lazyloadElems.length)) {
                                i = 0;
                                lowRuns++;
                                if (preloadExpand == null) {
                                    if (!("expand" in lazySizesConfig)) {
                                        lazySizesConfig.expand = docElem.clientHeight > 500 && docElem.clientWidth > 500 ? 500 : 370;
                                    }
                                    defaultExpand = lazySizesConfig.expand;
                                    preloadExpand = defaultExpand * lazySizesConfig.expFactor;
                                }
                                if (currentExpand < preloadExpand && isLoading < 1 && lowRuns > 2 && loadMode > 2 && !document.hidden) {
                                    currentExpand = preloadExpand;
                                    lowRuns = 0;
                                } else if (loadMode > 1 && lowRuns > 1 && isLoading < 6) {
                                    currentExpand = defaultExpand;
                                } else {
                                    currentExpand = shrinkExpand;
                                }
                                for (; i < eLlen; i++) {
                                    if (!lazyloadElems[i] || lazyloadElems[i]._lazyRace) {
                                        continue;
                                    }
                                    if (!supportScroll) {
                                        unveilElement(lazyloadElems[i]);
                                        continue;
                                    }
                                    if (!(elemExpandVal = lazyloadElems[i][_getAttribute]("data-expand")) || !(elemExpand = elemExpandVal * 1)) {
                                        elemExpand = currentExpand;
                                    }
                                    if (beforeExpandVal !== elemExpand) {
                                        eLvW = innerWidth + elemExpand * hFac;
                                        elvH = innerHeight + elemExpand;
                                        elemNegativeExpand = elemExpand * -1;
                                        beforeExpandVal = elemExpand;
                                    }
                                    rect = lazyloadElems[i].getBoundingClientRect();
                                    if (
                                        (eLbottom = rect.bottom) >= elemNegativeExpand &&
                                        (eLtop = rect.top) <= elvH &&
                                        (eLright = rect.right) >= elemNegativeExpand * hFac &&
                                        (eLleft = rect.left) <= eLvW &&
                                        (eLbottom || eLright || eLleft || eLtop) &&
                                        (lazySizesConfig.loadHidden || getCSS(lazyloadElems[i], "visibility") != "hidden") &&
                                        ((isCompleted && isLoading < 3 && !elemExpandVal && (loadMode < 3 || lowRuns < 4)) || isNestedVisible(lazyloadElems[i], elemExpand))
                                    ) {
                                        unveilElement(lazyloadElems[i]);
                                        loadedSomething = true;
                                        if (isLoading > 9) {
                                            break;
                                        }
                                    } else if (
                                        !loadedSomething &&
                                        isCompleted &&
                                        !autoLoadElem &&
                                        isLoading < 4 &&
                                        lowRuns < 4 &&
                                        loadMode > 2 &&
                                        (preloadElems[0] || lazySizesConfig.preloadAfterLoad) &&
                                        (preloadElems[0] || (!elemExpandVal && (eLbottom || eLright || eLleft || eLtop || lazyloadElems[i][_getAttribute](lazySizesConfig.sizesAttr) != "auto")))
                                    ) {
                                        autoLoadElem = preloadElems[0] || lazyloadElems[i];
                                    }
                                }
                                if (autoLoadElem && !loadedSomething) {
                                    unveilElement(autoLoadElem);
                                }
                            }
                        };
                        var throttledCheckElements = throttle(checkElements);
                        var switchLoadingClass = function (e) {
                            addClass(e.target, lazySizesConfig.loadedClass);
                            removeClass(e.target, lazySizesConfig.loadingClass);
                            addRemoveLoadEvents(e.target, rafSwitchLoadingClass);
                            triggerEvent(e.target, "lazyloaded");
                        };
                        var rafedSwitchLoadingClass = rAFIt(switchLoadingClass);
                        var rafSwitchLoadingClass = function (e) {
                            rafedSwitchLoadingClass({ target: e.target });
                        };
                        var changeIframeSrc = function (elem, src) {
                            try {
                                elem.contentWindow.location.replace(src);
                            } catch (e) {
                                elem.src = src;
                            }
                        };
                        var handleSources = function (source) {
                            var customMedia;
                            var sourceSrcset = source[_getAttribute](lazySizesConfig.srcsetAttr);
                            if ((customMedia = lazySizesConfig.customMedia[source[_getAttribute]("data-media") || source[_getAttribute]("media")])) {
                                source.setAttribute("media", customMedia);
                            }
                            if (sourceSrcset) {
                                source.setAttribute("srcset", sourceSrcset);
                            }
                        };
                        var lazyUnveil = rAFIt(function (elem, detail, isAuto, sizes, isImg) {
                            var src, srcset, parent, isPicture, event, firesLoad;
                            if (!(event = triggerEvent(elem, "lazybeforeunveil", detail)).defaultPrevented) {
                                if (sizes) {
                                    if (isAuto) {
                                        addClass(elem, lazySizesConfig.autosizesClass);
                                    } else {
                                        elem.setAttribute("sizes", sizes);
                                    }
                                }
                                srcset = elem[_getAttribute](lazySizesConfig.srcsetAttr);
                                src = elem[_getAttribute](lazySizesConfig.srcAttr);
                                if (isImg) {
                                    parent = elem.parentNode;
                                    isPicture = parent && regPicture.test(parent.nodeName || "");
                                }
                                firesLoad = detail.firesLoad || ("src" in elem && (srcset || src || isPicture));
                                event = { target: elem };
                                if (firesLoad) {
                                    addRemoveLoadEvents(elem, resetPreloading, true);
                                    clearTimeout(resetPreloadingTimer);
                                    resetPreloadingTimer = setTimeout(resetPreloading, 2500);
                                    addClass(elem, lazySizesConfig.loadingClass);
                                    addRemoveLoadEvents(elem, rafSwitchLoadingClass, true);
                                }
                                if (isPicture) {
                                    forEach.call(parent.getElementsByTagName("source"), handleSources);
                                }
                                if (srcset) {
                                    elem.setAttribute("srcset", srcset);
                                } else if (src && !isPicture) {
                                    if (regIframe.test(elem.nodeName)) {
                                        changeIframeSrc(elem, src);
                                    } else {
                                        elem.src = src;
                                    }
                                }
                                if (isImg && (srcset || isPicture)) {
                                    updatePolyfill(elem, { src: src });
                                }
                            }
                            if (elem._lazyRace) {
                                delete elem._lazyRace;
                            }
                            removeClass(elem, lazySizesConfig.lazyClass);
                            rAF(function () {
                                if (!firesLoad || (elem.complete && elem.naturalWidth > 1)) {
                                    if (firesLoad) {
                                        resetPreloading(event);
                                    } else {
                                        isLoading--;
                                    }
                                    switchLoadingClass(event);
                                }
                            }, true);
                        });
                        var unveilElement = function (elem) {
                            var detail;
                            var isImg = regImg.test(elem.nodeName);
                            var sizes = isImg && (elem[_getAttribute](lazySizesConfig.sizesAttr) || elem[_getAttribute]("sizes"));
                            var isAuto = sizes == "auto";
                            if ((isAuto || !isCompleted) && isImg && (elem[_getAttribute]("src") || elem.srcset) && !elem.complete && !hasClass(elem, lazySizesConfig.errorClass) && hasClass(elem, lazySizesConfig.lazyClass)) {
                                return;
                            }
                            detail = triggerEvent(elem, "lazyunveilread").detail;
                            if (isAuto) {
                                autoSizer.updateElem(elem, true, elem.offsetWidth);
                            }
                            elem._lazyRace = true;
                            isLoading++;
                            lazyUnveil(elem, detail, isAuto, sizes, isImg);
                        };
                        var onload = function () {
                            if (isCompleted) {
                                return;
                            }
                            if (Date.now() - started < 999) {
                                setTimeout(onload, 999);
                                return;
                            }
                            var afterScroll = debounce(function () {
                                lazySizesConfig.loadMode = 3;
                                throttledCheckElements();
                            });
                            isCompleted = true;
                            lazySizesConfig.loadMode = 3;
                            throttledCheckElements();
                            addEventListener(
                                "scroll",
                                function () {
                                    if (lazySizesConfig.loadMode == 3) {
                                        lazySizesConfig.loadMode = 2;
                                    }
                                    afterScroll();
                                },
                                true
                            );
                        };
                        return {
                            _: function () {
                                started = Date.now();
                                lazysizes.elements = document.getElementsByClassName(lazySizesConfig.lazyClass);
                                preloadElems = document.getElementsByClassName(lazySizesConfig.lazyClass + " " + lazySizesConfig.preloadClass);
                                hFac = lazySizesConfig.hFac;
                                addEventListener("scroll", throttledCheckElements, true);
                                addEventListener("resize", throttledCheckElements, true);
                                if (window.MutationObserver) {
                                    new MutationObserver(throttledCheckElements).observe(docElem, { childList: true, subtree: true, attributes: true });
                                } else {
                                    docElem[_addEventListener]("DOMNodeInserted", throttledCheckElements, true);
                                    docElem[_addEventListener]("DOMAttrModified", throttledCheckElements, true);
                                    setInterval(throttledCheckElements, 999);
                                }
                                addEventListener("hashchange", throttledCheckElements, true);
                                ["focus", "mouseover", "click", "load", "transitionend", "animationend", "webkitAnimationEnd"].forEach(function (name) {
                                    document[_addEventListener](name, throttledCheckElements, true);
                                });
                                if (/d$|^c/.test(document.readyState)) {
                                    onload();
                                } else {
                                    addEventListener("load", onload);
                                    document[_addEventListener]("DOMContentLoaded", throttledCheckElements);
                                    setTimeout(onload, 2e4);
                                }
                                if (lazysizes.elements.length) {
                                    checkElements();
                                    rAF._lsFlush();
                                } else {
                                    throttledCheckElements();
                                }
                            },
                            checkElems: throttledCheckElements,
                            unveil: unveilElement,
                        };
                    })();
                    var autoSizer = (function () {
                        var autosizesElems;
                        var sizeElement = rAFIt(function (elem, parent, event, width) {
                            var sources, i, len;
                            elem._lazysizesWidth = width;
                            width += "px";
                            elem.setAttribute("sizes", width);
                            if (regPicture.test(parent.nodeName || "")) {
                                sources = parent.getElementsByTagName("source");
                                for (i = 0, len = sources.length; i < len; i++) {
                                    sources[i].setAttribute("sizes", width);
                                }
                            }
                            if (!event.detail.dataAttr) {
                                updatePolyfill(elem, event.detail);
                            }
                        });
                        var getSizeElement = function (elem, dataAttr, width) {
                            var event;
                            var parent = elem.parentNode;
                            if (parent) {
                                width = getWidth(elem, parent, width);
                                event = triggerEvent(elem, "lazybeforesizes", { width: width, dataAttr: !!dataAttr });
                                if (!event.defaultPrevented) {
                                    width = event.detail.width;
                                    if (width && width !== elem._lazysizesWidth) {
                                        sizeElement(elem, parent, event, width);
                                    }
                                }
                            }
                        };
                        var updateElementsSizes = function () {
                            var i;
                            var len = autosizesElems.length;
                            if (len) {
                                i = 0;
                                for (; i < len; i++) {
                                    getSizeElement(autosizesElems[i]);
                                }
                            }
                        };
                        var debouncedUpdateElementsSizes = debounce(updateElementsSizes);
                        return {
                            _: function () {
                                autosizesElems = document.getElementsByClassName(lazySizesConfig.autosizesClass);
                                addEventListener("resize", debouncedUpdateElementsSizes);
                            },
                            checkElems: debouncedUpdateElementsSizes,
                            updateElem: getSizeElement,
                        };
                    })();
                    var init = function () {
                        if (!init.i) {
                            init.i = true;
                            autoSizer._();
                            loader._();
                        }
                    };
                    lazysizes = { cfg: lazySizesConfig, autoSizer: autoSizer, loader: loader, init: init, uP: updatePolyfill, aC: addClass, rC: removeClass, hC: hasClass, fire: triggerEvent, gW: getWidth, rAF: rAF };
                    return lazysizes;
                });
            },
            {},
        ],
        7: [
            function (require, module, exports) {
                (function (root, factory) {
                    if (typeof exports === "object") {
                        module.exports = factory();
                    } else if (typeof define === "function" && define.amd) {
                        define([], factory);
                    } else {
                        root.Parallaxify = factory();
                    }
                })(this, function () {
                    "use strict";
                    var requestAnimFrame = (function () {
                        return (
                            window.requestAnimationFrame ||
                            window.webkitRequestAnimationFrame ||
                            window.mozRequestAnimationFrame ||
                            window.oRequestAnimationFrame ||
                            window.msRequestAnimationFrame ||
                            function (callback) {
                                window.setTimeout(callback, 1e3 / 60);
                            }
                        );
                    })(),
                        Parallaxify = function (options) {
                            this.options = options || {};
                            this.options.backgroundYMin = this.options.backgroundYMin || 100;
                            this.options.backgroundYMax = this.options.backgroundYMax || 0;
                            this.options.elements = this.options.elements || ".js-Parallaxify";
                            this.elements = Parallaxify.toArray(typeof this.options.elements === "string" ? document.querySelectorAll(this.options.elements) : this.options.elements);
                        };
                    Parallaxify.toArray = function (els) {
                        return Array.prototype.slice.call(els);
                    };
                    try {
                        Parallaxify.toArray(document.documentElement);
                    } catch (e) {
                        Parallaxify.toArray = function (els) {
                            var i,
                                a = [];
                            for (i = 0; i < els.length; i++) {
                                a.push(els[i]);
                            }
                        };
                    }
                    Parallaxify.prototype = {
                        registerUpdate: function () {
                            var that = this;
                            this.requestUpdate();
                            window.addEventListener("resize", function () {
                                that.requestUpdate();
                            });
                        },
                        requestUpdate: function () {
                            var that = this;
                            if (this.latestScrollY != window.pageYOffset) {
                                this.latestScrollY = window.pageYOffset;
                                this.update();
                            }
                            requestAnimFrame(function () {
                                that.requestUpdate();
                            });
                        },
                        update: function () {
                            var i = this.elements.length;
                            while (i--) {
                                var rect = this.elements[i].getBoundingClientRect(),
                                    windowHeight = window.innerHeight || document.documentElement.clientHeight,
                                    documentHeight,
                                    pageYOffset,
                                    maxPageYOffset,
                                    pageYOffsetPercentage,
                                    windowHeightConsidered,
                                    yPositionInDocument,
                                    yMinPosition,
                                    yMaxPosition,
                                    yPosition,
                                    yMin,
                                    yMax,
                                    backgroundPosition;
                                if (rect.bottom > 0 && rect.top < windowHeight) {
                                    documentHeight = document.documentElement.scrollHeight;
                                    pageYOffset = window.pageYOffset;
                                    maxPageYOffset = documentHeight - windowHeight;
                                    if (maxPageYOffset) {
                                        pageYOffsetPercentage = pageYOffset / maxPageYOffset;
                                        windowHeightConsidered = windowHeight * pageYOffsetPercentage;
                                        yPositionInDocument = (pageYOffset + windowHeightConsidered) / documentHeight;
                                        yMinPosition = Math.max(0, (pageYOffset + rect.top - windowHeight) / maxPageYOffset);
                                        yMaxPosition = Math.min(1, (pageYOffset + rect.bottom) / maxPageYOffset);
                                        yPosition = (yPositionInDocument - yMinPosition) / (yMaxPosition - yMinPosition);
                                        yMin = parseInt(this.elements[i].getAttribute("data-parallaxify-y-min"), 10);
                                        yMax = parseInt(this.elements[i].getAttribute("data-parallaxify-y-max"), 10);
                                        if (isNaN(yMin)) {
                                            yMin = this.options.backgroundYMin;
                                        }
                                        if (isNaN(yMax)) {
                                            yMax = this.options.backgroundYMax;
                                        }
                                        backgroundPosition = window.getComputedStyle(this.elements[i], null).getPropertyValue("background-position").split(" ");
                                        if (backgroundPosition[0] === "") {
                                            backgroundPosition[0] = "50%";
                                        }
                                        backgroundPosition[1] = String(yPosition * (yMax - yMin) + yMin) + "%";
                                        this.elements[i].style.backgroundPosition = backgroundPosition.join(" ");
                                    }
                                }
                            }
                        },
                    };
                    return Parallaxify;
                });
            },
            {},
        ],
        8: [
            function (require, module, exports) {
                (function () {
                    "use strict";
                    var $;
                    var Swiper = function (container, params) {
                        if (!(this instanceof Swiper)) return new Swiper(container, params);
                        var defaults = {
                            direction: "horizontal",
                            touchEventsTarget: "container",
                            initialSlide: 0,
                            speed: 300,
                            autoplay: false,
                            autoplayDisableOnInteraction: true,
                            autoplayStopOnLast: false,
                            iOSEdgeSwipeDetection: false,
                            iOSEdgeSwipeThreshold: 20,
                            freeMode: false,
                            freeModeMomentum: true,
                            freeModeMomentumRatio: 1,
                            freeModeMomentumBounce: true,
                            freeModeMomentumBounceRatio: 1,
                            freeModeMomentumVelocityRatio: 1,
                            freeModeSticky: false,
                            freeModeMinimumVelocity: 0.02,
                            autoHeight: false,
                            setWrapperSize: false,
                            virtualTranslate: false,
                            effect: "slide",
                            coverflow: { rotate: 50, stretch: 0, depth: 100, modifier: 1, slideShadows: true },
                            flip: { slideShadows: true, limitRotation: true },
                            cube: { slideShadows: true, shadow: true, shadowOffset: 20, shadowScale: 0.94 },
                            fade: { crossFade: false },
                            parallax: false,
                            zoom: false,
                            zoomMax: 3,
                            zoomMin: 1,
                            zoomToggle: true,
                            scrollbar: null,
                            scrollbarHide: true,
                            scrollbarDraggable: false,
                            scrollbarSnapOnRelease: false,
                            keyboardControl: false,
                            mousewheelControl: false,
                            mousewheelReleaseOnEdges: false,
                            mousewheelInvert: false,
                            mousewheelForceToAxis: false,
                            mousewheelSensitivity: 1,
                            mousewheelEventsTarged: "container",
                            hashnav: false,
                            hashnavWatchState: false,
                            history: false,
                            replaceState: false,
                            breakpoints: undefined,
                            spaceBetween: 0,
                            slidesPerView: 1,
                            slidesPerColumn: 1,
                            slidesPerColumnFill: "column",
                            slidesPerGroup: 1,
                            centeredSlides: false,
                            slidesOffsetBefore: 0,
                            slidesOffsetAfter: 0,
                            roundLengths: false,
                            touchRatio: 1,
                            touchAngle: 45,
                            simulateTouch: true,
                            shortSwipes: true,
                            longSwipes: true,
                            longSwipesRatio: 0.5,
                            longSwipesMs: 300,
                            followFinger: true,
                            onlyExternal: false,
                            threshold: 0,
                            touchMoveStopPropagation: true,
                            touchReleaseOnEdges: false,
                            uniqueNavElements: true,
                            pagination: null,
                            paginationElement: "span",
                            paginationClickable: false,
                            paginationHide: false,
                            paginationBulletRender: null,
                            paginationProgressRender: null,
                            paginationFractionRender: null,
                            paginationCustomRender: null,
                            paginationType: "bullets",
                            resistance: true,
                            resistanceRatio: 0.85,
                            nextButton: null,
                            prevButton: null,
                            watchSlidesProgress: false,
                            watchSlidesVisibility: false,
                            grabCursor: false,
                            preventClicks: true,
                            preventClicksPropagation: true,
                            slideToClickedSlide: false,
                            lazyLoading: false,
                            lazyLoadingInPrevNext: false,
                            lazyLoadingInPrevNextAmount: 1,
                            lazyLoadingOnTransitionStart: false,
                            preloadImages: true,
                            updateOnImagesReady: true,
                            loop: false,
                            loopAdditionalSlides: 0,
                            loopedSlides: null,
                            control: undefined,
                            controlInverse: false,
                            controlBy: "slide",
                            normalizeSlideIndex: true,
                            allowSwipeToPrev: true,
                            allowSwipeToNext: true,
                            swipeHandler: null,
                            noSwiping: true,
                            noSwipingClass: "swiper-no-swiping",
                            passiveListeners: true,
                            containerModifierClass: "swiper-container-",
                            slideClass: "swiper-slide",
                            slideActiveClass: "swiper-slide-active",
                            slideDuplicateActiveClass: "swiper-slide-duplicate-active",
                            slideVisibleClass: "swiper-slide-visible",
                            slideDuplicateClass: "swiper-slide-duplicate",
                            slideNextClass: "swiper-slide-next",
                            slideDuplicateNextClass: "swiper-slide-duplicate-next",
                            slidePrevClass: "swiper-slide-prev",
                            slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
                            wrapperClass: "swiper-wrapper",
                            bulletClass: "swiper-pagination-bullet",
                            bulletActiveClass: "swiper-pagination-bullet-active",
                            buttonDisabledClass: "swiper-button-disabled",
                            paginationCurrentClass: "swiper-pagination-current",
                            paginationTotalClass: "swiper-pagination-total",
                            paginationHiddenClass: "swiper-pagination-hidden",
                            paginationProgressbarClass: "swiper-pagination-progressbar",
                            paginationClickableClass: "swiper-pagination-clickable",
                            paginationModifierClass: "swiper-pagination-",
                            lazyLoadingClass: "swiper-lazy",
                            lazyStatusLoadingClass: "swiper-lazy-loading",
                            lazyStatusLoadedClass: "swiper-lazy-loaded",
                            lazyPreloaderClass: "swiper-lazy-preloader",
                            notificationClass: "swiper-notification",
                            preloaderClass: "preloader",
                            zoomContainerClass: "swiper-zoom-container",
                            observer: false,
                            observeParents: false,
                            a11y: false,
                            prevSlideMessage: "Previous slide",
                            nextSlideMessage: "Next slide",
                            firstSlideMessage: "This is the first slide",
                            lastSlideMessage: "This is the last slide",
                            paginationBulletMessage: "Go to slide {{index}}",
                            runCallbacksOnInit: true,
                        };
                        var initialVirtualTranslate = params && params.virtualTranslate;
                        params = params || {};
                        var originalParams = {};
                        for (var param in params) {
                            if (
                                typeof params[param] === "object" &&
                                params[param] !== null &&
                                !(
                                    params[param].nodeType ||
                                    params[param] === window ||
                                    params[param] === document ||
                                    (typeof Dom7 !== "undefined" && params[param] instanceof Dom7) ||
                                    (typeof jQuery !== "undefined" && params[param] instanceof jQuery)
                                )
                            ) {
                                originalParams[param] = {};
                                for (var deepParam in params[param]) {
                                    originalParams[param][deepParam] = params[param][deepParam];
                                }
                            } else {
                                originalParams[param] = params[param];
                            }
                        }
                        for (var def in defaults) {
                            if (typeof params[def] === "undefined") {
                                params[def] = defaults[def];
                            } else if (typeof params[def] === "object") {
                                for (var deepDef in defaults[def]) {
                                    if (typeof params[def][deepDef] === "undefined") {
                                        params[def][deepDef] = defaults[def][deepDef];
                                    }
                                }
                            }
                        }
                        var s = this;
                        s.params = params;
                        s.originalParams = originalParams;
                        s.classNames = [];
                        if (typeof $ !== "undefined" && typeof Dom7 !== "undefined") {
                            $ = Dom7;
                        }
                        if (typeof $ === "undefined") {
                            if (typeof Dom7 === "undefined") {
                                $ = window.Dom7 || window.Zepto || window.jQuery;
                            } else {
                                $ = Dom7;
                            }
                            if (!$) return;
                        }
                        s.$ = $;
                        s.currentBreakpoint = undefined;
                        s.getActiveBreakpoint = function () {
                            if (!s.params.breakpoints) return false;
                            var breakpoint = false;
                            var points = [],
                                point;
                            for (point in s.params.breakpoints) {
                                if (s.params.breakpoints.hasOwnProperty(point)) {
                                    points.push(point);
                                }
                            }
                            points.sort(function (a, b) {
                                return parseInt(a, 10) > parseInt(b, 10);
                            });
                            for (var i = 0; i < points.length; i++) {
                                point = points[i];
                                if (point >= window.innerWidth && !breakpoint) {
                                    breakpoint = point;
                                }
                            }
                            return breakpoint || "max";
                        };
                        s.setBreakpoint = function () {
                            var breakpoint = s.getActiveBreakpoint();
                            if (breakpoint && s.currentBreakpoint !== breakpoint) {
                                var breakPointsParams = breakpoint in s.params.breakpoints ? s.params.breakpoints[breakpoint] : s.originalParams;
                                var needsReLoop = s.params.loop && breakPointsParams.slidesPerView !== s.params.slidesPerView;
                                for (var param in breakPointsParams) {
                                    s.params[param] = breakPointsParams[param];
                                }
                                s.currentBreakpoint = breakpoint;
                                if (needsReLoop && s.destroyLoop) {
                                    s.reLoop(true);
                                }
                            }
                        };
                        if (s.params.breakpoints) {
                            s.setBreakpoint();
                        }
                        s.container = $(container);
                        if (s.container.length === 0) return;
                        if (s.container.length > 1) {
                            var swipers = [];
                            s.container.each(function () {
                                var container = this;
                                swipers.push(new Swiper(this, params));
                            });
                            return swipers;
                        }
                        s.container[0].swiper = s;
                        s.container.data("swiper", s);
                        s.classNames.push(s.params.containerModifierClass + s.params.direction);
                        if (s.params.freeMode) {
                            s.classNames.push(s.params.containerModifierClass + "free-mode");
                        }
                        if (!s.support.flexbox) {
                            s.classNames.push(s.params.containerModifierClass + "no-flexbox");
                            s.params.slidesPerColumn = 1;
                        }
                        if (s.params.autoHeight) {
                            s.classNames.push(s.params.containerModifierClass + "autoheight");
                        }
                        if (s.params.parallax || s.params.watchSlidesVisibility) {
                            s.params.watchSlidesProgress = true;
                        }
                        if (s.params.touchReleaseOnEdges) {
                            s.params.resistanceRatio = 0;
                        }
                        if (["cube", "coverflow", "flip"].indexOf(s.params.effect) >= 0) {
                            if (s.support.transforms3d) {
                                s.params.watchSlidesProgress = true;
                                s.classNames.push(s.params.containerModifierClass + "3d");
                            } else {
                                s.params.effect = "slide";
                            }
                        }
                        if (s.params.effect !== "slide") {
                            s.classNames.push(s.params.containerModifierClass + s.params.effect);
                        }
                        if (s.params.effect === "cube") {
                            s.params.resistanceRatio = 0;
                            s.params.slidesPerView = 1;
                            s.params.slidesPerColumn = 1;
                            s.params.slidesPerGroup = 1;
                            s.params.centeredSlides = false;
                            s.params.spaceBetween = 0;
                            s.params.virtualTranslate = true;
                        }
                        if (s.params.effect === "fade" || s.params.effect === "flip") {
                            s.params.slidesPerView = 1;
                            s.params.slidesPerColumn = 1;
                            s.params.slidesPerGroup = 1;
                            s.params.watchSlidesProgress = true;
                            s.params.spaceBetween = 0;
                            if (typeof initialVirtualTranslate === "undefined") {
                                s.params.virtualTranslate = true;
                            }
                        }
                        if (s.params.grabCursor && s.support.touch) {
                            s.params.grabCursor = false;
                        }
                        s.wrapper = s.container.children("." + s.params.wrapperClass);
                        if (s.params.pagination) {
                            s.paginationContainer = $(s.params.pagination);
                            if (s.params.uniqueNavElements && typeof s.params.pagination === "string" && s.paginationContainer.length > 1 && s.container.find(s.params.pagination).length === 1) {
                                s.paginationContainer = s.container.find(s.params.pagination);
                            }
                            if (s.params.paginationType === "bullets" && s.params.paginationClickable) {
                                s.paginationContainer.addClass(s.params.paginationModifierClass + "clickable");
                            } else {
                                s.params.paginationClickable = false;
                            }
                            s.paginationContainer.addClass(s.params.paginationModifierClass + s.params.paginationType);
                        }
                        if (s.params.nextButton || s.params.prevButton) {
                            if (s.params.nextButton) {
                                s.nextButton = $(s.params.nextButton);
                                if (s.params.uniqueNavElements && typeof s.params.nextButton === "string" && s.nextButton.length > 1 && s.container.find(s.params.nextButton).length === 1) {
                                    s.nextButton = s.container.find(s.params.nextButton);
                                }
                            }
                            if (s.params.prevButton) {
                                s.prevButton = $(s.params.prevButton);
                                if (s.params.uniqueNavElements && typeof s.params.prevButton === "string" && s.prevButton.length > 1 && s.container.find(s.params.prevButton).length === 1) {
                                    s.prevButton = s.container.find(s.params.prevButton);
                                }
                            }
                        }
                        s.isHorizontal = function () {
                            return s.params.direction === "horizontal";
                        };
                        s.rtl = s.isHorizontal() && (s.container[0].dir.toLowerCase() === "rtl" || s.container.css("direction") === "rtl");
                        if (s.rtl) {
                            s.classNames.push(s.params.containerModifierClass + "rtl");
                        }
                        if (s.rtl) {
                            s.wrongRTL = s.wrapper.css("display") === "-webkit-box";
                        }
                        if (s.params.slidesPerColumn > 1) {
                            s.classNames.push(s.params.containerModifierClass + "multirow");
                        }
                        if (s.device.android) {
                            s.classNames.push(s.params.containerModifierClass + "android");
                        }
                        s.container.addClass(s.classNames.join(" "));
                        s.translate = 0;
                        s.progress = 0;
                        s.velocity = 0;
                        s.lockSwipeToNext = function () {
                            s.params.allowSwipeToNext = false;
                            if (s.params.allowSwipeToPrev === false && s.params.grabCursor) {
                                s.unsetGrabCursor();
                            }
                        };
                        s.lockSwipeToPrev = function () {
                            s.params.allowSwipeToPrev = false;
                            if (s.params.allowSwipeToNext === false && s.params.grabCursor) {
                                s.unsetGrabCursor();
                            }
                        };
                        s.lockSwipes = function () {
                            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = false;
                            if (s.params.grabCursor) s.unsetGrabCursor();
                        };
                        s.unlockSwipeToNext = function () {
                            s.params.allowSwipeToNext = true;
                            if (s.params.allowSwipeToPrev === true && s.params.grabCursor) {
                                s.setGrabCursor();
                            }
                        };
                        s.unlockSwipeToPrev = function () {
                            s.params.allowSwipeToPrev = true;
                            if (s.params.allowSwipeToNext === true && s.params.grabCursor) {
                                s.setGrabCursor();
                            }
                        };
                        s.unlockSwipes = function () {
                            s.params.allowSwipeToNext = s.params.allowSwipeToPrev = true;
                            if (s.params.grabCursor) s.setGrabCursor();
                        };
                        function round(a) {
                            return Math.floor(a);
                        }
                        s.setGrabCursor = function (moving) {
                            s.container[0].style.cursor = "move";
                            s.container[0].style.cursor = moving ? "-webkit-grabbing" : "-webkit-grab";
                            s.container[0].style.cursor = moving ? "-moz-grabbin" : "-moz-grab";
                            s.container[0].style.cursor = moving ? "grabbing" : "grab";
                        };
                        s.unsetGrabCursor = function () {
                            s.container[0].style.cursor = "";
                        };
                        if (s.params.grabCursor) {
                            s.setGrabCursor();
                        }
                        s.imagesToLoad = [];
                        s.imagesLoaded = 0;
                        s.loadImage = function (imgElement, src, srcset, sizes, checkForComplete, callback) {
                            var image;
                            function onReady() {
                                if (callback) callback();
                            }
                            if (!imgElement.complete || !checkForComplete) {
                                if (src) {
                                    image = new window.Image();
                                    image.onload = onReady;
                                    image.onerror = onReady;
                                    if (sizes) {
                                        image.sizes = sizes;
                                    }
                                    if (srcset) {
                                        image.srcset = srcset;
                                    }
                                    if (src) {
                                        image.src = src;
                                    }
                                } else {
                                    onReady();
                                }
                            } else {
                                onReady();
                            }
                        };
                        s.preloadImages = function () {
                            s.imagesToLoad = s.container.find("img");
                            function _onReady() {
                                if (typeof s === "undefined" || s === null || !s) return;
                                if (s.imagesLoaded !== undefined) s.imagesLoaded++;
                                if (s.imagesLoaded === s.imagesToLoad.length) {
                                    if (s.params.updateOnImagesReady) s.update();
                                    s.emit("onImagesReady", s);
                                }
                            }
                            for (var i = 0; i < s.imagesToLoad.length; i++) {
                                s.loadImage(
                                    s.imagesToLoad[i],
                                    s.imagesToLoad[i].currentSrc || s.imagesToLoad[i].getAttribute("src"),
                                    s.imagesToLoad[i].srcset || s.imagesToLoad[i].getAttribute("srcset"),
                                    s.imagesToLoad[i].sizes || s.imagesToLoad[i].getAttribute("sizes"),
                                    true,
                                    _onReady
                                );
                            }
                        };
                        s.autoplayTimeoutId = undefined;
                        s.autoplaying = false;
                        s.autoplayPaused = false;
                        function autoplay() {
                            var autoplayDelay = s.params.autoplay;
                            var activeSlide = s.slides.eq(s.activeIndex);
                            if (activeSlide.attr("data-swiper-autoplay")) {
                                autoplayDelay = activeSlide.attr("data-swiper-autoplay") || s.params.autoplay;
                            }
                            s.autoplayTimeoutId = setTimeout(function () {
                                if (s.params.loop) {
                                    s.fixLoop();
                                    s._slideNext();
                                    s.emit("onAutoplay", s);
                                } else {
                                    if (!s.isEnd) {
                                        s._slideNext();
                                        s.emit("onAutoplay", s);
                                    } else {
                                        if (!params.autoplayStopOnLast) {
                                            s._slideTo(0);
                                            s.emit("onAutoplay", s);
                                        } else {
                                            s.stopAutoplay();
                                        }
                                    }
                                }
                            }, autoplayDelay);
                        }
                        s.startAutoplay = function () {
                            if (typeof s.autoplayTimeoutId !== "undefined") return false;
                            if (!s.params.autoplay) return false;
                            if (s.autoplaying) return false;
                            s.autoplaying = true;
                            s.emit("onAutoplayStart", s);
                            autoplay();
                        };
                        s.stopAutoplay = function (internal) {
                            if (!s.autoplayTimeoutId) return;
                            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
                            s.autoplaying = false;
                            s.autoplayTimeoutId = undefined;
                            s.emit("onAutoplayStop", s);
                        };
                        s.pauseAutoplay = function (speed) {
                            if (s.autoplayPaused) return;
                            if (s.autoplayTimeoutId) clearTimeout(s.autoplayTimeoutId);
                            s.autoplayPaused = true;
                            if (speed === 0) {
                                s.autoplayPaused = false;
                                autoplay();
                            } else {
                                s.wrapper.transitionEnd(function () {
                                    if (!s) return;
                                    s.autoplayPaused = false;
                                    if (!s.autoplaying) {
                                        s.stopAutoplay();
                                    } else {
                                        autoplay();
                                    }
                                });
                            }
                        };
                        s.minTranslate = function () {
                            return -s.snapGrid[0];
                        };
                        s.maxTranslate = function () {
                            return -s.snapGrid[s.snapGrid.length - 1];
                        };
                        s.updateAutoHeight = function () {
                            var activeSlides = [];
                            var newHeight = 0;
                            var i;
                            if (s.params.slidesPerView !== "auto" && s.params.slidesPerView > 1) {
                                for (i = 0; i < Math.ceil(s.params.slidesPerView); i++) {
                                    var index = s.activeIndex + i;
                                    if (index > s.slides.length) break;
                                    activeSlides.push(s.slides.eq(index)[0]);
                                }
                            } else {
                                activeSlides.push(s.slides.eq(s.activeIndex)[0]);
                            }
                            for (i = 0; i < activeSlides.length; i++) {
                                if (typeof activeSlides[i] !== "undefined") {
                                    var height = activeSlides[i].offsetHeight;
                                    newHeight = height > newHeight ? height : newHeight;
                                }
                            }
                            if (newHeight) s.wrapper.css("height", newHeight + "px");
                        };
                        s.updateContainerSize = function () {
                            var width, height;
                            if (typeof s.params.width !== "undefined") {
                                width = s.params.width;
                            } else {
                                width = s.container[0].clientWidth;
                            }
                            if (typeof s.params.height !== "undefined") {
                                height = s.params.height;
                            } else {
                                height = s.container[0].clientHeight;
                            }
                            if ((width === 0 && s.isHorizontal()) || (height === 0 && !s.isHorizontal())) {
                                return;
                            }
                            width = width - parseInt(s.container.css("padding-left"), 10) - parseInt(s.container.css("padding-right"), 10);
                            height = height - parseInt(s.container.css("padding-top"), 10) - parseInt(s.container.css("padding-bottom"), 10);
                            s.width = width;
                            s.height = height;
                            s.size = s.isHorizontal() ? s.width : s.height;
                        };
                        s.updateSlidesSize = function () {
                            s.slides = s.wrapper.children("." + s.params.slideClass);
                            s.snapGrid = [];
                            s.slidesGrid = [];
                            s.slidesSizesGrid = [];
                            var spaceBetween = s.params.spaceBetween,
                                slidePosition = -s.params.slidesOffsetBefore,
                                i,
                                prevSlideSize = 0,
                                index = 0;
                            if (typeof s.size === "undefined") return;
                            if (typeof spaceBetween === "string" && spaceBetween.indexOf("%") >= 0) {
                                spaceBetween = (parseFloat(spaceBetween.replace("%", "")) / 100) * s.size;
                            }
                            s.virtualSize = -spaceBetween;
                            if (s.rtl) s.slides.css({ marginLeft: "", marginTop: "" });
                            else s.slides.css({ marginRight: "", marginBottom: "" });
                            var slidesNumberEvenToRows;
                            if (s.params.slidesPerColumn > 1) {
                                if (Math.floor(s.slides.length / s.params.slidesPerColumn) === s.slides.length / s.params.slidesPerColumn) {
                                    slidesNumberEvenToRows = s.slides.length;
                                } else {
                                    slidesNumberEvenToRows = Math.ceil(s.slides.length / s.params.slidesPerColumn) * s.params.slidesPerColumn;
                                }
                                if (s.params.slidesPerView !== "auto" && s.params.slidesPerColumnFill === "row") {
                                    slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, s.params.slidesPerView * s.params.slidesPerColumn);
                                }
                            }
                            var slideSize;
                            var slidesPerColumn = s.params.slidesPerColumn;
                            var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
                            var numFullColumns = slidesPerRow - (s.params.slidesPerColumn * slidesPerRow - s.slides.length);
                            for (i = 0; i < s.slides.length; i++) {
                                slideSize = 0;
                                var slide = s.slides.eq(i);
                                if (s.params.slidesPerColumn > 1) {
                                    var newSlideOrderIndex;
                                    var column, row;
                                    if (s.params.slidesPerColumnFill === "column") {
                                        column = Math.floor(i / slidesPerColumn);
                                        row = i - column * slidesPerColumn;
                                        if (column > numFullColumns || (column === numFullColumns && row === slidesPerColumn - 1)) {
                                            if (++row >= slidesPerColumn) {
                                                row = 0;
                                                column++;
                                            }
                                        }
                                        newSlideOrderIndex = column + (row * slidesNumberEvenToRows) / slidesPerColumn;
                                        slide.css({
                                            "-webkit-box-ordinal-group": newSlideOrderIndex,
                                            "-moz-box-ordinal-group": newSlideOrderIndex,
                                            "-ms-flex-order": newSlideOrderIndex,
                                            "-webkit-order": newSlideOrderIndex,
                                            order: newSlideOrderIndex,
                                        });
                                    } else {
                                        row = Math.floor(i / slidesPerRow);
                                        column = i - row * slidesPerRow;
                                    }
                                    slide
                                        .css("margin-" + (s.isHorizontal() ? "top" : "left"), row !== 0 && s.params.spaceBetween && s.params.spaceBetween + "px")
                                        .attr("data-swiper-column", column)
                                        .attr("data-swiper-row", row);
                                }
                                if (slide.css("display") === "none") continue;
                                if (s.params.slidesPerView === "auto") {
                                    slideSize = s.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true);
                                    if (s.params.roundLengths) slideSize = round(slideSize);
                                } else {
                                    slideSize = (s.size - (s.params.slidesPerView - 1) * spaceBetween) / s.params.slidesPerView;
                                    if (s.params.roundLengths) slideSize = round(slideSize);
                                    if (s.isHorizontal()) {
                                        s.slides[i].style.width = slideSize + "px";
                                    } else {
                                        s.slides[i].style.height = slideSize + "px";
                                    }
                                }
                                s.slides[i].swiperSlideSize = slideSize;
                                s.slidesSizesGrid.push(slideSize);
                                if (s.params.centeredSlides) {
                                    slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
                                    if (prevSlideSize === 0 && i !== 0) slidePosition = slidePosition - s.size / 2 - spaceBetween;
                                    if (i === 0) slidePosition = slidePosition - s.size / 2 - spaceBetween;
                                    if (Math.abs(slidePosition) < 1 / 1e3) slidePosition = 0;
                                    if (index % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                                    s.slidesGrid.push(slidePosition);
                                } else {
                                    if (index % s.params.slidesPerGroup === 0) s.snapGrid.push(slidePosition);
                                    s.slidesGrid.push(slidePosition);
                                    slidePosition = slidePosition + slideSize + spaceBetween;
                                }
                                s.virtualSize += slideSize + spaceBetween;
                                prevSlideSize = slideSize;
                                index++;
                            }
                            s.virtualSize = Math.max(s.virtualSize, s.size) + s.params.slidesOffsetAfter;
                            var newSlidesGrid;
                            if (s.rtl && s.wrongRTL && (s.params.effect === "slide" || s.params.effect === "coverflow")) {
                                s.wrapper.css({ width: s.virtualSize + s.params.spaceBetween + "px" });
                            }
                            if (!s.support.flexbox || s.params.setWrapperSize) {
                                if (s.isHorizontal()) s.wrapper.css({ width: s.virtualSize + s.params.spaceBetween + "px" });
                                else s.wrapper.css({ height: s.virtualSize + s.params.spaceBetween + "px" });
                            }
                            if (s.params.slidesPerColumn > 1) {
                                s.virtualSize = (slideSize + s.params.spaceBetween) * slidesNumberEvenToRows;
                                s.virtualSize = Math.ceil(s.virtualSize / s.params.slidesPerColumn) - s.params.spaceBetween;
                                if (s.isHorizontal()) s.wrapper.css({ width: s.virtualSize + s.params.spaceBetween + "px" });
                                else s.wrapper.css({ height: s.virtualSize + s.params.spaceBetween + "px" });
                                if (s.params.centeredSlides) {
                                    newSlidesGrid = [];
                                    for (i = 0; i < s.snapGrid.length; i++) {
                                        if (s.snapGrid[i] < s.virtualSize + s.snapGrid[0]) newSlidesGrid.push(s.snapGrid[i]);
                                    }
                                    s.snapGrid = newSlidesGrid;
                                }
                            }
                            if (!s.params.centeredSlides) {
                                newSlidesGrid = [];
                                for (i = 0; i < s.snapGrid.length; i++) {
                                    if (s.snapGrid[i] <= s.virtualSize - s.size) {
                                        newSlidesGrid.push(s.snapGrid[i]);
                                    }
                                }
                                s.snapGrid = newSlidesGrid;
                                if (Math.floor(s.virtualSize - s.size) - Math.floor(s.snapGrid[s.snapGrid.length - 1]) > 1) {
                                    s.snapGrid.push(s.virtualSize - s.size);
                                }
                            }
                            if (s.snapGrid.length === 0) s.snapGrid = [0];
                            if (s.params.spaceBetween !== 0) {
                                if (s.isHorizontal()) {
                                    if (s.rtl) s.slides.css({ marginLeft: spaceBetween + "px" });
                                    else s.slides.css({ marginRight: spaceBetween + "px" });
                                } else s.slides.css({ marginBottom: spaceBetween + "px" });
                            }
                            if (s.params.watchSlidesProgress) {
                                s.updateSlidesOffset();
                            }
                        };
                        s.updateSlidesOffset = function () {
                            for (var i = 0; i < s.slides.length; i++) {
                                s.slides[i].swiperSlideOffset = s.isHorizontal() ? s.slides[i].offsetLeft : s.slides[i].offsetTop;
                            }
                        };
                        s.currentSlidesPerView = function () {
                            var spv = 1,
                                i,
                                j;
                            if (s.params.centeredSlides) {
                                var size = s.slides[s.activeIndex].swiperSlideSize;
                                var breakLoop;
                                for (i = s.activeIndex + 1; i < s.slides.length; i++) {
                                    if (s.slides[i] && !breakLoop) {
                                        size += s.slides[i].swiperSlideSize;
                                        spv++;
                                        if (size > s.size) breakLoop = true;
                                    }
                                }
                                for (j = s.activeIndex - 1; j >= 0; j--) {
                                    if (s.slides[j] && !breakLoop) {
                                        size += s.slides[j].swiperSlideSize;
                                        spv++;
                                        if (size > s.size) breakLoop = true;
                                    }
                                }
                            } else {
                                for (i = s.activeIndex + 1; i < s.slides.length; i++) {
                                    if (s.slidesGrid[i] - s.slidesGrid[s.activeIndex] < s.size) {
                                        spv++;
                                    }
                                }
                            }
                            return spv;
                        };
                        s.updateSlidesProgress = function (translate) {
                            if (typeof translate === "undefined") {
                                translate = s.translate || 0;
                            }
                            if (s.slides.length === 0) return;
                            if (typeof s.slides[0].swiperSlideOffset === "undefined") s.updateSlidesOffset();
                            var offsetCenter = -translate;
                            if (s.rtl) offsetCenter = translate;
                            s.slides.removeClass(s.params.slideVisibleClass);
                            for (var i = 0; i < s.slides.length; i++) {
                                var slide = s.slides[i];
                                var slideProgress = (offsetCenter + (s.params.centeredSlides ? s.minTranslate() : 0) - slide.swiperSlideOffset) / (slide.swiperSlideSize + s.params.spaceBetween);
                                if (s.params.watchSlidesVisibility) {
                                    var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
                                    var slideAfter = slideBefore + s.slidesSizesGrid[i];
                                    var isVisible = (slideBefore >= 0 && slideBefore < s.size) || (slideAfter > 0 && slideAfter <= s.size) || (slideBefore <= 0 && slideAfter >= s.size);
                                    if (isVisible) {
                                        s.slides.eq(i).addClass(s.params.slideVisibleClass);
                                    }
                                }
                                slide.progress = s.rtl ? -slideProgress : slideProgress;
                            }
                        };
                        s.updateProgress = function (translate) {
                            if (typeof translate === "undefined") {
                                translate = s.translate || 0;
                            }
                            var translatesDiff = s.maxTranslate() - s.minTranslate();
                            var wasBeginning = s.isBeginning;
                            var wasEnd = s.isEnd;
                            if (translatesDiff === 0) {
                                s.progress = 0;
                                s.isBeginning = s.isEnd = true;
                            } else {
                                s.progress = (translate - s.minTranslate()) / translatesDiff;
                                s.isBeginning = s.progress <= 0;
                                s.isEnd = s.progress >= 1;
                            }
                            if (s.isBeginning && !wasBeginning) s.emit("onReachBeginning", s);
                            if (s.isEnd && !wasEnd) s.emit("onReachEnd", s);
                            if (s.params.watchSlidesProgress) s.updateSlidesProgress(translate);
                            s.emit("onProgress", s, s.progress);
                        };
                        s.updateActiveIndex = function () {
                            var translate = s.rtl ? s.translate : -s.translate;
                            var newActiveIndex, i, snapIndex;
                            for (i = 0; i < s.slidesGrid.length; i++) {
                                if (typeof s.slidesGrid[i + 1] !== "undefined") {
                                    if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1] - (s.slidesGrid[i + 1] - s.slidesGrid[i]) / 2) {
                                        newActiveIndex = i;
                                    } else if (translate >= s.slidesGrid[i] && translate < s.slidesGrid[i + 1]) {
                                        newActiveIndex = i + 1;
                                    }
                                } else {
                                    if (translate >= s.slidesGrid[i]) {
                                        newActiveIndex = i;
                                    }
                                }
                            }
                            if (s.params.normalizeSlideIndex) {
                                if (newActiveIndex < 0 || typeof newActiveIndex === "undefined") newActiveIndex = 0;
                            }
                            snapIndex = Math.floor(newActiveIndex / s.params.slidesPerGroup);
                            if (snapIndex >= s.snapGrid.length) snapIndex = s.snapGrid.length - 1;
                            if (newActiveIndex === s.activeIndex) {
                                return;
                            }
                            s.snapIndex = snapIndex;
                            s.previousIndex = s.activeIndex;
                            s.activeIndex = newActiveIndex;
                            s.updateClasses();
                            s.updateRealIndex();
                        };
                        s.updateRealIndex = function () {
                            s.realIndex = parseInt(s.slides.eq(s.activeIndex).attr("data-swiper-slide-index") || s.activeIndex, 10);
                        };
                        s.updateClasses = function () {
                            s.slides.removeClass(
                                s.params.slideActiveClass +
                                " " +
                                s.params.slideNextClass +
                                " " +
                                s.params.slidePrevClass +
                                " " +
                                s.params.slideDuplicateActiveClass +
                                " " +
                                s.params.slideDuplicateNextClass +
                                " " +
                                s.params.slideDuplicatePrevClass
                            );
                            var activeSlide = s.slides.eq(s.activeIndex);
                            activeSlide.addClass(s.params.slideActiveClass);
                            if (params.loop) {
                                if (activeSlide.hasClass(s.params.slideDuplicateClass)) {
                                    s.wrapper.children("." + s.params.slideClass + ":not(." + s.params.slideDuplicateClass + ')[data-swiper-slide-index="' + s.realIndex + '"]').addClass(s.params.slideDuplicateActiveClass);
                                } else {
                                    s.wrapper.children("." + s.params.slideClass + "." + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + s.realIndex + '"]').addClass(s.params.slideDuplicateActiveClass);
                                }
                            }
                            var nextSlide = activeSlide.next("." + s.params.slideClass).addClass(s.params.slideNextClass);
                            if (s.params.loop && nextSlide.length === 0) {
                                nextSlide = s.slides.eq(0);
                                nextSlide.addClass(s.params.slideNextClass);
                            }
                            var prevSlide = activeSlide.prev("." + s.params.slideClass).addClass(s.params.slidePrevClass);
                            if (s.params.loop && prevSlide.length === 0) {
                                prevSlide = s.slides.eq(-1);
                                prevSlide.addClass(s.params.slidePrevClass);
                            }
                            if (params.loop) {
                                if (nextSlide.hasClass(s.params.slideDuplicateClass)) {
                                    s.wrapper
                                        .children("." + s.params.slideClass + ":not(." + s.params.slideDuplicateClass + ')[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]')
                                        .addClass(s.params.slideDuplicateNextClass);
                                } else {
                                    s.wrapper
                                        .children("." + s.params.slideClass + "." + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]')
                                        .addClass(s.params.slideDuplicateNextClass);
                                }
                                if (prevSlide.hasClass(s.params.slideDuplicateClass)) {
                                    s.wrapper
                                        .children("." + s.params.slideClass + ":not(." + s.params.slideDuplicateClass + ')[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]')
                                        .addClass(s.params.slideDuplicatePrevClass);
                                } else {
                                    s.wrapper
                                        .children("." + s.params.slideClass + "." + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]')
                                        .addClass(s.params.slideDuplicatePrevClass);
                                }
                            }
                            if (s.paginationContainer && s.paginationContainer.length > 0) {
                                var current,
                                    total = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                                if (s.params.loop) {
                                    current = Math.ceil((s.activeIndex - s.loopedSlides) / s.params.slidesPerGroup);
                                    if (current > s.slides.length - 1 - s.loopedSlides * 2) {
                                        current = current - (s.slides.length - s.loopedSlides * 2);
                                    }
                                    if (current > total - 1) current = current - total;
                                    if (current < 0 && s.params.paginationType !== "bullets") current = total + current;
                                } else {
                                    if (typeof s.snapIndex !== "undefined") {
                                        current = s.snapIndex;
                                    } else {
                                        current = s.activeIndex || 0;
                                    }
                                }
                                if (s.params.paginationType === "bullets" && s.bullets && s.bullets.length > 0) {
                                    s.bullets.removeClass(s.params.bulletActiveClass);
                                    if (s.paginationContainer.length > 1) {
                                        s.bullets.each(function () {
                                            if ($(this).index() === current) $(this).addClass(s.params.bulletActiveClass);
                                        });
                                    } else {
                                        s.bullets.eq(current).addClass(s.params.bulletActiveClass);
                                    }
                                }
                                if (s.params.paginationType === "fraction") {
                                    s.paginationContainer.find("." + s.params.paginationCurrentClass).text(current + 1);
                                    s.paginationContainer.find("." + s.params.paginationTotalClass).text(total);
                                }
                                if (s.params.paginationType === "progress") {
                                    var scale = (current + 1) / total,
                                        scaleX = scale,
                                        scaleY = 1;
                                    if (!s.isHorizontal()) {
                                        scaleY = scale;
                                        scaleX = 1;
                                    }
                                    s.paginationContainer
                                        .find("." + s.params.paginationProgressbarClass)
                                        .transform("translate3d(0,0,0) scaleX(" + scaleX + ") scaleY(" + scaleY + ")")
                                        .transition(s.params.speed);
                                }
                                if (s.params.paginationType === "custom" && s.params.paginationCustomRender) {
                                    s.paginationContainer.html(s.params.paginationCustomRender(s, current + 1, total));
                                    s.emit("onPaginationRendered", s, s.paginationContainer[0]);
                                }
                            }
                            if (!s.params.loop) {
                                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                                    if (s.isBeginning) {
                                        s.prevButton.addClass(s.params.buttonDisabledClass);
                                        if (s.params.a11y && s.a11y) s.a11y.disable(s.prevButton);
                                    } else {
                                        s.prevButton.removeClass(s.params.buttonDisabledClass);
                                        if (s.params.a11y && s.a11y) s.a11y.enable(s.prevButton);
                                    }
                                }
                                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                                    if (s.isEnd) {
                                        s.nextButton.addClass(s.params.buttonDisabledClass);
                                        if (s.params.a11y && s.a11y) s.a11y.disable(s.nextButton);
                                    } else {
                                        s.nextButton.removeClass(s.params.buttonDisabledClass);
                                        if (s.params.a11y && s.a11y) s.a11y.enable(s.nextButton);
                                    }
                                }
                            }
                        };
                        s.updatePagination = function () {
                            if (!s.params.pagination) return;
                            if (s.paginationContainer && s.paginationContainer.length > 0) {
                                var paginationHTML = "";
                                if (s.params.paginationType === "bullets") {
                                    var numberOfBullets = s.params.loop ? Math.ceil((s.slides.length - s.loopedSlides * 2) / s.params.slidesPerGroup) : s.snapGrid.length;
                                    for (var i = 0; i < numberOfBullets; i++) {
                                        if (s.params.paginationBulletRender) {
                                            paginationHTML += s.params.paginationBulletRender(s, i, s.params.bulletClass);
                                        } else {
                                            paginationHTML += "<" + s.params.paginationElement + ' class="' + s.params.bulletClass + '"></' + s.params.paginationElement + ">";
                                        }
                                    }
                                    s.paginationContainer.html(paginationHTML);
                                    s.bullets = s.paginationContainer.find("." + s.params.bulletClass);
                                    if (s.params.paginationClickable && s.params.a11y && s.a11y) {
                                        s.a11y.initPagination();
                                    }
                                }
                                if (s.params.paginationType === "fraction") {
                                    if (s.params.paginationFractionRender) {
                                        paginationHTML = s.params.paginationFractionRender(s, s.params.paginationCurrentClass, s.params.paginationTotalClass);
                                    } else {
                                        paginationHTML = '<span class="' + s.params.paginationCurrentClass + '"></span>' + " / " + '<span class="' + s.params.paginationTotalClass + '"></span>';
                                    }
                                    s.paginationContainer.html(paginationHTML);
                                }
                                if (s.params.paginationType === "progress") {
                                    if (s.params.paginationProgressRender) {
                                        paginationHTML = s.params.paginationProgressRender(s, s.params.paginationProgressbarClass);
                                    } else {
                                        paginationHTML = '<span class="' + s.params.paginationProgressbarClass + '"></span>';
                                    }
                                    s.paginationContainer.html(paginationHTML);
                                }
                                if (s.params.paginationType !== "custom") {
                                    s.emit("onPaginationRendered", s, s.paginationContainer[0]);
                                }
                            }
                        };
                        s.update = function (updateTranslate) {
                            if (!s) return;
                            s.updateContainerSize();
                            s.updateSlidesSize();
                            s.updateProgress();
                            s.updatePagination();
                            s.updateClasses();
                            if (s.params.scrollbar && s.scrollbar) {
                                s.scrollbar.set();
                            }
                            var newTranslate;
                            function forceSetTranslate() {
                                var translate = s.rtl ? -s.translate : s.translate;
                                newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                                s.setWrapperTranslate(newTranslate);
                                s.updateActiveIndex();
                                s.updateClasses();
                            }
                            if (updateTranslate) {
                                var translated;
                                if (s.controller && s.controller.spline) {
                                    s.controller.spline = undefined;
                                }
                                if (s.params.freeMode) {
                                    forceSetTranslate();
                                    if (s.params.autoHeight) {
                                        s.updateAutoHeight();
                                    }
                                } else {
                                    if ((s.params.slidesPerView === "auto" || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                                        translated = s.slideTo(s.slides.length - 1, 0, false, true);
                                    } else {
                                        translated = s.slideTo(s.activeIndex, 0, false, true);
                                    }
                                    if (!translated) {
                                        forceSetTranslate();
                                    }
                                }
                            } else if (s.params.autoHeight) {
                                s.updateAutoHeight();
                            }
                        };
                        s.onResize = function (forceUpdatePagination) {
                            if (s.params.onBeforeResize) s.params.onBeforeResize(s);
                            if (s.params.breakpoints) {
                                s.setBreakpoint();
                            }
                            var allowSwipeToPrev = s.params.allowSwipeToPrev;
                            var allowSwipeToNext = s.params.allowSwipeToNext;
                            s.params.allowSwipeToPrev = s.params.allowSwipeToNext = true;
                            s.updateContainerSize();
                            s.updateSlidesSize();
                            if (s.params.slidesPerView === "auto" || s.params.freeMode || forceUpdatePagination) s.updatePagination();
                            if (s.params.scrollbar && s.scrollbar) {
                                s.scrollbar.set();
                            }
                            if (s.controller && s.controller.spline) {
                                s.controller.spline = undefined;
                            }
                            var slideChangedBySlideTo = false;
                            if (s.params.freeMode) {
                                var newTranslate = Math.min(Math.max(s.translate, s.maxTranslate()), s.minTranslate());
                                s.setWrapperTranslate(newTranslate);
                                s.updateActiveIndex();
                                s.updateClasses();
                                if (s.params.autoHeight) {
                                    s.updateAutoHeight();
                                }
                            } else {
                                s.updateClasses();
                                if ((s.params.slidesPerView === "auto" || s.params.slidesPerView > 1) && s.isEnd && !s.params.centeredSlides) {
                                    slideChangedBySlideTo = s.slideTo(s.slides.length - 1, 0, false, true);
                                } else {
                                    slideChangedBySlideTo = s.slideTo(s.activeIndex, 0, false, true);
                                }
                            }
                            if (s.params.lazyLoading && !slideChangedBySlideTo && s.lazy) {
                                s.lazy.load();
                            }
                            s.params.allowSwipeToPrev = allowSwipeToPrev;
                            s.params.allowSwipeToNext = allowSwipeToNext;
                            if (s.params.onAfterResize) s.params.onAfterResize(s);
                        };
                        s.touchEventsDesktop = { start: "mousedown", move: "mousemove", end: "mouseup" };
                        if (window.navigator.pointerEnabled) s.touchEventsDesktop = { start: "pointerdown", move: "pointermove", end: "pointerup" };
                        else if (window.navigator.msPointerEnabled) s.touchEventsDesktop = { start: "MSPointerDown", move: "MSPointerMove", end: "MSPointerUp" };
                        s.touchEvents = {
                            start: s.support.touch || !s.params.simulateTouch ? "touchstart" : s.touchEventsDesktop.start,
                            move: s.support.touch || !s.params.simulateTouch ? "touchmove" : s.touchEventsDesktop.move,
                            end: s.support.touch || !s.params.simulateTouch ? "touchend" : s.touchEventsDesktop.end,
                        };
                        if (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) {
                            (s.params.touchEventsTarget === "container" ? s.container : s.wrapper).addClass("swiper-wp8-" + s.params.direction);
                        }
                        s.initEvents = function (detach) {
                            var actionDom = detach ? "off" : "on";
                            var action = detach ? "removeEventListener" : "addEventListener";
                            var touchEventsTarget = s.params.touchEventsTarget === "container" ? s.container[0] : s.wrapper[0];
                            var target = s.support.touch ? touchEventsTarget : document;
                            var moveCapture = s.params.nested ? true : false;
                            if (s.browser.ie) {
                                touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, false);
                                target[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                                target[action](s.touchEvents.end, s.onTouchEnd, false);
                            } else {
                                if (s.support.touch) {
                                    var passiveListener = s.touchEvents.start === "touchstart" && s.support.passiveListener && s.params.passiveListeners ? { passive: true, capture: false } : false;
                                    touchEventsTarget[action](s.touchEvents.start, s.onTouchStart, passiveListener);
                                    touchEventsTarget[action](s.touchEvents.move, s.onTouchMove, moveCapture);
                                    touchEventsTarget[action](s.touchEvents.end, s.onTouchEnd, passiveListener);
                                }
                                if ((params.simulateTouch && !s.device.ios && !s.device.android) || (params.simulateTouch && !s.support.touch && s.device.ios)) {
                                    touchEventsTarget[action]("mousedown", s.onTouchStart, false);
                                    document[action]("mousemove", s.onTouchMove, moveCapture);
                                    document[action]("mouseup", s.onTouchEnd, false);
                                }
                            }
                            window[action]("resize", s.onResize);
                            if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                                s.nextButton[actionDom]("click", s.onClickNext);
                                if (s.params.a11y && s.a11y) s.nextButton[actionDom]("keydown", s.a11y.onEnterKey);
                            }
                            if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                                s.prevButton[actionDom]("click", s.onClickPrev);
                                if (s.params.a11y && s.a11y) s.prevButton[actionDom]("keydown", s.a11y.onEnterKey);
                            }
                            if (s.params.pagination && s.params.paginationClickable) {
                                s.paginationContainer[actionDom]("click", "." + s.params.bulletClass, s.onClickIndex);
                                if (s.params.a11y && s.a11y) s.paginationContainer[actionDom]("keydown", "." + s.params.bulletClass, s.a11y.onEnterKey);
                            }
                            if (s.params.preventClicks || s.params.preventClicksPropagation) touchEventsTarget[action]("click", s.preventClicks, true);
                        };
                        s.attachEvents = function () {
                            s.initEvents();
                        };
                        s.detachEvents = function () {
                            s.initEvents(true);
                        };
                        s.allowClick = true;
                        s.preventClicks = function (e) {
                            if (!s.allowClick) {
                                if (s.params.preventClicks) e.preventDefault();
                                if (s.params.preventClicksPropagation && s.animating) {
                                    e.stopPropagation();
                                    e.stopImmediatePropagation();
                                }
                            }
                        };
                        s.onClickNext = function (e) {
                            e.preventDefault();
                            if (s.isEnd && !s.params.loop) return;
                            s.slideNext();
                        };
                        s.onClickPrev = function (e) {
                            e.preventDefault();
                            if (s.isBeginning && !s.params.loop) return;
                            s.slidePrev();
                        };
                        s.onClickIndex = function (e) {
                            e.preventDefault();
                            var index = $(this).index() * s.params.slidesPerGroup;
                            if (s.params.loop) index = index + s.loopedSlides;
                            s.slideTo(index);
                        };
                        function findElementInEvent(e, selector) {
                            var el = $(e.target);
                            if (!el.is(selector)) {
                                if (typeof selector === "string") {
                                    el = el.parents(selector);
                                } else if (selector.nodeType) {
                                    var found;
                                    el.parents().each(function (index, _el) {
                                        if (_el === selector) found = selector;
                                    });
                                    if (!found) return undefined;
                                    else return selector;
                                }
                            }
                            if (el.length === 0) {
                                return undefined;
                            }
                            return el[0];
                        }
                        s.updateClickedSlide = function (e) {
                            var slide = findElementInEvent(e, "." + s.params.slideClass);
                            var slideFound = false;
                            if (slide) {
                                for (var i = 0; i < s.slides.length; i++) {
                                    if (s.slides[i] === slide) slideFound = true;
                                }
                            }
                            if (slide && slideFound) {
                                s.clickedSlide = slide;
                                s.clickedIndex = $(slide).index();
                            } else {
                                s.clickedSlide = undefined;
                                s.clickedIndex = undefined;
                                return;
                            }
                            if (s.params.slideToClickedSlide && s.clickedIndex !== undefined && s.clickedIndex !== s.activeIndex) {
                                var slideToIndex = s.clickedIndex,
                                    realIndex,
                                    duplicatedSlides,
                                    slidesPerView = s.params.slidesPerView === "auto" ? s.currentSlidesPerView() : s.params.slidesPerView;
                                if (s.params.loop) {
                                    if (s.animating) return;
                                    realIndex = parseInt($(s.clickedSlide).attr("data-swiper-slide-index"), 10);
                                    if (s.params.centeredSlides) {
                                        if (slideToIndex < s.loopedSlides - slidesPerView / 2 || slideToIndex > s.slides.length - s.loopedSlides + slidesPerView / 2) {
                                            s.fixLoop();
                                            slideToIndex = s.wrapper
                                                .children("." + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + s.params.slideDuplicateClass + ")")
                                                .eq(0)
                                                .index();
                                            setTimeout(function () {
                                                s.slideTo(slideToIndex);
                                            }, 0);
                                        } else {
                                            s.slideTo(slideToIndex);
                                        }
                                    } else {
                                        if (slideToIndex > s.slides.length - slidesPerView) {
                                            s.fixLoop();
                                            slideToIndex = s.wrapper
                                                .children("." + s.params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + s.params.slideDuplicateClass + ")")
                                                .eq(0)
                                                .index();
                                            setTimeout(function () {
                                                s.slideTo(slideToIndex);
                                            }, 0);
                                        } else {
                                            s.slideTo(slideToIndex);
                                        }
                                    }
                                } else {
                                    s.slideTo(slideToIndex);
                                }
                            }
                        };
                        var isTouched,
                            isMoved,
                            allowTouchCallbacks,
                            touchStartTime,
                            isScrolling,
                            currentTranslate,
                            startTranslate,
                            allowThresholdMove,
                            formElements = "input, select, textarea, button, video",
                            lastClickTime = Date.now(),
                            clickTimeout,
                            velocities = [],
                            allowMomentumBounce;
                        s.animating = false;
                        s.touches = { startX: 0, startY: 0, currentX: 0, currentY: 0, diff: 0 };
                        var isTouchEvent, startMoving;
                        s.onTouchStart = function (e) {
                            if (e.originalEvent) e = e.originalEvent;
                            isTouchEvent = e.type === "touchstart";
                            if (!isTouchEvent && "which" in e && e.which === 3) return;
                            if (s.params.noSwiping && findElementInEvent(e, "." + s.params.noSwipingClass)) {
                                s.allowClick = true;
                                return;
                            }
                            if (s.params.swipeHandler) {
                                if (!findElementInEvent(e, s.params.swipeHandler)) return;
                            }
                            var startX = (s.touches.currentX = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX);
                            var startY = (s.touches.currentY = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY);
                            if (s.device.ios && s.params.iOSEdgeSwipeDetection && startX <= s.params.iOSEdgeSwipeThreshold) {
                                return;
                            }
                            isTouched = true;
                            isMoved = false;
                            allowTouchCallbacks = true;
                            isScrolling = undefined;
                            startMoving = undefined;
                            s.touches.startX = startX;
                            s.touches.startY = startY;
                            touchStartTime = Date.now();
                            s.allowClick = true;
                            s.updateContainerSize();
                            s.swipeDirection = undefined;
                            if (s.params.threshold > 0) allowThresholdMove = false;
                            if (e.type !== "touchstart") {
                                var preventDefault = true;
                                if ($(e.target).is(formElements)) preventDefault = false;
                                if (document.activeElement && $(document.activeElement).is(formElements)) {
                                    document.activeElement.blur();
                                }
                                if (preventDefault) {
                                    e.preventDefault();
                                }
                            }
                            s.emit("onTouchStart", s, e);
                        };
                        s.onTouchMove = function (e) {
                            if (e.originalEvent) e = e.originalEvent;
                            if (isTouchEvent && e.type === "mousemove") return;
                            if (e.preventedByNestedSwiper) {
                                s.touches.startX = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
                                s.touches.startY = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;
                                return;
                            }
                            if (s.params.onlyExternal) {
                                s.allowClick = false;
                                if (isTouched) {
                                    s.touches.startX = s.touches.currentX = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
                                    s.touches.startY = s.touches.currentY = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;
                                    touchStartTime = Date.now();
                                }
                                return;
                            }
                            if (isTouchEvent && s.params.touchReleaseOnEdges && !s.params.loop) {
                                if (!s.isHorizontal()) {
                                    if ((s.touches.currentY < s.touches.startY && s.translate <= s.maxTranslate()) || (s.touches.currentY > s.touches.startY && s.translate >= s.minTranslate())) {
                                        return;
                                    }
                                } else {
                                    if ((s.touches.currentX < s.touches.startX && s.translate <= s.maxTranslate()) || (s.touches.currentX > s.touches.startX && s.translate >= s.minTranslate())) {
                                        return;
                                    }
                                }
                            }
                            if (isTouchEvent && document.activeElement) {
                                if (e.target === document.activeElement && $(e.target).is(formElements)) {
                                    isMoved = true;
                                    s.allowClick = false;
                                    return;
                                }
                            }
                            if (allowTouchCallbacks) {
                                s.emit("onTouchMove", s, e);
                            }
                            if (e.targetTouches && e.targetTouches.length > 1) return;
                            s.touches.currentX = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
                            s.touches.currentY = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;
                            if (typeof isScrolling === "undefined") {
                                var touchAngle;
                                if ((s.isHorizontal() && s.touches.currentY === s.touches.startY) || (!s.isHorizontal() && s.touches.currentX === s.touches.startX)) {
                                    isScrolling = false;
                                } else {
                                    touchAngle = (Math.atan2(Math.abs(s.touches.currentY - s.touches.startY), Math.abs(s.touches.currentX - s.touches.startX)) * 180) / Math.PI;
                                    isScrolling = s.isHorizontal() ? touchAngle > s.params.touchAngle : 90 - touchAngle > s.params.touchAngle;
                                }
                            }
                            if (isScrolling) {
                                s.emit("onTouchMoveOpposite", s, e);
                            }
                            if (typeof startMoving === "undefined") {
                                if (s.touches.currentX !== s.touches.startX || s.touches.currentY !== s.touches.startY) {
                                    startMoving = true;
                                }
                            }
                            if (!isTouched) return;
                            if (isScrolling) {
                                isTouched = false;
                                return;
                            }
                            if (!startMoving) {
                                return;
                            }
                            s.allowClick = false;
                            s.emit("onSliderMove", s, e);
                            e.preventDefault();
                            if (s.params.touchMoveStopPropagation && !s.params.nested) {
                                e.stopPropagation();
                            }
                            if (!isMoved) {
                                if (params.loop) {
                                    s.fixLoop();
                                }
                                startTranslate = s.getWrapperTranslate();
                                s.setWrapperTransition(0);
                                if (s.animating) {
                                    s.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd");
                                }
                                if (s.params.autoplay && s.autoplaying) {
                                    if (s.params.autoplayDisableOnInteraction) {
                                        s.stopAutoplay();
                                    } else {
                                        s.pauseAutoplay();
                                    }
                                }
                                allowMomentumBounce = false;
                                if (s.params.grabCursor && (s.params.allowSwipeToNext === true || s.params.allowSwipeToPrev === true)) {
                                    s.setGrabCursor(true);
                                }
                            }
                            isMoved = true;
                            var diff = (s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY);
                            diff = diff * s.params.touchRatio;
                            if (s.rtl) diff = -diff;
                            s.swipeDirection = diff > 0 ? "prev" : "next";
                            currentTranslate = diff + startTranslate;
                            var disableParentSwiper = true;
                            if (diff > 0 && currentTranslate > s.minTranslate()) {
                                disableParentSwiper = false;
                                if (s.params.resistance) currentTranslate = s.minTranslate() - 1 + Math.pow(-s.minTranslate() + startTranslate + diff, s.params.resistanceRatio);
                            } else if (diff < 0 && currentTranslate < s.maxTranslate()) {
                                disableParentSwiper = false;
                                if (s.params.resistance) currentTranslate = s.maxTranslate() + 1 - Math.pow(s.maxTranslate() - startTranslate - diff, s.params.resistanceRatio);
                            }
                            if (disableParentSwiper) {
                                e.preventedByNestedSwiper = true;
                            }
                            if (!s.params.allowSwipeToNext && s.swipeDirection === "next" && currentTranslate < startTranslate) {
                                currentTranslate = startTranslate;
                            }
                            if (!s.params.allowSwipeToPrev && s.swipeDirection === "prev" && currentTranslate > startTranslate) {
                                currentTranslate = startTranslate;
                            }
                            if (s.params.threshold > 0) {
                                if (Math.abs(diff) > s.params.threshold || allowThresholdMove) {
                                    if (!allowThresholdMove) {
                                        allowThresholdMove = true;
                                        s.touches.startX = s.touches.currentX;
                                        s.touches.startY = s.touches.currentY;
                                        currentTranslate = startTranslate;
                                        s.touches.diff = s.isHorizontal() ? s.touches.currentX - s.touches.startX : s.touches.currentY - s.touches.startY;
                                        return;
                                    }
                                } else {
                                    currentTranslate = startTranslate;
                                    return;
                                }
                            }
                            if (!s.params.followFinger) return;
                            if (s.params.freeMode || s.params.watchSlidesProgress) {
                                s.updateActiveIndex();
                            }
                            if (s.params.freeMode) {
                                if (velocities.length === 0) {
                                    velocities.push({ position: s.touches[s.isHorizontal() ? "startX" : "startY"], time: touchStartTime });
                                }
                                velocities.push({ position: s.touches[s.isHorizontal() ? "currentX" : "currentY"], time: new window.Date().getTime() });
                            }
                            s.updateProgress(currentTranslate);
                            s.setWrapperTranslate(currentTranslate);
                        };
                        s.onTouchEnd = function (e) {
                            if (e.originalEvent) e = e.originalEvent;
                            if (allowTouchCallbacks) {
                                s.emit("onTouchEnd", s, e);
                            }
                            allowTouchCallbacks = false;
                            if (!isTouched) return;
                            if (s.params.grabCursor && isMoved && isTouched && (s.params.allowSwipeToNext === true || s.params.allowSwipeToPrev === true)) {
                                s.setGrabCursor(false);
                            }
                            var touchEndTime = Date.now();
                            var timeDiff = touchEndTime - touchStartTime;
                            if (s.allowClick) {
                                s.updateClickedSlide(e);
                                s.emit("onTap", s, e);
                                if (timeDiff < 300 && touchEndTime - lastClickTime > 300) {
                                    if (clickTimeout) clearTimeout(clickTimeout);
                                    clickTimeout = setTimeout(function () {
                                        if (!s) return;
                                        if (s.params.paginationHide && s.paginationContainer.length > 0 && !$(e.target).hasClass(s.params.bulletClass)) {
                                            s.paginationContainer.toggleClass(s.params.paginationHiddenClass);
                                        }
                                        s.emit("onClick", s, e);
                                    }, 300);
                                }
                                if (timeDiff < 300 && touchEndTime - lastClickTime < 300) {
                                    if (clickTimeout) clearTimeout(clickTimeout);
                                    s.emit("onDoubleTap", s, e);
                                }
                            }
                            lastClickTime = Date.now();
                            setTimeout(function () {
                                if (s) s.allowClick = true;
                            }, 0);
                            if (!isTouched || !isMoved || !s.swipeDirection || s.touches.diff === 0 || currentTranslate === startTranslate) {
                                isTouched = isMoved = false;
                                return;
                            }
                            isTouched = isMoved = false;
                            var currentPos;
                            if (s.params.followFinger) {
                                currentPos = s.rtl ? s.translate : -s.translate;
                            } else {
                                currentPos = -currentTranslate;
                            }
                            if (s.params.freeMode) {
                                if (currentPos < -s.minTranslate()) {
                                    s.slideTo(s.activeIndex);
                                    return;
                                } else if (currentPos > -s.maxTranslate()) {
                                    if (s.slides.length < s.snapGrid.length) {
                                        s.slideTo(s.snapGrid.length - 1);
                                    } else {
                                        s.slideTo(s.slides.length - 1);
                                    }
                                    return;
                                }
                                if (s.params.freeModeMomentum) {
                                    if (velocities.length > 1) {
                                        var lastMoveEvent = velocities.pop(),
                                            velocityEvent = velocities.pop();
                                        var distance = lastMoveEvent.position - velocityEvent.position;
                                        var time = lastMoveEvent.time - velocityEvent.time;
                                        s.velocity = distance / time;
                                        s.velocity = s.velocity / 2;
                                        if (Math.abs(s.velocity) < s.params.freeModeMinimumVelocity) {
                                            s.velocity = 0;
                                        }
                                        if (time > 150 || new window.Date().getTime() - lastMoveEvent.time > 300) {
                                            s.velocity = 0;
                                        }
                                    } else {
                                        s.velocity = 0;
                                    }
                                    s.velocity = s.velocity * s.params.freeModeMomentumVelocityRatio;
                                    velocities.length = 0;
                                    var momentumDuration = 1e3 * s.params.freeModeMomentumRatio;
                                    var momentumDistance = s.velocity * momentumDuration;
                                    var newPosition = s.translate + momentumDistance;
                                    if (s.rtl) newPosition = -newPosition;
                                    var doBounce = false;
                                    var afterBouncePosition;
                                    var bounceAmount = Math.abs(s.velocity) * 20 * s.params.freeModeMomentumBounceRatio;
                                    if (newPosition < s.maxTranslate()) {
                                        if (s.params.freeModeMomentumBounce) {
                                            if (newPosition + s.maxTranslate() < -bounceAmount) {
                                                newPosition = s.maxTranslate() - bounceAmount;
                                            }
                                            afterBouncePosition = s.maxTranslate();
                                            doBounce = true;
                                            allowMomentumBounce = true;
                                        } else {
                                            newPosition = s.maxTranslate();
                                        }
                                    } else if (newPosition > s.minTranslate()) {
                                        if (s.params.freeModeMomentumBounce) {
                                            if (newPosition - s.minTranslate() > bounceAmount) {
                                                newPosition = s.minTranslate() + bounceAmount;
                                            }
                                            afterBouncePosition = s.minTranslate();
                                            doBounce = true;
                                            allowMomentumBounce = true;
                                        } else {
                                            newPosition = s.minTranslate();
                                        }
                                    } else if (s.params.freeModeSticky) {
                                        var j = 0,
                                            nextSlide;
                                        for (j = 0; j < s.snapGrid.length; j += 1) {
                                            if (s.snapGrid[j] > -newPosition) {
                                                nextSlide = j;
                                                break;
                                            }
                                        }
                                        if (Math.abs(s.snapGrid[nextSlide] - newPosition) < Math.abs(s.snapGrid[nextSlide - 1] - newPosition) || s.swipeDirection === "next") {
                                            newPosition = s.snapGrid[nextSlide];
                                        } else {
                                            newPosition = s.snapGrid[nextSlide - 1];
                                        }
                                        if (!s.rtl) newPosition = -newPosition;
                                    }
                                    if (s.velocity !== 0) {
                                        if (s.rtl) {
                                            momentumDuration = Math.abs((-newPosition - s.translate) / s.velocity);
                                        } else {
                                            momentumDuration = Math.abs((newPosition - s.translate) / s.velocity);
                                        }
                                    } else if (s.params.freeModeSticky) {
                                        s.slideReset();
                                        return;
                                    }
                                    if (s.params.freeModeMomentumBounce && doBounce) {
                                        s.updateProgress(afterBouncePosition);
                                        s.setWrapperTransition(momentumDuration);
                                        s.setWrapperTranslate(newPosition);
                                        s.onTransitionStart();
                                        s.animating = true;
                                        s.wrapper.transitionEnd(function () {
                                            if (!s || !allowMomentumBounce) return;
                                            s.emit("onMomentumBounce", s);
                                            s.setWrapperTransition(s.params.speed);
                                            s.setWrapperTranslate(afterBouncePosition);
                                            s.wrapper.transitionEnd(function () {
                                                if (!s) return;
                                                s.onTransitionEnd();
                                            });
                                        });
                                    } else if (s.velocity) {
                                        s.updateProgress(newPosition);
                                        s.setWrapperTransition(momentumDuration);
                                        s.setWrapperTranslate(newPosition);
                                        s.onTransitionStart();
                                        if (!s.animating) {
                                            s.animating = true;
                                            s.wrapper.transitionEnd(function () {
                                                if (!s) return;
                                                s.onTransitionEnd();
                                            });
                                        }
                                    } else {
                                        s.updateProgress(newPosition);
                                    }
                                    s.updateActiveIndex();
                                }
                                if (!s.params.freeModeMomentum || timeDiff >= s.params.longSwipesMs) {
                                    s.updateProgress();
                                    s.updateActiveIndex();
                                }
                                return;
                            }
                            var i,
                                stopIndex = 0,
                                groupSize = s.slidesSizesGrid[0];
                            for (i = 0; i < s.slidesGrid.length; i += s.params.slidesPerGroup) {
                                if (typeof s.slidesGrid[i + s.params.slidesPerGroup] !== "undefined") {
                                    if (currentPos >= s.slidesGrid[i] && currentPos < s.slidesGrid[i + s.params.slidesPerGroup]) {
                                        stopIndex = i;
                                        groupSize = s.slidesGrid[i + s.params.slidesPerGroup] - s.slidesGrid[i];
                                    }
                                } else {
                                    if (currentPos >= s.slidesGrid[i]) {
                                        stopIndex = i;
                                        groupSize = s.slidesGrid[s.slidesGrid.length - 1] - s.slidesGrid[s.slidesGrid.length - 2];
                                    }
                                }
                            }
                            var ratio = (currentPos - s.slidesGrid[stopIndex]) / groupSize;
                            if (timeDiff > s.params.longSwipesMs) {
                                if (!s.params.longSwipes) {
                                    s.slideTo(s.activeIndex);
                                    return;
                                }
                                if (s.swipeDirection === "next") {
                                    if (ratio >= s.params.longSwipesRatio) s.slideTo(stopIndex + s.params.slidesPerGroup);
                                    else s.slideTo(stopIndex);
                                }
                                if (s.swipeDirection === "prev") {
                                    if (ratio > 1 - s.params.longSwipesRatio) s.slideTo(stopIndex + s.params.slidesPerGroup);
                                    else s.slideTo(stopIndex);
                                }
                            } else {
                                if (!s.params.shortSwipes) {
                                    s.slideTo(s.activeIndex);
                                    return;
                                }
                                if (s.swipeDirection === "next") {
                                    s.slideTo(stopIndex + s.params.slidesPerGroup);
                                }
                                if (s.swipeDirection === "prev") {
                                    s.slideTo(stopIndex);
                                }
                            }
                        };
                        s._slideTo = function (slideIndex, speed) {
                            return s.slideTo(slideIndex, speed, true, true);
                        };
                        s.slideTo = function (slideIndex, speed, runCallbacks, internal) {
                            if (typeof runCallbacks === "undefined") runCallbacks = true;
                            if (typeof slideIndex === "undefined") slideIndex = 0;
                            if (slideIndex < 0) slideIndex = 0;
                            s.snapIndex = Math.floor(slideIndex / s.params.slidesPerGroup);
                            if (s.snapIndex >= s.snapGrid.length) s.snapIndex = s.snapGrid.length - 1;
                            var translate = -s.snapGrid[s.snapIndex];
                            if (s.params.autoplay && s.autoplaying) {
                                if (internal || !s.params.autoplayDisableOnInteraction) {
                                    s.pauseAutoplay(speed);
                                } else {
                                    s.stopAutoplay();
                                }
                            }
                            s.updateProgress(translate);
                            if (s.params.normalizeSlideIndex) {
                                for (var i = 0; i < s.slidesGrid.length; i++) {
                                    if (-Math.floor(translate * 100) >= Math.floor(s.slidesGrid[i] * 100)) {
                                        slideIndex = i;
                                    }
                                }
                            }
                            if (!s.params.allowSwipeToNext && translate < s.translate && translate < s.minTranslate()) {
                                return false;
                            }
                            if (!s.params.allowSwipeToPrev && translate > s.translate && translate > s.maxTranslate()) {
                                if ((s.activeIndex || 0) !== slideIndex) return false;
                            }
                            if (typeof speed === "undefined") speed = s.params.speed;
                            s.previousIndex = s.activeIndex || 0;
                            s.activeIndex = slideIndex;
                            s.updateRealIndex();
                            if ((s.rtl && -translate === s.translate) || (!s.rtl && translate === s.translate)) {
                                if (s.params.autoHeight) {
                                    s.updateAutoHeight();
                                }
                                s.updateClasses();
                                if (s.params.effect !== "slide") {
                                    s.setWrapperTranslate(translate);
                                }
                                return false;
                            }
                            s.updateClasses();
                            s.onTransitionStart(runCallbacks);
                            if (speed === 0 || s.browser.lteIE9) {
                                s.setWrapperTranslate(translate);
                                s.setWrapperTransition(0);
                                s.onTransitionEnd(runCallbacks);
                            } else {
                                s.setWrapperTranslate(translate);
                                s.setWrapperTransition(speed);
                                if (!s.animating) {
                                    s.animating = true;
                                    s.wrapper.transitionEnd(function () {
                                        if (!s) return;
                                        s.onTransitionEnd(runCallbacks);
                                    });
                                }
                            }
                            return true;
                        };
                        s.onTransitionStart = function (runCallbacks) {
                            if (typeof runCallbacks === "undefined") runCallbacks = true;
                            if (s.params.autoHeight) {
                                s.updateAutoHeight();
                            }
                            if (s.lazy) s.lazy.onTransitionStart();
                            if (runCallbacks) {
                                s.emit("onTransitionStart", s);
                                if (s.activeIndex !== s.previousIndex) {
                                    s.emit("onSlideChangeStart", s);
                                    if (s.activeIndex > s.previousIndex) {
                                        s.emit("onSlideNextStart", s);
                                    } else {
                                        s.emit("onSlidePrevStart", s);
                                    }
                                }
                            }
                        };
                        s.onTransitionEnd = function (runCallbacks) {
                            s.animating = false;
                            s.setWrapperTransition(0);
                            if (typeof runCallbacks === "undefined") runCallbacks = true;
                            if (s.lazy) s.lazy.onTransitionEnd();
                            if (runCallbacks) {
                                s.emit("onTransitionEnd", s);
                                if (s.activeIndex !== s.previousIndex) {
                                    s.emit("onSlideChangeEnd", s);
                                    if (s.activeIndex > s.previousIndex) {
                                        s.emit("onSlideNextEnd", s);
                                    } else {
                                        s.emit("onSlidePrevEnd", s);
                                    }
                                }
                            }
                            if (s.params.history && s.history) {
                                s.history.setHistory(s.params.history, s.activeIndex);
                            }
                            if (s.params.hashnav && s.hashnav) {
                                s.hashnav.setHash();
                            }
                        };
                        s.slideNext = function (runCallbacks, speed, internal) {
                            if (s.params.loop) {
                                if (s.animating) return false;
                                s.fixLoop();
                                var clientLeft = s.container[0].clientLeft;
                                return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
                            } else return s.slideTo(s.activeIndex + s.params.slidesPerGroup, speed, runCallbacks, internal);
                        };
                        s._slideNext = function (speed) {
                            return s.slideNext(true, speed, true);
                        };
                        s.slidePrev = function (runCallbacks, speed, internal) {
                            if (s.params.loop) {
                                if (s.animating) return false;
                                s.fixLoop();
                                var clientLeft = s.container[0].clientLeft;
                                return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
                            } else return s.slideTo(s.activeIndex - 1, speed, runCallbacks, internal);
                        };
                        s._slidePrev = function (speed) {
                            return s.slidePrev(true, speed, true);
                        };
                        s.slideReset = function (runCallbacks, speed, internal) {
                            return s.slideTo(s.activeIndex, speed, runCallbacks);
                        };
                        s.disableTouchControl = function () {
                            s.params.onlyExternal = true;
                            return true;
                        };
                        s.enableTouchControl = function () {
                            s.params.onlyExternal = false;
                            return true;
                        };
                        s.setWrapperTransition = function (duration, byController) {
                            s.wrapper.transition(duration);
                            if (s.params.effect !== "slide" && s.effects[s.params.effect]) {
                                s.effects[s.params.effect].setTransition(duration);
                            }
                            if (s.params.parallax && s.parallax) {
                                s.parallax.setTransition(duration);
                            }
                            if (s.params.scrollbar && s.scrollbar) {
                                s.scrollbar.setTransition(duration);
                            }
                            if (s.params.control && s.controller) {
                                s.controller.setTransition(duration, byController);
                            }
                            s.emit("onSetTransition", s, duration);
                        };
                        s.setWrapperTranslate = function (translate, updateActiveIndex, byController) {
                            var x = 0,
                                y = 0,
                                z = 0;
                            if (s.isHorizontal()) {
                                x = s.rtl ? -translate : translate;
                            } else {
                                y = translate;
                            }
                            if (s.params.roundLengths) {
                                x = round(x);
                                y = round(y);
                            }
                            if (!s.params.virtualTranslate) {
                                if (s.support.transforms3d) s.wrapper.transform("translate3d(" + x + "px, " + y + "px, " + z + "px)");
                                else s.wrapper.transform("translate(" + x + "px, " + y + "px)");
                            }
                            s.translate = s.isHorizontal() ? x : y;
                            var progress;
                            var translatesDiff = s.maxTranslate() - s.minTranslate();
                            if (translatesDiff === 0) {
                                progress = 0;
                            } else {
                                progress = (translate - s.minTranslate()) / translatesDiff;
                            }
                            if (progress !== s.progress) {
                                s.updateProgress(translate);
                            }
                            if (updateActiveIndex) s.updateActiveIndex();
                            if (s.params.effect !== "slide" && s.effects[s.params.effect]) {
                                s.effects[s.params.effect].setTranslate(s.translate);
                            }
                            if (s.params.parallax && s.parallax) {
                                s.parallax.setTranslate(s.translate);
                            }
                            if (s.params.scrollbar && s.scrollbar) {
                                s.scrollbar.setTranslate(s.translate);
                            }
                            if (s.params.control && s.controller) {
                                s.controller.setTranslate(s.translate, byController);
                            }
                            s.emit("onSetTranslate", s, s.translate);
                        };
                        s.getTranslate = function (el, axis) {
                            var matrix, curTransform, curStyle, transformMatrix;
                            if (typeof axis === "undefined") {
                                axis = "x";
                            }
                            if (s.params.virtualTranslate) {
                                return s.rtl ? -s.translate : s.translate;
                            }
                            curStyle = window.getComputedStyle(el, null);
                            if (window.WebKitCSSMatrix) {
                                curTransform = curStyle.transform || curStyle.webkitTransform;
                                if (curTransform.split(",").length > 6) {
                                    curTransform = curTransform
                                        .split(", ")
                                        .map(function (a) {
                                            return a.replace(",", ".");
                                        })
                                        .join(", ");
                                }
                                transformMatrix = new window.WebKitCSSMatrix(curTransform === "none" ? "" : curTransform);
                            } else {
                                transformMatrix =
                                    curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,");
                                matrix = transformMatrix.toString().split(",");
                            }
                            if (axis === "x") {
                                if (window.WebKitCSSMatrix) curTransform = transformMatrix.m41;
                                else if (matrix.length === 16) curTransform = parseFloat(matrix[12]);
                                else curTransform = parseFloat(matrix[4]);
                            }
                            if (axis === "y") {
                                if (window.WebKitCSSMatrix) curTransform = transformMatrix.m42;
                                else if (matrix.length === 16) curTransform = parseFloat(matrix[13]);
                                else curTransform = parseFloat(matrix[5]);
                            }
                            if (s.rtl && curTransform) curTransform = -curTransform;
                            return curTransform || 0;
                        };
                        s.getWrapperTranslate = function (axis) {
                            if (typeof axis === "undefined") {
                                axis = s.isHorizontal() ? "x" : "y";
                            }
                            return s.getTranslate(s.wrapper[0], axis);
                        };
                        s.observers = [];
                        function initObserver(target, options) {
                            options = options || {};
                            var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
                            var observer = new ObserverFunc(function (mutations) {
                                mutations.forEach(function (mutation) {
                                    s.onResize(true);
                                    s.emit("onObserverUpdate", s, mutation);
                                });
                            });
                            observer.observe(target, {
                                attributes: typeof options.attributes === "undefined" ? true : options.attributes,
                                childList: typeof options.childList === "undefined" ? true : options.childList,
                                characterData: typeof options.characterData === "undefined" ? true : options.characterData,
                            });
                            s.observers.push(observer);
                        }
                        s.initObservers = function () {
                            if (s.params.observeParents) {
                                var containerParents = s.container.parents();
                                for (var i = 0; i < containerParents.length; i++) {
                                    initObserver(containerParents[i]);
                                }
                            }
                            initObserver(s.container[0], { childList: false });
                            initObserver(s.wrapper[0], { attributes: false });
                        };
                        s.disconnectObservers = function () {
                            for (var i = 0; i < s.observers.length; i++) {
                                s.observers[i].disconnect();
                            }
                            s.observers = [];
                        };
                        s.createLoop = function () {
                            s.wrapper.children("." + s.params.slideClass + "." + s.params.slideDuplicateClass).remove();
                            var slides = s.wrapper.children("." + s.params.slideClass);
                            if (s.params.slidesPerView === "auto" && !s.params.loopedSlides) s.params.loopedSlides = slides.length;
                            s.loopedSlides = parseInt(s.params.loopedSlides || s.params.slidesPerView, 10);
                            s.loopedSlides = s.loopedSlides + s.params.loopAdditionalSlides;
                            if (s.loopedSlides > slides.length) {
                                s.loopedSlides = slides.length;
                            }
                            var prependSlides = [],
                                appendSlides = [],
                                i;
                            slides.each(function (index, el) {
                                var slide = $(this);
                                if (index < s.loopedSlides) appendSlides.push(el);
                                if (index < slides.length && index >= slides.length - s.loopedSlides) prependSlides.push(el);
                                slide.attr("data-swiper-slide-index", index);
                            });
                            for (i = 0; i < appendSlides.length; i++) {
                                s.wrapper.append($(appendSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
                            }
                            for (i = prependSlides.length - 1; i >= 0; i--) {
                                s.wrapper.prepend($(prependSlides[i].cloneNode(true)).addClass(s.params.slideDuplicateClass));
                            }
                        };
                        s.destroyLoop = function () {
                            s.wrapper.children("." + s.params.slideClass + "." + s.params.slideDuplicateClass).remove();
                            s.slides.removeAttr("data-swiper-slide-index");
                        };
                        s.reLoop = function (updatePosition) {
                            var oldIndex = s.activeIndex - s.loopedSlides;
                            s.destroyLoop();
                            s.createLoop();
                            s.updateSlidesSize();
                            if (updatePosition) {
                                s.slideTo(oldIndex + s.loopedSlides, 0, false);
                            }
                        };
                        s.fixLoop = function () {
                            var newIndex;
                            if (s.activeIndex < s.loopedSlides) {
                                newIndex = s.slides.length - s.loopedSlides * 3 + s.activeIndex;
                                newIndex = newIndex + s.loopedSlides;
                                s.slideTo(newIndex, 0, false, true);
                            } else if ((s.params.slidesPerView === "auto" && s.activeIndex >= s.loopedSlides * 2) || s.activeIndex > s.slides.length - s.params.slidesPerView * 2) {
                                newIndex = -s.slides.length + s.activeIndex + s.loopedSlides;
                                newIndex = newIndex + s.loopedSlides;
                                s.slideTo(newIndex, 0, false, true);
                            }
                        };
                        s.appendSlide = function (slides) {
                            if (s.params.loop) {
                                s.destroyLoop();
                            }
                            if (typeof slides === "object" && slides.length) {
                                for (var i = 0; i < slides.length; i++) {
                                    if (slides[i]) s.wrapper.append(slides[i]);
                                }
                            } else {
                                s.wrapper.append(slides);
                            }
                            if (s.params.loop) {
                                s.createLoop();
                            }
                            if (!(s.params.observer && s.support.observer)) {
                                s.update(true);
                            }
                        };
                        s.prependSlide = function (slides) {
                            if (s.params.loop) {
                                s.destroyLoop();
                            }
                            var newActiveIndex = s.activeIndex + 1;
                            if (typeof slides === "object" && slides.length) {
                                for (var i = 0; i < slides.length; i++) {
                                    if (slides[i]) s.wrapper.prepend(slides[i]);
                                }
                                newActiveIndex = s.activeIndex + slides.length;
                            } else {
                                s.wrapper.prepend(slides);
                            }
                            if (s.params.loop) {
                                s.createLoop();
                            }
                            if (!(s.params.observer && s.support.observer)) {
                                s.update(true);
                            }
                            s.slideTo(newActiveIndex, 0, false);
                        };
                        s.removeSlide = function (slidesIndexes) {
                            if (s.params.loop) {
                                s.destroyLoop();
                                s.slides = s.wrapper.children("." + s.params.slideClass);
                            }
                            var newActiveIndex = s.activeIndex,
                                indexToRemove;
                            if (typeof slidesIndexes === "object" && slidesIndexes.length) {
                                for (var i = 0; i < slidesIndexes.length; i++) {
                                    indexToRemove = slidesIndexes[i];
                                    if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                                    if (indexToRemove < newActiveIndex) newActiveIndex--;
                                }
                                newActiveIndex = Math.max(newActiveIndex, 0);
                            } else {
                                indexToRemove = slidesIndexes;
                                if (s.slides[indexToRemove]) s.slides.eq(indexToRemove).remove();
                                if (indexToRemove < newActiveIndex) newActiveIndex--;
                                newActiveIndex = Math.max(newActiveIndex, 0);
                            }
                            if (s.params.loop) {
                                s.createLoop();
                            }
                            if (!(s.params.observer && s.support.observer)) {
                                s.update(true);
                            }
                            if (s.params.loop) {
                                s.slideTo(newActiveIndex + s.loopedSlides, 0, false);
                            } else {
                                s.slideTo(newActiveIndex, 0, false);
                            }
                        };
                        s.removeAllSlides = function () {
                            var slidesIndexes = [];
                            for (var i = 0; i < s.slides.length; i++) {
                                slidesIndexes.push(i);
                            }
                            s.removeSlide(slidesIndexes);
                        };
                        s.effects = {
                            fade: {
                                setTranslate: function () {
                                    for (var i = 0; i < s.slides.length; i++) {
                                        var slide = s.slides.eq(i);
                                        var offset = slide[0].swiperSlideOffset;
                                        var tx = -offset;
                                        if (!s.params.virtualTranslate) tx = tx - s.translate;
                                        var ty = 0;
                                        if (!s.isHorizontal()) {
                                            ty = tx;
                                            tx = 0;
                                        }
                                        var slideOpacity = s.params.fade.crossFade ? Math.max(1 - Math.abs(slide[0].progress), 0) : 1 + Math.min(Math.max(slide[0].progress, -1), 0);
                                        slide.css({ opacity: slideOpacity }).transform("translate3d(" + tx + "px, " + ty + "px, 0px)");
                                    }
                                },
                                setTransition: function (duration) {
                                    s.slides.transition(duration);
                                    if (s.params.virtualTranslate && duration !== 0) {
                                        var eventTriggered = false;
                                        s.slides.transitionEnd(function () {
                                            if (eventTriggered) return;
                                            if (!s) return;
                                            eventTriggered = true;
                                            s.animating = false;
                                            var triggerEvents = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"];
                                            for (var i = 0; i < triggerEvents.length; i++) {
                                                s.wrapper.trigger(triggerEvents[i]);
                                            }
                                        });
                                    }
                                },
                            },
                            flip: {
                                setTranslate: function () {
                                    for (var i = 0; i < s.slides.length; i++) {
                                        var slide = s.slides.eq(i);
                                        var progress = slide[0].progress;
                                        if (s.params.flip.limitRotation) {
                                            progress = Math.max(Math.min(slide[0].progress, 1), -1);
                                        }
                                        var offset = slide[0].swiperSlideOffset;
                                        var rotate = -180 * progress,
                                            rotateY = rotate,
                                            rotateX = 0,
                                            tx = -offset,
                                            ty = 0;
                                        if (!s.isHorizontal()) {
                                            ty = tx;
                                            tx = 0;
                                            rotateX = -rotateY;
                                            rotateY = 0;
                                        } else if (s.rtl) {
                                            rotateY = -rotateY;
                                        }
                                        slide[0].style.zIndex = -Math.abs(Math.round(progress)) + s.slides.length;
                                        if (s.params.flip.slideShadows) {
                                            var shadowBefore = s.isHorizontal() ? slide.find(".swiper-slide-shadow-left") : slide.find(".swiper-slide-shadow-top");
                                            var shadowAfter = s.isHorizontal() ? slide.find(".swiper-slide-shadow-right") : slide.find(".swiper-slide-shadow-bottom");
                                            if (shadowBefore.length === 0) {
                                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "left" : "top") + '"></div>');
                                                slide.append(shadowBefore);
                                            }
                                            if (shadowAfter.length === 0) {
                                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "right" : "bottom") + '"></div>');
                                                slide.append(shadowAfter);
                                            }
                                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                                        }
                                        slide.transform("translate3d(" + tx + "px, " + ty + "px, 0px) rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg)");
                                    }
                                },
                                setTransition: function (duration) {
                                    s.slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
                                    if (s.params.virtualTranslate && duration !== 0) {
                                        var eventTriggered = false;
                                        s.slides.eq(s.activeIndex).transitionEnd(function () {
                                            if (eventTriggered) return;
                                            if (!s) return;
                                            if (!$(this).hasClass(s.params.slideActiveClass)) return;
                                            eventTriggered = true;
                                            s.animating = false;
                                            var triggerEvents = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"];
                                            for (var i = 0; i < triggerEvents.length; i++) {
                                                s.wrapper.trigger(triggerEvents[i]);
                                            }
                                        });
                                    }
                                },
                            },
                            cube: {
                                setTranslate: function () {
                                    var wrapperRotate = 0,
                                        cubeShadow;
                                    if (s.params.cube.shadow) {
                                        if (s.isHorizontal()) {
                                            cubeShadow = s.wrapper.find(".swiper-cube-shadow");
                                            if (cubeShadow.length === 0) {
                                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                                s.wrapper.append(cubeShadow);
                                            }
                                            cubeShadow.css({ height: s.width + "px" });
                                        } else {
                                            cubeShadow = s.container.find(".swiper-cube-shadow");
                                            if (cubeShadow.length === 0) {
                                                cubeShadow = $('<div class="swiper-cube-shadow"></div>');
                                                s.container.append(cubeShadow);
                                            }
                                        }
                                    }
                                    for (var i = 0; i < s.slides.length; i++) {
                                        var slide = s.slides.eq(i);
                                        var slideAngle = i * 90;
                                        var round = Math.floor(slideAngle / 360);
                                        if (s.rtl) {
                                            slideAngle = -slideAngle;
                                            round = Math.floor(-slideAngle / 360);
                                        }
                                        var progress = Math.max(Math.min(slide[0].progress, 1), -1);
                                        var tx = 0,
                                            ty = 0,
                                            tz = 0;
                                        if (i % 4 === 0) {
                                            tx = -round * 4 * s.size;
                                            tz = 0;
                                        } else if ((i - 1) % 4 === 0) {
                                            tx = 0;
                                            tz = -round * 4 * s.size;
                                        } else if ((i - 2) % 4 === 0) {
                                            tx = s.size + round * 4 * s.size;
                                            tz = s.size;
                                        } else if ((i - 3) % 4 === 0) {
                                            tx = -s.size;
                                            tz = 3 * s.size + s.size * 4 * round;
                                        }
                                        if (s.rtl) {
                                            tx = -tx;
                                        }
                                        if (!s.isHorizontal()) {
                                            ty = tx;
                                            tx = 0;
                                        }
                                        var transform = "rotateX(" + (s.isHorizontal() ? 0 : -slideAngle) + "deg) rotateY(" + (s.isHorizontal() ? slideAngle : 0) + "deg) translate3d(" + tx + "px, " + ty + "px, " + tz + "px)";
                                        if (progress <= 1 && progress > -1) {
                                            wrapperRotate = i * 90 + progress * 90;
                                            if (s.rtl) wrapperRotate = -i * 90 - progress * 90;
                                        }
                                        slide.transform(transform);
                                        if (s.params.cube.slideShadows) {
                                            var shadowBefore = s.isHorizontal() ? slide.find(".swiper-slide-shadow-left") : slide.find(".swiper-slide-shadow-top");
                                            var shadowAfter = s.isHorizontal() ? slide.find(".swiper-slide-shadow-right") : slide.find(".swiper-slide-shadow-bottom");
                                            if (shadowBefore.length === 0) {
                                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "left" : "top") + '"></div>');
                                                slide.append(shadowBefore);
                                            }
                                            if (shadowAfter.length === 0) {
                                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "right" : "bottom") + '"></div>');
                                                slide.append(shadowAfter);
                                            }
                                            if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
                                            if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
                                        }
                                    }
                                    s.wrapper.css({
                                        "-webkit-transform-origin": "50% 50% -" + s.size / 2 + "px",
                                        "-moz-transform-origin": "50% 50% -" + s.size / 2 + "px",
                                        "-ms-transform-origin": "50% 50% -" + s.size / 2 + "px",
                                        "transform-origin": "50% 50% -" + s.size / 2 + "px",
                                    });
                                    if (s.params.cube.shadow) {
                                        if (s.isHorizontal()) {
                                            cubeShadow.transform("translate3d(0px, " + (s.width / 2 + s.params.cube.shadowOffset) + "px, " + -s.width / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + s.params.cube.shadowScale + ")");
                                        } else {
                                            var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
                                            var multiplier = 1.5 - (Math.sin((shadowAngle * 2 * Math.PI) / 360) / 2 + Math.cos((shadowAngle * 2 * Math.PI) / 360) / 2);
                                            var scale1 = s.params.cube.shadowScale,
                                                scale2 = s.params.cube.shadowScale / multiplier,
                                                offset = s.params.cube.shadowOffset;
                                            cubeShadow.transform("scale3d(" + scale1 + ", 1, " + scale2 + ") translate3d(0px, " + (s.height / 2 + offset) + "px, " + -s.height / 2 / scale2 + "px) rotateX(-90deg)");
                                        }
                                    }
                                    var zFactor = s.isSafari || s.isUiWebView ? -s.size / 2 : 0;
                                    s.wrapper.transform("translate3d(0px,0," + zFactor + "px) rotateX(" + (s.isHorizontal() ? 0 : wrapperRotate) + "deg) rotateY(" + (s.isHorizontal() ? -wrapperRotate : 0) + "deg)");
                                },
                                setTransition: function (duration) {
                                    s.slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
                                    if (s.params.cube.shadow && !s.isHorizontal()) {
                                        s.container.find(".swiper-cube-shadow").transition(duration);
                                    }
                                },
                            },
                            coverflow: {
                                setTranslate: function () {
                                    var transform = s.translate;
                                    var center = s.isHorizontal() ? -transform + s.width / 2 : -transform + s.height / 2;
                                    var rotate = s.isHorizontal() ? s.params.coverflow.rotate : -s.params.coverflow.rotate;
                                    var translate = s.params.coverflow.depth;
                                    for (var i = 0, length = s.slides.length; i < length; i++) {
                                        var slide = s.slides.eq(i);
                                        var slideSize = s.slidesSizesGrid[i];
                                        var slideOffset = slide[0].swiperSlideOffset;
                                        var offsetMultiplier = ((center - slideOffset - slideSize / 2) / slideSize) * s.params.coverflow.modifier;
                                        var rotateY = s.isHorizontal() ? rotate * offsetMultiplier : 0;
                                        var rotateX = s.isHorizontal() ? 0 : rotate * offsetMultiplier;
                                        var translateZ = -translate * Math.abs(offsetMultiplier);
                                        var translateY = s.isHorizontal() ? 0 : s.params.coverflow.stretch * offsetMultiplier;
                                        var translateX = s.isHorizontal() ? s.params.coverflow.stretch * offsetMultiplier : 0;
                                        if (Math.abs(translateX) < 0.001) translateX = 0;
                                        if (Math.abs(translateY) < 0.001) translateY = 0;
                                        if (Math.abs(translateZ) < 0.001) translateZ = 0;
                                        if (Math.abs(rotateY) < 0.001) rotateY = 0;
                                        if (Math.abs(rotateX) < 0.001) rotateX = 0;
                                        var slideTransform = "translate3d(" + translateX + "px," + translateY + "px," + translateZ + "px)  rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg)";
                                        slide.transform(slideTransform);
                                        slide[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
                                        if (s.params.coverflow.slideShadows) {
                                            var shadowBefore = s.isHorizontal() ? slide.find(".swiper-slide-shadow-left") : slide.find(".swiper-slide-shadow-top");
                                            var shadowAfter = s.isHorizontal() ? slide.find(".swiper-slide-shadow-right") : slide.find(".swiper-slide-shadow-bottom");
                                            if (shadowBefore.length === 0) {
                                                shadowBefore = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "left" : "top") + '"></div>');
                                                slide.append(shadowBefore);
                                            }
                                            if (shadowAfter.length === 0) {
                                                shadowAfter = $('<div class="swiper-slide-shadow-' + (s.isHorizontal() ? "right" : "bottom") + '"></div>');
                                                slide.append(shadowAfter);
                                            }
                                            if (shadowBefore.length) shadowBefore[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
                                            if (shadowAfter.length) shadowAfter[0].style.opacity = -offsetMultiplier > 0 ? -offsetMultiplier : 0;
                                        }
                                    }
                                    if (s.browser.ie) {
                                        var ws = s.wrapper[0].style;
                                        ws.perspectiveOrigin = center + "px 50%";
                                    }
                                },
                                setTransition: function (duration) {
                                    s.slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
                                },
                            },
                        };
                        s.lazy = {
                            initialImageLoaded: false,
                            loadImageInSlide: function (index, loadInDuplicate) {
                                if (typeof index === "undefined") return;
                                if (typeof loadInDuplicate === "undefined") loadInDuplicate = true;
                                if (s.slides.length === 0) return;
                                var slide = s.slides.eq(index);
                                var img = slide.find("." + s.params.lazyLoadingClass + ":not(." + s.params.lazyStatusLoadedClass + "):not(." + s.params.lazyStatusLoadingClass + ")");
                                if (slide.hasClass(s.params.lazyLoadingClass) && !slide.hasClass(s.params.lazyStatusLoadedClass) && !slide.hasClass(s.params.lazyStatusLoadingClass)) {
                                    img = img.add(slide[0]);
                                }
                                if (img.length === 0) return;
                                img.each(function () {
                                    var _img = $(this);
                                    _img.addClass(s.params.lazyStatusLoadingClass);
                                    var background = _img.attr("data-background");
                                    var src = _img.attr("data-src"),
                                        srcset = _img.attr("data-srcset"),
                                        sizes = _img.attr("data-sizes");
                                    s.loadImage(_img[0], src || background, srcset, sizes, false, function () {
                                        if (typeof s === "undefined" || s === null || !s) return;
                                        if (background) {
                                            _img.css("background-image", 'url("' + background + '")');
                                            _img.removeAttr("data-background");
                                        } else {
                                            if (srcset) {
                                                _img.attr("srcset", srcset);
                                                _img.removeAttr("data-srcset");
                                            }
                                            if (sizes) {
                                                _img.attr("sizes", sizes);
                                                _img.removeAttr("data-sizes");
                                            }
                                            if (src) {
                                                _img.attr("src", src);
                                                _img.removeAttr("data-src");
                                            }
                                        }
                                        _img.addClass(s.params.lazyStatusLoadedClass).removeClass(s.params.lazyStatusLoadingClass);
                                        slide.find("." + s.params.lazyPreloaderClass + ", ." + s.params.preloaderClass).remove();
                                        if (s.params.loop && loadInDuplicate) {
                                            var slideOriginalIndex = slide.attr("data-swiper-slide-index");
                                            if (slide.hasClass(s.params.slideDuplicateClass)) {
                                                var originalSlide = s.wrapper.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + s.params.slideDuplicateClass + ")");
                                                s.lazy.loadImageInSlide(originalSlide.index(), false);
                                            } else {
                                                var duplicatedSlide = s.wrapper.children("." + s.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
                                                s.lazy.loadImageInSlide(duplicatedSlide.index(), false);
                                            }
                                        }
                                        s.emit("onLazyImageReady", s, slide[0], _img[0]);
                                    });
                                    s.emit("onLazyImageLoad", s, slide[0], _img[0]);
                                });
                            },
                            load: function () {
                                var i;
                                var slidesPerView = s.params.slidesPerView;
                                if (slidesPerView === "auto") {
                                    slidesPerView = 0;
                                }
                                if (!s.lazy.initialImageLoaded) s.lazy.initialImageLoaded = true;
                                if (s.params.watchSlidesVisibility) {
                                    s.wrapper.children("." + s.params.slideVisibleClass).each(function () {
                                        s.lazy.loadImageInSlide($(this).index());
                                    });
                                } else {
                                    if (slidesPerView > 1) {
                                        for (i = s.activeIndex; i < s.activeIndex + slidesPerView; i++) {
                                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                                        }
                                    } else {
                                        s.lazy.loadImageInSlide(s.activeIndex);
                                    }
                                }
                                if (s.params.lazyLoadingInPrevNext) {
                                    if (slidesPerView > 1 || (s.params.lazyLoadingInPrevNextAmount && s.params.lazyLoadingInPrevNextAmount > 1)) {
                                        var amount = s.params.lazyLoadingInPrevNextAmount;
                                        var spv = slidesPerView;
                                        var maxIndex = Math.min(s.activeIndex + spv + Math.max(amount, spv), s.slides.length);
                                        var minIndex = Math.max(s.activeIndex - Math.max(spv, amount), 0);
                                        for (i = s.activeIndex + slidesPerView; i < maxIndex; i++) {
                                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                                        }
                                        for (i = minIndex; i < s.activeIndex; i++) {
                                            if (s.slides[i]) s.lazy.loadImageInSlide(i);
                                        }
                                    } else {
                                        var nextSlide = s.wrapper.children("." + s.params.slideNextClass);
                                        if (nextSlide.length > 0) s.lazy.loadImageInSlide(nextSlide.index());
                                        var prevSlide = s.wrapper.children("." + s.params.slidePrevClass);
                                        if (prevSlide.length > 0) s.lazy.loadImageInSlide(prevSlide.index());
                                    }
                                }
                            },
                            onTransitionStart: function () {
                                if (s.params.lazyLoading) {
                                    if (s.params.lazyLoadingOnTransitionStart || (!s.params.lazyLoadingOnTransitionStart && !s.lazy.initialImageLoaded)) {
                                        s.lazy.load();
                                    }
                                }
                            },
                            onTransitionEnd: function () {
                                if (s.params.lazyLoading && !s.params.lazyLoadingOnTransitionStart) {
                                    s.lazy.load();
                                }
                            },
                        };
                        s.scrollbar = {
                            isTouched: false,
                            setDragPosition: function (e) {
                                var sb = s.scrollbar;
                                var x = 0,
                                    y = 0;
                                var translate;
                                var pointerPosition = s.isHorizontal()
                                    ? e.type === "touchstart" || e.type === "touchmove"
                                        ? e.targetTouches[0].pageX
                                        : e.pageX || e.clientX
                                    : e.type === "touchstart" || e.type === "touchmove"
                                        ? e.targetTouches[0].pageY
                                        : e.pageY || e.clientY;
                                var position = pointerPosition - sb.track.offset()[s.isHorizontal() ? "left" : "top"] - sb.dragSize / 2;
                                var positionMin = -s.minTranslate() * sb.moveDivider;
                                var positionMax = -s.maxTranslate() * sb.moveDivider;
                                if (position < positionMin) {
                                    position = positionMin;
                                } else if (position > positionMax) {
                                    position = positionMax;
                                }
                                position = -position / sb.moveDivider;
                                s.updateProgress(position);
                                s.setWrapperTranslate(position, true);
                            },
                            dragStart: function (e) {
                                var sb = s.scrollbar;
                                sb.isTouched = true;
                                e.preventDefault();
                                e.stopPropagation();
                                sb.setDragPosition(e);
                                clearTimeout(sb.dragTimeout);
                                sb.track.transition(0);
                                if (s.params.scrollbarHide) {
                                    sb.track.css("opacity", 1);
                                }
                                s.wrapper.transition(100);
                                sb.drag.transition(100);
                                s.emit("onScrollbarDragStart", s);
                            },
                            dragMove: function (e) {
                                var sb = s.scrollbar;
                                if (!sb.isTouched) return;
                                if (e.preventDefault) e.preventDefault();
                                else e.returnValue = false;
                                sb.setDragPosition(e);
                                s.wrapper.transition(0);
                                sb.track.transition(0);
                                sb.drag.transition(0);
                                s.emit("onScrollbarDragMove", s);
                            },
                            dragEnd: function (e) {
                                var sb = s.scrollbar;
                                if (!sb.isTouched) return;
                                sb.isTouched = false;
                                if (s.params.scrollbarHide) {
                                    clearTimeout(sb.dragTimeout);
                                    sb.dragTimeout = setTimeout(function () {
                                        sb.track.css("opacity", 0);
                                        sb.track.transition(400);
                                    }, 1e3);
                                }
                                s.emit("onScrollbarDragEnd", s);
                                if (s.params.scrollbarSnapOnRelease) {
                                    s.slideReset();
                                }
                            },
                            draggableEvents: (function () {
                                if (s.params.simulateTouch === false && !s.support.touch) return s.touchEventsDesktop;
                                else return s.touchEvents;
                            })(),
                            enableDraggable: function () {
                                var sb = s.scrollbar;
                                var target = s.support.touch ? sb.track : document;
                                $(sb.track).on(sb.draggableEvents.start, sb.dragStart);
                                $(target).on(sb.draggableEvents.move, sb.dragMove);
                                $(target).on(sb.draggableEvents.end, sb.dragEnd);
                            },
                            disableDraggable: function () {
                                var sb = s.scrollbar;
                                var target = s.support.touch ? sb.track : document;
                                $(sb.track).off(sb.draggableEvents.start, sb.dragStart);
                                $(target).off(sb.draggableEvents.move, sb.dragMove);
                                $(target).off(sb.draggableEvents.end, sb.dragEnd);
                            },
                            set: function () {
                                if (!s.params.scrollbar) return;
                                var sb = s.scrollbar;
                                sb.track = $(s.params.scrollbar);
                                if (s.params.uniqueNavElements && typeof s.params.scrollbar === "string" && sb.track.length > 1 && s.container.find(s.params.scrollbar).length === 1) {
                                    sb.track = s.container.find(s.params.scrollbar);
                                }
                                sb.drag = sb.track.find(".swiper-scrollbar-drag");
                                if (sb.drag.length === 0) {
                                    sb.drag = $('<div class="swiper-scrollbar-drag"></div>');
                                    sb.track.append(sb.drag);
                                }
                                sb.drag[0].style.width = "";
                                sb.drag[0].style.height = "";
                                sb.trackSize = s.isHorizontal() ? sb.track[0].offsetWidth : sb.track[0].offsetHeight;
                                sb.divider = s.size / s.virtualSize;
                                sb.moveDivider = sb.divider * (sb.trackSize / s.size);
                                sb.dragSize = sb.trackSize * sb.divider;
                                if (s.isHorizontal()) {
                                    sb.drag[0].style.width = sb.dragSize + "px";
                                } else {
                                    sb.drag[0].style.height = sb.dragSize + "px";
                                }
                                if (sb.divider >= 1) {
                                    sb.track[0].style.display = "none";
                                } else {
                                    sb.track[0].style.display = "";
                                }
                                if (s.params.scrollbarHide) {
                                    sb.track[0].style.opacity = 0;
                                }
                            },
                            setTranslate: function () {
                                if (!s.params.scrollbar) return;
                                var diff;
                                var sb = s.scrollbar;
                                var translate = s.translate || 0;
                                var newPos;
                                var newSize = sb.dragSize;
                                newPos = (sb.trackSize - sb.dragSize) * s.progress;
                                if (s.rtl && s.isHorizontal()) {
                                    newPos = -newPos;
                                    if (newPos > 0) {
                                        newSize = sb.dragSize - newPos;
                                        newPos = 0;
                                    } else if (-newPos + sb.dragSize > sb.trackSize) {
                                        newSize = sb.trackSize + newPos;
                                    }
                                } else {
                                    if (newPos < 0) {
                                        newSize = sb.dragSize + newPos;
                                        newPos = 0;
                                    } else if (newPos + sb.dragSize > sb.trackSize) {
                                        newSize = sb.trackSize - newPos;
                                    }
                                }
                                if (s.isHorizontal()) {
                                    if (s.support.transforms3d) {
                                        sb.drag.transform("translate3d(" + newPos + "px, 0, 0)");
                                    } else {
                                        sb.drag.transform("translateX(" + newPos + "px)");
                                    }
                                    sb.drag[0].style.width = newSize + "px";
                                } else {
                                    if (s.support.transforms3d) {
                                        sb.drag.transform("translate3d(0px, " + newPos + "px, 0)");
                                    } else {
                                        sb.drag.transform("translateY(" + newPos + "px)");
                                    }
                                    sb.drag[0].style.height = newSize + "px";
                                }
                                if (s.params.scrollbarHide) {
                                    clearTimeout(sb.timeout);
                                    sb.track[0].style.opacity = 1;
                                    sb.timeout = setTimeout(function () {
                                        sb.track[0].style.opacity = 0;
                                        sb.track.transition(400);
                                    }, 1e3);
                                }
                            },
                            setTransition: function (duration) {
                                if (!s.params.scrollbar) return;
                                s.scrollbar.drag.transition(duration);
                            },
                        };
                        s.controller = {
                            LinearSpline: function (x, y) {
                                var binarySearch = (function () {
                                    var maxIndex, minIndex, guess;
                                    return function (array, val) {
                                        minIndex = -1;
                                        maxIndex = array.length;
                                        while (maxIndex - minIndex > 1)
                                            if (array[(guess = (maxIndex + minIndex) >> 1)] <= val) {
                                                minIndex = guess;
                                            } else {
                                                maxIndex = guess;
                                            }
                                        return maxIndex;
                                    };
                                })();
                                this.x = x;
                                this.y = y;
                                this.lastIndex = x.length - 1;
                                var i1, i3;
                                var l = this.x.length;
                                this.interpolate = function (x2) {
                                    if (!x2) return 0;
                                    i3 = binarySearch(this.x, x2);
                                    i1 = i3 - 1;
                                    return ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) / (this.x[i3] - this.x[i1]) + this.y[i1];
                                };
                            },
                            getInterpolateFunction: function (c) {
                                if (!s.controller.spline) s.controller.spline = s.params.loop ? new s.controller.LinearSpline(s.slidesGrid, c.slidesGrid) : new s.controller.LinearSpline(s.snapGrid, c.snapGrid);
                            },
                            setTranslate: function (translate, byController) {
                                var controlled = s.params.control;
                                var multiplier, controlledTranslate;
                                function setControlledTranslate(c) {
                                    translate = c.rtl && c.params.direction === "horizontal" ? -s.translate : s.translate;
                                    if (s.params.controlBy === "slide") {
                                        s.controller.getInterpolateFunction(c);
                                        controlledTranslate = -s.controller.spline.interpolate(-translate);
                                    }
                                    if (!controlledTranslate || s.params.controlBy === "container") {
                                        multiplier = (c.maxTranslate() - c.minTranslate()) / (s.maxTranslate() - s.minTranslate());
                                        controlledTranslate = (translate - s.minTranslate()) * multiplier + c.minTranslate();
                                    }
                                    if (s.params.controlInverse) {
                                        controlledTranslate = c.maxTranslate() - controlledTranslate;
                                    }
                                    c.updateProgress(controlledTranslate);
                                    c.setWrapperTranslate(controlledTranslate, false, s);
                                    c.updateActiveIndex();
                                }
                                if (Array.isArray(controlled)) {
                                    for (var i = 0; i < controlled.length; i++) {
                                        if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                                            setControlledTranslate(controlled[i]);
                                        }
                                    }
                                } else if (controlled instanceof Swiper && byController !== controlled) {
                                    setControlledTranslate(controlled);
                                }
                            },
                            setTransition: function (duration, byController) {
                                var controlled = s.params.control;
                                var i;
                                function setControlledTransition(c) {
                                    c.setWrapperTransition(duration, s);
                                    if (duration !== 0) {
                                        c.onTransitionStart();
                                        c.wrapper.transitionEnd(function () {
                                            if (!controlled) return;
                                            if (c.params.loop && s.params.controlBy === "slide") {
                                                c.fixLoop();
                                            }
                                            c.onTransitionEnd();
                                        });
                                    }
                                }
                                if (Array.isArray(controlled)) {
                                    for (i = 0; i < controlled.length; i++) {
                                        if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
                                            setControlledTransition(controlled[i]);
                                        }
                                    }
                                } else if (controlled instanceof Swiper && byController !== controlled) {
                                    setControlledTransition(controlled);
                                }
                            },
                        };
                        s.hashnav = {
                            onHashCange: function (e, a) {
                                var newHash = document.location.hash.replace("#", "");
                                var activeSlideHash = s.slides.eq(s.activeIndex).attr("data-hash");
                                if (newHash !== activeSlideHash) {
                                    s.slideTo(s.wrapper.children("." + s.params.slideClass + '[data-hash="' + newHash + '"]').index());
                                }
                            },
                            attachEvents: function (detach) {
                                var action = detach ? "off" : "on";
                                $(window)[action]("hashchange", s.hashnav.onHashCange);
                            },
                            setHash: function () {
                                if (!s.hashnav.initialized || !s.params.hashnav) return;
                                if (s.params.replaceState && window.history && window.history.replaceState) {
                                    window.history.replaceState(null, null, "#" + s.slides.eq(s.activeIndex).attr("data-hash") || "");
                                } else {
                                    var slide = s.slides.eq(s.activeIndex);
                                    var hash = slide.attr("data-hash") || slide.attr("data-history");
                                    document.location.hash = hash || "";
                                }
                            },
                            init: function () {
                                if (!s.params.hashnav || s.params.history) return;
                                s.hashnav.initialized = true;
                                var hash = document.location.hash.replace("#", "");
                                if (hash) {
                                    var speed = 0;
                                    for (var i = 0, length = s.slides.length; i < length; i++) {
                                        var slide = s.slides.eq(i);
                                        var slideHash = slide.attr("data-hash") || slide.attr("data-history");
                                        if (slideHash === hash && !slide.hasClass(s.params.slideDuplicateClass)) {
                                            var index = slide.index();
                                            s.slideTo(index, speed, s.params.runCallbacksOnInit, true);
                                        }
                                    }
                                }
                                if (s.params.hashnavWatchState) s.hashnav.attachEvents();
                            },
                            destroy: function () {
                                if (s.params.hashnavWatchState) s.hashnav.attachEvents(true);
                            },
                        };
                        s.history = {
                            init: function () {
                                if (!s.params.history) return;
                                if (!window.history || !window.history.pushState) {
                                    s.params.history = false;
                                    s.params.hashnav = true;
                                    return;
                                }
                                s.history.initialized = true;
                                this.paths = this.getPathValues();
                                if (!this.paths.key && !this.paths.value) return;
                                this.scrollToSlide(0, this.paths.value, s.params.runCallbacksOnInit);
                                if (!s.params.replaceState) {
                                    window.addEventListener("popstate", this.setHistoryPopState);
                                }
                            },
                            setHistoryPopState: function () {
                                s.history.paths = s.history.getPathValues();
                                s.history.scrollToSlide(s.params.speed, s.history.paths.value, false);
                            },
                            getPathValues: function () {
                                var pathArray = window.location.pathname.slice(1).split("/");
                                var total = pathArray.length;
                                var key = pathArray[total - 2];
                                var value = pathArray[total - 1];
                                return { key: key, value: value };
                            },
                            setHistory: function (key, index) {
                                if (!s.history.initialized || !s.params.history) return;
                                var slide = s.slides.eq(index);
                                var value = this.slugify(slide.attr("data-history"));
                                if (!window.location.pathname.includes(key)) {
                                    value = key + "/" + value;
                                }
                                if (s.params.replaceState) {
                                    window.history.replaceState(null, null, value);
                                } else {
                                    window.history.pushState(null, null, value);
                                }
                            },
                            slugify: function (text) {
                                return text
                                    .toString()
                                    .toLowerCase()
                                    .replace(/\s+/g, "-")
                                    .replace(/[^\w\-]+/g, "")
                                    .replace(/\-\-+/g, "-")
                                    .replace(/^-+/, "")
                                    .replace(/-+$/, "");
                            },
                            scrollToSlide: function (speed, value, runCallbacks) {
                                if (value) {
                                    for (var i = 0, length = s.slides.length; i < length; i++) {
                                        var slide = s.slides.eq(i);
                                        var slideHistory = this.slugify(slide.attr("data-history"));
                                        if (slideHistory === value && !slide.hasClass(s.params.slideDuplicateClass)) {
                                            var index = slide.index();
                                            s.slideTo(index, speed, runCallbacks);
                                        }
                                    }
                                } else {
                                    s.slideTo(0, speed, runCallbacks);
                                }
                            },
                        };
                        function handleKeyboard(e) {
                            if (e.originalEvent) e = e.originalEvent;
                            var kc = e.keyCode || e.charCode;
                            if (!s.params.allowSwipeToNext && ((s.isHorizontal() && kc === 39) || (!s.isHorizontal() && kc === 40))) {
                                return false;
                            }
                            if (!s.params.allowSwipeToPrev && ((s.isHorizontal() && kc === 37) || (!s.isHorizontal() && kc === 38))) {
                                return false;
                            }
                            if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
                                return;
                            }
                            if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === "input" || document.activeElement.nodeName.toLowerCase() === "textarea")) {
                                return;
                            }
                            if (kc === 37 || kc === 39 || kc === 38 || kc === 40) {
                                var inView = false;
                                if (s.container.parents("." + s.params.slideClass).length > 0 && s.container.parents("." + s.params.slideActiveClass).length === 0) {
                                    return;
                                }
                                var windowScroll = { left: window.pageXOffset, top: window.pageYOffset };
                                var windowWidth = window.innerWidth;
                                var windowHeight = window.innerHeight;
                                var swiperOffset = s.container.offset();
                                if (s.rtl) swiperOffset.left = swiperOffset.left - s.container[0].scrollLeft;
                                var swiperCoord = [
                                    [swiperOffset.left, swiperOffset.top],
                                    [swiperOffset.left + s.width, swiperOffset.top],
                                    [swiperOffset.left, swiperOffset.top + s.height],
                                    [swiperOffset.left + s.width, swiperOffset.top + s.height],
                                ];
                                for (var i = 0; i < swiperCoord.length; i++) {
                                    var point = swiperCoord[i];
                                    if (point[0] >= windowScroll.left && point[0] <= windowScroll.left + windowWidth && point[1] >= windowScroll.top && point[1] <= windowScroll.top + windowHeight) {
                                        inView = true;
                                    }
                                }
                                if (!inView) return;
                            }
                            if (s.isHorizontal()) {
                                if (kc === 37 || kc === 39) {
                                    if (e.preventDefault) e.preventDefault();
                                    else e.returnValue = false;
                                }
                                if ((kc === 39 && !s.rtl) || (kc === 37 && s.rtl)) s.slideNext();
                                if ((kc === 37 && !s.rtl) || (kc === 39 && s.rtl)) s.slidePrev();
                            } else {
                                if (kc === 38 || kc === 40) {
                                    if (e.preventDefault) e.preventDefault();
                                    else e.returnValue = false;
                                }
                                if (kc === 40) s.slideNext();
                                if (kc === 38) s.slidePrev();
                            }
                            s.emit("onKeyPress", s, kc);
                        }
                        s.disableKeyboardControl = function () {
                            s.params.keyboardControl = false;
                            $(document).off("keydown", handleKeyboard);
                        };
                        s.enableKeyboardControl = function () {
                            s.params.keyboardControl = true;
                            $(document).on("keydown", handleKeyboard);
                        };
                        s.mousewheel = { event: false, lastScrollTime: new window.Date().getTime() };
                        function isEventSupported() {
                            var eventName = "onwheel";
                            var isSupported = eventName in document;
                            if (!isSupported) {
                                var element = document.createElement("div");
                                element.setAttribute(eventName, "return;");
                                isSupported = typeof element[eventName] === "function";
                            }
                            if (!isSupported && document.implementation && document.implementation.hasFeature && document.implementation.hasFeature("", "") !== true) {
                                isSupported = document.implementation.hasFeature("Events.wheel", "3.0");
                            }
                            return isSupported;
                        }
                        function normalizeWheel(event) {
                            var PIXEL_STEP = 10;
                            var LINE_HEIGHT = 40;
                            var PAGE_HEIGHT = 800;
                            var sX = 0,
                                sY = 0,
                                pX = 0,
                                pY = 0;
                            if ("detail" in event) {
                                sY = event.detail;
                            }
                            if ("wheelDelta" in event) {
                                sY = -event.wheelDelta / 120;
                            }
                            if ("wheelDeltaY" in event) {
                                sY = -event.wheelDeltaY / 120;
                            }
                            if ("wheelDeltaX" in event) {
                                sX = -event.wheelDeltaX / 120;
                            }
                            if ("axis" in event && event.axis === event.HORIZONTAL_AXIS) {
                                sX = sY;
                                sY = 0;
                            }
                            pX = sX * PIXEL_STEP;
                            pY = sY * PIXEL_STEP;
                            if ("deltaY" in event) {
                                pY = event.deltaY;
                            }
                            if ("deltaX" in event) {
                                pX = event.deltaX;
                            }
                            if ((pX || pY) && event.deltaMode) {
                                if (event.deltaMode === 1) {
                                    pX *= LINE_HEIGHT;
                                    pY *= LINE_HEIGHT;
                                } else {
                                    pX *= PAGE_HEIGHT;
                                    pY *= PAGE_HEIGHT;
                                }
                            }
                            if (pX && !sX) {
                                sX = pX < 1 ? -1 : 1;
                            }
                            if (pY && !sY) {
                                sY = pY < 1 ? -1 : 1;
                            }
                            return { spinX: sX, spinY: sY, pixelX: pX, pixelY: pY };
                        }
                        if (s.params.mousewheelControl) {
                            s.mousewheel.event = navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : isEventSupported() ? "wheel" : "mousewheel";
                        }
                        function handleMousewheel(e) {
                            if (e.originalEvent) e = e.originalEvent;
                            var delta = 0;
                            var rtlFactor = s.rtl ? -1 : 1;
                            var data = normalizeWheel(e);
                            if (s.params.mousewheelForceToAxis) {
                                if (s.isHorizontal()) {
                                    if (Math.abs(data.pixelX) > Math.abs(data.pixelY)) delta = data.pixelX * rtlFactor;
                                    else return;
                                } else {
                                    if (Math.abs(data.pixelY) > Math.abs(data.pixelX)) delta = data.pixelY;
                                    else return;
                                }
                            } else {
                                delta = Math.abs(data.pixelX) > Math.abs(data.pixelY) ? -data.pixelX * rtlFactor : -data.pixelY;
                            }
                            if (delta === 0) return;
                            if (s.params.mousewheelInvert) delta = -delta;
                            if (!s.params.freeMode) {
                                if (new window.Date().getTime() - s.mousewheel.lastScrollTime > 60) {
                                    if (delta < 0) {
                                        if ((!s.isEnd || s.params.loop) && !s.animating) {
                                            s.slideNext();
                                            s.emit("onScroll", s, e);
                                        } else if (s.params.mousewheelReleaseOnEdges) return true;
                                    } else {
                                        if ((!s.isBeginning || s.params.loop) && !s.animating) {
                                            s.slidePrev();
                                            s.emit("onScroll", s, e);
                                        } else if (s.params.mousewheelReleaseOnEdges) return true;
                                    }
                                }
                                s.mousewheel.lastScrollTime = new window.Date().getTime();
                            } else {
                                var position = s.getWrapperTranslate() + delta * s.params.mousewheelSensitivity;
                                var wasBeginning = s.isBeginning,
                                    wasEnd = s.isEnd;
                                if (position >= s.minTranslate()) position = s.minTranslate();
                                if (position <= s.maxTranslate()) position = s.maxTranslate();
                                s.setWrapperTransition(0);
                                s.setWrapperTranslate(position);
                                s.updateProgress();
                                s.updateActiveIndex();
                                if ((!wasBeginning && s.isBeginning) || (!wasEnd && s.isEnd)) {
                                    s.updateClasses();
                                }
                                if (s.params.freeModeSticky) {
                                    clearTimeout(s.mousewheel.timeout);
                                    s.mousewheel.timeout = setTimeout(function () {
                                        s.slideReset();
                                    }, 300);
                                } else {
                                    if (s.params.lazyLoading && s.lazy) {
                                        s.lazy.load();
                                    }
                                }
                                s.emit("onScroll", s, e);
                                if (s.params.autoplay && s.params.autoplayDisableOnInteraction) s.stopAutoplay();
                                if (position === 0 || position === s.maxTranslate()) return;
                            }
                            if (e.preventDefault) e.preventDefault();
                            else e.returnValue = false;
                            return false;
                        }
                        s.disableMousewheelControl = function () {
                            if (!s.mousewheel.event) return false;
                            var target = s.container;
                            if (s.params.mousewheelEventsTarged !== "container") {
                                target = $(s.params.mousewheelEventsTarged);
                            }
                            target.off(s.mousewheel.event, handleMousewheel);
                            s.params.mousewheelControl = false;
                            return true;
                        };
                        s.enableMousewheelControl = function () {
                            if (!s.mousewheel.event) return false;
                            var target = s.container;
                            if (s.params.mousewheelEventsTarged !== "container") {
                                target = $(s.params.mousewheelEventsTarged);
                            }
                            target.on(s.mousewheel.event, handleMousewheel);
                            s.params.mousewheelControl = true;
                            return true;
                        };
                        function setParallaxTransform(el, progress) {
                            el = $(el);
                            var p, pX, pY;
                            var rtlFactor = s.rtl ? -1 : 1;
                            p = el.attr("data-swiper-parallax") || "0";
                            pX = el.attr("data-swiper-parallax-x");
                            pY = el.attr("data-swiper-parallax-y");
                            if (pX || pY) {
                                pX = pX || "0";
                                pY = pY || "0";
                            } else {
                                if (s.isHorizontal()) {
                                    pX = p;
                                    pY = "0";
                                } else {
                                    pY = p;
                                    pX = "0";
                                }
                            }
                            if (pX.indexOf("%") >= 0) {
                                pX = parseInt(pX, 10) * progress * rtlFactor + "%";
                            } else {
                                pX = pX * progress * rtlFactor + "px";
                            }
                            if (pY.indexOf("%") >= 0) {
                                pY = parseInt(pY, 10) * progress + "%";
                            } else {
                                pY = pY * progress + "px";
                            }
                            el.transform("translate3d(" + pX + ", " + pY + ",0px)");
                        }
                        s.parallax = {
                            setTranslate: function () {
                                s.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                                    setParallaxTransform(this, s.progress);
                                });
                                s.slides.each(function () {
                                    var slide = $(this);
                                    slide.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                                        var progress = Math.min(Math.max(slide[0].progress, -1), 1);
                                        setParallaxTransform(this, progress);
                                    });
                                });
                            },
                            setTransition: function (duration) {
                                if (typeof duration === "undefined") duration = s.params.speed;
                                s.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function () {
                                    var el = $(this);
                                    var parallaxDuration = parseInt(el.attr("data-swiper-parallax-duration"), 10) || duration;
                                    if (duration === 0) parallaxDuration = 0;
                                    el.transition(parallaxDuration);
                                });
                            },
                        };
                        s.zoom = {
                            scale: 1,
                            currentScale: 1,
                            isScaling: false,
                            gesture: { slide: undefined, slideWidth: undefined, slideHeight: undefined, image: undefined, imageWrap: undefined, zoomMax: s.params.zoomMax },
                            image: {
                                isTouched: undefined,
                                isMoved: undefined,
                                currentX: undefined,
                                currentY: undefined,
                                minX: undefined,
                                minY: undefined,
                                maxX: undefined,
                                maxY: undefined,
                                width: undefined,
                                height: undefined,
                                startX: undefined,
                                startY: undefined,
                                touchesStart: {},
                                touchesCurrent: {},
                            },
                            velocity: { x: undefined, y: undefined, prevPositionX: undefined, prevPositionY: undefined, prevTime: undefined },
                            getDistanceBetweenTouches: function (e) {
                                if (e.targetTouches.length < 2) return 1;
                                var x1 = e.targetTouches[0].pageX,
                                    y1 = e.targetTouches[0].pageY,
                                    x2 = e.targetTouches[1].pageX,
                                    y2 = e.targetTouches[1].pageY;
                                var distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
                                return distance;
                            },
                            onGestureStart: function (e) {
                                var z = s.zoom;
                                if (!s.support.gestures) {
                                    if (e.type !== "touchstart" || (e.type === "touchstart" && e.targetTouches.length < 2)) {
                                        return;
                                    }
                                    z.gesture.scaleStart = z.getDistanceBetweenTouches(e);
                                }
                                if (!z.gesture.slide || !z.gesture.slide.length) {
                                    z.gesture.slide = $(this);
                                    if (z.gesture.slide.length === 0) z.gesture.slide = s.slides.eq(s.activeIndex);
                                    z.gesture.image = z.gesture.slide.find("img, svg, canvas");
                                    z.gesture.imageWrap = z.gesture.image.parent("." + s.params.zoomContainerClass);
                                    z.gesture.zoomMax = z.gesture.imageWrap.attr("data-swiper-zoom") || s.params.zoomMax;
                                    if (z.gesture.imageWrap.length === 0) {
                                        z.gesture.image = undefined;
                                        return;
                                    }
                                }
                                z.gesture.image.transition(0);
                                z.isScaling = true;
                            },
                            onGestureChange: function (e) {
                                var z = s.zoom;
                                if (!s.support.gestures) {
                                    if (e.type !== "touchmove" || (e.type === "touchmove" && e.targetTouches.length < 2)) {
                                        return;
                                    }
                                    z.gesture.scaleMove = z.getDistanceBetweenTouches(e);
                                }
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                if (s.support.gestures) {
                                    z.scale = e.scale * z.currentScale;
                                } else {
                                    z.scale = (z.gesture.scaleMove / z.gesture.scaleStart) * z.currentScale;
                                }
                                if (z.scale > z.gesture.zoomMax) {
                                    z.scale = z.gesture.zoomMax - 1 + Math.pow(z.scale - z.gesture.zoomMax + 1, 0.5);
                                }
                                if (z.scale < s.params.zoomMin) {
                                    z.scale = s.params.zoomMin + 1 - Math.pow(s.params.zoomMin - z.scale + 1, 0.5);
                                }
                                z.gesture.image.transform("translate3d(0,0,0) scale(" + z.scale + ")");
                            },
                            onGestureEnd: function (e) {
                                var z = s.zoom;
                                if (!s.support.gestures) {
                                    if (e.type !== "touchend" || (e.type === "touchend" && e.changedTouches.length < 2)) {
                                        return;
                                    }
                                }
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                z.scale = Math.max(Math.min(z.scale, z.gesture.zoomMax), s.params.zoomMin);
                                z.gesture.image.transition(s.params.speed).transform("translate3d(0,0,0) scale(" + z.scale + ")");
                                z.currentScale = z.scale;
                                z.isScaling = false;
                                if (z.scale === 1) z.gesture.slide = undefined;
                            },
                            onTouchStart: function (s, e) {
                                var z = s.zoom;
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                if (z.image.isTouched) return;
                                if (s.device.os === "android") e.preventDefault();
                                z.image.isTouched = true;
                                z.image.touchesStart.x = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
                                z.image.touchesStart.y = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
                            },
                            onTouchMove: function (e) {
                                var z = s.zoom;
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                s.allowClick = false;
                                if (!z.image.isTouched || !z.gesture.slide) return;
                                if (!z.image.isMoved) {
                                    z.image.width = z.gesture.image[0].offsetWidth;
                                    z.image.height = z.gesture.image[0].offsetHeight;
                                    z.image.startX = s.getTranslate(z.gesture.imageWrap[0], "x") || 0;
                                    z.image.startY = s.getTranslate(z.gesture.imageWrap[0], "y") || 0;
                                    z.gesture.slideWidth = z.gesture.slide[0].offsetWidth;
                                    z.gesture.slideHeight = z.gesture.slide[0].offsetHeight;
                                    z.gesture.imageWrap.transition(0);
                                    if (s.rtl) z.image.startX = -z.image.startX;
                                    if (s.rtl) z.image.startY = -z.image.startY;
                                }
                                var scaledWidth = z.image.width * z.scale;
                                var scaledHeight = z.image.height * z.scale;
                                if (scaledWidth < z.gesture.slideWidth && scaledHeight < z.gesture.slideHeight) return;
                                z.image.minX = Math.min(z.gesture.slideWidth / 2 - scaledWidth / 2, 0);
                                z.image.maxX = -z.image.minX;
                                z.image.minY = Math.min(z.gesture.slideHeight / 2 - scaledHeight / 2, 0);
                                z.image.maxY = -z.image.minY;
                                z.image.touchesCurrent.x = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
                                z.image.touchesCurrent.y = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;
                                if (!z.image.isMoved && !z.isScaling) {
                                    if (
                                        (s.isHorizontal() && Math.floor(z.image.minX) === Math.floor(z.image.startX) && z.image.touchesCurrent.x < z.image.touchesStart.x) ||
                                        (Math.floor(z.image.maxX) === Math.floor(z.image.startX) && z.image.touchesCurrent.x > z.image.touchesStart.x)
                                    ) {
                                        z.image.isTouched = false;
                                        return;
                                    } else if (
                                        (!s.isHorizontal() && Math.floor(z.image.minY) === Math.floor(z.image.startY) && z.image.touchesCurrent.y < z.image.touchesStart.y) ||
                                        (Math.floor(z.image.maxY) === Math.floor(z.image.startY) && z.image.touchesCurrent.y > z.image.touchesStart.y)
                                    ) {
                                        z.image.isTouched = false;
                                        return;
                                    }
                                }
                                e.preventDefault();
                                e.stopPropagation();
                                z.image.isMoved = true;
                                z.image.currentX = z.image.touchesCurrent.x - z.image.touchesStart.x + z.image.startX;
                                z.image.currentY = z.image.touchesCurrent.y - z.image.touchesStart.y + z.image.startY;
                                if (z.image.currentX < z.image.minX) {
                                    z.image.currentX = z.image.minX + 1 - Math.pow(z.image.minX - z.image.currentX + 1, 0.8);
                                }
                                if (z.image.currentX > z.image.maxX) {
                                    z.image.currentX = z.image.maxX - 1 + Math.pow(z.image.currentX - z.image.maxX + 1, 0.8);
                                }
                                if (z.image.currentY < z.image.minY) {
                                    z.image.currentY = z.image.minY + 1 - Math.pow(z.image.minY - z.image.currentY + 1, 0.8);
                                }
                                if (z.image.currentY > z.image.maxY) {
                                    z.image.currentY = z.image.maxY - 1 + Math.pow(z.image.currentY - z.image.maxY + 1, 0.8);
                                }
                                if (!z.velocity.prevPositionX) z.velocity.prevPositionX = z.image.touchesCurrent.x;
                                if (!z.velocity.prevPositionY) z.velocity.prevPositionY = z.image.touchesCurrent.y;
                                if (!z.velocity.prevTime) z.velocity.prevTime = Date.now();
                                z.velocity.x = (z.image.touchesCurrent.x - z.velocity.prevPositionX) / (Date.now() - z.velocity.prevTime) / 2;
                                z.velocity.y = (z.image.touchesCurrent.y - z.velocity.prevPositionY) / (Date.now() - z.velocity.prevTime) / 2;
                                if (Math.abs(z.image.touchesCurrent.x - z.velocity.prevPositionX) < 2) z.velocity.x = 0;
                                if (Math.abs(z.image.touchesCurrent.y - z.velocity.prevPositionY) < 2) z.velocity.y = 0;
                                z.velocity.prevPositionX = z.image.touchesCurrent.x;
                                z.velocity.prevPositionY = z.image.touchesCurrent.y;
                                z.velocity.prevTime = Date.now();
                                z.gesture.imageWrap.transform("translate3d(" + z.image.currentX + "px, " + z.image.currentY + "px,0)");
                            },
                            onTouchEnd: function (s, e) {
                                var z = s.zoom;
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                if (!z.image.isTouched || !z.image.isMoved) {
                                    z.image.isTouched = false;
                                    z.image.isMoved = false;
                                    return;
                                }
                                z.image.isTouched = false;
                                z.image.isMoved = false;
                                var momentumDurationX = 300;
                                var momentumDurationY = 300;
                                var momentumDistanceX = z.velocity.x * momentumDurationX;
                                var newPositionX = z.image.currentX + momentumDistanceX;
                                var momentumDistanceY = z.velocity.y * momentumDurationY;
                                var newPositionY = z.image.currentY + momentumDistanceY;
                                if (z.velocity.x !== 0) momentumDurationX = Math.abs((newPositionX - z.image.currentX) / z.velocity.x);
                                if (z.velocity.y !== 0) momentumDurationY = Math.abs((newPositionY - z.image.currentY) / z.velocity.y);
                                var momentumDuration = Math.max(momentumDurationX, momentumDurationY);
                                z.image.currentX = newPositionX;
                                z.image.currentY = newPositionY;
                                var scaledWidth = z.image.width * z.scale;
                                var scaledHeight = z.image.height * z.scale;
                                z.image.minX = Math.min(z.gesture.slideWidth / 2 - scaledWidth / 2, 0);
                                z.image.maxX = -z.image.minX;
                                z.image.minY = Math.min(z.gesture.slideHeight / 2 - scaledHeight / 2, 0);
                                z.image.maxY = -z.image.minY;
                                z.image.currentX = Math.max(Math.min(z.image.currentX, z.image.maxX), z.image.minX);
                                z.image.currentY = Math.max(Math.min(z.image.currentY, z.image.maxY), z.image.minY);
                                z.gesture.imageWrap.transition(momentumDuration).transform("translate3d(" + z.image.currentX + "px, " + z.image.currentY + "px,0)");
                            },
                            onTransitionEnd: function (s) {
                                var z = s.zoom;
                                if (z.gesture.slide && s.previousIndex !== s.activeIndex) {
                                    z.gesture.image.transform("translate3d(0,0,0) scale(1)");
                                    z.gesture.imageWrap.transform("translate3d(0,0,0)");
                                    z.gesture.slide = z.gesture.image = z.gesture.imageWrap = undefined;
                                    z.scale = z.currentScale = 1;
                                }
                            },
                            toggleZoom: function (s, e) {
                                var z = s.zoom;
                                if (!z.gesture.slide) {
                                    z.gesture.slide = s.clickedSlide ? $(s.clickedSlide) : s.slides.eq(s.activeIndex);
                                    z.gesture.image = z.gesture.slide.find("img, svg, canvas");
                                    z.gesture.imageWrap = z.gesture.image.parent("." + s.params.zoomContainerClass);
                                }
                                if (!z.gesture.image || z.gesture.image.length === 0) return;
                                var touchX,
                                    touchY,
                                    offsetX,
                                    offsetY,
                                    diffX,
                                    diffY,
                                    translateX,
                                    translateY,
                                    imageWidth,
                                    imageHeight,
                                    scaledWidth,
                                    scaledHeight,
                                    translateMinX,
                                    translateMinY,
                                    translateMaxX,
                                    translateMaxY,
                                    slideWidth,
                                    slideHeight;
                                if (typeof z.image.touchesStart.x === "undefined" && e) {
                                    touchX = e.type === "touchend" ? e.changedTouches[0].pageX : e.pageX;
                                    touchY = e.type === "touchend" ? e.changedTouches[0].pageY : e.pageY;
                                } else {
                                    touchX = z.image.touchesStart.x;
                                    touchY = z.image.touchesStart.y;
                                }
                                if (z.scale && z.scale !== 1) {
                                    z.scale = z.currentScale = 1;
                                    z.gesture.imageWrap.transition(300).transform("translate3d(0,0,0)");
                                    z.gesture.image.transition(300).transform("translate3d(0,0,0) scale(1)");
                                    z.gesture.slide = undefined;
                                } else {
                                    z.scale = z.currentScale = z.gesture.imageWrap.attr("data-swiper-zoom") || s.params.zoomMax;
                                    if (e) {
                                        slideWidth = z.gesture.slide[0].offsetWidth;
                                        slideHeight = z.gesture.slide[0].offsetHeight;
                                        offsetX = z.gesture.slide.offset().left;
                                        offsetY = z.gesture.slide.offset().top;
                                        diffX = offsetX + slideWidth / 2 - touchX;
                                        diffY = offsetY + slideHeight / 2 - touchY;
                                        imageWidth = z.gesture.image[0].offsetWidth;
                                        imageHeight = z.gesture.image[0].offsetHeight;
                                        scaledWidth = imageWidth * z.scale;
                                        scaledHeight = imageHeight * z.scale;
                                        translateMinX = Math.min(slideWidth / 2 - scaledWidth / 2, 0);
                                        translateMinY = Math.min(slideHeight / 2 - scaledHeight / 2, 0);
                                        translateMaxX = -translateMinX;
                                        translateMaxY = -translateMinY;
                                        translateX = diffX * z.scale;
                                        translateY = diffY * z.scale;
                                        if (translateX < translateMinX) {
                                            translateX = translateMinX;
                                        }
                                        if (translateX > translateMaxX) {
                                            translateX = translateMaxX;
                                        }
                                        if (translateY < translateMinY) {
                                            translateY = translateMinY;
                                        }
                                        if (translateY > translateMaxY) {
                                            translateY = translateMaxY;
                                        }
                                    } else {
                                        translateX = 0;
                                        translateY = 0;
                                    }
                                    z.gesture.imageWrap.transition(300).transform("translate3d(" + translateX + "px, " + translateY + "px,0)");
                                    z.gesture.image.transition(300).transform("translate3d(0,0,0) scale(" + z.scale + ")");
                                }
                            },
                            attachEvents: function (detach) {
                                var action = detach ? "off" : "on";
                                if (s.params.zoom) {
                                    var target = s.slides;
                                    var passiveListener = s.touchEvents.start === "touchstart" && s.support.passiveListener && s.params.passiveListeners ? { passive: true, capture: false } : false;
                                    if (s.support.gestures) {
                                        s.slides[action]("gesturestart", s.zoom.onGestureStart, passiveListener);
                                        s.slides[action]("gesturechange", s.zoom.onGestureChange, passiveListener);
                                        s.slides[action]("gestureend", s.zoom.onGestureEnd, passiveListener);
                                    } else if (s.touchEvents.start === "touchstart") {
                                        s.slides[action](s.touchEvents.start, s.zoom.onGestureStart, passiveListener);
                                        s.slides[action](s.touchEvents.move, s.zoom.onGestureChange, passiveListener);
                                        s.slides[action](s.touchEvents.end, s.zoom.onGestureEnd, passiveListener);
                                    }
                                    s[action]("touchStart", s.zoom.onTouchStart);
                                    s.slides.each(function (index, slide) {
                                        if ($(slide).find("." + s.params.zoomContainerClass).length > 0) {
                                            $(slide)[action](s.touchEvents.move, s.zoom.onTouchMove);
                                        }
                                    });
                                    s[action]("touchEnd", s.zoom.onTouchEnd);
                                    s[action]("transitionEnd", s.zoom.onTransitionEnd);
                                    if (s.params.zoomToggle) {
                                        s.on("doubleTap", s.zoom.toggleZoom);
                                    }
                                }
                            },
                            init: function () {
                                s.zoom.attachEvents();
                            },
                            destroy: function () {
                                s.zoom.attachEvents(true);
                            },
                        };
                        s._plugins = [];
                        for (var plugin in s.plugins) {
                            var p = s.plugins[plugin](s, s.params[plugin]);
                            if (p) s._plugins.push(p);
                        }
                        s.callPlugins = function (eventName) {
                            for (var i = 0; i < s._plugins.length; i++) {
                                if (eventName in s._plugins[i]) {
                                    s._plugins[i][eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                                }
                            }
                        };
                        function normalizeEventName(eventName) {
                            if (eventName.indexOf("on") !== 0) {
                                if (eventName[0] !== eventName[0].toUpperCase()) {
                                    eventName = "on" + eventName[0].toUpperCase() + eventName.substring(1);
                                } else {
                                    eventName = "on" + eventName;
                                }
                            }
                            return eventName;
                        }
                        s.emitterEventListeners = {};
                        s.emit = function (eventName) {
                            if (s.params[eventName]) {
                                s.params[eventName](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                            }
                            var i;
                            if (s.emitterEventListeners[eventName]) {
                                for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                                    s.emitterEventListeners[eventName][i](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                                }
                            }
                            if (s.callPlugins) s.callPlugins(eventName, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                        };
                        s.on = function (eventName, handler) {
                            eventName = normalizeEventName(eventName);
                            if (!s.emitterEventListeners[eventName]) s.emitterEventListeners[eventName] = [];
                            s.emitterEventListeners[eventName].push(handler);
                            return s;
                        };
                        s.off = function (eventName, handler) {
                            var i;
                            eventName = normalizeEventName(eventName);
                            if (typeof handler === "undefined") {
                                s.emitterEventListeners[eventName] = [];
                                return s;
                            }
                            if (!s.emitterEventListeners[eventName] || s.emitterEventListeners[eventName].length === 0) return;
                            for (i = 0; i < s.emitterEventListeners[eventName].length; i++) {
                                if (s.emitterEventListeners[eventName][i] === handler) s.emitterEventListeners[eventName].splice(i, 1);
                            }
                            return s;
                        };
                        s.once = function (eventName, handler) {
                            eventName = normalizeEventName(eventName);
                            var _handler = function () {
                                handler(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
                                s.off(eventName, _handler);
                            };
                            s.on(eventName, _handler);
                            return s;
                        };
                        s.a11y = {
                            makeFocusable: function ($el) {
                                $el.attr("tabIndex", "0");
                                return $el;
                            },
                            addRole: function ($el, role) {
                                $el.attr("role", role);
                                return $el;
                            },
                            addLabel: function ($el, label) {
                                $el.attr("aria-label", label);
                                return $el;
                            },
                            disable: function ($el) {
                                $el.attr("aria-disabled", true);
                                return $el;
                            },
                            enable: function ($el) {
                                $el.attr("aria-disabled", false);
                                return $el;
                            },
                            onEnterKey: function (event) {
                                if (event.keyCode !== 13) return;
                                if ($(event.target).is(s.params.nextButton)) {
                                    s.onClickNext(event);
                                    if (s.isEnd) {
                                        s.a11y.notify(s.params.lastSlideMessage);
                                    } else {
                                        s.a11y.notify(s.params.nextSlideMessage);
                                    }
                                } else if ($(event.target).is(s.params.prevButton)) {
                                    s.onClickPrev(event);
                                    if (s.isBeginning) {
                                        s.a11y.notify(s.params.firstSlideMessage);
                                    } else {
                                        s.a11y.notify(s.params.prevSlideMessage);
                                    }
                                }
                                if ($(event.target).is("." + s.params.bulletClass)) {
                                    $(event.target)[0].click();
                                }
                            },
                            liveRegion: $('<span class="' + s.params.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>'),
                            notify: function (message) {
                                var notification = s.a11y.liveRegion;
                                if (notification.length === 0) return;
                                notification.html("");
                                notification.html(message);
                            },
                            init: function () {
                                if (s.params.nextButton && s.nextButton && s.nextButton.length > 0) {
                                    s.a11y.makeFocusable(s.nextButton);
                                    s.a11y.addRole(s.nextButton, "button");
                                    s.a11y.addLabel(s.nextButton, s.params.nextSlideMessage);
                                }
                                if (s.params.prevButton && s.prevButton && s.prevButton.length > 0) {
                                    s.a11y.makeFocusable(s.prevButton);
                                    s.a11y.addRole(s.prevButton, "button");
                                    s.a11y.addLabel(s.prevButton, s.params.prevSlideMessage);
                                }
                                $(s.container).append(s.a11y.liveRegion);
                            },
                            initPagination: function () {
                                if (s.params.pagination && s.params.paginationClickable && s.bullets && s.bullets.length) {
                                    s.bullets.each(function () {
                                        var bullet = $(this);
                                        s.a11y.makeFocusable(bullet);
                                        s.a11y.addRole(bullet, "button");
                                        s.a11y.addLabel(bullet, s.params.paginationBulletMessage.replace(/{{index}}/, bullet.index() + 1));
                                    });
                                }
                            },
                            destroy: function () {
                                if (s.a11y.liveRegion && s.a11y.liveRegion.length > 0) s.a11y.liveRegion.remove();
                            },
                        };
                        s.init = function () {
                            if (s.params.loop) s.createLoop();
                            s.updateContainerSize();
                            s.updateSlidesSize();
                            s.updatePagination();
                            if (s.params.scrollbar && s.scrollbar) {
                                s.scrollbar.set();
                                if (s.params.scrollbarDraggable) {
                                    s.scrollbar.enableDraggable();
                                }
                            }
                            if (s.params.effect !== "slide" && s.effects[s.params.effect]) {
                                if (!s.params.loop) s.updateProgress();
                                s.effects[s.params.effect].setTranslate();
                            }
                            if (s.params.loop) {
                                s.slideTo(s.params.initialSlide + s.loopedSlides, 0, s.params.runCallbacksOnInit);
                            } else {
                                s.slideTo(s.params.initialSlide, 0, s.params.runCallbacksOnInit);
                                if (s.params.initialSlide === 0) {
                                    if (s.parallax && s.params.parallax) s.parallax.setTranslate();
                                    if (s.lazy && s.params.lazyLoading) {
                                        s.lazy.load();
                                        s.lazy.initialImageLoaded = true;
                                    }
                                }
                            }
                            s.attachEvents();
                            if (s.params.observer && s.support.observer) {
                                s.initObservers();
                            }
                            if (s.params.preloadImages && !s.params.lazyLoading) {
                                s.preloadImages();
                            }
                            if (s.params.zoom && s.zoom) {
                                s.zoom.init();
                            }
                            if (s.params.autoplay) {
                                s.startAutoplay();
                            }
                            if (s.params.keyboardControl) {
                                if (s.enableKeyboardControl) s.enableKeyboardControl();
                            }
                            if (s.params.mousewheelControl) {
                                if (s.enableMousewheelControl) s.enableMousewheelControl();
                            }
                            if (s.params.hashnavReplaceState) {
                                s.params.replaceState = s.params.hashnavReplaceState;
                            }
                            if (s.params.history) {
                                if (s.history) s.history.init();
                            }
                            if (s.params.hashnav) {
                                if (s.hashnav) s.hashnav.init();
                            }
                            if (s.params.a11y && s.a11y) s.a11y.init();
                            s.emit("onInit", s);
                        };
                        s.cleanupStyles = function () {
                            s.container.removeClass(s.classNames.join(" ")).removeAttr("style");
                            s.wrapper.removeAttr("style");
                            if (s.slides && s.slides.length) {
                                s.slides
                                    .removeClass([s.params.slideVisibleClass, s.params.slideActiveClass, s.params.slideNextClass, s.params.slidePrevClass].join(" "))
                                    .removeAttr("style")
                                    .removeAttr("data-swiper-column")
                                    .removeAttr("data-swiper-row");
                            }
                            if (s.paginationContainer && s.paginationContainer.length) {
                                s.paginationContainer.removeClass(s.params.paginationHiddenClass);
                            }
                            if (s.bullets && s.bullets.length) {
                                s.bullets.removeClass(s.params.bulletActiveClass);
                            }
                            if (s.params.prevButton) $(s.params.prevButton).removeClass(s.params.buttonDisabledClass);
                            if (s.params.nextButton) $(s.params.nextButton).removeClass(s.params.buttonDisabledClass);
                            if (s.params.scrollbar && s.scrollbar) {
                                if (s.scrollbar.track && s.scrollbar.track.length) s.scrollbar.track.removeAttr("style");
                                if (s.scrollbar.drag && s.scrollbar.drag.length) s.scrollbar.drag.removeAttr("style");
                            }
                        };
                        s.destroy = function (deleteInstance, cleanupStyles) {
                            s.detachEvents();
                            s.stopAutoplay();
                            if (s.params.scrollbar && s.scrollbar) {
                                if (s.params.scrollbarDraggable) {
                                    s.scrollbar.disableDraggable();
                                }
                            }
                            if (s.params.loop) {
                                s.destroyLoop();
                            }
                            if (cleanupStyles) {
                                s.cleanupStyles();
                            }
                            s.disconnectObservers();
                            if (s.params.zoom && s.zoom) {
                                s.zoom.destroy();
                            }
                            if (s.params.keyboardControl) {
                                if (s.disableKeyboardControl) s.disableKeyboardControl();
                            }
                            if (s.params.mousewheelControl) {
                                if (s.disableMousewheelControl) s.disableMousewheelControl();
                            }
                            if (s.params.a11y && s.a11y) s.a11y.destroy();
                            if (s.params.history && !s.params.replaceState) {
                                window.removeEventListener("popstate", s.history.setHistoryPopState);
                            }
                            if (s.params.hashnav && s.hashnav) {
                                s.hashnav.destroy();
                            }
                            s.emit("onDestroy");
                            if (deleteInstance !== false) s = null;
                        };
                        s.init();
                        return s;
                    };
                    Swiper.prototype = {
                        isSafari: (function () {
                            var ua = window.navigator.userAgent.toLowerCase();
                            return ua.indexOf("safari") >= 0 && ua.indexOf("chrome") < 0 && ua.indexOf("android") < 0;
                        })(),
                        isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window.navigator.userAgent),
                        isArray: function (arr) {
                            return Object.prototype.toString.apply(arr) === "[object Array]";
                        },
                        browser: {
                            ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
                            ieTouch: (window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1) || (window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1),
                            lteIE9: (function () {
                                var div = document.createElement("div");
                                div.innerHTML = "\x3c!--[if lte IE 9]><i></i><![endif]--\x3e";
                                return div.getElementsByTagName("i").length === 1;
                            })(),
                        },
                        device: (function () {
                            var ua = window.navigator.userAgent;
                            var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
                            var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
                            var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
                            var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
                            return { ios: ipad || iphone || ipod, android: android };
                        })(),
                        support: {
                            touch:
                                (window.Modernizr && Modernizr.touch === true) ||
                                (function () {
                                    return !!("ontouchstart" in window || (window.DocumentTouch && document instanceof DocumentTouch));
                                })(),
                            transforms3d:
                                (window.Modernizr && Modernizr.csstransforms3d === true) ||
                                (function () {
                                    var div = document.createElement("div").style;
                                    return "webkitPerspective" in div || "MozPerspective" in div || "OPerspective" in div || "MsPerspective" in div || "perspective" in div;
                                })(),
                            flexbox: (function () {
                                var div = document.createElement("div").style;
                                var styles = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" ");
                                for (var i = 0; i < styles.length; i++) {
                                    if (styles[i] in div) return true;
                                }
                            })(),
                            observer: (function () {
                                return "MutationObserver" in window || "WebkitMutationObserver" in window;
                            })(),
                            passiveListener: (function () {
                                var supportsPassive = false;
                                try {
                                    var opts = Object.defineProperty({}, "passive", {
                                        get: function () {
                                            supportsPassive = true;
                                        },
                                    });
                                    window.addEventListener("testPassiveListener", null, opts);
                                } catch (e) { }
                                return supportsPassive;
                            })(),
                            gestures: (function () {
                                return "ongesturestart" in window;
                            })(),
                        },
                        plugins: {},
                    };
                    var Dom7 = (function () {
                        var Dom7 = function (arr) {
                            var _this = this,
                                i = 0;
                            for (i = 0; i < arr.length; i++) {
                                _this[i] = arr[i];
                            }
                            _this.length = arr.length;
                            return this;
                        };
                        var $ = function (selector, context) {
                            var arr = [],
                                i = 0;
                            if (selector && !context) {
                                if (selector instanceof Dom7) {
                                    return selector;
                                }
                            }
                            if (selector) {
                                if (typeof selector === "string") {
                                    var els,
                                        tempParent,
                                        html = selector.trim();
                                    if (html.indexOf("<") >= 0 && html.indexOf(">") >= 0) {
                                        var toCreate = "div";
                                        if (html.indexOf("<li") === 0) toCreate = "ul";
                                        if (html.indexOf("<tr") === 0) toCreate = "tbody";
                                        if (html.indexOf("<td") === 0 || html.indexOf("<th") === 0) toCreate = "tr";
                                        if (html.indexOf("<tbody") === 0) toCreate = "table";
                                        if (html.indexOf("<option") === 0) toCreate = "select";
                                        tempParent = document.createElement(toCreate);
                                        tempParent.innerHTML = selector;
                                        for (i = 0; i < tempParent.childNodes.length; i++) {
                                            arr.push(tempParent.childNodes[i]);
                                        }
                                    } else {
                                        if (!context && selector[0] === "#" && !selector.match(/[ .<>:~]/)) {
                                            els = [document.getElementById(selector.split("#")[1])];
                                        } else {
                                            els = (context || document).querySelectorAll(selector);
                                        }
                                        for (i = 0; i < els.length; i++) {
                                            if (els[i]) arr.push(els[i]);
                                        }
                                    }
                                } else if (selector.nodeType || selector === window || selector === document) {
                                    arr.push(selector);
                                } else if (selector.length > 0 && selector[0].nodeType) {
                                    for (i = 0; i < selector.length; i++) {
                                        arr.push(selector[i]);
                                    }
                                }
                            }
                            return new Dom7(arr);
                        };
                        Dom7.prototype = {
                            addClass: function (className) {
                                if (typeof className === "undefined") {
                                    return this;
                                }
                                var classes = className.split(" ");
                                for (var i = 0; i < classes.length; i++) {
                                    for (var j = 0; j < this.length; j++) {
                                        this[j].classList.add(classes[i]);
                                    }
                                }
                                return this;
                            },
                            removeClass: function (className) {
                                var classes = className.split(" ");
                                for (var i = 0; i < classes.length; i++) {
                                    for (var j = 0; j < this.length; j++) {
                                        this[j].classList.remove(classes[i]);
                                    }
                                }
                                return this;
                            },
                            hasClass: function (className) {
                                if (!this[0]) return false;
                                else return this[0].classList.contains(className);
                            },
                            toggleClass: function (className) {
                                var classes = className.split(" ");
                                for (var i = 0; i < classes.length; i++) {
                                    for (var j = 0; j < this.length; j++) {
                                        this[j].classList.toggle(classes[i]);
                                    }
                                }
                                return this;
                            },
                            attr: function (attrs, value) {
                                if (arguments.length === 1 && typeof attrs === "string") {
                                    if (this[0]) return this[0].getAttribute(attrs);
                                    else return undefined;
                                } else {
                                    for (var i = 0; i < this.length; i++) {
                                        if (arguments.length === 2) {
                                            this[i].setAttribute(attrs, value);
                                        } else {
                                            for (var attrName in attrs) {
                                                this[i][attrName] = attrs[attrName];
                                                this[i].setAttribute(attrName, attrs[attrName]);
                                            }
                                        }
                                    }
                                    return this;
                                }
                            },
                            removeAttr: function (attr) {
                                for (var i = 0; i < this.length; i++) {
                                    this[i].removeAttribute(attr);
                                }
                                return this;
                            },
                            data: function (key, value) {
                                if (typeof value === "undefined") {
                                    if (this[0]) {
                                        var dataKey = this[0].getAttribute("data-" + key);
                                        if (dataKey) return dataKey;
                                        else if (this[0].dom7ElementDataStorage && key in this[0].dom7ElementDataStorage) return this[0].dom7ElementDataStorage[key];
                                        else return undefined;
                                    } else return undefined;
                                } else {
                                    for (var i = 0; i < this.length; i++) {
                                        var el = this[i];
                                        if (!el.dom7ElementDataStorage) el.dom7ElementDataStorage = {};
                                        el.dom7ElementDataStorage[key] = value;
                                    }
                                    return this;
                                }
                            },
                            transform: function (transform) {
                                for (var i = 0; i < this.length; i++) {
                                    var elStyle = this[i].style;
                                    elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
                                }
                                return this;
                            },
                            transition: function (duration) {
                                if (typeof duration !== "string") {
                                    duration = duration + "ms";
                                }
                                for (var i = 0; i < this.length; i++) {
                                    var elStyle = this[i].style;
                                    elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
                                }
                                return this;
                            },
                            on: function (eventName, targetSelector, listener, capture) {
                                function handleLiveEvent(e) {
                                    var target = e.target;
                                    if ($(target).is(targetSelector)) listener.call(target, e);
                                    else {
                                        var parents = $(target).parents();
                                        for (var k = 0; k < parents.length; k++) {
                                            if ($(parents[k]).is(targetSelector)) listener.call(parents[k], e);
                                        }
                                    }
                                }
                                var events = eventName.split(" ");
                                var i, j;
                                for (i = 0; i < this.length; i++) {
                                    if (typeof targetSelector === "function" || targetSelector === false) {
                                        if (typeof targetSelector === "function") {
                                            listener = arguments[1];
                                            capture = arguments[2] || false;
                                        }
                                        for (j = 0; j < events.length; j++) {
                                            this[i].addEventListener(events[j], listener, capture);
                                        }
                                    } else {
                                        for (j = 0; j < events.length; j++) {
                                            if (!this[i].dom7LiveListeners) this[i].dom7LiveListeners = [];
                                            this[i].dom7LiveListeners.push({ listener: listener, liveListener: handleLiveEvent });
                                            this[i].addEventListener(events[j], handleLiveEvent, capture);
                                        }
                                    }
                                }
                                return this;
                            },
                            off: function (eventName, targetSelector, listener, capture) {
                                var events = eventName.split(" ");
                                for (var i = 0; i < events.length; i++) {
                                    for (var j = 0; j < this.length; j++) {
                                        if (typeof targetSelector === "function" || targetSelector === false) {
                                            if (typeof targetSelector === "function") {
                                                listener = arguments[1];
                                                capture = arguments[2] || false;
                                            }
                                            this[j].removeEventListener(events[i], listener, capture);
                                        } else {
                                            if (this[j].dom7LiveListeners) {
                                                for (var k = 0; k < this[j].dom7LiveListeners.length; k++) {
                                                    if (this[j].dom7LiveListeners[k].listener === listener) {
                                                        this[j].removeEventListener(events[i], this[j].dom7LiveListeners[k].liveListener, capture);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                return this;
                            },
                            once: function (eventName, targetSelector, listener, capture) {
                                var dom = this;
                                if (typeof targetSelector === "function") {
                                    targetSelector = false;
                                    listener = arguments[1];
                                    capture = arguments[2];
                                }
                                function proxy(e) {
                                    listener(e);
                                    dom.off(eventName, targetSelector, proxy, capture);
                                }
                                dom.on(eventName, targetSelector, proxy, capture);
                            },
                            trigger: function (eventName, eventData) {
                                for (var i = 0; i < this.length; i++) {
                                    var evt;
                                    try {
                                        evt = new window.CustomEvent(eventName, { detail: eventData, bubbles: true, cancelable: true });
                                    } catch (e) {
                                        evt = document.createEvent("Event");
                                        evt.initEvent(eventName, true, true);
                                        evt.detail = eventData;
                                    }
                                    this[i].dispatchEvent(evt);
                                }
                                return this;
                            },
                            transitionEnd: function (callback) {
                                var events = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
                                    i,
                                    j,
                                    dom = this;
                                function fireCallBack(e) {
                                    if (e.target !== this) return;
                                    callback.call(this, e);
                                    for (i = 0; i < events.length; i++) {
                                        dom.off(events[i], fireCallBack);
                                    }
                                }
                                if (callback) {
                                    for (i = 0; i < events.length; i++) {
                                        dom.on(events[i], fireCallBack);
                                    }
                                }
                                return this;
                            },
                            width: function () {
                                if (this[0] === window) {
                                    return window.innerWidth;
                                } else {
                                    if (this.length > 0) {
                                        return parseFloat(this.css("width"));
                                    } else {
                                        return null;
                                    }
                                }
                            },
                            outerWidth: function (includeMargins) {
                                if (this.length > 0) {
                                    if (includeMargins) return this[0].offsetWidth + parseFloat(this.css("margin-right")) + parseFloat(this.css("margin-left"));
                                    else return this[0].offsetWidth;
                                } else return null;
                            },
                            height: function () {
                                if (this[0] === window) {
                                    return window.innerHeight;
                                } else {
                                    if (this.length > 0) {
                                        return parseFloat(this.css("height"));
                                    } else {
                                        return null;
                                    }
                                }
                            },
                            outerHeight: function (includeMargins) {
                                if (this.length > 0) {
                                    if (includeMargins) return this[0].offsetHeight + parseFloat(this.css("margin-top")) + parseFloat(this.css("margin-bottom"));
                                    else return this[0].offsetHeight;
                                } else return null;
                            },
                            offset: function () {
                                if (this.length > 0) {
                                    var el = this[0];
                                    var box = el.getBoundingClientRect();
                                    var body = document.body;
                                    var clientTop = el.clientTop || body.clientTop || 0;
                                    var clientLeft = el.clientLeft || body.clientLeft || 0;
                                    var scrollTop = window.pageYOffset || el.scrollTop;
                                    var scrollLeft = window.pageXOffset || el.scrollLeft;
                                    return { top: box.top + scrollTop - clientTop, left: box.left + scrollLeft - clientLeft };
                                } else {
                                    return null;
                                }
                            },
                            css: function (props, value) {
                                var i;
                                if (arguments.length === 1) {
                                    if (typeof props === "string") {
                                        if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(props);
                                    } else {
                                        for (i = 0; i < this.length; i++) {
                                            for (var prop in props) {
                                                this[i].style[prop] = props[prop];
                                            }
                                        }
                                        return this;
                                    }
                                }
                                if (arguments.length === 2 && typeof props === "string") {
                                    for (i = 0; i < this.length; i++) {
                                        this[i].style[props] = value;
                                    }
                                    return this;
                                }
                                return this;
                            },
                            each: function (callback) {
                                for (var i = 0; i < this.length; i++) {
                                    callback.call(this[i], i, this[i]);
                                }
                                return this;
                            },
                            html: function (html) {
                                if (typeof html === "undefined") {
                                    return this[0] ? this[0].innerHTML : undefined;
                                } else {
                                    for (var i = 0; i < this.length; i++) {
                                        this[i].innerHTML = html;
                                    }
                                    return this;
                                }
                            },
                            text: function (text) {
                                if (typeof text === "undefined") {
                                    if (this[0]) {
                                        return this[0].textContent.trim();
                                    } else return null;
                                } else {
                                    for (var i = 0; i < this.length; i++) {
                                        this[i].textContent = text;
                                    }
                                    return this;
                                }
                            },
                            is: function (selector) {
                                if (!this[0]) return false;
                                var compareWith, i;
                                if (typeof selector === "string") {
                                    var el = this[0];
                                    if (el === document) return selector === document;
                                    if (el === window) return selector === window;
                                    if (el.matches) return el.matches(selector);
                                    else if (el.webkitMatchesSelector) return el.webkitMatchesSelector(selector);
                                    else if (el.mozMatchesSelector) return el.mozMatchesSelector(selector);
                                    else if (el.msMatchesSelector) return el.msMatchesSelector(selector);
                                    else {
                                        compareWith = $(selector);
                                        for (i = 0; i < compareWith.length; i++) {
                                            if (compareWith[i] === this[0]) return true;
                                        }
                                        return false;
                                    }
                                } else if (selector === document) return this[0] === document;
                                else if (selector === window) return this[0] === window;
                                else {
                                    if (selector.nodeType || selector instanceof Dom7) {
                                        compareWith = selector.nodeType ? [selector] : selector;
                                        for (i = 0; i < compareWith.length; i++) {
                                            if (compareWith[i] === this[0]) return true;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                            },
                            index: function () {
                                if (this[0]) {
                                    var child = this[0];
                                    var i = 0;
                                    while ((child = child.previousSibling) !== null) {
                                        if (child.nodeType === 1) i++;
                                    }
                                    return i;
                                } else return undefined;
                            },
                            eq: function (index) {
                                if (typeof index === "undefined") return this;
                                var length = this.length;
                                var returnIndex;
                                if (index > length - 1) {
                                    return new Dom7([]);
                                }
                                if (index < 0) {
                                    returnIndex = length + index;
                                    if (returnIndex < 0) return new Dom7([]);
                                    else return new Dom7([this[returnIndex]]);
                                }
                                return new Dom7([this[index]]);
                            },
                            append: function (newChild) {
                                var i, j;
                                for (i = 0; i < this.length; i++) {
                                    if (typeof newChild === "string") {
                                        var tempDiv = document.createElement("div");
                                        tempDiv.innerHTML = newChild;
                                        while (tempDiv.firstChild) {
                                            this[i].appendChild(tempDiv.firstChild);
                                        }
                                    } else if (newChild instanceof Dom7) {
                                        for (j = 0; j < newChild.length; j++) {
                                            this[i].appendChild(newChild[j]);
                                        }
                                    } else {
                                        this[i].appendChild(newChild);
                                    }
                                }
                                return this;
                            },
                            prepend: function (newChild) {
                                var i, j;
                                for (i = 0; i < this.length; i++) {
                                    if (typeof newChild === "string") {
                                        var tempDiv = document.createElement("div");
                                        tempDiv.innerHTML = newChild;
                                        for (j = tempDiv.childNodes.length - 1; j >= 0; j--) {
                                            this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
                                        }
                                    } else if (newChild instanceof Dom7) {
                                        for (j = 0; j < newChild.length; j++) {
                                            this[i].insertBefore(newChild[j], this[i].childNodes[0]);
                                        }
                                    } else {
                                        this[i].insertBefore(newChild, this[i].childNodes[0]);
                                    }
                                }
                                return this;
                            },
                            insertBefore: function (selector) {
                                var before = $(selector);
                                for (var i = 0; i < this.length; i++) {
                                    if (before.length === 1) {
                                        before[0].parentNode.insertBefore(this[i], before[0]);
                                    } else if (before.length > 1) {
                                        for (var j = 0; j < before.length; j++) {
                                            before[j].parentNode.insertBefore(this[i].cloneNode(true), before[j]);
                                        }
                                    }
                                }
                            },
                            insertAfter: function (selector) {
                                var after = $(selector);
                                for (var i = 0; i < this.length; i++) {
                                    if (after.length === 1) {
                                        after[0].parentNode.insertBefore(this[i], after[0].nextSibling);
                                    } else if (after.length > 1) {
                                        for (var j = 0; j < after.length; j++) {
                                            after[j].parentNode.insertBefore(this[i].cloneNode(true), after[j].nextSibling);
                                        }
                                    }
                                }
                            },
                            next: function (selector) {
                                if (this.length > 0) {
                                    if (selector) {
                                        if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) return new Dom7([this[0].nextElementSibling]);
                                        else return new Dom7([]);
                                    } else {
                                        if (this[0].nextElementSibling) return new Dom7([this[0].nextElementSibling]);
                                        else return new Dom7([]);
                                    }
                                } else return new Dom7([]);
                            },
                            nextAll: function (selector) {
                                var nextEls = [];
                                var el = this[0];
                                if (!el) return new Dom7([]);
                                while (el.nextElementSibling) {
                                    var next = el.nextElementSibling;
                                    if (selector) {
                                        if ($(next).is(selector)) nextEls.push(next);
                                    } else nextEls.push(next);
                                    el = next;
                                }
                                return new Dom7(nextEls);
                            },
                            prev: function (selector) {
                                if (this.length > 0) {
                                    if (selector) {
                                        if (this[0].previousElementSibling && $(this[0].previousElementSibling).is(selector)) return new Dom7([this[0].previousElementSibling]);
                                        else return new Dom7([]);
                                    } else {
                                        if (this[0].previousElementSibling) return new Dom7([this[0].previousElementSibling]);
                                        else return new Dom7([]);
                                    }
                                } else return new Dom7([]);
                            },
                            prevAll: function (selector) {
                                var prevEls = [];
                                var el = this[0];
                                if (!el) return new Dom7([]);
                                while (el.previousElementSibling) {
                                    var prev = el.previousElementSibling;
                                    if (selector) {
                                        if ($(prev).is(selector)) prevEls.push(prev);
                                    } else prevEls.push(prev);
                                    el = prev;
                                }
                                return new Dom7(prevEls);
                            },
                            parent: function (selector) {
                                var parents = [];
                                for (var i = 0; i < this.length; i++) {
                                    if (selector) {
                                        if ($(this[i].parentNode).is(selector)) parents.push(this[i].parentNode);
                                    } else {
                                        parents.push(this[i].parentNode);
                                    }
                                }
                                return $($.unique(parents));
                            },
                            parents: function (selector) {
                                var parents = [];
                                for (var i = 0; i < this.length; i++) {
                                    var parent = this[i].parentNode;
                                    while (parent) {
                                        if (selector) {
                                            if ($(parent).is(selector)) parents.push(parent);
                                        } else {
                                            parents.push(parent);
                                        }
                                        parent = parent.parentNode;
                                    }
                                }
                                return $($.unique(parents));
                            },
                            find: function (selector) {
                                var foundElements = [];
                                for (var i = 0; i < this.length; i++) {
                                    var found = this[i].querySelectorAll(selector);
                                    for (var j = 0; j < found.length; j++) {
                                        foundElements.push(found[j]);
                                    }
                                }
                                return new Dom7(foundElements);
                            },
                            children: function (selector) {
                                var children = [];
                                for (var i = 0; i < this.length; i++) {
                                    var childNodes = this[i].childNodes;
                                    for (var j = 0; j < childNodes.length; j++) {
                                        if (!selector) {
                                            if (childNodes[j].nodeType === 1) children.push(childNodes[j]);
                                        } else {
                                            if (childNodes[j].nodeType === 1 && $(childNodes[j]).is(selector)) children.push(childNodes[j]);
                                        }
                                    }
                                }
                                return new Dom7($.unique(children));
                            },
                            remove: function () {
                                for (var i = 0; i < this.length; i++) {
                                    if (this[i].parentNode) this[i].parentNode.removeChild(this[i]);
                                }
                                return this;
                            },
                            add: function () {
                                var dom = this;
                                var i, j;
                                for (i = 0; i < arguments.length; i++) {
                                    var toAdd = $(arguments[i]);
                                    for (j = 0; j < toAdd.length; j++) {
                                        dom[dom.length] = toAdd[j];
                                        dom.length++;
                                    }
                                }
                                return dom;
                            },
                        };
                        $.fn = Dom7.prototype;
                        $.unique = function (arr) {
                            var unique = [];
                            for (var i = 0; i < arr.length; i++) {
                                if (unique.indexOf(arr[i]) === -1) unique.push(arr[i]);
                            }
                            return unique;
                        };
                        return $;
                    })();
                    var swiperDomPlugins = ["jQuery", "Zepto", "Dom7"];
                    for (var i = 0; i < swiperDomPlugins.length; i++) {
                        if (window[swiperDomPlugins[i]]) {
                            addLibraryPlugin(window[swiperDomPlugins[i]]);
                        }
                    }
                    var domLib;
                    if (typeof Dom7 === "undefined") {
                        domLib = window.Dom7 || window.Zepto || window.jQuery;
                    } else {
                        domLib = Dom7;
                    }
                    function addLibraryPlugin(lib) {
                        lib.fn.swiper = function (params) {
                            var firstInstance;
                            lib(this).each(function () {
                                var s = new Swiper(this, params);
                                if (!firstInstance) firstInstance = s;
                            });
                            return firstInstance;
                        };
                    }
                    if (domLib) {
                        if (!("transitionEnd" in domLib.fn)) {
                            domLib.fn.transitionEnd = function (callback) {
                                var events = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
                                    i,
                                    j,
                                    dom = this;
                                function fireCallBack(e) {
                                    if (e.target !== this) return;
                                    callback.call(this, e);
                                    for (i = 0; i < events.length; i++) {
                                        dom.off(events[i], fireCallBack);
                                    }
                                }
                                if (callback) {
                                    for (i = 0; i < events.length; i++) {
                                        dom.on(events[i], fireCallBack);
                                    }
                                }
                                return this;
                            };
                        }
                        if (!("transform" in domLib.fn)) {
                            domLib.fn.transform = function (transform) {
                                for (var i = 0; i < this.length; i++) {
                                    var elStyle = this[i].style;
                                    elStyle.webkitTransform = elStyle.MsTransform = elStyle.msTransform = elStyle.MozTransform = elStyle.OTransform = elStyle.transform = transform;
                                }
                                return this;
                            };
                        }
                        if (!("transition" in domLib.fn)) {
                            domLib.fn.transition = function (duration) {
                                if (typeof duration !== "string") {
                                    duration = duration + "ms";
                                }
                                for (var i = 0; i < this.length; i++) {
                                    var elStyle = this[i].style;
                                    elStyle.webkitTransitionDuration = elStyle.MsTransitionDuration = elStyle.msTransitionDuration = elStyle.MozTransitionDuration = elStyle.OTransitionDuration = elStyle.transitionDuration = duration;
                                }
                                return this;
                            };
                        }
                        if (!("outerWidth" in domLib.fn)) {
                            domLib.fn.outerWidth = function (includeMargins) {
                                if (this.length > 0) {
                                    if (includeMargins) return this[0].offsetWidth + parseFloat(this.css("margin-right")) + parseFloat(this.css("margin-left"));
                                    else return this[0].offsetWidth;
                                } else return null;
                            };
                        }
                    }
                    window.Swiper = Swiper;
                })();
                if (typeof module !== "undefined") {
                    module.exports = window.Swiper;
                } else if (typeof define === "function" && define.amd) {
                    define([], function () {
                        "use strict";
                        return window.Swiper;
                    });
                }
            },
            {},
        ],
    },
    {},
    [2]
);
